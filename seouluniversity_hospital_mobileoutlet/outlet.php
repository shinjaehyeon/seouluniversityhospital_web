<?php

	header("Content-Type: text/html; charset=UTF-8");

	$db = new mysqli("localhost", $db_user, $db_password, $db_name) or die("mysqli db connect error");
	$db->set_charset("utf8");
	$act = "none";
	if (isset($_REQUEST["act"])) {
		$act = $_REQUEST["act"];
	} else {

	}

	/******************************************************************************************/

	switch ($act) {
		/**************************************************************************************/
		/* Mobile1000 (스플래시) */
		/**************************************************************************************/
		case 'mobile1001': // 앱 구동시 필요한 데이터 요청하기
			mobile1001();
			break;



		/**************************************************************************************/
		/* Mobile2000 (로그인) */
		/**************************************************************************************/	
		case 'mobile2010': // 페이스북로그인 계정 DB에 있는지 확인 요청 보내기
			mobile2010();
			break;	
		case 'mobile2020': // 카카오톡로그인 계정 DB에 있는지 확인 요청 보내기
			mobile2020();
			break;		
		case 'mobile2030': // 네이버로그인 계정 DB에 있는지 확인 요청 보내기
			mobile2030();
			break;	
		case 'mobile2040': // 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 보내기
			mobile2040();
			break;



		/**************************************************************************************/
		/* Mobile2500 (회원가입) */
		/**************************************************************************************/



		/**************************************************************************************/
		/* Mobile2510 (회원가입 - STEP 01 약관 동의) */
		/**************************************************************************************/		
		case 'mobile2515': // 약관 동의 내용 서버에 요청하기
			mobile2515();
			break;
		



		/**************************************************************************************/
		/* Mobile2510 (회원가입 - STEP 02 정보 입력) */
		/**************************************************************************************/		
		case 'mobile2522': // 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청
			mobile2522();
			break;
		case 'mobile2526': // 이메일 중복체크 요청
			mobile2526();
			break;



		/**************************************************************************************/
		/* Mobile3000 (메인) */
		/**************************************************************************************/		
		case 'mobile3001': // 서버에 프로필정보 요청 (이름, 최근진료날짜, 총 예약횟수, 총 진단 횟수)
			mobile3001();
			break;
		case 'mobile3003': // 서버에 프로필정보에 휴대폰 번호 및 인적정보가 업데이트 되어있는지에 대한 여부 요청
			mobile3003();
			break;
		case 'mobile3010': // 서버에 최근 진료예약 내역 요청
			mobile3010();
			break;
		case 'mobile3020': // 서버에 진료예약 취소 요청
			mobile3020();
			break;



		/**************************************************************************************/
		/* Mobile3500 (프로필 정보 업데이트) */
		/**************************************************************************************/	
		case 'mobile3501': // 서버에 프로필 정보 요청
			mobile3501();
			break;
		case 'mobile3521': // 서버에 프로필 정보 업데이트 요청
			mobile3521();
			break;
		case 'mobile3523': // 서버에 푸시키 삭제 요청하기
			mobile3523();
			break;



		/**************************************************************************************/
		/* Mobile4500 (진료예약) */
		/**************************************************************************************/



		/**************************************************************************************/
		/* Mobile4600 (진료예약신청) */
		/**************************************************************************************/	
		case 'mobile4601': // 서버에 입력받은 예약정보를 DB에 저장 요청
			mobile4601();
			break;



		/**************************************************************************************/
		/* Mobile4620 (진료예약신청 - STEP 01 진료과/증상 선택) */
		/**************************************************************************************/
		case 'mobile4621': // 서버에 진료과 리스트 요청
			mobile4621();
			break;
		case 'mobile4623': // 서버에 해당 진료과에 맞는 의료진 리스트 요청
			mobile4623();
			break;









		/**************************************************************************************/
		/* Mobile4660 (진료예약신청 - STEP 03 날짜/시간 선택) */
		/**************************************************************************************/
		case 'mobile4661': // 선택된 진료과, 의료진, 날짜를 토대로 서버에 시간 리스트 요청
			mobile4661();
			break;




		/**************************************************************************************/
		/* Mobile5000 (진료과/의료진) */
		/**************************************************************************************/
		



		/**************************************************************************************/
		/* Mobile5020 (진료과) */
		/**************************************************************************************/
		case 'mobile5021': // 서버에 진료과 리스트 요청
			mobile5021();
			break;



		/**************************************************************************************/
		/* Mobile5040 (의료진) */
		/**************************************************************************************/
		case 'mobile5041': // 서버에 의료진 리스트 요청
			mobile5041();
			break;



		/**************************************************************************************/
		/* Mobile5100 (진료과 상세페이지) */
		/**************************************************************************************/	
		case 'mobile5101': // 진료과 이미지 요청
			mobile5101();
			break;
		case 'mobile5121': // 서버에 진료과 소개글/설명글 요청
			mobile5121();
			break;



		/**************************************************************************************/
		/* Mobile5200 (의료진 상세페이지) */
		/**************************************************************************************/
		case 'mobile5201': // 의료진 이미지 요청
			mobile5201();
			break;

		case 'mobile5221': // 서버에 의료진 기본정보 요청
			mobile5221();
			break;
		case 'mobile5241': // 서버에 해당 의료진의 감사댓글 리스트 요청
			mobile5241();
			break;
		case 'mobile5243': // 서버에 해당 의료진 댓글 DB에 업로드 요청
			mobile5243();
			break;




		/**************************************************************************************/
		/* Mobile5500 (1:1 문의) */
		/**************************************************************************************/




		/**************************************************************************************/
		/* Mobile5520 (1:1 문의하기) */
		/**************************************************************************************/
		case 'mobile5521': // 서버에 1:1 문의내용 DB 업로드 요청
			mobile5521();
			break;



		/**************************************************************************************/
		/* Mobile5540 (1:1 문의내역) */
		/**************************************************************************************/
		case 'mobile5541': // 서버에 1:1 문의내역 리스트 요청
			mobile5541();
			break;



		/**************************************************************************************/
		/* mobile6500 (순번대기표) */
		/**************************************************************************************/
		case 'mobile6501': // 서버에 서울대학교 본원, 어린이병원, 암병원 각각 접수창구의 현재 번호 요청
			mobile6501();
			break;

		case 'mobile6505': // 서버에 현재 대기중인 내 번호표가 있는지 여부 요청
			mobile6505();
			break;

		case 'mobile6507': // 서버에 순번대기표 뽑기 요청
			mobile6507();
			break;

		case 'mobile6509': // 서버에 순번대기표 취소 요청
			mobile6509();
			break;



		/**************************************************************************************/
		/* mobile7000 (진단서) */
		/**************************************************************************************/
		case 'mobile7001': // 서버에 진단서 리스트 요청
			mobile7001();
			break;



		/**************************************************************************************/
		/* mobile7100 (진단서 상세정보) */
		/**************************************************************************************/
		case 'mobile7101': // 서버에 진단서 상세정보 요청
			mobile7101();
			break;



		/**************************************************************************************/
		/* mobile7500 (푸쉬알림 내역) */
		/**************************************************************************************/
		case 'mobile7501': // 서버에 받은 푸쉬 목록 요청
			mobile7501();
			break;
		case 'mobile7503': // 서버에 푸쉬 읽음 처리 요청
			mobile7503();
			break;




		/**************************************************************************************/
		/* mobile8000 (병원뉴스) */
		/**************************************************************************************/
		case 'mobile8001': // 병원뉴스 리스트 요청
			mobile8001();
			break;




		/**************************************************************************************/
		/* mobile9000 (설정) */
		/**************************************************************************************/
		case 'mobile9001': // 서버에 푸쉬 알림 수신 여부 요청
			mobile9001();
			break;
		case 'mobile9003': // 서버에 푸쉬 알림 수신 여부 업로드
			mobile9003();
			break;




		/**************************************************************************************/
		/* 기타 */
		/**************************************************************************************/	
		default:
			# code...
			echo $db->stat();
			break;
	}

	/******************************************************************************************/

	// 앱 구동시 필요한 데이터 요청하기
	function mobile1001() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		echo '{"result":"ok", "member":'.$member.'}';
	}

	// 페이스북로그인 계정 DB에 있는지 확인 요청 보내기
	function mobile2010() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$id = $_REQUEST["id"];
		$name = $_REQUEST["name"];
		$signUpMethod = $_REQUEST["signUpMethod"];

		$pushkey = $_REQUEST["pushkey"];

		$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `email`='{$id}' AND `signUpMethod`={$signUpMethod}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 페이스북 로그인 계정이 이미 존재하면
			$row = $res->fetch_object();
			$memberPk = $row->suhmember;

			$sql=<<<SQL
UPDATE `suhMembers` SET `pushkey`='{$pushkey}' 
WHERE `suhmember`={$memberPk} 
SQL;
			$res = $db->query($sql);

			echo '{"member_pk":'.$memberPk.'}';

		} else {
			// 페이스북 로그인 계정이 없으면
			$sql2=<<<SQL
INSERT INTO `suhMembers`
(`name`, `theMobileCompany`, `phoneNumber`, `email`, `password`, `theBloodType`, `addr`, `signUpMethod`, `status`)VALUES
('{$name}', 900900, '', '{$id}', '', 900900, '', 500802, 500101)
SQL;
			$res2 = $db->query($sql2);

			$sql2=<<<SQL
SELECT LAST_INSERT_ID() AS `pk`
SQL;
			$res2 = $db->query($sql2);
			$row2 = $res2->fetch_object();
			$memberPk = $row2->pk;

			echo '{"member_pk":'.$memberPk.'}';
		}

	}

	// 카카오톡로그인 계정 DB에 있는지 확인 요청 보내기
	function mobile2020() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$id = $_REQUEST["id"];
		$name = $_REQUEST["name"];
		$signUpMethod = $_REQUEST["signUpMethod"];

		$pushkey = $_REQUEST["pushkey"];

		$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `email`='{$id}' AND `signUpMethod`={$signUpMethod}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 카카오 로그인 계정이 이미 존재하면
			$row = $res->fetch_object();
			$memberPk = $row->suhmember;

			$sql=<<<SQL
UPDATE `suhMembers` SET `pushkey`='{$pushkey}' 
WHERE `suhmember`={$memberPk} 
SQL;
			$res = $db->query($sql);

			echo '{"member_pk":'.$memberPk.'}';

		} else {
			// 카카오 로그인 계정이 없으면
			$sql2=<<<SQL
INSERT INTO `suhMembers`
(`name`, `theMobileCompany`, `phoneNumber`, `email`, `password`, `theBloodType`, `addr`, `signUpMethod`, `status`, `pushkey`)VALUES
('{$name}', 900900, '', '{$id}', '', 900900, '', {$signUpMethod}, 500101, '{$pushkey}')
SQL;
			$res2 = $db->query($sql2);

			$sql2=<<<SQL
SELECT LAST_INSERT_ID() AS `pk`
SQL;
			$res2 = $db->query($sql2);
			$row2 = $res2->fetch_object();
			$memberPk = $row2->pk;

			echo '{"member_pk":'.$memberPk.'}';
		}
	}

	// 네이버로그인 계정 DB에 있는지 확인 요청 보내기
	function mobile2030() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$id = $_REQUEST["id"];
		$name = $_REQUEST["name"];
		$signUpMethod = $_REQUEST["signUpMethod"];

		$pushkey = $_REQUEST["pushkey"];

		$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `email`='{$id}' AND `signUpMethod`={$signUpMethod}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 네이버 로그인 계정이 이미 존재하면
			$row = $res->fetch_object();
			$memberPk = $row->suhmember;

			$sql=<<<SQL
UPDATE `suhMembers` SET `pushkey`='{$pushkey}' 
WHERE `suhmember`={$memberPk} 
SQL;
			$res = $db->query($sql);

			echo '{"member_pk":'.$memberPk.'}';

		} else {
			// 네이버 로그인 계정이 없으면
			$sql2=<<<SQL
INSERT INTO `suhMembers`
(`name`, `theMobileCompany`, `phoneNumber`, `email`, `password`, `theBloodType`, `addr`, `signUpMethod`, `status`, `pushkey`)VALUES
('{$name}', 900900, '', '{$id}', '', 900900, '', {$signUpMethod}, 500101, '{$pushkey}')
SQL;
			$res2 = $db->query($sql2);

			$sql2=<<<SQL
SELECT LAST_INSERT_ID() AS `pk`
SQL;
			$res2 = $db->query($sql2);
			$row2 = $res2->fetch_object();
			$memberPk = $row2->pk;

			echo '{"member_pk":'.$memberPk.'}';
		}
	}

	// 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 보내기
	function mobile2040() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$id = $_REQUEST["id"];
		$pw = $_REQUEST["pw"];
		$md5pw = md5($pw);

		$pushkey = $_REQUEST["pushkey"];



		$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `email`='{$id}' AND `password`='{$md5pw}'
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 일치하는 계정이 있을 경우
			$row = $res->fetch_object();
			$suhmember = $row->suhmember;

			$sql=<<<SQL
UPDATE `suhMembers` SET `pushkey`='{$pushkey}' 
WHERE `suhmember`={$suhmember} 
SQL;
			$res = $db->query($sql);

			echo '{"result":"ok", "suhmember":'.$suhmember.'}';
		} else {
			// 일치하는 계정이 없을 경우
			echo '{"result":"no"}';
		}
	}

	// 약관 동의 내용 서버에 요청하기
	function mobile2515() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$fp = fopen("./terms/service_terms.txt","r"); 
		$doc_data = '';
		while(!feof($fp)) {
			$doc_data .= fgets($fp); 
		}
		fclose($fp); 
		$doc_data = htmlspecialchars($doc_data);
		$doc_data = preg_replace('/\r\n|\r|\n/','<br />',$doc_data);

		$fp = fopen("./terms/personal_terms.txt","r"); 
		$doc_data2 = '';
		while(!feof($fp)) {
			$doc_data2 .= fgets($fp); 
		}
		fclose($fp); 
		$doc_data2 = htmlspecialchars($doc_data2);
		$doc_data2 = preg_replace('/\r\n|\r|\n/','<br />',$doc_data2);


		$jsondata=<<<JSON
{
	"service_terms":"{$doc_data}",
	"personal_terms":"{$doc_data2}",
	"result":"ok"
}
JSON;
				
		echo $jsondata;
	}

	// 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청
	function mobile2522() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$checkedName = htmlspecialchars($_REQUEST["checkedName"]); // 이름
		$checkedGender = $_REQUEST["checkedGender"]; // 성별
		$checkedMobileCompanyNumber = $_REQUEST["checkedMobileCompanyNumber"]; // 통신사 번호
		$checkedPhoneNumber = $_REQUEST["checkedPhoneNumber"]; // 휴대폰 번호
		$checkedEmail = htmlspecialchars($_REQUEST["checkedEmail"]); // 이메일
		$checkedPassword = $_REQUEST["checkedPassword"]; // 비밀번호
		$checkedBirthday = $_REQUEST["checkedBirthday"]; // 생년월일
		$checkedBloodTypeNumber = $_REQUEST["checkedBloodTypeNumber"]; // 혈액형 번호
		$checkedAddress = htmlspecialchars($_REQUEST["checkedAddress"]); // 주소

		$md5Password = md5($checkedPassword);

		$sql=<<<SQL
INSERT INTO `suhMembers`
(`name`,`gender`,`theMobileCompany`,`phoneNumber`,`email`,`password`,`birthday`,`theBloodType`,`addr`, `signUpMethod`, `status`)VALUES
('{$checkedName}', {$checkedGender}, {$checkedMobileCompanyNumber}, '{$checkedPhoneNumber}', '{$checkedEmail}', '{$md5Password}', STR_TO_DATE('{$checkedBirthday}', '%Y%m%d'), {$checkedBloodTypeNumber}, '{$checkedAddress}', 500801
, 500101)
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 이메일 중복체크 요청
	function mobile2526() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$checkedEmail = htmlspecialchars($_REQUEST["checkedEmail"]); // 이메일

		$sql=<<<SQL
SELECT `email` FROM `suhMembers` WHERE `email`='{$checkedEmail}'
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 이메일이 이미 존재하면
			echo '{"emailUse":"no"}'; // 적합하지 않다는 의미의 no
		} else {
			// 이메일이 존재하지 않으면
			echo '{"emailUse":"ok"}'; // 적합하다는 의미의 ok
		}

		$stmt->close();
	}

	// 서버에 프로필정보 요청 (이름, 최근진료날짜, 총 예약횟수, 총 진단 횟수)
	function mobile3001() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$sql=<<<SQL
SELECT 
`name`,
(SELECT `suhCRH`.`hope_datetime` FROM `suhCRH` WHERE `suhCRH`.`status`=500202 AND `suhCRH`.`member`={$member} ORDER BY `suhCRH`.`hope_datetime` DESC LIMIT 1) AS `recent_clinic_datetime`,
(SELECT COUNT(`member`) FROM `suhCRH` WHERE `suhCRH`.`member`={$member}) AS `all_reservation_num`,
(SELECT COUNT(`member`) FROM `suhCRH` WHERE `suhCRH`.`member`={$member} AND `suhCRH`.`status`=500202) AS `all_clinic_num`
FROM `suhMembers` 
LEFT JOIN `suhCRH` AS `suhCRH1`
ON `suhMembers`.`suhmember`=`suhCRH1`.`member`
WHERE `suhmember`={$member} LIMIT 1
SQL;

		// echo $sql;

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 있을 경우
			$row = $res->fetch_object();

			$name = $row->name;
			$recent_clinic_datetime = $row->recent_clinic_datetime;
			if ($recent_clinic_datetime == null || $recent_clinic_datetime == "") {
				$recent_clinic_full_date = '-';
			} else {
				$recent_clinic_year = dateDivide($recent_clinic_datetime, 'yy');
				$recent_clinic_month = dateDivide($recent_clinic_datetime, 'MM');
				$recent_clinic_date = dateDivide($recent_clinic_datetime, 'DD');
				$recent_clinic_full_date = $recent_clinic_year.'.'.$recent_clinic_month.'.'.$recent_clinic_date;
			}
			
			$all_reservation_num = $row->all_reservation_num;
			$all_clinic_num = $row->all_clinic_num;

			$jsondata=<<<JSON
{
	"data":{
		"name":"{$name}",
		"recent_clinic_full_date":"{$recent_clinic_full_date}",
		"all_reservation_num":{$all_reservation_num},
		"all_clinic_num":{$all_clinic_num}
	},
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			// 결과가 없을 경우
			echo '{"result":"no"}';
		}
	}

	// 서버에 프로필정보에 휴대폰 번호 및 인적정보가 업데이트 되어있는지에 대한 여부 요청
	function mobile3003() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$sql=<<<SQL
SELECT `phoneNumber` FROM `suhMembers` WHERE `suhmember`={$member}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();
			$phoneNumber = $row->phoneNumber;

			if ($phoneNumber == null || $phoneNumber == '') {
				echo '{"result":"no"}';
			} else {
				echo '{"result":"ok"}';
			}

		} else {
			echo '{"result":"no"}';
		}
	}

	// 최근 진료예약 내역 요청 보내기
	function mobile3010() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1;
		$period_size = $_REQUEST["period_size"];

		$text = '';
		if ($period_size > 0) {
			$text=<<<SQL
AND date(`suhCRH`.`datetime`) >= date(subdate(now(), INTERVAL {$period_size} DAY)) 
AND date(`suhCRH`.`datetime`) <= date(now()) 
SQL;
		}

		$sql=<<<SQL
SELECT 
`suhCRH`.`suhcrh`, 
`suhCRH`.`member`, 
`suhCRH`.`datetime`,
`suhCRH`.`hope_datetime`, 
`suhCRH`.`department`, 
`suhDepartments1`.`department` AS `department_string`,
`suhCRH`.`doctor`, 
`suhDoctors1`.`name` AS `doctor_name`,
`suhCRH`.`status`,
`suhCodes1`.`description` AS `status_string`
FROM `suhCRH` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhCRH`.`status`=`suhCodes1`.`codeNumber` 
LEFT JOIN `suhDepartments` AS `suhDepartments1` 
ON `suhCRH`.`department`=`suhDepartments1`.`suhd` 
LEFT JOIN `suhDoctors` AS `suhDoctors1` 
ON `suhCRH`.`doctor`=`suhDoctors1`.`doctor` 
WHERE `suhCRH`.`member`={$member}
{$text}
ORDER BY `suhCRH`.`suhcrh` DESC
LIMIT {$index}, {$view_num_plus}
SQL;

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 1개 이상이면
			$jsondata = '{"data":[';
			$ii = 0;
			while ($row = $res->fetch_object()) {
				$ii++;
				if ($ii == $view_num_plus) {
					break;
				}

				$suhcrh = $row->suhcrh;

				$hope_datetime = $row->hope_datetime;

				$hope_year = dateDivide($hope_datetime, 'yy');
				$hope_year_full = dateDivide($hope_datetime, 'yyyy');
				$hope_month = (int) dateDivide($hope_datetime, 'mm');
				$hope_date = (int) dateDivide($hope_datetime, 'dd');
				$hope_day = dateDivide($hope_datetime, 'day');

				$hope_hour = (int) dateDivide($hope_datetime, 'h');
				$hope_minute = (int) dateDivide($hope_datetime, 'i'); 

				$pm_am = '';
				if ($hope_hour == 12) {
					$pm_am = '오후';
				} else if ($hope_hour < 12) {
					$pm_am = '오전';
				} else if ($hope_hour > 12) {
					$pm_am = '오후';
					$hope_hour = $hope_hour - 12;
				}

				$clinic_reservation_date = $hope_year.'년 '.$hope_month.'월 '.$hope_date.'일 '.$hope_day.'요일';
				$clinic_reservation_time = $pm_am.' '.$hope_hour.'시 '.$hope_minute.'분';
				if ($hope_minute == 0) {
					$clinic_reservation_time = $pm_am.' '.$hope_hour.'시 ';
				}

				$department_string = $row->department_string;
				$doctor_name = $row->doctor_name;

				$status = $row->status;
				$status_string = $row->status_string;

				if (toCompareDate(date("Y-m-d H:i:s"), $hope_datetime) == 1) {
					// 오늘날짜가 진료희망일보다 이후일 때
					// 아직 status가 예약중 상태이면 미방문으로 바꾸기
					if ($status == 500201) {
						$status = 500204;
						$status_string = '미방문';
					}	
				}

				$jsondata.=<<<JSON
{
	"suhcrh":{$suhcrh},
	"doctor_name":"{$doctor_name}",
	"department_string":"{$department_string}",
	"clinic_reservation_date":"{$clinic_reservation_date}",
	"year":{$hope_year_full},
	"month":{$hope_month},
	"date":{$hope_date},
	"clinic_reservation_time":"{$clinic_reservation_time}",
	"status":{$status},
	"status_string":"{$status_string}"
},
JSON;
			}

			if ($view_num_plus == $res->num_rows) {
				$more_info = 'ok';
			} else {
				$more_info = 'no';
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$more_info.'"}';

			echo $jsondata;
		} else {
			echo '{"result":"no", "more_info":"no"}';
		}

	}

	// 서버에 진료예약 취소 요청
	function mobile3020() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$crh_pk = $_REQUEST["crh_pk"];

		$sql=<<<SQL
UPDATE `suhCRH` SET `cancel_datetime`=now(), `status`=500203 
WHERE `member`={$member} AND `suhcrh`={$crh_pk}
SQL;
		$res = $db->query($sql);

		if ($res) {
			// 쿼리가 성공했으면
			echo '{"result":"ok"}';
		} else {
			// 쿼리가 실패했으면
			echo '{"result":"no"}';
		}
	}

	// 서버에 프로필 정보 요청
	function mobile3501() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$sql=<<<SQL
SELECT 
`suhMembers`.`name`,
`suhMembers`.`gender`,
`suhCodes1`.`description` AS `gender_string`,
`suhMembers`.`theMobileCompany`, 
`suhMembers`.`phoneNumber`,
`suhMembers`.`email`,
`suhMembers`.`birthday`,
`suhMembers`.`theBloodType`,
`suhMembers`.`addr`,
`suhMembers`.`signUpMethod` 
FROM `suhMembers` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhMembers`.`gender`=`suhCodes1`.`codeNumber`
WHERE `suhMembers`.`suhmember`={$member}
LIMIT 1
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 있으면
			$row = $res->fetch_object();

			$name = $row->name;
			$gender = $row->gender;
			$gender_string = $row->gender_string;

			$theMobileCompany = $row->theMobileCompany;

			$phoneNumber = $row->phoneNumber;
			if ($phoneNumber == null) {
				$phoneNumber = "";
			}

			$email = $row->email;

			$birthday = $row->birthday;
			if ($birthday == null) {
				$birthday_year = 0;
				$birthday_month = 0;
				$birthday_date = 0;
			} else {
				$birthday_year = (int) dateDivide($birthday, 'yyyy');
				$birthday_month = (int) dateDivide($birthday, 'mm');
				$birthday_date = (int) dateDivide($birthday, 'dd');
			}

			$theBloodType = $row->theBloodType;
			$addr = $row->addr;
			$signUpMethod = $row->signUpMethod;

			$jsondata=<<<JSON
{
	"data":{
		"name":"{$name}",
		"gender":{$gender},
		"gender_string":"{$gender_string}",
		"theMobileCompany":{$theMobileCompany},
		"phoneNumber":"{$phoneNumber}",
		"email":"{$email}",
		"birthday_year":{$birthday_year},
		"birthday_month":{$birthday_month},
		"birthday_date":{$birthday_date},
		"theBloodType":{$theBloodType},
		"addr":"{$addr}",
		"signUpMethod":{$signUpMethod}
	},
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			// 결과가 없으면
			echo '{"result":"no"}';
		}


	}

	// 서버에 프로필 정보 업데이트 요청
	function mobile3521() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$checked_gender = $_REQUEST["checked_gender"];
		$checked_mobile_company = $_REQUEST["checked_mobile_company"];
		$checked_phone_number = $_REQUEST["checked_phone_number"];
		$checked_password = $_REQUEST["checked_password"];
		$checked_birthday = $_REQUEST["checked_birthday"];
		$checked_blood_type = $_REQUEST["checked_blood_type"];
		$checked_addr = htmlspecialchars($_REQUEST["checked_addr"]);

		$text = '';
		if ($checked_password != "") {
			$md5password = md5($checked_password);
			$text=<<<TEXT
`password`='{$md5password}',
TEXT;
		}

		$sql=<<<SQL
UPDATE `suhMembers` SET 
`gender`={$checked_gender}, 
`theMobileCompany`={$checked_mobile_company}, 
`phoneNumber`='{$checked_phone_number}',
{$text}
`birthday`=STR_TO_DATE('{$checked_birthday}', '%Y%m%d'),
`theBloodType`={$checked_blood_type},
`addr`='{$checked_addr}'
WHERE `suhmember`={$member}
SQL;

		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok", "checked_birthday":"'.$checked_birthday.'"}';
		} else {
			echo '{"result":"no"}';
		}

	}

	// 서버에 푸시키 삭제 요청하기
	function mobile3523() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$sql=<<<SQL
UPDATE `suhMembers` SET `pushkey`='' WHERE `suhmember`={$member}
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 서버에 입력받은 예약정보를 DB에 저장 요청
	function mobile4601() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$step01_department = $_REQUEST["step01_department"]; // 진료과
		$step01_symptom = addslashes($_REQUEST["step01_symptom"]); // 증상

		$step02_doctor_primarykey = $_REQUEST["step02_doctor_primarykey"]; // 의료진

		$step03_select_year = $_REQUEST["step03_select_year"]; // 년
		$step03_select_month = $_REQUEST["step03_select_month"]; // 월
		$step03_select_date = $_REQUEST["step03_select_date"]; // 일
		$step03_select_hour = $_REQUEST["step03_select_hour"]; // 시
		$step03_select_minute = $_REQUEST["step03_select_minute"]; // 분


		$hope_datetime = $step03_select_year.'-'.$step03_select_month.'-'.$step03_select_date.' '.$step03_select_hour.':'.$step03_select_minute.':00';
		// code..	

		// echo $step01_department.', '.$step01_symptom.', '.$step02_doctor_primarykey.', '.$step03_select_year.', '.$step03_select_month.', '.$step03_select_date.', '.$step03_select_hour.', '.$step03_select_minute.' and '.$hope_datetime;
		

		// 해당 년월일 의료진의 시, 분에 예약되어 있는지 확인하기
		$sql=<<<SQL
SELECT `suhcrh` FROM `suhCRH` 
WHERE STR_TO_DATE(`hope_datetime`, '%Y-%m-%d %h:%i:%s')=STR_TO_DATE('{$hope_datetime}', '%Y-%m-%d %h:%i:%s') 
AND `doctor`={$step02_doctor_primarykey} 
AND `status`=500201
SQL;
		$res = $db->query($sql);
		if ($res->num_rows >= 1) {
			// 이미 누가 그 의료진의 그 시간에 예약을 했으면 result : "already" 를 띄우기
			echo '{"result":"already"}';
			exit;
		}


		$sql=<<<SQL
INSERT INTO `suhCRH`
(`member`,`hope_datetime`,`department`,`symptom`,`doctor`,`status`)VALUES
({$member}, '{$hope_datetime}', {$step01_department}, '{$step01_symptom}', {$step02_doctor_primarykey}, 500201)
SQL;
		
	
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 서버에 진료과 리스트 요청
	function mobile4621() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$sql=<<<SQL
SELECT `suhd`, `department` FROM `suhDepartments` ORDER BY `department` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$suhd = $row->suhd;
				$department = $row->department;

				$jsondata.=<<<JSON
{
	"suhd":{$suhd},
	"department":"{$department}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';
			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}

	// 서버에 해당 진료과에 맞는 의료진 리스트 요청
	function mobile4623() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$department = $_REQUEST["department"]; // ex) 800101
		$department_string = $_REQUEST["department_string"]; // ex) 내과

		$sql=<<<SQL
SELECT `doctor`, `name` FROM `suhDoctors` WHERE `affiliation_department`={$department}
ORDER BY `name` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 있을 경우
			$jsondata = '{"data":[';
			while($row = $res->fetch_object()) {
				$doctor = $row->doctor;
				$name = $row->name;

				$jsondata.=<<<JSON
{
	"doctor":{$doctor},
	"department_string":"{$department_string}",
	"name":"{$name}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			// 결과가 없을 경우
			echo '{"result":"no"}';
		}

	}

	// 선택된 진료과, 의료진, 날짜를 토대로 서버에 시간 리스트 요청하기
	function mobile4661() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$department = $_REQUEST["department"];
		$doctor = $_REQUEST["doctor"];
		$datetimeHypon = $_REQUEST["datetimeHypon"];
		$datetimeNoHypon = $_REQUEST["datetimeNoHypon"];

		$sql=<<<SQL
SELECT 
`suhDoctorSchedule`.`posible_datetime`, 
`suhCRH1`.`suhcrh` AS `suhCRH_pk` 
FROM `suhDoctorSchedule` 
LEFT JOIN `suhCRH` AS `suhCRH1`
ON `suhDoctorSchedule`.`posible_datetime` = `suhCRH1`.`hope_datetime`
AND `suhDoctorSchedule`.`doctor`=`suhCRH1`.`doctor` 
WHERE STR_TO_DATE(`suhDoctorSchedule`.`posible_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$datetimeHypon}', '%Y-%m-%d') 
AND `suhDoctorSchedule`.`doctor`={$doctor}
ORDER BY `posible_datetime` ASC 
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$posible_datetime = $row->posible_datetime;

				// pk가 null이 아니면 이미 누가 예약한 사람이 있다는 의미
				$suhCRH_pk = $row->suhCRH_pk;
				if ($suhCRH_pk == null) {
					$suhCRH_pk = 0; // 0 이면 아무도 그 시간에 예약하지 않았다는 의미
				}

				$year = dateDivide($posible_datetime, 'YY');
				$month = (int) dateDivide($posible_datetime, 'mm');
				$date = (int) dateDivide($posible_datetime, 'dd');

				$hour = dateDivide($posible_datetime, 'HH');
				$minute = dateDivide($posible_datetime, 'ii');

				$jsondata.=<<<JSON
{
	"year":{$year},
	"month":{$month},
	"date":{$date},
	"hour":{$hour},
	"minute":{$minute},
	"already_suhCRH_pk":{$suhCRH_pk}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}

	}

	// 서버에 진료과 리스트 요청
	function mobile5021() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$align_index = $_REQUEST["align_index"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1; 

		$text = '';
		switch ($align_index) {
			case 0:
				$text=<<<TEXT
1
TEXT;
				break;
			case 1:
				$text=<<<TEXT
(`department` RLIKE '^(ㄱ|ㄲ)' OR ( `department` >= '가' AND `department` < '나' ))
TEXT;
				break;
			case 2:
				$text=<<<TEXT
(`department` RLIKE '^ㄴ' OR ( `department` >= '나' AND `department` < '다' ))
TEXT;
				break;
			case 3:
				$text=<<<TEXT
(`department` RLIKE '^(ㄷ|ㄸ)' OR ( `department` >= '다' AND `department` < '라' ))
TEXT;
				break;
			case 4:
				$text=<<<TEXT
(`department` RLIKE '^ㄹ' OR ( `department` >= '라' AND `department` < '마' ))
TEXT;
				break;
			case 5:
				$text=<<<TEXT
(`department` RLIKE '^ㅁ' OR ( `department` >= '마' AND `department` < '바' ))
TEXT;
				break;
			case 6:
				$text=<<<TEXT
(`department` RLIKE '^ㅂ' OR ( `department` >= '바' AND `department` < '사' ))
TEXT;
				break;
			case 7:
				$text=<<<TEXT
(`department` RLIKE '^(ㅅ|ㅆ)' OR ( `department` >= '사' AND `department` < '아' ))
TEXT;
				break;
			case 8:
				$text=<<<TEXT
(`department` RLIKE '^ㅇ' OR ( `department` >= '아' AND `department` < '자' ))
TEXT;
				break;
			case 9:
				$text=<<<TEXT
(`department` RLIKE '^(ㅈ|ㅉ)' OR ( `department` >= '자' AND `department` < '차' ))
TEXT;
				break;
			case 10:
				$text=<<<TEXT
(`department` RLIKE '^ㅊ' OR ( `department` >= '차' AND `department` < '카' ))
TEXT;
				break;
			case 11:
				$text=<<<TEXT
(`department` RLIKE '^ㅋ' OR ( `department` >= '카' AND `department` < '타' ))
TEXT;
				break;
			case 12:
				$text=<<<TEXT
(`department` RLIKE '^ㅌ' OR ( `department` >= '타' AND `department` < '파' ))
TEXT;
				break;
			case 13:
				$text=<<<TEXT
(`department` RLIKE '^ㅍ' OR ( `department` >= '파' AND `department` < '하' ))
TEXT;
				break;
			case 14:
				$text=<<<TEXT
(`department` RLIKE '^ㅎ' OR ( `department` >= '하'))
TEXT;
				break;
			default:
				$text=<<<TEXT
1
TEXT;
				break;
		}


		$sql=<<<SQL
SELECT `suhd`,`department` FROM `suhDepartments` 
WHERE {$text}
LIMIT {$index}, {$view_num_plus}
SQL;

		$res = $db->query($sql);

		$numrows = $res->num_rows;
		if ($numrows >= 1) {

			$jsondata = '{"data":[';

			$ii = 0;
			while ($row = $res->fetch_object()) {


				$suhd = $row->suhd;
				$department = $row->department;

				$jsondata.=<<<JSON
{
	"suhd":{$suhd},
	"department":"{$department}"
},
JSON;
				$ii++;

				if ($ii == $view_num) {
					break;
				}
			}

			$is_more_info = "ok";
			if ($numrows != $view_num_plus) {
				$is_more_info = "no";
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$is_more_info.'"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}

	}

	// 서버에 의료진 리스트 요청
	function mobile5041() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$currentSelectedDepartmentCode = $_REQUEST["currentSelectedDepartmentCode"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1; 

		if ($currentSelectedDepartmentCode == 0) {
			$text=<<<TEXT
1
TEXT;
		} else {
			$text=<<<TEXT
`affiliation_department`={$currentSelectedDepartmentCode}
TEXT;
		}

		$sql=<<<SQL
SELECT `doctor`,`name`,`affiliation_department` FROM `suhDoctors` 
WHERE {$text}
ORDER BY `name` ASC
LIMIT {$index}, {$view_num_plus}
SQL;
		$res = $db->query($sql);
		$numrows = $res->num_rows;

		if ($numrows >= 1) {

			$jsondata = '{"data":[';


			$ii = 0;
			while ($row = $res->fetch_object()) {
				$doctor_pk = $row->doctor;
				$doctor_name = $row->name;
				$doctor_department = $row->affiliation_department;

				$jsondata.=<<<JSON
{
	"doctor_pk":{$doctor_pk},
	"doctor_name":"{$doctor_name}",
	"doctor_department":{$doctor_department}
},
JSON;
				$ii++;

				if ($ii == $view_num) {
					break;
				}
			}

			$is_more_info = "ok";
			if ($numrows != $view_num_plus) {
				$is_more_info = "no";
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$is_more_info.'"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료과 이미지 요청
	function mobile5101() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$department_pk = $_REQUEST["department_pk"];

		$sql=<<<SQL
SELECT `image`, `note1` FROM `suhDepartments` WHERE `suhd`={$department_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$image = $row->image;
			$note1 = $row->note1;

			$jsondata=<<<JSON
{
	"image":"{$image}",
	"note1":{$note1},
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}

	}

	// 서버에 진료과 소개글/설명글 요청
	function mobile5121() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$department_pk = $_REQUEST["department_pk"];

		$sql=<<<SQL
SELECT `description` FROM `suhDepartments` WHERE `suhd`={$department_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$row = $res->fetch_object();
			$description = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->description));

			echo '{"result":"ok", "description":"'.$description.'"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 의료진 이미지 요청
	function mobile5201() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$doctor_pk = $_REQUEST["doctor_pk"];

		$sql=<<<SQL
SELECT `profile_image` FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$profile_image = $row->profile_image;
			$image_exist = $row->image_exist;

			echo $profile_image;

		} else {
			echo 'no';
		}
	}

	// 서버에 의료진 기본정보 요청
	function mobile5221() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$doctor_pk = $_REQUEST["doctor_pk"];

		$sql=<<<SQL
SELECT `profile_info`,`school_history`,`career` FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$row = $res->fetch_object();
			$profile_info = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->profile_info));
			$school_history = $row->school_history;
			$career = $row->career;

			$jsondata=<<<JSON
{
	"profile_info":"{$profile_info}",
	"school_history":"{$school_history}",
	"career":"{$career}",
	"result":"ok"
}
JSON;

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}

	// 서버에 해당 의료진의 감사댓글 리스트 요청
	function mobile5241() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$doctor_pk = $_REQUEST["doctor_pk"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1; 

		$sql=<<<SQL
SELECT 
`suhDoctorComments`.`suhdc`,
`suhDoctorComments`.`member`,
`suhMembers1`.`name` AS `member_name`,
`suhDoctorComments`.`datetime`,
`suhDoctorComments`.`comment` 
FROM `suhDoctorComments` 
LEFT JOIN `suhMembers` AS `suhMembers1`
ON `suhDoctorComments`.`member`=`suhMembers1`.`suhmember`
WHERE `suhDoctorComments`.`doctor`={$doctor_pk} 
AND `suhDoctorComments`.`status`=900400 
ORDER BY `suhDoctorComments`.`suhdc` DESC
LIMIT {$index}, {$view_num_plus}
SQL;
		$res = $db->query($sql);
		$numrows = $res->num_rows;

		if ($numrows >= 1) {

			$jsondata = '{"data":[';


			$ii = 0;
			while ($row = $res->fetch_object()) {
				$comment_pk = $row->suhdc;
				$member_pk = $row->member;
				$member_name = $row->member_name;
				$datetime = $row->datetime;
				$comment = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->comment));


				$jsondata.=<<<JSON
{
	"comment_pk":{$comment_pk},
	"member_pk":{$member_pk},
	"member_name":"{$member_name}",
	"datetime":"{$datetime}",
	"comment":"{$comment}"
},
JSON;
				$ii++;

				if ($ii == $view_num) {
					break;
				}
			}

			$is_more_info = "ok";
			if ($numrows != $view_num_plus) {
				$is_more_info = "no";
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$is_more_info.'"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}

	}


	// 서버에 해당 의료진 댓글 DB에 업로드 요청
	function mobile5243() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$doctor = $_REQUEST["doctor"]; // doctor pk
		$comment = addslashes($_REQUEST["comment"]); // 댓글 내용

		$sql=<<<SQL
INSERT INTO `suhDoctorComments`
(`member`,`doctor`,`comment`)VALUES
({$member}, {$doctor}, "{$comment}")
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 서버에 1:1 문의내용 DB 업로드 요청
	function mobile5521() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk

		$questionTypeCode = $_REQUEST["questionTypeCode"];
		$questionTitle = addslashes($_REQUEST["questionTitle"]);
		$questionContent = addslashes($_REQUEST["questionContent"]);

		$sql=<<<SQL
INSERT INTO `suhQuestions`
(`member`,`question_type`,`question_title`, `question_content`)VALUES
({$member}, {$questionTypeCode}, '{$questionTitle}', '{$questionContent}')
SQL;
		// echo $sql;
		// exit;

		$res = $db->query($sql);


		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}

	}

	// 서버에 1:1 문의내역 리스트 요청
	function mobile5541() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1;
		$period_size = $_REQUEST["period_size"];

		$text = '';
		if ($period_size > 0) {
			$text=<<<SQL
AND date(`suhQuestions`.`question_datetime`) >= date(subdate(now(), INTERVAL {$period_size} DAY)) 
AND date(`suhQuestions`.`question_datetime`) <= date(now()) 
SQL;
		}

		$sql=<<<SQL
SELECT 
`suhQuestions`.`suhq`, 
`suhQuestions`.`member`, 
`suhQuestions`.`question_type`, 
`suhCodes1`.`description` AS `question_type_str`,
`suhQuestions`.`question_datetime`,
`suhQuestions`.`question_title`,
`suhQuestions`.`question_content`,
`suhAnswers1`.`answer_datetime` AS `answer_datetime`,
`suhAnswers1`.`answer_title` AS `answer_title`,
`suhAnswers1`.`answer_content` AS `answer_content`
FROM `suhQuestions` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhQuestions`.`question_type`=`suhCodes1`.`codeNumber`
LEFT JOIN `suhAnswers` AS `suhAnswers1`
ON `suhQuestions`.`suhq`=`suhAnswers1`.`suhq`
WHERE `suhQuestions`.`member`={$member}
{$text}
ORDER BY `suhQuestions`.`question_datetime` DESC
LIMIT {$index}, {$view_num_plus}
SQL;

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 1개 이상이면
			$jsondata = '{"data":[';
			$ii = 0;
			while ($row = $res->fetch_object()) {
				$ii++;
				if ($ii == $view_num_plus) {
					break;
				}

				$questionPrimaryKey = $row->suhq;
				$memberPrimaryKey = $row->member;

				$questionTypePrimaryKey = $row->question_type;
				$questionTypeString = $row->question_type_str;

				$questionDatetime = $row->question_datetime;
				$questionTitle = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->question_title));
				$questionContent = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->question_content));

				$answerDatetime = $row->answer_datetime;
				$answerTitle = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->answer_title));
				$answerContent = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->answer_content));


				$jsondata.=<<<JSON
{
	"questionPrimaryKey":{$questionPrimaryKey},
	"memberPrimaryKey":{$memberPrimaryKey},
	"questionTypePrimaryKey":{$questionTypePrimaryKey},
	"questionTypeString":"{$questionTypeString}",
	"questionDatetime":"{$questionDatetime}",
	"questionTitle":"{$questionTitle}",
	"questionContent":"{$questionContent}",
	"answerDatetime":"{$answerDatetime}",
	"answerTitle":"{$answerTitle}",
	"answerContent":"{$answerContent}"
},
JSON;
			}

			if ($view_num_plus == $res->num_rows) {
				$more_info = 'ok';
			} else {
				$more_info = 'no';
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$more_info.'"}';

			echo $jsondata;
		} else {
			echo '{"result":"no", "more_info":"no"}';
		}
	}

	// 서버에 서울대학교 본원, 어린이병원, 암병원 각각 접수창구의 현재 번호 요청
	function mobile6501() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$today = date("Y-m-d");

		// 본원의 1번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700201 
AND `counter_number`=1 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700201_counter_1_number_exist = 'ok';
			$h_700201_counter_1_number = $row->ticket_number;
			$h_700201_counter_1_number_pk = $row->suhrt;

		} else {
			$h_700201_counter_1_number_exist = 'no';
			$h_700201_counter_1_number = 0;
			$h_700201_counter_1_number_pk = 0;
		}




		// 본원의 2번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700201 
AND `counter_number`=2 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700201_counter_2_number_exist = 'ok';
			$h_700201_counter_2_number = $row->ticket_number;
			$h_700201_counter_2_number_pk = $row->suhrt;

		} else {
			$h_700201_counter_2_number_exist = 'no';
			$h_700201_counter_2_number = 0;
			$h_700201_counter_2_number_pk = 0;
		}





		// 본원의 3번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700201 
AND `counter_number`=3 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700201_counter_3_number_exist = 'ok';
			$h_700201_counter_3_number = $row->ticket_number;
			$h_700201_counter_3_number_pk = $row->suhrt;

		} else {
			$h_700201_counter_3_number_exist = 'no';
			$h_700201_counter_3_number = 0;
			$h_700201_counter_3_number_pk = 0;
		}





		// 어린이병원의 1번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700202 
AND `counter_number`=1 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700202_counter_1_number_exist = 'ok';
			$h_700202_counter_1_number = $row->ticket_number;
			$h_700202_counter_1_number_pk = $row->suhrt;

		} else {
			$h_700202_counter_1_number_exist = 'no';
			$h_700202_counter_1_number = 0;
			$h_700202_counter_1_number_pk = 0;
		}





		// 어린이병원의 2번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700202 
AND `counter_number`=2 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700202_counter_2_number_exist = 'ok';
			$h_700202_counter_2_number = $row->ticket_number;
			$h_700202_counter_2_number_pk = $row->suhrt;

		} else {
			$h_700202_counter_2_number_exist = 'no';
			$h_700202_counter_2_number = 0;
			$h_700202_counter_2_number_pk = 0;
		}






		// 어린이병원의 3번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700202 
AND `counter_number`=3 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700202_counter_3_number_exist = 'ok';
			$h_700202_counter_3_number = $row->ticket_number;
			$h_700202_counter_3_number_pk = $row->suhrt;

		} else {
			$h_700202_counter_3_number_exist = 'no';
			$h_700202_counter_3_number = 0;
			$h_700202_counter_3_number_pk = 0;
		}








		// 암병원의 1번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700203 
AND `counter_number`=1 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700203_counter_1_number_exist = 'ok';
			$h_700203_counter_1_number = $row->ticket_number;
			$h_700203_counter_1_number_pk = $row->suhrt;

		} else {
			$h_700203_counter_1_number_exist = 'no';
			$h_700203_counter_1_number = 0;
			$h_700203_counter_1_number_pk = 0;
		}






		// 암병원의 2번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700203 
AND `counter_number`=2 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700203_counter_2_number_exist = 'ok';
			$h_700203_counter_2_number = $row->ticket_number;
			$h_700203_counter_2_number_pk = $row->suhrt;

		} else {
			$h_700203_counter_2_number_exist = 'no';
			$h_700203_counter_2_number = 0;
			$h_700203_counter_2_number_pk = 0;
		}







		// 암병원의 3번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700203 
AND `counter_number`=3 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700203_counter_3_number_exist = 'ok';
			$h_700203_counter_3_number = $row->ticket_number;
			$h_700203_counter_3_number_pk = $row->suhrt;

		} else {
			$h_700203_counter_3_number_exist = 'no';
			$h_700203_counter_3_number = 0;
			$h_700203_counter_3_number_pk = 0;
		}




		// 내 번호 알아내기
		$sql=<<<SQL
SELECT `hospital_type`, `get_ticket_number`, `status` FROM `suhReceptionTicketLogs` WHERE 
STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `member`={$member} 
AND (`status`=700301 OR `status`=700305) 
ORDER BY `get_ticket_number` ASC 
LIMIT 1
SQL;
		// AND `status`=700301


		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			// 결과가 있으면	
			$row = $res->fetch_object();
			$my_number_h_type = $row->hospital_type;
			$my_number = $row->get_ticket_number;
			$my_number_status = $row->status;
		} else {
			// 결과가 없으면
			$my_number_h_type = 0;
			$my_number = 0;
			$my_number_status = 0;
		}




		$jsondata=<<<JSON
{
	"h_700201_counter_1_number_exist":"{$h_700201_counter_1_number_exist}",
	"h_700201_counter_1_number":{$h_700201_counter_1_number},
	"h_700201_counter_1_number_pk":{$h_700201_counter_1_number_pk},
	"h_700201_counter_2_number_exist":"{$h_700201_counter_2_number_exist}",
	"h_700201_counter_2_number":{$h_700201_counter_2_number},
	"h_700201_counter_2_number_pk":{$h_700201_counter_2_number_pk},
	"h_700201_counter_3_number_exist":"{$h_700201_counter_3_number_exist}",
	"h_700201_counter_3_number":{$h_700201_counter_3_number},
	"h_700201_counter_3_number_pk":{$h_700201_counter_3_number_pk},
	"h_700202_counter_1_number_exist":"{$h_700202_counter_1_number_exist}",
	"h_700202_counter_1_number":{$h_700202_counter_1_number},
	"h_700202_counter_1_number_pk":{$h_700202_counter_1_number_pk},
	"h_700202_counter_2_number_exist":"{$h_700202_counter_2_number_exist}",
	"h_700202_counter_2_number":{$h_700202_counter_2_number},
	"h_700202_counter_2_number_pk":{$h_700202_counter_2_number_pk},
	"h_700202_counter_3_number_exist":"{$h_700202_counter_3_number_exist}",
	"h_700202_counter_3_number":{$h_700202_counter_3_number},
	"h_700202_counter_3_number_pk":{$h_700202_counter_3_number_pk},
	"h_700203_counter_1_number_exist":"{$h_700203_counter_1_number_exist}",
	"h_700203_counter_1_number":{$h_700203_counter_1_number},
	"h_700203_counter_1_number_pk":{$h_700203_counter_1_number_pk},
	"h_700203_counter_2_number_exist":"{$h_700203_counter_2_number_exist}",
	"h_700203_counter_2_number":{$h_700203_counter_2_number},
	"h_700203_counter_2_number_pk":{$h_700203_counter_2_number_pk},
	"h_700203_counter_3_number_exist":"{$h_700203_counter_3_number_exist}",
	"h_700203_counter_3_number":{$h_700203_counter_3_number},
	"h_700203_counter_3_number_pk":{$h_700203_counter_3_number_pk},

	"my_number_h_type":{$my_number_h_type},
	"my_number":{$my_number},
	"my_number_status":{$my_number_status},

	"result":"ok"
}
JSON;

		echo $jsondata;
		exit;
	}

	// 서버에 현재 대기중인 내 번호표가 있는지 여부 요청
	function mobile6505() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk

		$today_datetime = date('Y-m-d');

		$sql=<<<SQL
SELECT `hospital_type`, `get_ticket_number` FROM `suhReceptionTicketLogs` 
WHERE STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today_datetime}', '%Y-%m-%d') 
AND `status`=700301 
AND `member`={$member} 
ORDER BY `get_ticket_number` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			// 결과가 있으면	
			$row = $res->fetch_object();
			$my_number_h_type = $row->hospital_type;
			$my_number = $row->get_ticket_number;

			echo '{"result":"ok", "my_number_h_type":'.$my_number_h_type.', "my_number":'.$my_number.'}';
		} else {
			// 결과가 없으면
			$my_number_h_type = 0;
			$my_number = 0;

			echo '{"result":"no"}';
		}


	}

	// 서버에 순번대기표 뽑기 요청
	function mobile6507() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$hospital_type = $_REQUEST["hospital_type"];

		// 현재 대기중인 순번 대기표가 있는지 확인하기
		$today_datetime = date('Y-m-d');

		$sql=<<<SQL
SELECT `hospital_type`, `get_ticket_number` FROM `suhReceptionTicketLogs` 
WHERE STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today_datetime}', '%Y-%m-%d') 
AND `status`=700301 
AND `member`={$member} 
ORDER BY `get_ticket_number` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			// 대기중인 번호표가 존재하면 
			echo '{"result":"already"}';
			exit;
		}



		// 현재 대기중 상태이던 취소된 상태이던 제일 최신의 번호에서 + 1 해서 삽입하기
		$sql=<<<SQL
SET @x1 = 0;

SELECT @x1:=`get_ticket_number` FROM `suhReceptionTicketLogs` WHERE STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today_datetime}', '%Y-%m-%d') ORDER BY `get_ticket_number` DESC LIMIT 1;

INSERT INTO `suhReceptionTicketLogs` (`member`,`hospital_type`,`get_ticket_number`,`status`)VALUES({$member}, {$hospital_type}, (@x1+1) ,700301);
SQL;

		$res = $db->multi_query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}


	}

	// 서버에 순번대기표 취소 요청
	function mobile6509() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk

		$today_datetime = date('Y-m-d');



		$sql=<<<SQL
SELECT `hospital_type`, `get_ticket_number` FROM `suhReceptionTicketLogs` 
WHERE STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today_datetime}', '%Y-%m-%d') 
AND `status`=700301 
AND `member`={$member} 
ORDER BY `get_ticket_number` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);

		if ($res->num_rows != 1) {
			// 대기중인 번호표가 존재하지 않으면 
			echo '{"result":"empty"}';
			exit;
		}




		$sql=<<<SQL
UPDATE `suhReceptionTicketLogs` SET `status`=700302
WHERE `member`={$member}
AND STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today_datetime}', '%Y-%m-%d') 
AND `status`=700301 
SQL;
		


		$res = $db->query($sql);

		if ($res) {
			// 취소 성공
			echo '{"result":"ok"}';
		} else {
			// 취소 실패
			echo '{"result":"no"}';
		}		


	}

	// 서버에 진단서 리스트 요청
	function mobile7001() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1;
		$period_size = $_REQUEST["period_size"];

		$text = '';
		if ($period_size > 0) {
			$text=<<<SQL
AND date(`suhMedicalCertificate`.`medical_datetime`) >= date(subdate(now(), INTERVAL {$period_size} DAY)) 
AND date(`suhMedicalCertificate`.`medical_datetime`) <= date(now()) 
SQL;
		}

		$sql=<<<SQL
SELECT 
`suhMedicalCertificate`.`suhmc`,
`suhMedicalCertificate`.`member`,
`suhMedicalCertificate`.`medical_datetime`,
`suhMedicalCertificate`.`doctor`,
`suhDoctors1`.`name` AS `doctor_name`,
`suhMedicalCertificate`.`department`,
`suhDepartments1`.`department` AS `department_string`
FROM `suhMedicalCertificate` 
LEFT JOIN `suhDoctors` AS `suhDoctors1` 
ON `suhMedicalCertificate`.`doctor`=`suhDoctors1`.`doctor` 
LEFT JOIN `suhDepartments` AS `suhDepartments1` 
ON `suhMedicalCertificate`.`department`=`suhDepartments1`.`suhd` 
WHERE `suhMedicalCertificate`.`member`={$member}
{$text}
ORDER BY `suhMedicalCertificate`.`suhmc` DESC
LIMIT {$index}, {$view_num_plus}
SQL;

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 1개 이상이면
			$jsondata = '{"data":[';
			$ii = 0;
			while ($row = $res->fetch_object()) {
				$ii++;
				if ($ii == $view_num_plus) {
					break;
				}

				$suhmc = $row->suhmc;
				$member = $row->member;
				$medical_datetime = $row->medical_datetime;
				$doctor = $row->doctor;
				$doctor_name = addslashes($row->doctor_name);
				$department = $row->department;
				$department_string = addslashes($row->department_string);

				$medical_certificate_year = dateDivide($medical_datetime, 'yyyy');
				$medical_certificate_month = (int) dateDivide($medical_datetime, 'mm');
				$medical_certificate_month = attachZero($medical_certificate_month);
				$medical_certificate_date = (int) dateDivide($medical_datetime, 'dd');
				$medical_certificate_date = attachZero($medical_certificate_date);
				$medical_certificate_day = dateDivide($medical_datetime, 'day');

				$medical_certificate_hour = (int) dateDivide($medical_datetime, 'h');
				$medical_certificate_hour = attachZero($medical_certificate_hour);
				$medical_certificate_minute = (int) dateDivide($medical_datetime, 'i');	
				$medical_certificate_minute = attachZero($medical_certificate_minute);

				$pm_am = '';
				if ((int) $medical_certificate_hour == 12) {
					$pm_am = '오후';
				} else if ((int) $medical_certificate_hour < 12) {
					$pm_am = '오전';
				} else if ((int) $medical_certificate_hour > 12) {
					$pm_am = '오후';
					$medical_certificate_hour = $medical_certificate_hour - 12;
					$medical_certificate_hour = attachZero($medical_certificate_hour);
				}

				$medical_certificate_date = $medical_certificate_year.'년 '.$medical_certificate_month.'월 '.$medical_certificate_date.'일 '.$medical_certificate_day.'요일';
				$medical_certificate_time = $pm_am.' '.$medical_certificate_hour.'시 '.$medical_certificate_minute.'분';
				if ((int) $medical_certificate_minute == 0) {
					$medical_certificate_time = $pm_am.' '.$medical_certificate_hour.'시 ';
				}

				
				$passed_day = dateDiff(date('Y-m-d H:i:s'), $medical_datetime, 'd');
				$passed_hour = dateDiff(date('Y-m-d H:i:s'), $medical_datetime, 'H');
				$passed_minute = dateDiff(date('Y-m-d H:i:s'), $medical_datetime, 'i');


				$jsondata.=<<<JSON
{
	"suhmc":{$suhmc},
	"member":{$member},
	"medical_datetime":"{$medical_datetime}",
	"doctor":{$doctor},
	"doctor_name":"{$doctor_name}",
	"department":{$department},
	"department_string":"{$department_string}",
	"medical_certificate_date":"{$medical_certificate_date}",
	"medical_certificate_time":"{$medical_certificate_time}",
	"passed_day":{$passed_day},
	"passed_hour":{$passed_hour},
	"passed_minute":{$passed_minute}
},
JSON;
			}

			if ($view_num_plus == $res->num_rows) {
				$more_info = 'ok';
			} else {
				$more_info = 'no';
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$more_info.'"}';

			echo $jsondata;
		} else {
			echo '{"result":"no", "more_info":"no"}';
		}

	}

	// 서버에 진단서 상세정보 요청
	function mobile7101() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$suhmc = $_REQUEST["suhmc"]; // 진단서 pk

		// 진단서 기본 정보 가져오기
		$sql=<<<SQL
SELECT 
`suhMedicalCertificate`.`suhmc`,
`suhMedicalCertificate`.`member`,
`suhMedicalCertificate`.`medical_datetime`,
`suhMedicalCertificate`.`doctor`,
`suhDoctors1`.`name` AS `doctor_name`,
`suhMedicalCertificate`.`department`,
`suhDepartments1`.`department` AS `department_string`,
`suhMedicalCertificate`.`doctor_comment`
FROM `suhMedicalCertificate` 
LEFT JOIN `suhDoctors` AS `suhDoctors1` 
ON `suhMedicalCertificate`.`doctor`=`suhDoctors1`.`doctor` 
LEFT JOIN `suhDepartments` AS `suhDepartments1` 
ON `suhMedicalCertificate`.`department`=`suhDepartments1`.`suhd` 
WHERE `suhMedicalCertificate`.`suhmc`={$suhmc}
SQL;

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 1개 이상이면
			$jsondata = '{"data1":';

			$row = $res->fetch_object();

			$suhmc = $row->suhmc;
			$member = $row->member;
			$medical_datetime = $row->medical_datetime;
			$doctor = $row->doctor;
			$doctor_name = $row->doctor_name;
			$department = $row->department;
			$department_string = $row->department_string;
			$doctor_comment = preg_replace('/\r\n|\r|\n/','\n',addslashes($row->doctor_comment));


			$medical_certificate_year = dateDivide($medical_datetime, 'yyyy');
			$medical_certificate_month = (int) dateDivide($medical_datetime, 'mm');
			$medical_certificate_month = attachZero($medical_certificate_month);
			$medical_certificate_date = (int) dateDivide($medical_datetime, 'dd');
			$medical_certificate_date = attachZero($medical_certificate_date);
			$medical_certificate_day = dateDivide($medical_datetime, 'day');

			$medical_certificate_hour = (int) dateDivide($medical_datetime, 'h');
			$medical_certificate_hour = attachZero($medical_certificate_hour);
			$medical_certificate_minute = (int) dateDivide($medical_datetime, 'i');	
			$medical_certificate_minute = attachZero($medical_certificate_minute);

			$pm_am = '';
			if ((int) $medical_certificate_hour == 12) {
				$pm_am = '오후';
			} else if ((int) $medical_certificate_hour < 12) {
				$pm_am = '오전';
			} else if ((int) $medical_certificate_hour > 12) {
				$pm_am = '오후';
				$medical_certificate_hour = $medical_certificate_hour - 12;
				$medical_certificate_hour = attachZero($medical_certificate_hour);
			}

			$medical_certificate_date = $medical_certificate_year.'년 '.$medical_certificate_month.'월 '.$medical_certificate_date.'일 '.$medical_certificate_day.'요일';
			$medical_certificate_time = $pm_am.' '.$medical_certificate_hour.'시 '.$medical_certificate_minute.'분';
			if ((int) $medical_certificate_minute == 0) {
				$medical_certificate_time = $pm_am.' '.$medical_certificate_hour.'시 ';
			}


			$jsondata.=<<<JSON
{
	"suhmc":{$suhmc},
	"member":{$member},
	"medical_datetime":"{$medical_datetime}",
	"doctor":{$doctor},
	"doctor_name":"{$doctor_name}",
	"department":{$department},
	"department_string":"{$department_string}",
	"medical_certificate_date":"{$medical_certificate_date}",
	"medical_certificate_time":"{$medical_certificate_time}",
	"doctor_comment":"{$doctor_comment}"
},
JSON;
			


			// 진단서에 있는 처방약 리스트 가져오기
			$jsondata.='"data2":[';

			$sql=<<<SQL
SELECT 
`suhPrescriptionDrug`.`suhpd`,
`suhPrescriptionDrug`.`suhmc`,
`suhPrescriptionDrug`.`drug_code`,
`suhDrugList1`.`title` AS `drug_name`,
`suhPrescriptionDrug`.`oneday_dose`,
`suhPrescriptionDrug`.`oneday_number`,
`suhPrescriptionDrug`.`dose_day_num`,
`suhPrescriptionDrug`.`note` 
FROM `suhPrescriptionDrug` 
LEFT JOIN `suhDrugList` AS `suhDrugList1` 
ON `suhPrescriptionDrug`.`drug_code`=`suhDrugList1`.`suhdl` 
WHERE `suhPrescriptionDrug`.`suhmc`={$suhmc} 
ORDER BY `suhPrescriptionDrug`.`drug_code` ASC 
SQL;
			
		

			$res = $db->query($sql);

			if ($res->num_rows >= 1) {
				while($row = $res->fetch_object()) {
					$suhpd = $row->suhpd;
					$drug_code = $row->drug_code;
					$drug_name = $row->drug_name;

					$oneday_dose = $row->oneday_dose;
					$oneday_number = $row->oneday_number;
					$dose_day_num = $row->dose_day_num;
					$note = preg_replace('/\r\n|\r|\n/','\n',addslashes($row->note));


					$jsondata.=<<<JSON
{
	"suhpd":{$suhpd},
	"drug_code":{$drug_code},
	"drug_name":"{$drug_name}",
	"oneday_dose":{$oneday_dose},
	"oneday_number":{$oneday_number},
	"dose_day_num":{$dose_day_num},
	"note":"{$note}"
},
JSON;
				}

				$jsondata = substr($jsondata, 0, -1);
				$jsondata.='], "result":"ok"}';

				echo $jsondata;
			} else {
				$jsondata.='], "result":"ok"}';
				echo $jsondata;
			}

			
		} else {
			echo '{"result":"no"}';
		}





	}


	// 서버에 받은 푸쉬 목록 요청
	function mobile7501() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1;
		$period_size = $_REQUEST["period_size"];

		$text = '';
		if ($period_size > 0) {
			$text=<<<SQL
AND date(`suhPushPersonal`.`datetime`) >= date(subdate(now(), INTERVAL {$period_size} DAY)) 
AND date(`suhPushPersonal`.`datetime`) <= date(now()) 
SQL;
		}

		$sql=<<<SQL
SELECT 
`suhPushPersonal`.`suhpp`, 
`suhPushPersonal`.`suhpg`, 
`suhPushPersonal`.`member`, 
`suhPushPersonal`.`datetime`, 
`suhPushPersonal`.`title`, 
`suhPushPersonal`.`content`, 
`suhPushPersonal`.`status`, 
`suhPushGroup1`.`link_page` AS `link_page` 
FROM `suhPushPersonal` 
LEFT JOIN `suhPushGroup` AS `suhPushGroup1` 
ON `suhPushPersonal`.`suhpg`=`suhPushGroup1`.`suhpg` 
WHERE `suhPushPersonal`.`member`={$member} 
{$text} 
AND `suhPushPersonal`.`status`!=600203 
ORDER BY `suhPushPersonal`.`datetime` DESC 
LIMIT {$index}, {$view_num_plus}
SQL;

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 1개 이상이면
			$jsondata = '{"data":[';
			$ii = 0;
			while ($row = $res->fetch_object()) {
				$ii++;
				if ($ii == $view_num_plus) {
					break;
				}

				$suhpp = $row->suhpp;
				$suhpg = $row->suhpg;
				$member = $row->member;
				$datetime = $row->datetime;
				$title = addslashes($row->title);
				$content = preg_replace('/\r\n|\r|\n/','\n',addslashes($row->content));
				$status = $row->status;
				$link_page = $row->link_page;

				/*************************/



				$push_get_year = dateDivide($datetime, 'yyyy');
				$push_get_month = (int) dateDivide($datetime, 'mm');
				$push_get_month = attachZero($push_get_month);
				$push_get_date = (int) dateDivide($datetime, 'dd');
				$push_get_date = attachZero($push_get_date);
				$push_get_day = dateDivide($datetime, 'day');

				$push_get_hour = (int) dateDivide($datetime, 'h');
				$push_get_hour = attachZero($push_get_hour);
				$push_get_minute = (int) dateDivide($datetime, 'i');	
				$push_get_minute = attachZero($push_get_minute);

				$pm_am = '';
				if ((int) $push_get_hour == 12) {
					$pm_am = '오후';
				} else if ((int) $push_get_hour < 12) {
					$pm_am = '오전';
				} else if ((int) $push_get_hour > 12) {
					$pm_am = '오후';
					$push_get_hour = $push_get_hour - 12;
					$push_get_hour = attachZero($push_get_hour);
				}

				$push_get_date = $push_get_year.'년 '.$push_get_month.'월 '.$push_get_date.'일 '.$push_get_day.'요일';
				$push_get_time = $pm_am.' '.$push_get_hour.'시 '.$push_get_minute.'분';
				if ((int) $push_get_minute == 0) {
					$push_get_time = $pm_am.' '.$push_get_hour.'시 ';
				}

				
				$passed_day = dateDiff(date('Y-m-d H:i:s'), $datetime, 'd');
				$passed_hour = dateDiff(date('Y-m-d H:i:s'), $datetime, 'H');
				$passed_minute = dateDiff(date('Y-m-d H:i:s'), $datetime, 'i');


				$jsondata.=<<<JSON
{
	"suhpp":{$suhpp},
	"suhpg":{$suhpg},
	"member":{$member},
	"datetime":"{$datetime}",
	"title":"{$title}",
	"content":"{$content}",
	"status":{$status},
	"link_page":{$link_page},
	"push_get_date":"{$push_get_date}",
	"push_get_time":"{$push_get_time}",
	"passed_day":{$passed_day},
	"passed_hour":{$passed_hour},
	"passed_minute":{$passed_minute}
},
JSON;
			}

			if ($view_num_plus == $res->num_rows) {
				$more_info = 'ok';
			} else {
				$more_info = 'no';
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$more_info.'"}';

			echo $jsondata;
		} else {
			echo '{"result":"no", "more_info":"no"}';
		}
	}

	// 서버에 푸쉬 읽음 처리 요청
	function mobile7503() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$push_pk = $_REQUEST["push_pk"];


		$sql=<<<SQL
UPDATE `suhPushPersonal` SET `status`=600201 
WHERE `suhpp`={$push_pk}
AND `member`={$member} 
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}

	}

	// 병원뉴스 리스트 요청
	function mobile8001() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$view_num_plus = $view_num + 1;


		$sql=<<<SQL
SELECT 
`suhnews`, 
`title`, 
`content`, 
`datetime`, 
`view_index` 
FROM `suhNews` 
WHERE `status`!=900300 
ORDER BY `suhnews` DESC  
LIMIT {$index}, {$view_num_plus}
SQL;

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 1개 이상이면
			$jsondata = '{"data":[';
			$ii = 0;
			while ($row = $res->fetch_object()) {
				$ii++;
				if ($ii == $view_num_plus) {
					break;
				}

				$suhnews = $row->suhnews;
				$title = addslashes($row->title);
				$content = preg_replace('/\r\n|\r|\n/','\n', addslashes($row->content));
				$datetime = $row->datetime;
				$view_index = $row->view_index;
				

				$jsondata.=<<<JSON
{
	"suhnews":{$suhnews},
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"view_index":{$view_index}
},
JSON;
			}

			if ($view_num_plus == $res->num_rows) {
				$more_info = 'ok';
			} else {
				$more_info = 'no';
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "more_info":"'.$more_info.'"}';

			echo $jsondata;
		} else {
			echo '{"result":"no", "more_info":"no"}';
		}
	}

	// 서버에 푸쉬 알림 수신 여부 요청
	function mobile9001() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk

		$sql=<<<SQL
SELECT `is_get_push` FROM `suhMembers` WHERE `suhmember`={$member}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {

			$row = $res->fetch_object();

			$is_get_push = $row->is_get_push;

			$jsondata=<<<JSON
{
	"is_get_push":{$is_get_push},
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
	}

	// 서버에 푸쉬 알림 수신 여부 업로드
	function mobile9003() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; // member pk
		$value = $_REQUEST["value"];

		$sql=<<<SQL
UPDATE `suhMembers` SET `is_get_push`={$value} WHERE `suhmember`={$member}
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok", "success_value":'.$value.'}';
		} else {
			echo '{"result":"no"}';
		}

	}

































	/******************************************************************************************/

	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }

    // 날짜 비교 함수 첫번째 파라미터 날짜가 더 크면 1, 같으면 0. 작으면 -1
    function toCompareDate($str1, $str2) {
    	$date1 = strtotime($str1);
    	$date2 = strtotime($str2);

    	if ($date1 > $date2) {
    		return 1;
    	} else if ($date1 == $date2) {
    		return 0;
    	} else {
    		return -1;
    	}
    }

    // 두 날짜 사이의 차이일수 구하기
	function dateDiff($sStartDate, $sEndDate, $type) {
	    $sStartTime = strtotime($sStartDate);
	    $sEndTime = strtotime($sEndDate);
	 
	 	$value = $sStartTime - $sEndTime;
	 	
	 	$dateDiffer = 0;
	 	switch ($type) {
	 		case 'd':
	 			$dateDiffer = $value / (60*60*24);
	 			break;
	 		case 'H':
	 			$dateDiffer = $value / (60*60);
	 			break;
 			case 'i':
 				$dateDiffer = $value / (60);
 				break;
	 		default:
	 			# code...
	 			break;
	 	}

	 	return floor($dateDiffer); 
	}

	function attachZero($numbers) {
		$value = ''.$numbers;
		if ($numbers < 10) {
			$value = '0'.$numbers;
		}
		return $value;
	}

    /******************************************************************************************/

	$db->close();
?>