<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 N의학정보 상세정보" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - N의학정보 상세정보</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web4100.js"></script>
	</head>
	<body id="body" page-code="web4100">
		<input type="hidden" name="pk" value="<?php echo $_REQUEST['pk']; ?>" />

		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>



		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>



		<div class="sub_big_title_box web4000">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					N의학정보
				</div>
				<div class="comment">
					여러가지 질병 및 의학에 대한 정보를 얻을 수 있습니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 건강정보 > 질병/의학정보 > <a href="./web4000.php">N의학정보</a> > N의학정보 상세정보
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>




		<section class="web4100 title">
			<div class="ko common">
				자율신경 실조증
			</div>
			<div class="en common">
				[autonomic dysfunction]
			</div>
		</section>


		<section class="web4100 simple_info_box">
			<ul class="clearFix">
				<li class="clearFix">
					<div class="title common">
						한 줄 설명
					</div>
					<div class="description common">
						자율신경계와 관계되는 교감, 부교감 신경계의 이상으로 발생하는 증후군
					</div>
				</li>
				<li class="clearFix">
					<div class="title common">
						관련 진료과
					</div>
					<div class="description common">
						신경과, 마취통증의학과, 정신건강의학과
					</div>
				</li>
				<li class="clearFix">
					<div class="title common">
						관련신체기관
					</div>
					<div class="description common">
						뇌, 척수, 신경
					</div>
				</li>
			</ul>
		</section>


		<section class="web4100 all_open_all_close_button_box clearFix">
			<ul class="clearFix">
				<li class="all_open common">
					전체펼치기
				</li>
				<li class="all_close common">
					전체접기
				</li>
			</ul>
		</section>


		<section class="web4100 accordion_list">
			<ul class="clearFix setTopVirtualBox">
				<?php for ($i=0; $i<5; $i++) { ?>
				<li class="close">
					<div class="title">
						정의
					</div>
					<div class="open_close_button">
						<img src="./images/web0000_bottom_arrow_black_icon.png" alt="아래방향 검은색 아이콘" title="아래방향 검은색 아이콘" />
					</div>
					<div class="content">
						자율신경계는 내분비계와 더불어 심혈관, 호흡, 소화, 비뇨기 및 생식기관, 체온조절계, 동공 조절 등의 <br />
						기능을 조절해 신체의 항상성을 유지하게 하는 역할을 한다. <br /><br />

						대뇌반구 수준과 뇌줄기 수준, 척수 수준과 말초신경 수준으로 그 구조를 나눌 수 있으며, <br />
						특히 말초신경 수준에서 말초의 자율신경계는 서로 대항 작용을 하는 교감 신경계와 부교감 신경계로 구성된다. <br /><br />

						이러한 자율신경계의 조절이 제대로 이루어지지 않는 경우를 자율신경 실조증이라고 한다.
					</div>
				</li>
				<?php } ?>
			</ul>
		</section>


		<section class="web4100 license_box clearFix">
			<div class="img common">
				<img src="./images/web0000_license_img.png" alt="저작권 관련 경고 이미지" title="저작권 관련 경고 이미지" />
			</div>
			<div class="txt common">
				해당 콘텐츠 저작권은 서울대학교병원에 있습니다. <br />
				무단 이용하는 경우 저작권법 등에 따라 법적 책임을 질 수 있습니다.
			</div>
		</section>


		<?php include "footer.php"; ?>
	</body>
</html>