<?php
	session_start();

	header("Content-Type: text/html; charset=UTF-8");

	$db = new mysqli("localhost", $db_user, $db_password, $db_name) or die("mysqli db connect error");
	$db->set_charset("utf8");
	$act = "none";
	if (isset($_REQUEST["act"])) {
		$act = $_REQUEST["act"];
	} else {

	}

	$newbee = 'UTF-8';
	$origin ='EUC-KR';




	switch ($act) {
		/**************************************************************************************/
		/* web0000 (공통사항) */
		/**************************************************************************************/
		case 'web0010': // 로그아웃 요청
			web0010();
			break;



		/**************************************************************************************/
		/* web1000 (메인) */
		/**************************************************************************************/
		case 'web1015': // 병원뉴스 최신 10개 리스트 가져오기
			web1015();
			break;




		/**************************************************************************************/
		/* web1100 (로그인) */
		/**************************************************************************************/
		case 'web1110': // 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 보내기
			web1110();
			break;




		/**************************************************************************************/
		/* web1200 (회원가입) */
		/**************************************************************************************/
		case 'web1210': // 이메일 중복체크 요청하기
			web1210();
			break;
		case 'web1220': // 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청
			web1220();
			break;



		/**************************************************************************************/
		/* web1500 (마이페이지) */
		/**************************************************************************************/
		case 'web1510': // 진료예약 내역 가져오기
			web1510();
			break;
		case 'web1516':	// 진료예약 내역 페이징 - 페이지 맨 마지막 번호 알아내기
			web1516();
			break;
		case 'web1520': // 진료예약 취소 요청 보내기
			web1520();
			break;

		case 'web1530': // 회원 상세정보 가져오기
			web1530();
			break;
		case 'web1532': // 회원정보 수정하기
			web1532();
			break;



		/**************************************************************************************/
		/* web2000 (진료예약) */
		/**************************************************************************************/
		case 'web2003': // 진료과 목록 요청하고 보여주기
			web2003();
			break;
		case 'web2007': // 진료과 pk에 맞는 의료진 리스트 가져와 뿌리기
			web2007();
			break;
		case 'web2009': // 의료진 이미지 가져오기
			web2009();
			break;
		case 'web2011': // 날짜 선택시 의료진, 날짜에 맞는 시간 뿌리기
			web2011();
			break;
		case 'web2020': // 서버에 입력받은 예약정보를 DB에 저장 요청
			web2020();
			break;




		/**************************************************************************************/
		/* web2100 (진료과 소개) */
		/**************************************************************************************/
		case 'web2101': // 진료과 리스트 가져오기
			web2101();
			break;



		/**************************************************************************************/
		/* web2200 (진료과 상세페이지) */
		/**************************************************************************************/
		case 'web2201': // 진료과 상세정보 가져오기
			web2201();
			break;
		case 'web2202': // 진료과 이미지 가져오기
			web2202();
			break;
		case 'web2203': // 의료진 이미지 가져오기
			web2203();
			break;




		/**************************************************************************************/
		/* web2300 (의료진 소개) */
		/**************************************************************************************/
		case 'web2301': // 진료과 리스트 가져오기
			web2301();
			break;
		case 'web2302': // 의료진 리스트 가져오기
			web2302();
			break;
		case 'web2303': // 의료진 이미지 가져오기 
			web2303();
			break;



		/**************************************************************************************/
		/* web2400 (의료진 상세페이지) */
		/**************************************************************************************/
		case 'web2401': // 의료진 상세정보 가져오기
			web2401();
			break;
		case 'web2402': // 의료진 이미지 가져오기
			web2402();
			break;






		/**************************************************************************************/
		/* web4000 (N의학정보) */
		/**************************************************************************************/
		case 'web4010': // 오늘의 의학정보 - 랜덤으로 N의학정보 4개 가져오기
			web4010();
			break;
		case 'web4015': // N의학정보 가져오기
			web4015();
			break;




		/**************************************************************************************/
		/* web4100 (N의학정보 - 상세페이지) */
		/**************************************************************************************/
		case 'web4101': // N의학정보 상세정보 가져오기
			web4101();
			break;






		/**************************************************************************************/
		/* web5000 (고객의소리 게시판) */
		/**************************************************************************************/
		case 'web5001': // 고객의소리 - 감사합니다 가져오기
			web5001();
			break;
		case 'web5007': // 고객의소리 - 감사합니다 페이징 - 페이지 맨 마지막 번호 알아내기
			web5007();
			break;

		case 'web5011': // 고객의소리 - 건의합니다 가져오기
			web5011();
			break;
		case 'web5017': // 고객의소리 - 건의합니다 페이징 - 페이지 맨 마지막 번호 알아내기
			web5017();
			break;




		/**************************************************************************************/
		/* web5100 (고객의소리 등록페이지) */
		/**************************************************************************************/
		case 'web5111': // 내용 종류 리스트 가져오기
			web5111();
			break;
		case 'web5121': // 서버 DB에 고객의소리 내용 저장하기
			web5121();
			break;	





		/**************************************************************************************/
		/* web5200 (고객의소리 게시판 상세정보) */
		/**************************************************************************************/
		case 'web5210': // 고객의소리 게시글 상세정보 가져오기
			web5210();
			break;






		/**************************************************************************************/
		/* web6000 (병원뉴스) */
		/**************************************************************************************/
		case 'web6008': // 병원뉴스 최근 3개 가져오기
			web6008();
			break;
		case 'web6010': // 병원뉴스 리스트 가져오기
			web6010();
			break;
		case 'web6020': // 페이지 맨 마지막 번호 알아내기
			web6020();
			break;



		/**************************************************************************************/
		/* web6100 (병원뉴스 상세페이지) */
		/**************************************************************************************/
		case 'web6110': // 병원뉴스 상세정보 가져오기
			web6110();
			break;



		



		case  'test':
			test();
			break;
		default:

			break;


	}


	/**************************************************************************************/

	// 로그아웃 요청
	function web0010() {
		$_SESSION['is_login'] = '';
		$_SESSION['user_id'] = '';
		$_SESSION['user_name'] = '';
		$_SESSION['user_primarykey'] = '';
		session_destroy();

		echo '{"result":"ok"}';
	}

	// 병원뉴스 최신 10개 리스트 가져오기
	function web1015() {
		$db = $GLOBALS["db"];
		

		$sql=<<<SQL
SELECT 
`suhNews`.`suhnews`, 
`suhNews`.`admin`, 
`suhAdmin1`.`name` AS `admin_name`, 
`suhNews`.`title`, 
`suhNews`.`datetime`, 
`suhNews`.`attach_file_flag`, 
`suhNews`.`view_index` 
FROM `suhNews` 
LEFT JOIN `suhAdmin` AS `suhAdmin1` 
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin` 
ORDER BY `suhNews`.`suhnews` DESC
LIMIT 8
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhnews_pk = $row->suhnews;
				$admin = $row->admin;
				$admin_name = $row->admin_name;
				$title = $row->title;
				$datetime = $row->datetime;
				$attach_file_flag = $row->attach_file_flag;
				$view_index = $row->view_index;

				$jsondata.=<<<JSON
{
	"suhnews_pk":{$suhnews_pk},
	"admin":{$admin},
	"admin_name":"{$admin_name}",
	"title":"{$title}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';
			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
	}

	// 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 보내기
	function web1110() {
		$db = $GLOBALS["db"];

		$id = addslashes($_REQUEST["id"]);
		$pw = addslashes($_REQUEST["pw"]);
		$md5pw = md5($pw);

		$sql=<<<SQL
SELECT `suhmember`,`name` FROM `suhMembers` WHERE `email`='{$id}' AND `password`='{$md5pw}'
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 일치하는 계정이 있을 경우
			$row = $res->fetch_object();
			$suhmember = $row->suhmember;
			$name = $row->name;

			$_SESSION['is_login'] = 'ok';
			$_SESSION['user_id'] = $id;
			$_SESSION['user_name'] = $name;
			$_SESSION['user_primarykey'] = $suhmember;

			echo '{"result":"ok", "suhmember":'.$suhmember.'}';
		} else {
			// 일치하는 계정이 없을 경우
			echo '{"result":"no"}';
		}
	}

	// 이메일 중복체크 요청하기
	function web1210() {
		$db = $GLOBALS["db"];

		$checkedEmail = addslashes($_REQUEST["checkedEmail"]); // 이메일

		$sql=<<<SQL
SELECT `email` FROM `suhMembers` WHERE `email`='{$checkedEmail}'
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 이메일이 이미 존재하면
			echo '{"emailUse":"no"}'; // 적합하지 않다는 의미의 no
		} else {
			// 이메일이 존재하지 않으면
			echo '{"emailUse":"ok"}'; // 적합하다는 의미의 ok
		}
	}

	// 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청
	function web1220() {
		$db = $GLOBALS["db"];

		$checkedName = htmlspecialchars($_REQUEST["checkedName"]); // 이름
		$checkedGender = $_REQUEST["checkedGender"];
		$checkedMobileCompanyNumber = $_REQUEST["checkedMobileCompanyNumber"]; // 통신사 번호
		$checkedPhoneNumber = $_REQUEST["checkedPhoneNumber"]; // 휴대폰 번호
		$checkedEmail = htmlspecialchars($_REQUEST["checkedEmail"]); // 이메일
		$checkedPassword = $_REQUEST["checkedPassword"]; // 비밀번호
		$checkedBirthday = $_REQUEST["checkedBirthday"]; // 생년월일
		$checkedBloodTypeNumber = $_REQUEST["checkedBloodTypeNumber"]; // 혈액형 번호
		$checkedAddress = htmlspecialchars($_REQUEST["checkedAddress"]); // 주소

		$md5Password = md5($checkedPassword);

		$sql=<<<SQL
INSERT INTO `suhMembers`
(`name`, `gender`, `theMobileCompany`,`phoneNumber`,`email`,`password`,`birthday`,`theBloodType`,`addr`, `signUpMethod`, `status`)VALUES
('{$checkedName}', {$checkedGender}, {$checkedMobileCompanyNumber}, '{$checkedPhoneNumber}', '{$checkedEmail}', '{$md5Password}', '{$checkedBirthday}', {$checkedBloodTypeNumber}, '{$checkedAddress}', 500801
, 500101)
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료예약 내역 가져오기
	function web1510() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"]; 
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

		$start_index = $index * $view_num;

		$sql=<<<SQL
SELECT 
`suhCRH`.`suhcrh`,
`suhCRH`.`hope_datetime`,
`suhCRH`.`department`,
`suhDepartments1`.`department` AS `department_string`,
`suhCRH`.`doctor`,
`suhDoctors1`.`name` AS `doctor_name`,
`suhCRH`.`status`
FROM `suhCRH`
LEFT JOIN `suhDepartments` AS `suhDepartments1`
ON `suhCRH`.`department`=`suhDepartments1`.`suhd` 
LEFT JOIN `suhDoctors` AS `suhDoctors1`
ON `suhCRH`.`doctor`=`suhDoctors1`.`doctor` 
WHERE `member`={$member} 
ORDER BY `suhCRH`.`suhcrh` DESC 
LIMIT {$start_index}, {$view_num}
SQL;

		
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhcrh = $row->suhcrh;
				$hope_datetime = $row->hope_datetime;
				$department = $row->department;
				$department_string = $row->department_string;
				$doctor = $row->doctor;
				$doctor_name = $row->doctor_name;
				$status = $row->status;

				$jsondata.=<<<JSON
{
	"suhcrh":{$suhcrh},
	"hope_datetime":"{$hope_datetime}",
	"department":{$department},
	"department_string":"{$department_string}",
	"doctor":{$doctor},
	"doctor_name":"{$doctor_name}",
	"status":{$status}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<10; $ii++) {
					if ($first_indexs % 10 == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;


				$sql=<<<SQL
SELECT `suhcrh` FROM `suhCRH` WHERE `member`={$member}
ORDER BY `suhcrh` DESC LIMIT {$first_indexs}, 101
SQL;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == 101) {
					$pagenum = 10;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/10);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료예약 내역 페이징 - 페이지 맨 마지막 번호 알아내기
	function web1516() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];

		$sql=<<<SQL
SELECT `suhcrh` FROM `suhCRH` WHERE `member`={$member} 
ORDER BY `suhcrh` DESC
SQL;
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / 10) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<10; $i++) {
			if ($first_index % 10 == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
	}

	// 진료예약 취소 요청 보내기
	function web1520() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$crh_pk = $_REQUEST["crh_pk"];

		






		$sql=<<<SQL
UPDATE `suhCRH` SET `cancel_datetime`=now(), `status`=500203 
WHERE `member`={$member} AND `suhcrh`={$crh_pk}
SQL;
		$res = $db->query($sql);

		if ($res) {
			// 쿼리가 성공했으면
			echo '{"result":"ok"}';
		} else {
			// 쿼리가 실패했으면
			echo '{"result":"no"}';
		}
	}

	// 회원 상세정보 가져오기
	function web1530() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["member"];

		$sql=<<<SQL
SELECT 
`suhMembers`.`name`,
`suhMembers`.`gender`,
`suhMembers`.`theMobileCompany`,
`suhMembers`.`phoneNumber`,
`suhMembers`.`email`,
`suhMembers`.`birthday`,
`suhMembers`.`theBloodType`,
`suhMembers`.`addr` 
FROM `suhMembers` 
WHERE `suhMembers`.`suhmember`={$pk}
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$name = $row->name;
			$gender = $row->gender;
			$theMobileCompany = $row->theMobileCompany;
			$phoneNumber = $row->phoneNumber;
			$email = $row->email;
			$birthday = $row->birthday;
			$theBloodType = $row->theBloodType;
			$addr = $row->addr;

			$jsondata=<<<JSON
{
	"name":"{$name}",
	"gender":{$gender},
	"theMobileCompany":{$theMobileCompany},
	"phoneNumber":"{$phoneNumber}",
	"email":"{$email}",
	"birthday":"{$birthday}",
	"theBloodType":{$theBloodType},
	"addr":"{$addr}",
	"result":"ok"
}
JSON;
			echo $jsondata;
	
		} else {
			echo '{"result":"no"}';
		}
	}

	// 회원정보 수정하기
	function web1532() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$gender = $_REQUEST["gender"];
		$mobileCompany = $_REQUEST["mobileCompany"];
		$phoneNumber = htmlspecialchars($_REQUEST["phoneNumber"]);
		$birthday = $_REQUEST["birthday"];
		$bloodtype = $_REQUEST["bloodtype"];
		$addr = htmlspecialchars($_REQUEST["addr"]);

		$sql=<<<SQL
UPDATE `suhMembers` SET 
`gender`={$gender},
`theMobileCompany`={$mobileCompany},
`phoneNumber`='{$phoneNumber}',
`birthday`='{$birthday}',
`theBloodType`={$bloodtype},
`addr`='{$addr}' 
WHERE `suhmember`={$member}
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료과 목록 요청하고 보여주기
	function web2003() {
		$db = $GLOBALS["db"];

		$sql=<<<SQL
SELECT `suhd`, `department` FROM `suhDepartments` ORDER BY `department` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$suhd = $row->suhd;
				$department = $row->department;

				$jsondata.=<<<JSON
{
	"suhd":{$suhd},
	"department":"{$department}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';
			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료과 pk에 맞는 의료진 리스트 가져와 뿌리기
	function web2007() {
		$db = $GLOBALS["db"];
		$department_pk = $_REQUEST["department_pk"]; // ex) 800101
		$department_string = $_REQUEST["department_string"]; // ex) 내과

		$sql=<<<SQL
SELECT `doctor`, `name` FROM `suhDoctors` WHERE `affiliation_department`={$department_pk}
ORDER BY `name` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			// 결과가 있을 경우
			$jsondata = '{"data":[';
			while($row = $res->fetch_object()) {
				$doctor = $row->doctor;
				$name = $row->name;

				$jsondata.=<<<JSON
{
	"doctor":{$doctor},
	"department_string":"{$department_string}",
	"name":"{$name}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			// 결과가 없을 경우
			echo '{"result":"no"}';
		}
	}

	// 의료진 이미지 가져오기
	function web2009() {
		$db = $GLOBALS["db"];
		$doctor_pk = $_REQUEST["doctor_pk"];

		$sql=<<<SQL
SELECT `profile_image` FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {

			$row = $res->fetch_object();
			$profile_image = $row->profile_image;

			if ($profile_image != '') {
				echo $profile_image;
			} else {
				echo 'no';	
			}

		} else {
			echo 'no';
		}
	}

	// 날짜 선택시 의료진, 날짜에 맞는 시간 뿌리기
	function web2011() {
		$db = $GLOBALS["db"];

		$department = $_REQUEST["department"];
		$doctor = $_REQUEST["doctor"];
		$datetimeHypon = $_REQUEST["datetimeHypon"];

		$sql=<<<SQL
SELECT 
`suhDoctorSchedule`.`posible_datetime`, 
`suhCRH1`.`suhcrh` AS `suhCRH_pk` 
FROM `suhDoctorSchedule` 
LEFT JOIN `suhCRH` AS `suhCRH1`
ON `suhDoctorSchedule`.`posible_datetime` = `suhCRH1`.`hope_datetime`
AND `suhDoctorSchedule`.`doctor`=`suhCRH1`.`doctor` 
WHERE STR_TO_DATE(`suhDoctorSchedule`.`posible_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$datetimeHypon}', '%Y-%m-%d') 
AND `suhDoctorSchedule`.`doctor`={$doctor}
ORDER BY `posible_datetime` ASC 
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$posible_datetime = $row->posible_datetime;

				// pk가 null이 아니면 이미 누가 예약한 사람이 있다는 의미
				$suhCRH_pk = $row->suhCRH_pk;
				if ($suhCRH_pk == null) {
					$suhCRH_pk = 0; // 0 이면 아무도 그 시간에 예약하지 않았다는 의미
				}

				$year = dateDivide($posible_datetime, 'YY');
				$month = (int) dateDivide($posible_datetime, 'mm');
				$date = (int) dateDivide($posible_datetime, 'dd');

				$hour = (int) dateDivide($posible_datetime, 'HH');
				$minute = (int) dateDivide($posible_datetime, 'ii');

				$jsondata.=<<<JSON
{
	"year":{$year},
	"month":{$month},
	"date":{$date},
	"hour":{$hour},
	"minute":{$minute},
	"already_suhCRH_pk":{$suhCRH_pk}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}

	// 서버에 입력받은 예약정보를 DB에 저장 요청
	function web2020() {
		$db = $GLOBALS["db"];
		$member = $_SESSION['user_primarykey'];

		if ($member == '') {
			echo '{"result":"notlogin"}';
			exit;
		}

		$step01_department = $_REQUEST["step01_department"]; // 진료과
		$step01_symptom = ''; // 증상

		$step02_doctor_primarykey = $_REQUEST["step02_doctor_primarykey"]; // 의료진

		$step03_select_year = $_REQUEST["step03_select_year"]; // 년
		$step03_select_month = $_REQUEST["step03_select_month"]; // 월
		$step03_select_date = $_REQUEST["step03_select_date"]; // 일
		$step03_select_hour = $_REQUEST["step03_select_hour"]; // 시
		$step03_select_minute = $_REQUEST["step03_select_minute"]; // 분


		$hope_datetime = $step03_select_year.'-'.$step03_select_month.'-'.$step03_select_date.' '.$step03_select_hour.':'.$step03_select_minute.':00';
		// code..	

		// echo $step01_department.', '.$step01_symptom.', '.$step02_doctor_primarykey.', '.$step03_select_year.', '.$step03_select_month.', '.$step03_select_date.', '.$step03_select_hour.', '.$step03_select_minute.' and '.$hope_datetime;
		

		// 해당 년월일 의료진의 시, 분에 예약되어 있는지 확인하기
		$sql=<<<SQL
SELECT `suhcrh` FROM `suhCRH` 
WHERE STR_TO_DATE(`hope_datetime`, '%Y-%m-%d %h:%i:%s')=STR_TO_DATE('{$hope_datetime}', '%Y-%m-%d %h:%i:%s') 
AND `doctor`={$step02_doctor_primarykey} 
AND `status`=500201
SQL;
		$res = $db->query($sql);
		if ($res->num_rows >= 1) {
			// 이미 누가 그 의료진의 그 시간에 예약을 했으면 result : "already" 를 띄우기
			echo '{"result":"already"}';
			exit;
		}


		$sql=<<<SQL
INSERT INTO `suhCRH`
(`member`,`hope_datetime`,`department`,`symptom`,`doctor`,`status`)VALUES
({$member}, '{$hope_datetime}', {$step01_department}, '{$step01_symptom}', {$step02_doctor_primarykey}, 500201)
SQL;
		
	
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료과 리스트 가져오기
	function web2101() {
		$db = $GLOBALS["db"];

		$sql=<<<SQL
SELECT `suhd`, `department` FROM `suhDepartments` ORDER BY `department` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhd = $row->suhd;
				$department = $row->department;

				$jsondata.=<<<JSON
{
	"suhd":{$suhd},
	"department":"{$department}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료과 상세정보 가져오기
	function web2201() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];

		$sql=<<<SQL
SELECT `department`,`description` FROM `suhDepartments` WHERE `suhd`={$pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$department = $row->department;
			$description = setCleanText($row->description, 'html');


			$sql=<<<SQL
SELECT `doctor`,`name` FROM `suhDoctors` WHERE `affiliation_department`={$pk}
SQL;
			$res = $db->query($sql);

			$jsondata = '{"data":[';
			$flag = 0;
			if ($res->num_rows >= 1) {
				$flag++;
				while ($row=$res->fetch_object()) {
					$doctor = $row->doctor;
					$name = $row->name;
					$profile_image = $row->profile_image;

					$jsondata.=<<<JSON
{
	"doctor":{$doctor},
	"name":"{$name}"
},
JSON;
				}
			}

			if ($flag >= 1) {
				$jsondata = substr($jsondata, 0, -1);
			}

			$jsondata.='], "department":"'.$department.'", "description":"'.$description.'", "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
	}

	// 진료과 이미지 가져오기
	function web2202() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];

		$sql=<<<SQL
SELECT `image` FROM `suhDepartments` WHERE `suhd`={$pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$image = $row->image;

			echo $image;
		} else {
			echo 'no';
		}
	}

	// 의료진 이미지 가져오기
	function web2203() {
		$db = $GLOBALS["db"];
		$doctor_pk = $_REQUEST["doctor_pk"];

		$sql=<<<SQL
SELECT `profile_image` FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$image = $row->profile_image;


			if ($image == '') {
				echo 'no';
			} else {
				echo $image;
			}
			
		} else {
			echo 'no';
		}
	}

	// 진료과 리스트 가져오기
	function web2301() {
		$db = $GLOBALS["db"];

		$sql=<<<SQL
SELECT `suhd`,`department` FROM `suhDepartments` ORDER BY `department` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row=$res->fetch_object()) {
				$suhd = $row->suhd;
				$department = $row->department;

				$jsondata.=<<<JSON
{
	"suhd":{$suhd},
	"department":"{$department}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
	}

	// 의료진 리스트 가져오기
	function web2302() {
		$db = $GLOBALS["db"];

		$sql=<<<SQL
SELECT `doctor`, `name`, `affiliation_department` FROM `suhDoctors` ORDER BY `name` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$doctor = $row->doctor;
				$name = $row->name;
				$affiliation_department = $row->affiliation_department;

				$jsondata.=<<<JSON
{
	"doctor":{$doctor},
	"name":"{$name}",
	"affiliation_department":{$affiliation_department}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';
			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}

	// 의료진 이미지 가져오기 
	function web2303() {
		$db = $GLOBALS["db"];
		$doctor_pk = $_REQUEST["doctor_pk"];

		$sql=<<<SQL
SELECT `profile_image` FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$row = $res->fetch_object();
			$profile_image = $row->profile_image;

			if ($profile_image == '') {
				echo 'no';
			} else {
				echo $profile_image;
			}

		} else {
			echo 'no';
		}
	}

	// 의료진 상세정보 가져오기
	function web2401() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];

		$sql=<<<SQL
SELECT 
`name`,
`affiliation_department`,
`suhDepartments1`.`department` AS `affiliation_department_string`,
`profile_info`, 
`school_history`,
`career` 
FROM `suhDoctors` 
LEFT JOIN `suhDepartments` AS `suhDepartments1` 
ON `suhDoctors`.`affiliation_department`=`suhDepartments1`.`suhd`  
WHERE `doctor`={$pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$name = $row->name;
			$affiliation_department = $row->affiliation_department;
			$affiliation_department_string = $row->affiliation_department_string;
			$profile_info = preg_replace('/\r\n|\r|\n/',' ',$row->profile_info);
			$school_history = $row->school_history;
			$career = $row->career;


			$jsondata=<<<JSON
{
	"name":"{$name}",
	"affiliation_department":{$affiliation_department},
	"affiliation_department_string":"{$affiliation_department_string}",
	"profile_info":"{$profile_info}",
	"school_history":"{$school_history}",
	"career":"{$career}",
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}

	}

	// 의료진 이미지 가져오기
	function web2402() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];

		$sql=<<<SQL
SELECT 
`profile_image` 
FROM `suhDoctors`  
WHERE `doctor`={$pk}
SQL;

		$res = $db->query($sql);
		if ($res->num_rows >= 1) {

			$row = $res->fetch_object();
			$profile_image = $row->profile_image;

			if ($profile_image == '') {
				echo 'no';
			} else {
				echo $profile_image;
			}

		} else {
			echo 'no';
		}
	}

	// 오늘의 의학정보 - 랜덤으로 N의학정보 4개 가져오기
	function web4010() {
		$db = $GLOBALS["db"];

		$sql=<<<SQL
SELECT 
`suhnmi`, 
`title_ko`, 
`title_en`, 
`one_line_description` 
FROM `suhNMedicalInformation` 
ORDER BY rand() LIMIT 4
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$suhnmi = $row->suhnmi;
				$title_ko = setCleanText($row->title_ko, 'html');
				$title_en = setCleanText($row->title_en, 'html');
				$one_line_description = setCleanText($row->one_line_description, 'html');

				$jsondata.=<<<JSON
{
	"suhnmi":{$suhnmi},
	"title_ko":"{$title_ko}",
	"title_en":"{$title_en}",
	"one_line_description":"{$one_line_description}"
},
JSON;
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';	

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}

	// N의학정보 가져오기
	function web4015() {
		$db = $GLOBALS["db"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		
		$search_word = $_REQUEST["search_word"];
		$search_filter = $_REQUEST["search_filter"];

		$search_filter_array = array('','ㄱ','ㄴ','ㄷ','ㄹ','ㅁ','ㅂ','ㅅ','ㅇ','ㅈ','ㅊ','ㅋ','ㅌ','ㅍ','ㅎ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$search_filter_array2 = array('', '(ㄱ|ㄲ)', 'ㄴ', '(ㄷ|ㄸ)', 'ㄹ', 'ㅁ', 'ㅂ', '(ㅅ|ㅆ)', 'ㅇ', '(ㅈ|ㅉ)', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ');
		$search_filter_array3 = array('', '가', '나', '다', '라', '마', '바', '사', '아', '자', '차', '카', '타', '파', '하', 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');


		$start_index = $index * $view_num;



		$text1 = '';
		if ($search_word != '') {
			$text1=<<<TEXT
AND (`title_ko` LIKE '%{$search_word}%' OR `title_en` LIKE '%{$search_word}%')
TEXT;
		}

		$text2 = '';

		if ($search_filter == 0) {

		} else if ($search_filter >= 1 && $search_filter < 14) {
			$text2=<<<TEXT
AND (`title_ko` RLIKE '^{$search_filter_array2[$search_filter]}' OR ( `title_ko` >= '{$search_filter_array3[$search_filter]}' AND `title_ko` < '{$search_filter_array3[$search_filter+1]}' ))
TEXT;
		} else if ($search_filter == 14) {
			$text2=<<<TEXT
AND (`title_ko` RLIKE '^{$search_filter_array2[$search_filter]}' OR ( `title_ko` >= '{$search_filter_array3[$search_filter]}'))
TEXT;
		} else if ($search_filter >= 15 && $search_filter < 41) {
			$text2=<<<TEXT
AND (`title_ko` LIKE '{$search_filter_array[$search_filter]}%' OR `title_ko` LIKE '{$search_filter_array3[$search_filter]}%')
TEXT;
		} else {

		}


		



		$sql=<<<SQL
SELECT 
`suhnmi`, 
`title_ko`, 
`title_en`, 
`one_line_description`
FROM `suhNMedicalInformation` 
WHERE `status`=900400 
{$text1}
{$text2}
ORDER BY `title_ko` ASC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql;  exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhnmi = $row->suhnmi;
				$title_ko = setCleanText($row->title_ko, 'html');
				$title_en = setCleanText($row->title_en, 'html');
				$one_line_description = setCleanText($row->one_line_description, 'html');
				$description = setCleanText($one_line_description, 'html');
				

				$jsondata.=<<<JSON
{
	"suhnmi":{$suhnmi},
	"title_ko":"{$title_ko}",
	"title_en":"{$title_en}",
	"description":"{$description}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<10; $ii++) {
					if ($first_indexs % 10 == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;


				$sql=<<<SQL
SELECT `suhnmi` FROM `suhNMedicalInformation` 
WHERE `status`=900400 
{$text1} 
{$text2} 
ORDER BY `title_ko` ASC LIMIT {$first_indexs}, 101
SQL;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == 101) {
					$pagenum = 10;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/10);
					$nextpageflag = 0;
				}



				$sql=<<<SQL
SELECT `suhnmi` FROM `suhNMedicalInformation` 
WHERE `status`=900400 
{$text1} 
{$text2} 
ORDER BY `title_ko` ASC
SQL;
				$res = $db->query($sql);
				$all_num = $res->num_rows;


				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', "all_num":'.$all_num.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}

				
		
	}

	// N의학정보 - 페이지 맨 마지막 번호 알아내기
	function web4021() {
		$db = $GLOBALS["db"];
		$search_word = $_REQUEST["search_word"];
		$search_filter = $_REQUEST["search_filter"];

		$search_filter_array = array('','ㄱ','ㄴ','ㄷ','ㄹ','ㅁ','ㅂ','ㅅ','ㅇ','ㅈ','ㅊ','ㅋ','ㅌ','ㅍ','ㅎ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$search_filter_array2 = array('', '(ㄱ|ㄲ)', 'ㄴ', '(ㄷ|ㄸ)', 'ㄹ', 'ㅁ', 'ㅂ', '(ㅅ|ㅆ)', 'ㅇ', '(ㅈ|ㅉ)', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ');
		$search_filter_array3 = array('', '가', '나', '다', '라', '마', '바', '사', '아', '자', '차', '카', '타', '파', '하', 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');


		$start_index = $index * $view_num;



		$text1 = '';
		if ($search_word != '') {
			$text1=<<<TEXT
AND (`title_ko` LIKE '%{$search_word}%' OR `title_en` LIKE '%{$search_word}%')
TEXT;
		}

		$text2 = '';

		if ($search_filter == 0) {

		} else if ($search_filter >= 1 && $search_filter < 14) {
			$text2=<<<TEXT
AND (`title_ko` RLIKE '^{$search_filter_array2[$search_filter]}' OR ( `title_ko` >= '{$search_filter_array3[$search_filter]}' AND `title_ko` < '{$search_filter_array3[$search_filter+1]}' ))
TEXT;
		} else if ($search_filter == 14) {
			$text2=<<<TEXT
AND (`title_ko` RLIKE '^{$search_filter_array2[$search_filter]}' OR ( `title_ko` >= '{$search_filter_array3[$search_filter]}'))
TEXT;
		} else if ($search_filter >= 15 && $search_filter < 41) {
			$text2=<<<TEXT
AND (`title_ko` LIKE '{$search_filter_array[$search_filter]}%' OR `title_ko` LIKE '{$search_filter_array3[$search_filter]}%')
TEXT;
		} else {

		}



		$sql=<<<SQL
SELECT 
`suhnmi` 
FROM `suhNMedicalInformation` 
WHERE `status`=900400 
{$text1}
{$text2}
ORDER BY `title_ko` ASC 
SQL;
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / 10) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<10; $i++) {
			if ($first_index % 10 == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
	}

	// N의학정보 상세정보 가져오기
	function web4101() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];

		$sql=<<<SQL
SELECT 
`title_ko`, 
`title_en`, 
`one_line_description`, 
`chain_department`, 
`chain_body_simple`, 
`chain_symptom` 
FROM `suhNMedicalInformation`
WHERE `suhnmi`={$pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$title_ko = setCleanText($row->title_ko, 'html');
			$title_en = setCleanText($row->title_en, 'html');
			$one_line_description = setCleanText($row->one_line_description, 'html');
			$chain_department = setCleanText($row->chain_department, 'html');
			$chain_body_simple = setCleanText($row->chain_body_simple, 'html');
			$chain_symptom = setCleanText($row->chain_symptom, 'html');

			$sql=<<<SQL
SELECT 
`suhNMedicalInformationValue`.`value_type`, 
`suhCodes1`.`description` AS `value_type_string`,
`suhNMedicalInformationValue`.`value` 
FROM `suhNMedicalInformationValue` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhNMedicalInformationValue`.`value_type`=`suhCodes1`.`codeNumber`
WHERE `suhNMedicalInformationValue`.`suhnmi`={$pk}
SQL;
			$res = $db->query($sql);

			if ($res->num_rows >= 1) {
				$more_info_array = '[';
				while($row = $res->fetch_object()) {
					$value_type = $row->value_type;
					$value_type_string = $row->value_type_string;
					$value = setCleanText($row->value, 'html');

					$more_info_array.=<<<JSON
{
	"value_type":{$value_type},
	"value_type_string":"{$value_type_string}",
	"value":"{$value}"
},
JSON;
				}
				$more_info_array = substr($more_info_array, 0, -1);
				$more_info_array.=']';
			} else {
				$more_info_array = '[]';
			}

			$jsondata=<<<JSON
{
	"title_ko":"{$title_ko}",
	"title_en":"{$title_en}",
	"one_line_description":"{$one_line_description}",
	"chain_department":"{$chain_department}",
	"chain_body_simple":"{$chain_body_simple}",
	"chain_symptom":"{$chain_symptom}",
	"more_info_array":{$more_info_array},
	"result":"ok"
}
JSON;
			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}

		

	}

	// 고객의소리 - 감사합니다 가져오기
	function web5001() {
		$db = $GLOBALS["db"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		$board_type = $_REQUEST["board_type"];
		$content_type = $_REQUEST["content_type"];
		$search_word = $_REQUEST["search_word"];

		$text = '';
		if ($content_type != 0) {
			$text=<<<TEXT
AND `suhCustomerBoard`.`content_type`={$content_type}
TEXT;
		}

		$start_index = $index * $view_num;


		$texts = '';
		if ($search_word != '') {
			$texts=<<<TEXT
AND `suhCustomerBoard`.`title` LIKE '%{$search_word}%' 
TEXT;
		} 



		$sql=<<<SQL
SELECT 
`suhCustomerBoard`.`suhc`,
`suhCustomerBoard`.`board_type`,
`suhCustomerBoard`.`content_type`,
`suhCustomerBoard`.`title`,
`suhCustomerBoard`.`member`,
`suhMembers1`.`name` AS `member_name`, 
`suhCustomerBoard`.`datetime`,
`suhCustomerBoard`.`attach_file_flag`,
`suhCustomerBoard`.`view_index`,
`suhCustomerBoard`.`status`
FROM `suhCustomerBoard` 
LEFT JOIN `suhMembers` AS `suhMembers1`
ON `suhCustomerBoard`.`member`=`suhMembers1`.`suhmember`
WHERE `suhCustomerBoard`.`board_type`={$board_type} 
AND `suhCustomerBoard`.`status` != 900300
{$text}
{$texts}
ORDER BY `suhCustomerBoard`.`suhc` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhc = $row->suhc;
				$board_type = $row->board_type;
				$content_type = $row->content_type;
				$title = htmlspecialchars($row->title);
				$member = $row->member;
				$member_name = $row->member_name;
				$datetime = $row->datetime;
				$attach_file_flag = $row->attach_file_flag;
				$view_index = $row->view_index;
				$status = $row->status;

				$jsondata.=<<<JSON
{
	"suhc":{$suhc},
	"board_type":{$board_type},
	"content_type":{$content_type},
	"title":"{$title}",
	"member":{$member},
	"member_name":"{$member_name}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"status":{$status}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<10; $ii++) {
					if ($first_indexs % 10 == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;


				$sql=<<<SQL
SELECT `suhCustomerBoard`.`suhc` FROM `suhCustomerBoard` 
WHERE `suhCustomerBoard`.`board_type`={$board_type} 
{$texts}
ORDER BY `suhCustomerBoard`.`suhc` DESC LIMIT {$first_indexs}, 101
SQL;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == 101) {
					$pagenum = 10;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/10);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
	}

	// 고객의소리 - 감사합니다 페이징 - 페이지 맨 마지막 번호 알아내기
	function web5007() {
		$db = $GLOBALS["db"];
		$board_type = $_REQUEST["board_type"];

		$sql=<<<SQL
SELECT `suhc` FROM `suhCustomerBoard` 
WHERE `board_type`={$board_type} 
ORDER BY `suhc` DESC
SQL;
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / 10) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<10; $i++) {
			if ($first_index % 10 == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
	}


	// 고객의소리 - 건의합니다 가져오기
	function web5011() {
		$db = $GLOBALS["db"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		$board_type = $_REQUEST["board_type"];
		$content_type = $_REQUEST["content_type"];
		$search_word = $_REQUEST["search_word"];

		$text = '';
		if ($content_type != 0) {
			$text=<<<TEXT
AND `suhCustomerBoard`.`content_type`={$content_type}
TEXT;
		}

		$start_index = $index * $view_num;


		$texts = '';
		if ($search_word != '') {
			$texts=<<<TEXT
AND `suhCustomerBoard`.`title` LIKE '%{$search_word}%' 
TEXT;
		} 



		$sql=<<<SQL
SELECT 
`suhCustomerBoard`.`suhc`,
`suhCustomerBoard`.`board_type`,
`suhCustomerBoard`.`content_type`,
`suhCustomerBoard`.`title`,
`suhCustomerBoard`.`member`,
`suhMembers1`.`name` AS `member_name`, 
`suhCustomerBoard`.`datetime`,
`suhCustomerBoard`.`attach_file_flag`,
`suhCustomerBoard`.`view_index`,
`suhCustomerBoard`.`status`
FROM `suhCustomerBoard` 
LEFT JOIN `suhMembers` AS `suhMembers1`
ON `suhCustomerBoard`.`member`=`suhMembers1`.`suhmember`
WHERE `suhCustomerBoard`.`board_type`={$board_type} 
AND `suhCustomerBoard`.`status` != 900300
{$text}
{$texts}
ORDER BY `suhCustomerBoard`.`suhc` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhc = $row->suhc;
				$board_type = $row->board_type;
				$content_type = $row->content_type;
				$title = htmlspecialchars($row->title);
				$member = $row->member;
				$member_name = $row->member_name;
				$datetime = $row->datetime;
				$attach_file_flag = $row->attach_file_flag;
				$view_index = $row->view_index;
				$status = $row->status;

				$jsondata.=<<<JSON
{
	"suhc":{$suhc},
	"board_type":{$board_type},
	"content_type":{$content_type},
	"title":"{$title}",
	"member":{$member},
	"member_name":"{$member_name}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"status":{$status}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<10; $ii++) {
					if ($first_indexs % 10 == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;


				$sql=<<<SQL
SELECT `suhCustomerBoard`.`suhc` FROM `suhCustomerBoard` 
WHERE `suhCustomerBoard`.`board_type`={$board_type} 
{$texts}
ORDER BY `suhCustomerBoard`.`suhc` DESC LIMIT {$first_indexs}, 101
SQL;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == 101) {
					$pagenum = 10;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/10);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
	}

	// 고객의소리 - 건의합니다 페이징 - 페이지 맨 마지막 번호 알아내기
	function web5017() {
		$db = $GLOBALS["db"];
		$board_type = $_REQUEST["board_type"];

		$sql=<<<SQL
SELECT `suhc` FROM `suhCustomerBoard` 
WHERE `board_type`={$board_type} 
ORDER BY `suhc` DESC
SQL;
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / 10) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<10; $i++) {
			if ($first_index % 10 == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
	}

	// 내용 종류 리스트 가져오기
	function web5111() {
		$db = $GLOBALS["db"];
		$board_type = $_REQUEST["board_type"];

		if ($board_type == 800701) {
			// 감사합니다
			$sql=<<<SQL
SELECT `codeNumber`, `description` FROM `suhCodes` 
WHERE `codeNumber`>=800801 AND `codeNumber`<800900
SQL;
		} else if ($board_type == 800702) {
			// 건의합니다
			$sql=<<<SQL
SELECT `codeNumber`, `description` FROM `suhCodes` 
WHERE `codeNumber`>=800901 AND `codeNumber`<801000
SQL;
		} else {
			echo '{"result":"no"}';
			exit;
		}

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$codeNumber = $row->codeNumber;
				$description = $row->description;

				$jsondata.=<<<JSON
{
	"code_number":{$codeNumber},
	"description":"{$description}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
			exit;
		}
	}

	// 서버 DB에 고객의소리 내용 저장하기
	function web5121() {
		$db = $GLOBALS["db"];
		$member = $_REQUEST["member"];
		$board_type = $_REQUEST["board_type"];
		$content_type = $_REQUEST["content_type"];
		$title = addslashes($_REQUEST["title"]);
		$content = addslashes($_REQUEST["content"]);
		
		$file_ary = reArrayFiles($_FILES["attach_file"]);

		
		
		if ($file_ary[0]['size'] == 0) {
			$attach_file_flag = 0;
		} else {
			$attach_file_flag = 1;
		}
		// foreach ($file_ary as $file) {
	 //        // print 'File Name: ' . $file['name'];
	 //        // print 'File Type: ' . $file['type'];
	 //        // print 'File Size: ' . $file['size'];
	 //        echo 'file size = '.$file['size'];
	 //    }

		
		$sql=<<<SQL
INSERT INTO `suhCustomerBoard`
(`board_type`,`content_type`,`title`,`content`,`member`,`attach_file_flag`,`view_index`,`status`)VALUES
({$board_type}, {$content_type}, '{$title}', '{$content}', {$member}, {$attach_file_flag}, 0, 900400)
SQL;
		$res = $db->query($sql);

		
		if ($res) {
			$sql=<<<SQL
SELECT LAST_INSERT_ID() AS `recent_pk`
SQL;
			$res = $db->query($sql);
			$row = $res->fetch_object();
			$recent_pk = $row->recent_pk;

			

			// insert 성공 했을경우
			if ($attach_file_flag == 1) {
				echo '5 <br /><span class="dfsdfsdf"></span>';

				$upload_dir = './customer_board/customer'.$board_type.'_'.$recent_pk.'/';
				if (!is_dir($upload_dir)) {
					mkdir($upload_dir);
				}


				$aa = 0;
				for ($i=0; $i<count($file_ary); $i++) {

					if (file_exists($file_ary[$i]['tmp_name'])) {
						$target_file = $upload_dir.basename($file_ary[$i]["name"]);
						$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
					
					    if (move_uploaded_file($file_ary[$i]['tmp_name'], $target_file)) {
					        
					    } else {
					        
					    }
					}

					$aa++;
					if ($aa >= 200) {
						break;
					}
				}

		

				$text=<<<TEXT
<script>
	alert("등록 되었습니다.");
	location.href="./web5000.php";
</script>
TEXT;
				echo $text;
				exit;
			} else {
				$text=<<<TEXT
<script>
	alert("등록 되었습니다.");
	location.href="./web5000.php";
</script>
TEXT;
				echo $text;
				exit;
			}

		} else {
			// insert 실패 했을경우


			$text=<<<TEXT
<script>
	alert("등록중에 오류가 발생하였습니다. 지속적으로 이 오류가 발생한다면 문의주시길 바랍니다.");
	location.href="./web5000.php";
</script>
TEXT;
				echo $text;
				exit;
		}

	}

	function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}

	// 고객의소리 게시글 상세정보 가져오기
	function web5210() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];
		$newbee = $GLOBALS["newbee"];
		$origin = $GLOBALS["origin"];

		// 조회수 업
		$sql=<<<SQL
UPDATE `suhCustomerBoard` SET `view_index`=`view_index`+1 WHERE `suhc`={$pk}
SQL;
		$res = $db->query($sql);





		$sql=<<<SQL
SELECT 
`suhCustomerBoard`.`board_type`,
`suhCodes1`.`description` AS `board_type_string`,
`suhCustomerBoard`.`content_type`,
`suhCodes2`.`description` AS `content_type_string`,
`suhCustomerBoard`.`title`,
`suhCustomerBoard`.`content`,
`suhCustomerBoard`.`member`,
`suhMembers1`.`name` AS `member_name`,
`suhCustomerBoard`.`datetime`,
`suhCustomerBoard`.`attach_file_flag`,
`suhCustomerBoard`.`view_index`
FROM `suhCustomerBoard` 
LEFT JOIN `suhMembers` AS `suhMembers1`
ON `suhCustomerBoard`.`member`=`suhMembers1`.`suhmember`
LEFT JOIN `suhCodes` AS `suhCodes1`
ON `suhCustomerBoard`.`board_type`=`suhCodes1`.`codeNumber`
LEFT JOIN `suhCodes` AS `suhCodes2`
ON `suhCustomerBoard`.`content_type`=`suhCodes2`.`codeNumber`
WHERE `suhCustomerBoard`.`suhc`={$pk}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$board_type = $row->board_type;
			$board_type_string = $row->board_type_string;
			$content_type = $row->content_type;
			$content_type_string = $row->content_type_string;
			$title = htmlspecialchars($row->title);
			$content = preg_replace('/\r\n|\r|\n/','<br />', htmlspecialchars($row->content));
			$content = preg_replace('/[\t|\s{2,}]/','  ', $content);
			$member = $row->member;
			$member_name = $row->member_name;
			$datetime = $row->datetime;
			$attach_file_flag = $row->attach_file_flag;
			$view_index = $row->view_index;

			if ($attach_file_flag == 1) {
				// 폴더명 지정
				$dir = "./customer_board/customer".$board_type."_".$pk.'/';
				 
				// 핸들 획득
				$handle  = opendir($dir);
				 
				$files = array();
				 
				// 디렉터리에 포함된 파일을 저장한다.
				$iii = 0;
				while (false !== ($filename = readdir($handle))) {
				    if($filename == "." || $filename == ".."){
				        continue;
				    }
				 
				    // 파일인 경우만 목록에 추가한다.
				    if(is_file($dir . "/" . $filename)){
				        $files[] = mb_convert_encoding($filename,$newbee,$origin);
				    }
				    $iii++;
				    if ($iii >= 10) {
				    	break;
				    }
				}
				 
				// 핸들 해제 
				closedir($handle);
				 
				// 정렬, 역순으로 정렬하려면 rsort 사용
				sort($files);
			} else {
				$files = array();
			}


			$attach_file_array = '[';
			$oo = 0;
			for ($i=0; $i<count($files); $i++) {
				$oo++;
				$attach_file_array.=<<<JSON
{
	"filename":"{$files[$i]}"
},
JSON;
			}
			if ($oo >= 1) {
				$attach_file_array = substr($attach_file_array, 0, -1);
			}
			$attach_file_array.=']';





			// 이전글 검색
			$sql=<<<SQL
SELECT `suhc`, `title` FROM `suhCustomerBoard`
WHERE `suhc`<{$pk} AND `board_type`={$board_type} 
AND `status` != 900300 
ORDER BY `suhc` DESC LIMIT 1;
SQL;
			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$prev_pk = $row->suhc;
				$prev_title = htmlspecialchars($row->title);
			} else {
				$prev_pk = 0;
				$prev_title = '';
			}


			// 다음글 검색
			$sql=<<<SQL
SELECT `suhc`, `title` FROM `suhCustomerBoard`
WHERE `suhc`>{$pk} AND `board_type`={$board_type} 
AND `status` != 900300 
ORDER BY `suhc` ASC LIMIT 1;
SQL;
			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$next_pk = $row->suhc;
				$next_title = htmlspecialchars($row->title);
			} else {
				$next_pk = 0;
				$next_title = '';
			}





			$jsondata=<<<JSON
{
	"board_type":{$board_type},
	"board_type_string":"{$board_type_string}",
	"content_type":{$content_type},
	"content_type_string":"{$content_type_string}",
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"member":{$member},
	"member_name":"{$member_name}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"attach_file_array":{$attach_file_array},
	"prev_pk":{$prev_pk},
	"prev_title":"{$prev_title}",
	"next_pk":{$next_pk},
	"next_title":"{$next_title}",
	"result":"ok"	
}
JSON;

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}


	// 병원뉴스 최근 3개 가져오기
	function web6008() {
		$db = $GLOBALS["db"];
		$newbee = $GLOBALS["newbee"];
		$origin = $GLOBALS["origin"];

		$sql=<<<SQL
SELECT 
`suhNews`.`suhnews`, 
`suhNews`.`admin`, 
`suhAdmin1`.`name` AS `admin_name`, 
`suhNews`.`title`, 
`suhNews`.`content`, 
`suhNews`.`datetime`, 
`suhNews`.`attach_file_flag`, 
`suhNews`.`view_index` 
FROM `suhNews` 
LEFT JOIN `suhAdmin` AS `suhAdmin1` 
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin` 
WHERE `status`=900400 
ORDER BY `suhNews`.`suhnews` DESC 
LIMIT 0, 3
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhnews_pk = $row->suhnews;
				$admin = $row->admin;
				$admin_name = $row->admin_name;
				$title = htmlspecialchars($row->title);
				$content = preg_replace('/\r\n|\r|\n/',' ',htmlspecialchars($row->content));
				$datetime = $row->datetime;
				$attach_file_flag = $row->attach_file_flag;
				$view_index = $row->view_index;




				if ($attach_file_flag == 1) {
					// 폴더명 지정
					$dir = "./suhnews/suhnews".$suhnews_pk.'/';
					 
					// 핸들 획득
					$handle  = opendir($dir);
					 
					$files = array();
					 
					// 디렉터리에 포함된 파일을 저장한다.
					$iii = 0;
					while (false !== ($filename = readdir($handle))) {
					    if($filename == "." || $filename == ".."){
					        continue;
					    }
					 
					    // 파일인 경우만 목록에 추가한다.
					    if(is_file($dir . "/" . $filename)){
					        $files[] = mb_convert_encoding($filename,$newbee,$origin);
					    }
					    $iii++;
					    if ($iii >= 10) {
					    	break;
					    }
					}
					 
					// 핸들 해제 
					closedir($handle);
					 
					// 정렬, 역순으로 정렬하려면 rsort 사용
					sort($files);
				} else {
					$files = array();
				}

				$attach_file_array = '[';
				$oo = 0;
				for ($ik=0; $ik<count($files); $ik++) {
					$attach_file_array.=<<<JSON
{
"filename":"{$files[$ik]}"
},
JSON;
					$oo++;
				}
				if ($oo >= 1) {
					$attach_file_array = substr($attach_file_array, 0, -1);
				}
				$attach_file_array.=']';








				$jsondata.=<<<JSON
{
	"suhnews_pk":{$suhnews_pk},
	"admin":{$admin},
	"admin_name":"{$admin_name}",
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"attach_file_array":{$attach_file_array}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}

	}

	// 병원뉴스 리스트 가져오기
	function web6010() {
		$db = $GLOBALS["db"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		$search_word = $_REQUEST["search_word"];

		$start_index = $index * $view_num;

		$texts = '';
		if ($search_word != '') {
			$texts=<<<TEXT
AND `suhNews`.`title` LIKE '%{$search_word}%' 
TEXT;
		} 

		$sql=<<<SQL
SELECT 
`suhNews`.`suhnews`, 
`suhNews`.`admin`, 
`suhAdmin1`.`name` AS `admin_name`, 
`suhNews`.`title`, 
`suhNews`.`datetime`, 
`suhNews`.`attach_file_flag`, 
`suhNews`.`view_index` 
FROM `suhNews` 
LEFT JOIN `suhAdmin` AS `suhAdmin1` 
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin` 
WHERE `status`=900400 
{$texts}
ORDER BY `suhNews`.`suhnews` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhnews_pk = $row->suhnews;
				$admin = $row->admin;
				$admin_name = $row->admin_name;
				$title = htmlspecialchars($row->title);
				$datetime = htmlspecialchars($row->datetime);
				$attach_file_flag = $row->attach_file_flag;
				$view_index = $row->view_index;

				$jsondata.=<<<JSON
{
	"suhnews_pk":{$suhnews_pk},
	"admin":{$admin},
	"admin_name":"{$admin_name}",
	"title":"{$title}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<10; $ii++) {
					if ($first_indexs % 10 == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;


				$sql=<<<SQL
SELECT `suhnews` FROM `suhNews` 
WHERE `status`=900400 
{$texts} 
ORDER BY `suhnews` DESC 
LIMIT {$first_indexs}, 101
SQL;
				// echo $sql; exit;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == 101) {
					$pagenum = 10;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/10);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
	}
	
	// 페이지 맨 마지막 번호 알아내기
	function web6020() {
		$db = $GLOBALS["db"];

		$sql=<<<SQL
SELECT `suhnews` FROM `suhNews` ORDER BY `suhnews` DESC
SQL;
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / 10) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<10; $i++) {
			if ($first_index % 10 == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
	}

	// 병원뉴스 상세정보 가져오기
	function web6110() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];
		$newbee = $GLOBALS["newbee"];
		$origin = $GLOBALS["origin"];

		// 조회수 업
		$sql=<<<SQL
UPDATE `suhNews` SET `view_index`=`view_index`+1 WHERE `suhnews`={$pk}
SQL;
		$res = $db->query($sql);





		$sql=<<<SQL
SELECT 
`suhNews`.`admin`,
`suhAdmin1`.`name` AS `admin_name`,
`suhNews`.`title`,
`suhNews`.`content`,
`suhNews`.`datetime`,
`suhNews`.`attach_file_flag`,
`suhNews`.`view_index` 
FROM `suhNews`
LEFT JOIN `suhAdmin` AS `suhAdmin1`
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin`
WHERE `suhNews`.`suhnews`={$pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$admin_name = $row->admin_name;
			$title = htmlspecialchars($row->title);
			$content = preg_replace('/\r\n|\r|\n/','<br />',htmlspecialchars($row->content));
			$datetime = $row->datetime;
			$attach_file_flag = $row->attach_file_flag;
			$view_index = $row->view_index;

			if ($attach_file_flag == 1) {
				// 폴더명 지정
				$dir = "./suhnews/suhnews".$pk.'/';
				 
				// 핸들 획득
				$handle  = opendir($dir);
				 
				$files = array();
				 
				// 디렉터리에 포함된 파일을 저장한다.
				$iii = 0;
				while (false !== ($filename = readdir($handle))) {
				    if($filename == "." || $filename == ".."){
				        continue;
				    }
				 
				    // 파일인 경우만 목록에 추가한다.
				    if(is_file($dir . "/" . $filename)){
				        $files[] = mb_convert_encoding($filename,$newbee,$origin);
				    }
				    $iii++;
				    if ($iii >= 10) {
				    	break;
				    }
				}
				 
				// 핸들 해제 
				closedir($handle);
				 
				// 정렬, 역순으로 정렬하려면 rsort 사용
				sort($files);
			} else {
				$files = array();
			}


			$attach_file_array = '[';
			$oo = 0;
			for ($i=0; $i<count($files); $i++) {
				$oo++;
				$attach_file_array.=<<<JSON
{
	"filename":"{$files[$i]}"
},
JSON;
			}
			if ($oo >= 1) {
				$attach_file_array = substr($attach_file_array, 0, -1);
			}
			$attach_file_array.=']';





			// 이전글 검색
			$sql=<<<SQL
SELECT `suhnews`, `title` FROM `suhNews`
WHERE `suhnews`<{$pk} ORDER BY `suhnews` DESC LIMIT 1;
SQL;
			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$prev_pk = $row->suhnews;
				$prev_title = $row->title;
			} else {
				$prev_pk = 0;
				$prev_title = '';
			}


			// 다음글 검색
			$sql=<<<SQL
SELECT `suhnews`, `title` FROM `suhNews`
WHERE `suhnews`>{$pk} ORDER BY `suhnews` ASC LIMIT 1;
SQL;
			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$next_pk = $row->suhnews;
				$next_title = $row->title;
			} else {
				$next_pk = 0;
				$next_title = '';
			}





			$jsondata=<<<JSON
{
	"admin_name":"{$admin_name}",
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"attach_file_array":{$attach_file_array},
	"prev_pk":{$prev_pk},
	"prev_title":"{$prev_title}",
	"next_pk":{$next_pk},
	"next_title":"{$next_title}",
	"result":"ok"	
}
JSON;

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
	}


























	function test() {
		$suhnews_pk = 121;
		$newbee = 'UTF-8';
		$origin ='EUC-KR';


		// 폴더명 지정
		$dir = "./suhnews/suhnews".$suhnews_pk.'/';
		 
		// 핸들 획득
		$handle  = opendir($dir);
		 
		$files = array();
		 
		// 디렉터리에 포함된 파일을 저장한다.
		$iii = 0;
		while (false !== ($filename = readdir($handle))) {
		    if($filename == "." || $filename == ".."){
		        continue;
		    }
		 
		    // 파일인 경우만 목록에 추가한다.
		    if(is_file($dir . "/" . $filename)){
		        $files[] = mb_convert_encoding($filename,$newbee,$origin);
		    }
		    $iii++;
		    if ($iii >= 10) {
		    	break;
		    }
		}
		 
		// 핸들 해제 
		closedir($handle);
		 
		// 정렬, 역순으로 정렬하려면 rsort 사용
		sort($files);

		$attach_file_array = '[';
		$oo = 0;
		for ($ik=0; $ik<count($files); $ik++) {
			$attach_file_array.=<<<JSON
{
"filename":"{$files[$ik]}"
},
JSON;
			$oo++;
		}
		if ($oo >= 1) {
			$attach_file_array = substr($attach_file_array, 0, -1);
		}
		$attach_file_array.=']';
		echo $attach_file_array;
	}






	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }

    // html, android에 출력될 수 있도록 치환하기
	function setCleanText($str, $mode) {
		$return_str = '';
		switch ($mode) {
			case 'html':
				$return_str = htmlspecialchars($str);
				$return_str = preg_replace('/\t+/',' ', $return_str);
				$return_str = preg_replace('/\r\n|\r|\n/','<br />', $return_str);
				break;
			case 'textarea':
				$return_str = htmlspecialchars($str); 
				$return_str = preg_replace('/\t+/',' ', $return_str);
				$return_str = preg_replace('/\r\n|\r|\n/','\n', $return_str);
				break;
			case 'android':
				$return_str = addslashes($str);
				$return_str = preg_replace('/\t+/',' ', $return_str);
				$return_str = preg_replace('/\r\n|\r|\n/','\n', $return_str);
				break;
			default:
				$return_str = htmlspecialchars($str);
				$return_str = preg_replace('/\t+/',' ', $return_str);
				$return_str = preg_replace('/\r\n|\r|\n/','<br />', $return_str);
				break;
		}
		return $return_str;
	}
?>
