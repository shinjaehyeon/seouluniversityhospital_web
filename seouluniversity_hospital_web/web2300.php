<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 의료진 소개" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 의료진 소개</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web2300.js"></script>
	</head>
	<body id="body" page-code="web2300">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>



		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>



		<div class="sub_big_title_box web2300">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					의료진 소개
				</div>
				<div class="comment">
					서울대학교병원의 다양한 의료진을 소개합니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 진료과/의료진 > <a href="./web2300.php">의료진 소개</a>
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>








		<section class="search_area_type_a">
			<input type="text" name="search_value" placeholder="의료진 이름을 검색하세요." />
			<div class="search_button">

			</div>
		</section>





		<section class="list_area_type_a">
			<ul class="doctor_list_area web2300 clearFix">
				<!-- <li department-pk="800201">
					<div class="title">
						내과
					</div>
					<ul class="doctor_list clearFix">
						<li>	
							<a href="./web2400.php?pk=800201">		
								<div class="overlay_display">			
									상세정보 바로가기		
								</div>		
								<div class="doctor_photo" datas-pk="800201">

								</div>		
								<div class="doctor_name setTopVirtualBox">			
									<div>				
										최모준			
									</div>		
								</div>	
							</a>
						</li>

					</ul>
				</li> -->

			</ul>
		</section>






		
		


		<?php include "footer.php"; ?>
	</body>
</html>