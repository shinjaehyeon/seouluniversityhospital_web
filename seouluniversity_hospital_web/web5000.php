<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 고객의소리 게시판" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 고객의소리 게시판</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web5000.js"></script>
	</head>
	<body id="body" page-code="web5000">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>



		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>



		<div class="sub_big_title_box web5000">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					고객의소리 게시판
				</div>
				<div class="comment">
					감사합니다 및 건의합니다에 해당하는 글을 확인하고 남길 수 있습니다. 
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 고객참여 > 고객의소리 > <a href="./web5000.php">고객의소리 게시판</a>
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>



		<section class="web5000 tab_button_box clearFix">
			<div class="tab_btn_1 common active" tab-index="0">
				감사합니다
			</div>
			<div class="tab_btn_2 common" tab-index="1">
				건의합니다
			</div>
		</section>


		<section class="web5000 tab_line_box">
			<div class="box_1200">
				<div class="line tab_btn_1">

				</div>
			</div>
		</section>



		<section class="web5000 board_box">
			<div class="tab_1_board tab_board show">
				<ul>
					<li class="title clearFix">
						<div>
							번호
						</div> 
						<div>
							제목
						</div>
						<div>
							작성자
						</div>
						<div>
							작성일
						</div>
						<div>
							첨부파일
						</div>
						<div>
							조회수
						</div>
					</li>
					<!-- <li class="clearFix">
						<div>
							번호
						</div> 
						<div class="left">
							제목
						</div>
						<div>
							작성자
						</div>
						<div>
							작성일
						</div>
						<div class="attach">
							
						</div>
						<div>
							조회수
						</div>
					</li> -->
				</ul>

				<div class="search_write_box">
					<div class="search_box clearFix">
						<input type="text" name="web5000_tab1_search" />
						<div class="tab1_search_button">

						</div>
					</div>
					<div class="write_button">
						글쓰기
					</div>
				</div>

				<section class="tab_1_board board_paging_controller_box">
					<div class="clearFix">
						<div class="best_prev">
							<<
						</div>
						<div class="line">

						</div>
						<div class="prev">
							<
						</div>
						<ul class="page_num clearFix">
							<li class="active">
								1
							</li>
							<li>
								2
							</li>
						</ul>
						<div class="next">
							>
						</div>
						<div class="line">

						</div>
						<div class="best_next">
							>>
						</div>
					</div>
				</section>
			</div>
			<div class="tab_2_board tab_board">
				<ul>
					<li class="title clearFix">
						<div>
							번호
						</div> 
						<div>
							제목
						</div>
						<div>
							작성자
						</div>
						<div>
							작성일
						</div>
						<div>
							첨부파일
						</div>
						<div>
							조회수
						</div>
					</li>
					<li class="clearFix">
						<div>
							13
						</div> 
						<div class="left">
							건의합니다
						</div>
						<div>
							ssss
						</div>
						<div>
							2018-06-13
						</div>
						<div class="attach">
							
						</div>
						<div>
							조회수
						</div>
					</li>
				</ul>

				<div class="search_write_box">
					<div class="search_box clearFix">
						<input type="text" name="web5000_tab2_search" />
						<div class="tab2_search_button">

						</div>
					</div>
					<div class="write_button">
						글쓰기
					</div>
				</div>

				<section class="tab_2_board board_paging_controller_box">
					<div class="clearFix">
						<div class="best_prev">
							<<
						</div>
						<div class="line">

						</div>
						<div class="prev">
							<
						</div>
						<ul class="page_num clearFix">
							<li class="active">
								1
							</li>
							<li>
								2
							</li>
						</ul>
						<div class="next">
							>
						</div>
						<div class="line">

						</div>
						<div class="best_next">
							>>
						</div>
					</div>
				</section>
			</div>
		</section>








		<?php include "footer.php"; ?>
	</body>
</html>