<?php
	session_start();
?>
<header class="header">
	<div class="header_1200 setTopVirtualBox">

		<a href="./index.php" class="header_logo_box">
			<img src="./images/logo_white.png" alt="서울대학교병원 로고 이미지" title="서울대학교병원 로고 이미지" />
		</a>

		<ul class="header_small_menu_box clearFix">
			<li>
				<a href="#">원격</a>
			</li>
			<li>
				<?php
					if ($_SESSION['is_login'] == 'ok') {
						echo '<a href="#" class="logout_button">로그아웃</a>';
					} else {
						echo '<a href="./web1100.php">로그인</a>';
					}
				?>
				
			</li>
			<li>
				<?php
					if ($_SESSION['is_login'] == 'ok') {
						echo '<a href="./web1500.php">마이페이지</a>';
					} else {
						echo '<a href="./web1200.php">회원가입</a>';
					}
				?>
			</li>
			<li>
				<a href="#">채용안내</a>
			</li>
			<li>
				<a href="#">언어</a>
			</li>
		</ul>

	</div>
</header>