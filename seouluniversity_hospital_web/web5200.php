<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 고객의소리 게시판 상세정보" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 고객의소리 게시판 상세정보</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web5200.js"></script>
	</head>
	<body id="body" page-code="web5200">
		<input type="hidden" name="pk" value="<?php echo $_REQUEST['pk']; ?>" />

		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>



		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>



		<div class="sub_big_title_box web5200">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					고객의소리 게시판 상세정보
				</div>
				<div class="comment">
					고객의소리 게시판의 상세내용입니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 고객참여 > 고객의소리 > <a href="./web5000.php">고객의소리 게시판</a> > 상세정보
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>











		<section class="board_detail_page_type_A">
			<div class="title_box setTopVirtualBox">
				<div class="title" style="margin-top:14px;">
					<div class="board_type" style="font-size:14px;color:#999;">

					</div>
					<div class="board_title">

					</div>
				</div>
				<div class="infobox1 clearFix">
					<div class="common" style="margin-right:6px;">
						작성자 : 
					</div>
					<div class="common writer" style="margin-right:60px;">	
						
					</div>
					<div class="common" style="margin-right:6px;">
						조회수 : 
					</div>
					<div class="common view_index" style="margin-right:60px;">	
						
					</div>
					<div class="common" style="margin-right:6px;">
						작성일 : 
					</div>
					<div class="common datetime">
						
					</div>
				</div>
			</div>
			<div class="attach_file_box">
				<div class="title">
					첨부파일
				</div>
				<ul class="attach_file_list">
					<!-- <li>
						-
						<a href="#">
							2018 1분기 결산.hwp
						</a>
					</li> -->
				</ul>
				<div class="no_result">
					첨부파일이 없습니다.
				</div>
			</div>
			<div class="content_box">
				
			</div>
			<div class="prevnextletter">
				<div class="next_letter clearFix" style="margin-bottom:15px;">
					<div class="next_title">
						다음글
					</div>
					<div class="next_board">
						<a href="#">
							혁신적 간암 표지자 분석 기술 개발
						</a>
					</div>
				</div>	
				<div class="prev_letter clearFix">
					<div class="prev_title">
						이전글
					</div>
					<div class="prev_board">
						<a href="#">
							혁신적 간암 표지자 분석 기술 개발
						</a>
					</div>
				</div>	
			</div>
			<div class="list_page_link_button" onclick="location.href='./web5000.php';">
				목록으로
			</div>
		</section>











		<?php include "footer.php"; ?>
	</body>
</html>