<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 로그인" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 로그인</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web1100.js"></script>
		
	</head>
	<body id="body" page-code="web1100">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>



		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>



		<div class="sub_big_title_box web1100">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					로그인
				</div>
				<div class="comment">
					로그인 후에 서울대학교병원의 더 다양한 서비스를 이용하실 수 있습니다. 
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					홈 > 로그인
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>







		<section class="basic_login_form_box web1100 clearFix">
			<div class="left">
				<div class="intro_image">
					
				</div>
			</div>
			<div class="right">
				<form class="setTopVirtualBox">
					<ul class="id_pw_box">
						<li class="clearFix" style="margin-bottom:10px;">
							<div class="title">
								아이디
							</div>
							<div>	
								<input type="text" name="id" />
							</div>
						</li>
						<li class="clearFix">
							<div class="title">
								비밀번호
							</div>
							<div>	
								<input type="password" name="pw" />
							</div>
						</li>
					</ul>
					<div class="group11 clearFix" style="margin-bottom:30px;">
						<div class="id_pw_find_box clearFix">
							<a href="#">
								아이디/비밀번호 찾기
							</a>
						</div>
						<div class="join_login_box clearFix">
							<ul class="clearFix button_box">
								<li class="join" onclick="location.href='./web1200.php';">
									회원가입
								</li>
								<li class="login">
									로그인
								</li>
							</ul>
						</div>
					</div>
				</form>
				<section class="sns_login_button_box">
					<ul class="clearFix">
						<li class="facebook">
							<div style="margin-left:70px;">
								페이스북으로 로그인
							</div>
						</li>
						<li class="kakao">
							<div style="margin-left:88px;">
								카카오로 로그인
							</div>
						</li>
						<li class="naver">
							<div style="margin-left:88px;">
								네이버로 로그인
							</div>
						</li>
					</ul>
				</section>	
			</div>
		</section>






		<?php include "footer.php"; ?>
	</body>
</html>