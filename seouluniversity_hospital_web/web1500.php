<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 마이페이지" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 마이페이지</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web1500.js"></script>
	</head>
	<body id="body" page-code="web1500">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>



		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>




		<input type="hidden" name="member_pk" value="<?php echo $_SESSION['user_primarykey']; ?>" />


		<?php
			if ($_SESSION['is_login'] == '') {
				$text=<<<TEXT
<script>
	alert("잘못된 접근입니다.");
	history.back(1);
</script>
TEXT;
				echo $text;
			}
		?>



		<div class="sub_big_title_box web1500">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					마이페이지
				</div>
				<div class="comment">
					개인정보 수정 및 진료예약 등 각종 내역 확인
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					홈 > 마이페이지
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>








		<section class="web1500 profile_info_box setTopVirtualBox">
			<div class="name_box clearFix">
				<img class="human_icon" src="./images/human_icon.png" alt="" title="" />
				<div class="name">
					<?php echo $_SESSION['user_name']; ?>
				</div>
				<div class="welcome_text">
					님 환영합니다.
				</div>
			</div>

			<div class="member_out_button">
				회원탈퇴
			</div>
		</section>






		<section class="web1500 tab_button_box">
			<ul class="clearFix">
				<li class="active">
					예약확인/취소
				</li>
				<li>
					개인정보수정
				</li>
				<li>
					진료회원승인/취소
				</li>
				<li>
					증명서발급내역
				</li>
				<li>
					상담내역
				</li>
				<li>
					즐겨찾기
				</li>
			</ul>
		</section>

		<section class="web1500 tab_line_box">
			<div class="tab_line_box_1200">
				<div class="line">

				</div>
			</div>
		</section>





		<section class="web1500 tab_content_box">




			<!-- tab1 내용 -->
			<section class="tab1_content tabcontent setTopVirtualBox show">
				<div class="title">
					진료예약내역
				</div>
				<div class="crhlist_box">
					<ul>
						<li class="title clearFix">
							<div class="col">
								진료예정일
							</div>
							<div class="col">
								진료과
							</div>
							<div class="col">
								담당의료진
							</div>
							<div class="col">
								상태
							</div>
						</li>
						<li class="clearFix">
							<div class="col">
								2018년 05월 23일 / 13시 30분
							</div>
							<div class="col">
								호흡기내과
							</div>
							<div class="col">
								김동길
							</div>
							<div class="col">
								<span class="reservation">
									예약됨
								</span>
								<span class="empty_box" style="width:10px;">

								</span>
								<span class="cancel">
									취소하기
								</span>
							</div>
						</li>
					</ul>
				</div>
				<section class="allboard_paging_controller_box">
					<div class="clearFix">
						<div class="best_prev">
							<<
						</div>
						<div class="line prev">

						</div>
						<div class="prev">
							<
						</div>
						<ul class="page_num clearFix">
							
						</ul>
						<div class="next">
							>
						</div>
						<div class="line next">

						</div>
						<div class="best_next">
							>>
						</div>
					</div>
				</section>
				<section class="warning">
					<div>
						<img src="./images/warning_icon.png" alt="주의 아이콘" title="주의 아이콘" />
						<div>
							주의하세요!
						</div>
					</div>
					<div>
						진료예약취소는 진료일 이전(자정)까지 가능합니다. (수납기록 및 검사예약이 없는 진료만 변경/취소가 가능)<br />
						특정 진료과(정신건강의학과, 방사선종양학과, 영상의학과, 마취통증의학과)의 예약이 조회되지 않을 수 있습니다.<br />
						예약이 조회되지 않을 경우에는 예약센터(1588-5700)나 해당 진료과로 문의해 주십시오.
					</div>
				</section>
			</section>







			<!-- tab2 내용 -->
			<section class="tab2_content tabcontent setTopVirtualBox">
				<div class="title">
					개인정보수정
				</div>


				<div class="editStyleC">
					<ul>
						<li class="clearFix">
							<div class="column_title setTopVirtualBox">
								<div>
									이름
								</div>
							</div>
							<div class="column_content setTopVirtualBox">
								<div class="name not_edit_text">
									<?php echo $_SESSION['user_name']; ?>
								</div>
							</div>
						</li>
						<li class="clearFix">
							<div class="column_title setTopVirtualBox">
								<div>
									성별
								</div>
							</div>
							<div class="column_content setTopVirtualBox">
								<select name="gender">
									<option value="900900">미지정</option>
									<option value="800501">남자</option>
									<option value="800502">여자</option>
								</select>
							</div>
						</li>
						<li class="clearFix">
							<div class="column_title setTopVirtualBox">
								<div>
									휴대폰 통신사
								</div>
							</div>
							<div class="column_content setTopVirtualBox">
								<select name="mobileCompany">
									<option value="900900">미지정</option>
									<option value="700101">LG U+</option>
									<option value="700102">SKT</option>
									<option value="700103">KT</option>
									<option value="700104">LG U+ 알뜰폰</option>
									<option value="700105">SKT 알뜰폰</option>
									<option value="700106">KT 알뜰폰</option>
								</select>
							</div>
						</li>
						<li class="clearFix">
							<div class="column_title setTopVirtualBox">
								<div>
									휴대폰 번호
								</div>
							</div>
							<div class="column_content setTopVirtualBox">
								<input type="text" name="phoneNumber" style="width:200px;" />
							</div>
						</li>
						<li class="clearFix">
							<div class="column_title setTopVirtualBox">
								<div>
									생일
								</div>
							</div>
							<div class="column_content setTopVirtualBox">
								<div class="gender">
									<input type="date" name="birthday" />
								</div>
							</div>
						</li>
						<li class="clearFix">
							<div class="column_title setTopVirtualBox">
								<div>
									혈액형
								</div>
							</div>
							<div class="column_content setTopVirtualBox">
								<select name="bloodtype">
									<option value="900900">미지정</option>
									<option value="600101">O형</option>
									<option value="600102">A형</option>
									<option value="600103">B형</option>
									<option value="600104">AB형</option>
								</select>
							</div>
						</li>
						<li class="clearFix">
							<div class="column_title setTopVirtualBox">
								<div>
									주소
								</div>
							</div>
							<div class="column_content setTopVirtualBox">
								<textarea name="addr" style="width:460px;height:120px;"></textarea>
							</div>
						</li>
					</ul>
					<div class="edit_button">
						수정하기
					</div>
				</div>


			</section>




			<!-- tab3 내용 -->
			<section class="tab3_content tabcontent setTopVirtualBox" style="height:400px;">
				<!-- <div class="title">
					개인정보수정
				</div> -->
			</section>



			<!-- tab4 내용 -->
			<section class="tab4_content tabcontent setTopVirtualBox" style="height:400px;">
				<!-- <div class="title">
					개인정보수정
				</div> -->
			</section>



			<!-- tab5 내용 -->
			<section class="tab5_content tabcontent setTopVirtualBox" style="height:400px;">
				<!-- <div class="title">
					개인정보수정
				</div> -->
			</section>



			<!-- tab6 내용 -->
			<section class="tab6_content tabcontent setTopVirtualBox" style="height:400px;">
				<!-- <div class="title">
					개인정보수정
				</div> -->
			</section>




		</section>





		<?php include "footer.php"; ?>
	</body>
</html>