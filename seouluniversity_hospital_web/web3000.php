<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 오시는길" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 오시는길</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web3000.js"></script>
	</head>
	<body id="body" page-code="web3000">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>


		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>




		<div class="sub_big_title_box web3000">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					오시는길
				</div>
				<div class="comment">
					서울대학교병원으로 오시는길을 알려드립니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 이용안내 > <a href="./web3000.php">오시는길</a>
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>



		<section class="minimap_box">
			<img src="./images/web3000_come_road_img.jpg" alt="오시는길 이미지" title="오시는길 이미지" />
		</section>



		<section class="come_road_case_list_box">

			<div class="route_group">
				<div class="title">
					지하철
				</div>
				<ul class="list">
					<li class="clearFix">
						<div class="left common">
							혜화(서울대학교병원)역
						</div>
						<div class="right common">
							4호선 혜화(서울대학교병원)역 3번 출구 (암병원까지 도보 8~10분 거리)
						</div>
					</li>
				</ul>
			</div>

			<div class="route_group">
				<div class="title">
					버스
				</div>
				<ul class="list">
					<li class="clearFix">
						<div class="left common">
							혜화역, 서울대학교병원입구 3번 출구 앞 하행 [01-221]
						</div>
						<div class="right common">
							간선 : 109, 273, 601, N16<br />
							지선 : 2112
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							혜화역, 마로니에 공원 앞 상행 [01-220]
						</div>
						<div class="right common">
							간선 : 100, 102, 104, 106, 107, 108, 109, 140, 143, 150, 160, 162, 273, 301, 710, N16<br />
							지선 : 2112
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							창경궁 입구 상행 [01-224]
						</div>
						<div class="right common">
							간선 : 151, 171, 172, 272, 601
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							창경궁 서울대학교병원 하행 [01-002]
						</div>
						<div class="right common">
							간선 : 100, 102, 104, 106, 107, 108, 140,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 143, 150, 151, 160, 162, 171, 172, 273, 301, 710<br />
							공항 : 6011
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							창경궁, 고궁호텔, 메이플레이스호텔 정류장 [01-733]
						</div>
						<div class="right common">
							공항 : 2112, 종로07, 종로08 [01585]
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							마을버스 - 종로12 [서울대학교병원 주변 버스정류장]
						</div>
						<div class="right common">
							주요정차역 : 서울대학교병원본관 → 원남로터리 → 종로5가입구 → 종로4가 → <br />
							종로3가역(9번출구) → 돈화문 → 원남로터리 → 서울대학교치과대학 → 서울대장례식장 → <br />
							혜화동로터리 → 혜화(서울대학교병원)역3번출구 → 서울대학교병원본관
						</div>
					</li>
				</ul>
			</div>

			<div class="route_group">
				<div class="title">
					고속버스
				</div>
				<ul class="list">
					<li class="clearFix">
						<div class="left common">
							강남터미널
						</div>
						<div class="right common">
							③고속터미널역 지하철 3호선 → ④충무로역(4호선으로 환승) →<br />
							혜화(서울대학교병원)역 3번 출구
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							남부터미널
						</div>
						<div class="right common">
							③남부터미널역 지하철 3호선 → ④충무로역(4호선으로 환승) →<br />
							혜화(서울대학교병원)역 3번 출구
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							동서울터미널
						</div>
						<div class="right common">
							②강변역 지하철 2호선 → ④동대문역사공원역(4호선으로 환승) →<br />
							혜화(서울대학교병원)역 3번 출구
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							상봉터미널
						</div>
						<div class="right common">
							⑦상봉역 지하철 7호선 → ⑤군자역(5호선으로 환승) →<br />
							④동대문운동장역(4호선으로 환승) → 혜화(서울대학교병원)역 3번 출구
						</div>
					</li>
				</ul>
			</div>

			<div class="route_group">
				<div class="title">
					고속도로
				</div>
				<ul class="list">
					<li class="clearFix">
						<div class="left common">
							네비게이션 주소
						</div>
						<div class="right common">
							서울시 종로구 대학로 101 (서울시 종로구 연건동 28)
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							경부선
						</div>
						<div class="right common">
							서울요금소 → 한남대교 → 을지로 → 이화사거리 → 대학로 → 서울대학교병원
						</div>
					</li>
				</ul>
			</div>

			<div class="route_group">
				<div class="title">
					기차/공항
				</div>
				<ul class="list">
					<li class="clearFix">
						<div class="left common">
							서울역
						</div>
						<div class="right common">
							④서울역 지하철 4호선 → 혜화(서울대학교병원)역 3번 출구
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							용산역
						</div>
						<div class="right common">
							④신용산역 지하철 4호선 → 혜화(서울대학교병원)역 3번 출구
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							김포공항
						</div>
						<div class="right common">
							⑤김포공항역 지하철 5호선 → ④동대문역사공원역(4호선으로 환승) →<br />
							혜화(서울대학교병원)역 3번 출구
						</div>
					</li>
					<li class="clearFix">
						<div class="left common">
							인천공항
						</div>
						<div class="right common">
							[5B 12] 6011 리무진 버스 → 성대입구하차, 종로12 마을버스 → 본관앞 하차
						</div>
					</li>
				</ul>
			</div>

		</section>



		<?php include "footer.php"; ?>

	</body>
</html>