<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 진료과 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin9100.js"></script>
	</head>
	<body page-code="admin9100" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 진료과 관리
        </div>


        

       


        


        <div class="formStlyeE admin9100">
            <form name="departmentAddForm" action="./outlet.php" enctype="multipart/form-data" method="post">
                <input type="hidden" name="act" value="admin9110" />
                <ul>
                    <li class="clearFix">
                        <div class="title">
                            진료과 코드
                        </div>
                        <div class="content">
                            <input type="number" name="departmentCode" class="input_box_style department" style="width:160px;height:40px;" step="1" placeholder="800101~800199" maxlength="6" max="800199" min="800101" />
                        </div>
                        <div class="overlap_check_button">
                            중복확인
                        </div>
                        <div class="member_index_display">
                            
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            진료과 명칭
                        </div>
                        <div class="content">
                            <input type="text" name="departmentTitle" class="input_box_style" style="width:600px;height:40px;" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            진료과 사진
                        </div>
                        <div class="content">
                            <div>
                                <input type="file" name="departmentImage" class="input_box_style" />
                            </div>
                            <div class="imageDisplayView">
                                <img class="department_preview" src="" alt="미리보기 이미지" title="미리보기 이미지" />
                            </div>
                        </div>
                    </li>
                    
                    <li class="clearFix">
                        <div class="title">
                            진료과 소개글 
                        </div>
                        <div class="content">
                            <textarea name="departmentContent" class="input_box_style" style="width:456px;height:400px;padding:10px;"></textarea>
                        </div>
                    </li>
                </ul>
                <div class="department_upload_button">
                    진료과 추가
                </div>
            </form>
        </div>









		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>