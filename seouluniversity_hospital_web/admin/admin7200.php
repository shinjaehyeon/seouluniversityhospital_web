<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 병원뉴스 상세페이지</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin7200.js"></script>
        <style>
            .button_area {
                margin:20px;
                padding:0px;
                position:relative;
                display:block;
            }
            .button_area a {
                display:block;
                text-decoration:none;
                text-align:center;
                width:100px;
                height:50px;
                line-height:50px;
                color:#fff;
                background-color:#008ace;
            }
        </style>
	</head>
	<body page-code="admin7200" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />
        <input type="hidden" name="pk" value="<?php echo $_REQUEST['pk']; ?>" />





        <div class="big_title">
            ■ 병원뉴스 관리
        </div>


        

       

        <div class="viewStyleD">
            <ul>
                <li class="clearFix">
                    <div class="title">
                        <!-- 제목 -->
                    </div>
                </li>
                <li class="clearFix">
                    <div class="writer">
                        <!-- 작성자 : 홍길동(admin1) -->
                    </div>
                    <div class="datetime">
                        <!-- 작성일 : 2017-08-01 16:44:31 -->
                    </div>
                    <div class="view_index">
                        <!-- 조회수 : 11 -->
                    </div>
                </li>
                <li class="clearFix">
                    <ul class="file_list">
                        
                    </ul>
                </li>
                <li style="padding:40px 0px;">
                    <div class="content">
                        <!-- 내용 -->
                    </div>
                </li>
            </ul>
        </div>








        <div class="button_area admin7200">
            <a href="./admin7300.php?pk=<?php echo $_REQUEST['pk']; ?>">
                수정
            </a>
        </div>

        

        







		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>