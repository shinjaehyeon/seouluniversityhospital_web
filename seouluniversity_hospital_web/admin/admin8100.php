<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 1:1문의 관리 상세내용</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin8100.js"></script>
	</head>
	<body page-code="admin8100" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />

        <input type="hidden" name="question_pk" value="<?php echo $_REQUEST['pk']; ?>" />




        



        <div class="viewStyleD admin8100">
            <ul>
                <li class="clearFix">
                    <div class="question_type">
                        <!-- 약 복용/처방 -->
                    </div>
                </li>
                <li class="clearFix">
                    <div class="writer">
                        <!-- 문의자 : 홍길동(admin1) -->
                    </div>
                    <div class="datetime">
                        <!-- 작성일 : 2017-08-01 16:44:31 -->
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        <!-- 서울대학교병원에 어떻게 가나요? -->
                    </div>
                </li>
                <li style="padding:40px 0px;">
                    <div class="content">
                        <!-- 내용 -->
                    </div>
                </li>
            </ul>
        </div>








        <div class="answer_view_box">
            <div class="box_title">
                <답변>
            </div>
            <div class="answer_title">
                <!-- 해당 문의에 대한 답변입니다.  -->
            </div>
            <div class="answer_datetime">
                <!-- 2018-07-11 13:01:15 -->
            </div>
            <div class="answer_content">
                <!-- 안녕하세요. 고객님 이번에 문의 주신 내용은  -->
            </div>
        </div>









        <div class="answer_input_box">
            <div class="box_title">
                <답변>
            </div>
            <div class="box_content">
                <div>
                    <input type="text=" name="answerTitle" placeholder="제목을 입력해주세요." />
                </div>
                <div style="margin-top:10px;">
                    <textarea name="answerContent" placeholder="내용을 입력해주세요."></textarea>
                </div>
            </div>
            <div class="answer_upload_button">
                답변 등록
            </div>
        </div>

        

       


      










      




		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>