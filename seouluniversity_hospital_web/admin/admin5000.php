<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 회원 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin5000.js"></script>
	</head>
	<body page-code="admin5000" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 회원 관리
        </div>


        



        <div class="searchFilterTypeA">
            <ul>
                <li class="clearFix">
                    <div class="title">
                        성별
                    </div>
                    <div>
                        <input class="searchMemberGenderInput" id="genderAll" type="radio" name="searchMemberGender" value="0" checked="checked" />
                        <label class="searchMemberGenderInputLabel" for="genderAll">전체</label>
                        <input class="searchMemberGenderInput" id="genderMan" type="radio" name="searchMemberGender" value="800501" />
                        <label class="searchMemberGenderInputLabel" for="genderMan">남자</label>
                        <input class="searchMemberGenderInput" id="genderWoman" type="radio" name="searchMemberGender" value="800502" />
                        <label class="searchMemberGenderInputLabel" for="genderWoman">여자</label>
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        생년월일
                    </div>
                    <div>
                        <input id="birthdayAll" class="searchMemberBirthdayUse" type="checkbox" name="searchMemberBirthdayUse" checked="checked" />
                        <label for="birthdayAll" class="searchMemberBirthdayUseLabel">
                            전체
                        </label>
                        <input class="searchMemberBirthdayInput lock" type="date" name="searchMemberBirthday" value="0" readonly="readonly" />
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        혈액형
                    </div>
                    <div>
                        <select class="searchMemberBloodtypeSelect" name="searchMemberBloodtype">
                            <option value="0">전체</option>
                            <option value="600101">O형</option>
                            <option value="600102">A형</option>
                            <option value="600103">B형</option>
                            <option value="600104">AB형</option>
                        </select>
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        이름
                    </div>
                    <div>
                        <input class="searchMemberNameInput" type="text" name="searchMemberName" value="" />
                    </div>
                </li>
            </ul>
            <div class="finalSearchButton">
                검색
            </div>
        </div>






        <div class="member_total_num">
            
        </div>




        <div class="boardTypeB">
            <div class="boardTitle">
                <ul class="clearFix" style="margin-right:17px;">
                    <li class="col1">
                        고유번호
                    </li>
                    <li class="col2">
                        이름
                    </li>
                    <li class="col3">
                        성별
                    </li>
                    <li class="col4">
                        생일
                    </li>
                    <li class="col5">
                        혈액형
                    </li>
                    <li class="col6">
                        상태
                    </li>
                    <li class="col7">

                    </li>
                </ul>
            </div>
            <div class="memberListBox">
                <ul>
                    <!-- <li class="clearFix">
                        <div class="col1 member_pk">
                            13
                        </div>
                        <div class="col2 mamber_name">
                            <input class="memberNameInput" type="text" name="memberName" value="신재현" />
                        </div>
                        <div class="col3 member_gender">
                            <select class="memberGenderSelect" name="memberGender">
                                <option value="800501">남자</option>
                                <option value="800502">여자</option>
                            </select>
                        </div>
                        <div class="col4 member_birthday">
                            <input class="memberBirthdayInput" type="date" name="memberBirthday" value="1994-10-26" />
                        </div>
                        <div class="col5 member_joinMethod">
                            <select class="memberBloodtypeSelect" name="memberBloodtype">
                                <option value="600101">O형</option>
                                <option value="600102">A형</option>
                                <option value="600103">B형</option>
                                <option value="600104">AB형</option>
                            </select>
                        </div>
                        <div class="col6 member_status">
                            <select class="memberStatusSelect" name="memberStatus">
                                <option value="500101">일반상태</option>
                                <option value="500102">탈퇴상태</option>
                                <option value="500103">휴먼상태</option>
                                <option value="500104">정지상태</option>
                            </select>
                        </div>
                        <div class="col7 member_edit_button_area">
                            <div class="member_edit_button">
                                수정
                            </div>
                            <div class="member_detail_button">
                                상세
                            </div>
                        </div>
                    </li> -->
                    
                </ul>
            </div>
        </div>


        <div class="pagingControllerBox">
            <div class="clearFix">
                <div class="best_prev">
                    <<
                </div>
                <div class="prev">
                    <
                </div>
                <ul class="page_num clearFix">
                    <li class="active">
                        1
                    </li>
                    <li>
                        2
                    </li>
                </ul>
                <div class="next">
                    >
                </div>
                <div class="linenext">

                </div>
                <div class="best_next">
                    >>
                </div>
            </div>
        </div>



        







		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>