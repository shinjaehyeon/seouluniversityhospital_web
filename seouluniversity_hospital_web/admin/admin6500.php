<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - N의학정보 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin6500.js"></script>
	</head>
	<body page-code="admin6500" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ N의학정보 관리
        </div>


        

       


        <div class="searchFilterTypeA admin6500">
            <ul>
                <li class="clearFix">
                    <div class="title">
                        의학정보명
                    </div>
                    <div>
                        <select name="titleSearchType" class="inputstyle" style="width:100px;height:30px;margin-top:5px;margin-right:5px;padding:0px 5px;float:left;">
                            <option value="1">한글 명칭</option>
                            <option value="2">영어 명칭</option>
                        </select>
                        <input type="text" class="inputstyle" name="title" style="width:300px;height:30px;margin-top:5px;padding:0px 5px;float:left;" />
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        첫 시작 초성
                    </div>
                    <div>
                        <select name="firstWord" class="selectStyle questionType">
                            <option value="0" class="default">전체</option>
                            <option value="1">ㄱ</option>
                            <option value="2">ㄴ</option>
                            <option value="3">ㄷ</option>
                            <option value="4">ㄹ</option>
                            <option value="5">ㅁ</option>
                            <option value="6">ㅂ</option>
                            <option value="7">ㅅ</option>
                            <option value="8">ㅇ</option>
                            <option value="9">ㅈ</option>
                            <option value="10">ㅊ</option>
                            <option value="11">ㅋ</option>
                            <option value="12">ㅊ</option>
                            <option value="13">ㅍ</option>
                            <option value="14">ㅎ</option>
                            <option value="15">A</option>
                            <option value="16">B</option>
                            <option value="17">C</option>
                            <option value="18">D</option>
                            <option value="19">E</option>
                            <option value="20">F</option>
                            <option value="21">G</option>
                            <option value="22">H</option>
                            <option value="23">I</option>
                            <option value="24">J</option>
                            <option value="25">K</option>
                            <option value="26">L</option>
                            <option value="27">M</option>
                            <option value="28">N</option>
                            <option value="29">O</option>
                            <option value="30">P</option>
                            <option value="31">Q</option>
                            <option value="32">R</option>
                            <option value="33">S</option>
                            <option value="34">T</option>
                            <option value="35">U</option>
                            <option value="36">V</option>
                            <option value="37">W</option>
                            <option value="38">X</option>
                            <option value="39">Y</option>
                            <option value="40">Z</option>
                        </select>
                    </div>
                </li>
            </ul>
            <div class="finalSearchButton">
                검색
            </div>
        </div>




        <div class="delete_mode_comment_box">
            <div class="delete_mode_comment">
                삭제 모드입니다. 삭제할 리스트를 선택해주세요.
            </div>
        </div>




        <div class="group3 clearFix">
            <a href="./admin6600.php" class="wirte_button" style="width:170px;float:left;">
                N의학정보 업로드
            </a>
            <div class="delete_mode" style="float:right;">
                삭제모드 열기
            </div>
            <div class="all_select" data-status="0" style="float:right;">
                전체선택
            </div>
            <div class="selected_list_delete_button" style="float:right;">
                선택된 리스트 삭제
            </div>
        </div>





        <div class="listTypeD admin6500">
            <ul>
                <!-- <li data-pk="13" class="setTopVirtualBox">
                    <div class="title clearFix">
                        <div class="pk">
                            132
                        </div>
                        <div class="real_title">
                            ABO 신생아용혈성질환 [ABO hemolytic disease of the newborn]
                        </div>
                    </div>
                    <div class="one_description">
                        태반을 통과한 산모의 ABO 혈액형 항체가 태아나 신생아의 적혈구 항원과 결합하여 적혈구가 파괴되는 질환
                    </div>
                </li> -->
            </ul>
        </div>




        <div class="pagingControllerBox">
            <div class="clearFix">
                <div class="best_prev">
                    <<
                </div>
                <div class="prev">
                    <
                </div>
                <ul class="page_num clearFix">
                    <li class="active">
                        1
                    </li>
                    <li>
                        2
                    </li>
                </ul>
                <div class="next">
                    >
                </div>
                <div class="linenext">

                </div>
                <div class="best_next">
                    >>
                </div>
            </div>
        </div>





		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>