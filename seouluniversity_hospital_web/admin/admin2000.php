<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 대시보드</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin2000.js"></script>
	</head>
	<body page-code="admin2000" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />




        <div class="big_title">
            ■ 대시보드
        </div>




		<div class="admin2000 admin_profile_info clearFix">

			<div class="name_connect_log">
				<div class="name_box clearFix">
					<div class="only_name">
						<?php
							echo $_SESSION['admin_name'];
						?>
					</div>
					<div class="welcome">
						님 환영합니다.
					</div>
				</div>

				<div class="connect_log_box clearFix">
					<div class="title">
						- 이전 접속 기록 : &nbsp;
					</div>
					<div class="content">
						<?php
							if ($_SESSION['recent_login_exist'] == 'ok') {

								$year = dateDivide($_SESSION['recent_login_datetime'], 'yyyy');
								$month = dateDivide($_SESSION['recent_login_datetime'], 'mm');
								$date = dateDivide($_SESSION['recent_login_datetime'], 'dd');
								$hour = dateDivide($_SESSION['recent_login_datetime'], 'hh');
								$minute = dateDivide($_SESSION['recent_login_datetime'], 'ii');
								$second = dateDivide($_SESSION['recent_login_datetime'], 'ss');

								echo $year.'년 '.$month.'월 '.$date.'일 '.$hour.'시 '.$minute.'분 '.$second.'초  ('.$_SESSION['recent_login_addr'].')';

							} else {
								echo '이전 접속 기록이 없습니다.';
							}
						?>
					</div>
				</div>
			</div>

			<div class="logout_button">
				로그아웃
			</div>

		</div>





        <div class="dashboard_list_area clearFix">


            <div class="column1 basic_column">
                <div class="title">
                    전체 회원 수
                </div>
                <div class="total_member_amount numberType1">
                    
                </div>
            </div>


            <div class="column2 basic_column">
                <div class="title">
                    전체 진료과 수
                </div>
                <div class="department_total_num numberType1">

                </div>
            </div>


            <div class="column3 basic_column last_row">
                <div class="title">
                    전체 의료진 수
                </div>
                <div class="doctor_total_num numberType1">

                </div>
            </div>


            <div class="column2 basic_column">
                <div class="title">
                    오늘 올라온 1:1문의 수
                </div>
                <div class="today_oneone_question_total_num numberType1">

                </div>
            </div>


            <div class="column3 basic_column">
                <div class="title">
                    오늘 진료예약 신청 수
                </div>
                <div class="today_clinic_reservation_total_num numberType1">

                </div>
            </div>


            <div class="column3 basic_column last_row">
                <div class="title">
                    오늘 순번대기표 수
                </div>
                <div class="today_waiting_ticket_total_num numberType1">

                </div>
            </div>




        </div>












		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>