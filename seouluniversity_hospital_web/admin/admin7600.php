<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 고객의소리 상세내용</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin7600.js"></script>
        <style>
            .content_delete_button.admin7600 {
                width:70px;
                height:40px;
                line-height:40px;
                display:block;
                margin:20px;
                padding:0px;
                position:relative;
                background-color:#f8536a;
                text-align:center;
                color:#fff;
                font-size:14px;
                font-weight:normal;
                cursor:pointer;
            }

            .attach_file_list {
                display:block;
                margin:0px 0px 0px 30px;
                padding:0px;
                float:left;
            }
            .attach_file_list li {
                display:block;
                margin:0px 0px 3px 0px;
                padding:0px;
                list-style-type:none;
            }
            .attach_file_list li > a.file_name {
                float:left;
                text-decoration:none;
                color:#008ace;
            }
            .attach_file_list li > a.file_name:hover {
                text-decoration:underline;
            }
            .attach_file_list li > div.file_delete_button {
                float:left;
                text-decoration:none;
                color:#f8536a;
                margin-left:10px;
                cursor:pointer;
                display:none;
            }
            .attach_file_list li > div.file_delete_button:hover {
                text-decoration:underline;
            }
        </style>
	</head>
	<body page-code="admin7600" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />

        <input type="hidden" name="pk" value="<?php echo $_REQUEST['pk']; ?>" />




        



        <div class="viewStyleD admin8100">
            <ul>
                <li class="clearFix">
                    <div class="question_type board_type" style="font-size:20px;float:left;">
                        
                    </div>
                    <div class="content_type" style="font-size:14px;float:left;color:#999;margin-top:7px;margin-left:8px;">
                        
                    </div>
                </li>
                <li class="clearFix">
                    <div class="writer">
                        
                    </div>
                    <div class="datetime">
                        
                    </div>
                    <div class="view_indexs" style="float:left;font-size:14px;font-weight:normal;color:#999;margin-left:40px;">

                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        
                    </div>
                </li>
                <li class="clearFix">
                    <div class="attach" style="float:left;">
                        첨부파일
                    </div>
                    <ul class="attach_file_list">                        
                        
                    </ul>
                    <div class="no_result" style="display:none;">
                        이 없습니다.
                    </div>
                </li>
                <li style="padding:40px 0px;color:#777;">
                    <div class="content">
                        내용
                    </div>
                </li>
            </ul>
        </div>




        <div class="content_delete_button admin7600">
            삭제
        </div>




		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>