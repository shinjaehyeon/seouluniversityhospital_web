<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 순번대기표 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin3000.js"></script>
	</head>
	<body page-code="admin3000" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 순번대기표 관리
        </div>




        <div class="admin3000 all_view hide">
            <div class="title">
                전체 현황
            </div>
            <div class="hospital_type_list clearFix">
                <div hospital-type="700201">
                    <div class="title">
                        본원
                    </div>
                    <ul>
                        <li>
                            <div class="where">
                            &lt;1번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                        <li>
                            <div class="where">
                            &lt;2번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                        <li>
                            <div class="where">
                            &lt;3번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                    </ul>
                </div>
                <div hospital-type="700202"> 
                    <div class="title">
                        어린이병원
                    </div>
                    <ul>
                        <li>
                            <div class="where">
                            &lt;1번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                        <li>
                            <div class="where">
                            &lt;2번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                        <li>
                            <div class="where">
                            &lt;3번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                    </ul>
                </div>
                <div hospital-type="700203">
                    <div class="title">
                        암병원
                    </div>
                    <ul>
                        <li>
                            <div class="where">
                            &lt;1번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                        <li>
                            <div class="where">
                            &lt;2번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                        <li>
                            <div class="where">
                            &lt;3번 접수처&gt;
                            </div>
                            <div class="current_number_title">
                                현재번호
                            </div>
                            <div class="current_number">
                                131
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
		








        <div class="admin3000 ticketadmin">
            <ul class="tab_box clearFix">
                <li class="active" hospital-type="700201">
                    본원
                </li>
                <li hospital-type="700202">
                    어린이병원
                </li>
                <li hospital-type="700203">
                    암병원
                </li>
            </ul>
            <ul class="tab_content setTopVirtualBox">
                <li hospital-type="700201" class="setTopVirtualBox show">
                    <ul class="counter_list clearFix">
                        <li>
                            <div class="counter_title">
                                1번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700201one" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700201one" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" hospital-type="700201" counter-number="1">
                                다음 번호 받기
                            </div>
                        </li>
                        <li>
                            <div class="counter_title">
                                2번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700201two" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700201two" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" class="next_number_button" hospital-type="700201" counter-number="2">
                                다음 번호 받기
                            </div>
                        </li>
                        <li>
                            <div class="counter_title">
                                3번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700201three" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700201three" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" class="next_number_button" hospital-type="700201" counter-number="3">
                                다음 번호 받기
                            </div>
                        </li>
                    </ul>
                </li>
                <li hospital-type="700202" class="setTopVirtualBox">
                    <ul class="counter_list clearFix">
                        <li>
                            <div class="counter_title">
                                1번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700202one" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700202one" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" hospital-type="700202" counter-number="1">
                                다음 번호 받기
                            </div>
                        </li>
                        <li>
                            <div class="counter_title">
                                2번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700202two" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700202two" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" class="next_number_button" hospital-type="700202" counter-number="2">
                                다음 번호 받기
                            </div>
                        </li>
                        <li>
                            <div class="counter_title">
                                3번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700202three" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700202three" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" class="next_number_button" hospital-type="700202" counter-number="3">
                                다음 번호 받기
                            </div>
                        </li>
                    </ul>
                </li>
                <li hospital-type="700203" class="setTopVirtualBox">
                    <ul class="counter_list clearFix">
                        <li>
                            <div class="counter_title">
                                1번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700203one" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700203one" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" hospital-type="700203" counter-number="1">
                                다음 번호 받기
                            </div>
                        </li>
                        <li>
                            <div class="counter_title">
                                2번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700203two" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700203two" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" class="next_number_button" hospital-type="700203" counter-number="2">
                                다음 번호 받기
                            </div>
                        </li>
                        <li>
                            <div class="counter_title">
                                3번 접수처
                            </div>
                            <div class="current_number_box">
                                <div>
                                    현재번호
                                </div>
                                <div>
                                    -
                                </div>
                            </div>
                            <div class="current_number_check">
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700203three" value="700304" /> 
                                        <div class="radio_text">해결</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="clearFix">
                                        <input class="radio_button" type="radio" name="h700203three" value="700303" /> 
                                        <div class="radio_text">미해결</div>
                                    </div>
                                </div>
                            </div>
                            <div class="next_number_button" class="next_number_button" hospital-type="700203" counter-number="3">
                                다음 번호 받기
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>









		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>