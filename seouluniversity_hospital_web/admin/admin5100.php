<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 회원 상세/수정 페이지</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin5100.js"></script>
	</head>
	<body page-code="admin5100" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />





        <div class="big_title">
            ■ 회원 관리
        </div>


        



        

        <form name="memberForm" action="./outlet.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="act" value="admin5112" />
            <input type="hidden" name="pk" value="<?php echo $_REQUEST['pk']; ?>" />
            <div class="listStyleDD">
                <ul>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                고유번호
                            </div>
                        </div>
                        <div class="column_content">
                            <div class="member_primarykey not_edit">
                                <?php echo $_REQUEST['pk']; ?>
                            </div>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                이름
                            </div>
                        </div>
                        <div class="column_content">
                            <input type="text" name="memberName" value="" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                성별
                            </div>
                        </div>
                        <div class="column_content">
                            <select name="memberGender">
                                <option value="900900">지정되지 않음</option>
                                <option value="800501">남자</option>
                                <option value="800502">여자</option>
                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                휴대폰 통신사
                            </div>
                        </div>
                        <div class="column_content">
                            <select name="memberMobileCompany">
                                <option value="900900">지정되지 않음</option>
                                <option value="700101">LG U+</option>
                                <option value="700102">SKT</option>
                                <option value="700103">KT</option>
                                <option value="700104">LG U+ 알뜰폰</option>
                                <option value="700105">SKT 알뜰폰</option>
                                <option value="700106">KT 알뜰폰</option>
                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                휴대폰 번호
                            </div>
                        </div>
                        <div class="column_content">
                            <input type="text" name="memberPhoneNumber" value="" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                이메일
                            </div>
                        </div>
                        <div class="column_content">
                            <div class="member_email not_edit">
                                jwisedom@naver.com
                            </div>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                생일
                            </div>
                        </div>
                        <div class="column_content">
                            <input type="date" name="memberBirthday" value="" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                혈액형
                            </div>
                        </div>
                        <div class="column_content">
                            <select name="memberBloodtype">
                                <option value="900900">지정되지 않음</option>
                                <option value="600101">O형</option>
                                <option value="600102">A형</option>
                                <option value="600103">B형</option>
                                <option value="600104">AB형</option>
                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                주소
                            </div>
                        </div>
                        <div class="column_content">
                            <textarea name="memberAddr" style="width:50%;height:80px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                가입일
                            </div>
                        </div>
                        <div class="column_content">
                            <div class="member_join_datetime not_edit">
                                
                            </div>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                가입방법
                            </div>
                        </div>
                        <div class="column_content">
                            <div class="member_join_method not_edit">
                                
                            </div>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                현재상태
                            </div>
                        </div>
                        <div class="column_content">
                            <select name="memberStatus">
                                <option value="500101">일반상태</option>
                                <option value="500102">탈퇴상태</option>
                                <option value="500103">휴먼상태</option>
                                <option value="500104">정지상태</option>
                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="column_title">
                            <div>
                                푸시알림 수신여부
                            </div>
                        </div>
                        <div class="column_content">
                            <div class="member_is_get_push not_edit">
                                
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="button_areas">
                    <div class="button_box clearFix">
                        <div class="list_button" onclick="location.href='./admin5000.php';">
                            목록으로
                        </div>
                        <div class="edit_button">
                            수정하기
                        </div>
                    </div>
                </div>
            </div>
        </form>









		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>