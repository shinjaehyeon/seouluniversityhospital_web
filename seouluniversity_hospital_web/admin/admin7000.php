<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 병원뉴스 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin7000.js"></script>
	</head>
	<body page-code="admin7000" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 병원뉴스 관리
        </div>


        

       


       <div class="searchFilterTypeA admin7000">
            <ul>
                <li class="clearFix">
                    <div class="title">
                        작성날짜
                    </div>
                    <div>
                        <input type="date" name="startDate" />
                        <div class="decoration1">
                            ~
                        </div>
                        <input type="date" name="endDate" />
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        작성자
                    </div>
                    <div>
                        <select name="writer">
                            <option value="0" class="default">전체</option>
                        </select>

                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        제목
                    </div>
                    <div>
                        <input type="text" name="title" />
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        내용
                    </div>
                    <div>
                        <input type="text" name="content" />
                    </div>
                </li>
            </ul>
            <div class="finalSearchButton">
                검색
            </div>
        </div>




        <div class="delete_mode_comment_box">
            <div class="delete_mode_comment">
                삭제 모드입니다. 삭제할 리스트를 선택해주세요.
            </div>
        </div>




        <div class="group3 clearFix">
            <a href="./admin7100.php" class="news_wirte_button" style="float:left;">
                뉴스 업로드
            </a>

            <div class="delete_mode" style="float:right;">
                삭제모드 열기
            </div>
            <div class="all_select" data-status="0" style="float:right;">
                전체선택
            </div>
            <div class="selected_list_delete_button" style="float:right;">
                선택된 리스트 삭제
            </div>
        </div>





        <div class="boardTypeC">
            <ul>
                <li>
                    <div class="datetime_writer_area clearFix">
                        <div>
                            125
                        </div>
                        <div>
                            2018-07-08
                        </div>
                        <div>
                            admin1(admin1) 작성
                        </div>
                    </div>
                    <div class="title">
                        서울대학교병원 첫 환영식 개최
                    </div>
                    <div class="content">
                        아스피린은 혈소판 효소(사이클로옥시제네이즈)를 억제해, 혈소판 응고를 제한하는 효과가 있어 심혈관계 고위험군에서 항혈소판 약제로 널리 사용된다. 아스피린은 혈소판 효소(사이클로옥시제네이즈)를 억제해, 혈소판 응고를 제한하는 효과가 있어 심혈관계 고위험군에서 항혈소판 약제로 널리 사용된다. 
                    </div>
                    <div class="view_index">
                        <div>
                            조회수
                        </div>
                        <div>
                            11
                        </div>
                    </div>
                </li>
            </ul>
        </div>







        <div class="pagingControllerBox">
            <div class="clearFix">
                <div class="best_prev">
                    <<
                </div>
                <div class="prev">
                    <
                </div>
                <ul class="page_num clearFix">
                    <li class="active">
                        1
                    </li>
                    <li>
                        2
                    </li>
                </ul>
                <div class="next">
                    >
                </div>
                <div class="linenext">

                </div>
                <div class="best_next">
                    >>
                </div>
            </div>
        </div>
        







		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>