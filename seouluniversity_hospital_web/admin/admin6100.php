<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 진료예약 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin6100.js"></script>
	</head>
	<body page-code="admin6100" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

        <input type="hidden" name="doctor_pk" value="<?php echo $_REQUEST['doctor_pk']; ?>" />
        <input type="hidden" name="doctor_name" value="<?php echo $_REQUEST['doctor_name']; ?>" />
        <input type="hidden" name="doctor_department" value="<?php echo $_REQUEST['doctor_department']; ?>" />
        <input type="hidden" name="doctor_departmentint" value="<?php echo $_REQUEST['doctor_departmentint']; ?>" />

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 진료예약 관리
        </div>


        

        

        <div class="doctor_name_box clearFix">
            <div class="doctor_name">
                <?php echo $_REQUEST['doctor_name']; ?> 의료진
            </div>
            <div class="doctor_department">
                <?php echo $_REQUEST['doctor_department']; ?>
            </div>


            <div class="not_enter_update">
                미방문자 업데이트 하기
            </div>
        </div>









        <div class="group1 clearFix">
            <div class="calendar_box_egg">
                <div class="calendar_box">
                    <div class="clinic_calendar_area">
                        <div class="month_and_arrow_button_area setTopVirtualBox">
                            <div class="month_and_arrow_button_area_inner clearFix">
                                <div class="prev_month_button">

                                </div>
                                <div class="year_month_display">
                                    <span class="year">
                                        2018년
                                    </span>
                                    <span class="month">
                                        5월
                                    </span>
                                </div>
                                <div class="next_month_button">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="real_calendar">
                            <div class="real_calendar_inner">
                                <div class="day_display_area">
                                    <ul class="clearFix">
                                        <li>
                                            일
                                        </li>
                                        <li>
                                            월
                                        </li>
                                        <li>
                                            화
                                        </li>
                                        <li>
                                            수
                                        </li>
                                        <li>
                                            목
                                        </li>
                                        <li>
                                            금
                                        </li>
                                        <li>
                                            토
                                        </li>
                                    </ul>
                                </div>
                                <div class="date_display_area">
                                    <ul class="clearFix">
                                        <!-- 1주 -->
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <!-- 2주 -->
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <!-- 3주 -->
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <!-- 4주 -->
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <!-- 5주 -->
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <!-- 6주 -->
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                        <li data-year="" data-month="" data-date="">
                                            <span class="date">
                                                1
                                            </span>
                                            <div class="today">
                                                오늘
                                            </div>
                                        </li>
                                    </ul>
                                </div> 
                            </div>
                        </div>
                        <div style="height:17px;">

                        </div>
                    </div>
                </div>
            </div>
            <div class="clinic_time_list_box setTopVirtualBox">
                <div class="title clearFix">
                    <span>
                        진료가능시간 <span class="fullDate"></span>
                    </span>
                </div>
                <div class="time_add_area clearFix">
                    <span class="timeInput">
                        <input type="time" name="clinicTime" />
                    </span>
                    <span class="add_button">
                        추가하기
                    </span>
                </div>
                <div class="list_area">
                    <ul>
                        <!-- <li class="clearFix setTopVirtualBox">
                            <div class="time">
                                14:30
                            </div>
                            <div class="member_name">
                                예약된 내역이   신재현 예약함
                            </div>
                            <div class="member_phone">
                                없습니다.   (010-5428-6905)
                            </div>
                            <div class="cancel_button">
                                삭제
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>








        <!-- 진단서 작성하는 영역 -->
        <div class="medical_certificate_box" schedule-pk="" member-pk="">
            <div class="title">
                진단서 작성
            </div>
            <ul class="medical_certificate_form_box">
                <li class="clearFix">
                    <div class="title">
                        진단 대상
                    </div>
                    <div class="content clearFix">
                        <input class="inputstyle" type="text" name="resultName" value="신재현" readonly="readonly" />
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        증상
                    </div>
                    <div class="content clearFix">
                        <div class="symptom">

                        </div>
                    </div>
                </li>
                <li class="clearFix">
                    <div class="title">
                        처방약품 선택
                    </div>
                    <div class="content clearFix">
                        <select style="width:120px;" class="inputstyle drugstyle" name="resultDrug1">
                            <option value="0" class="default">선택하세요.</option>
                        </select>
                        <select style="width:140px;margin-left:10px;" class="inputstyle drugstyle" name="resultDrug2">
                            <option value="0" class="default">선택하세요.</option>
                        </select>
                        <select style="width:250px;margin-left:10px;" id="resultDrug3" class="inputstyle drugstyle" name="resultDrug3">
                            <option value="0" class="default">선택하세요.</option>
                        </select>
                        <div class="drug_add_button">
                            추가
                        </div>
                    </div>
                </li>
                <li class="clearFix" style="height:auto;">
                    <div class="title">
                        처방약품 리스트
                    </div>
                    <div class="content clearFix setTopVirtualBox">
                        <ul class="drug_list_ul">
                            <!-- <li class="clearFix setTopVirtualBox">
                                <div class="clearFix">
                                    <div class="drug_pk">
                                        No.<span class="only_pk">35</span>
                                    </div>
                                    <div class="drug_name">
                                        리뉴멀티플러스하드렌즈보존액
                                    </div>  
                                    <div class="drug_note">
                                        <input type="text" placeholder="약품 비고 설명(선택)" />
                                    </div>
                                    <div class="drug_remove">
                                        제거
                                    </div>
                                </div>
                                <div class="clearFix">
                                    <input class="oneday_dose" type="number" step="0.1" placeholder="하루 투여량" />
                                    <input class="oneday_number" type="number" step="0.1" placeholder="하루 투여 횟수" />
                                    <input class="dose_day_num" type="number" step="0.1" placeholder="투여 일수" />
                                </div>
                            </li> -->
                            
                        </ul>

                    </div>
                </li>
                <li class="clearFix" style="height:auto;">
                    <div class="title">
                        의사 소견
                    </div>
                    <div class="content clearFix">
                        <textarea name="doctor_opinion"></textarea>
                    </div>
                </li>
            </ul>
            <div class="upload_button">
                진단서 작성하기
            </div>
        </div>
        







		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>