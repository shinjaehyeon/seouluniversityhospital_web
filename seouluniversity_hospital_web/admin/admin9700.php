<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 의료진 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin9700.js"></script>
        <style>
            .school_add_button {
                width:70px;
                height:40px;
                background-color:#008ace;
                float:left;
                color:#fff;
                text-align:center;
                line-height:40px;
                position:relative;
                margin-left:10px;
                cursor:pointer;
            }
            ul.school_list {
                display:block;
                margin:10px 0px 5px 0px;
                padding:0px;
                position:relative;
            }
            ul.school_list li {
                display:block;
                padding:4px 0px;
                position:relative;
                list-style-type:none;
                border-bottom:1px solid #ccc;
            }
            ul.school_list li .s_period {
                width:150px;
                float:left;
            }
            ul.school_list li .s_content {
                width:210px;
                float:left;
            }
            ul.school_list li .s_up_button {
                float:left;
                cursor:pointer;
                font-size:14px;
                font-weight:normal;
                color:#008ace;
                margin:0px 4px;
            }
            ul.school_list li .s_down_button {
                float:left;
                cursor:pointer;
                font-size:14px;
                font-weight:normal;
                color:#f8536a;
                margin:0px 4px;
            }
            ul.school_list li .s_delete_button {
                float:right;
                cursor:pointer;
                font-size:14px;
                font-weight:normal;
                color:#f8536a;
                margin-left:10px;
            }

            .career_add_button {
                width:70px;
                height:40px;
                background-color:#008ace;
                float:left;
                color:#fff;
                text-align:center;
                line-height:40px;
                position:relative;
                margin-left:10px;
                cursor:pointer;
            }
            ul.career_list {
                display:block;
                margin:10px 0px 5px 0px;
                padding:0px;
                position:relative;
            }
            ul.career_list li {
                display:block;
                padding:4px 0px;
                position:relative;
                list-style-type:none;
                border-bottom:1px solid #ccc;
            }
            ul.career_list li .c_period {
                width:150px;
                float:left;
            }
            ul.career_list li .c_content {
                width:210px;
                float:left;
            }
            ul.career_list li .c_up_button {
                float:left;
                cursor:pointer;
                font-size:14px;
                font-weight:normal;
                color:#008ace;
                margin:0px 4px;
            }
            ul.career_list li .c_down_button {
                float:left;
                cursor:pointer;
                font-size:14px;
                font-weight:normal;
                color:#f8536a;
                margin:0px 4px;
            }
            ul.career_list li .c_delete_button {
                float:right;
                cursor:pointer;
                font-size:14px;
                font-weight:normal;
                color:#f8536a;
                margin-left:10px;
            }

            .comment_button {
                width:120px;
                height:40px;
                line-height:40px;
                text-align:center;
                float:left;
                display:block;
                margin:0px 0px 0px 10px;
                padding:0px;
                position:relative;
                background-color:#444;
                color:#fff;
                font-size:14px;
                font-weight:normal;
                cursor:pointer;
            }
        </style>
	</head>
	<body page-code="admin9700" id="body">
		<?php

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 의료진 관리
        </div>


        

       


        


        <div class="formStlyeE admin9600">
            <form name="doctorAddForm" action="./outlet.php" enctype="multipart/form-data" method="post">
                <input type="hidden" name="act" value="admin9715" />
                <input type="hidden" name="doctor_pk" value="<?php echo $_REQUEST['doctor_pk']; ?>" />
                <input type="hidden" name="is_first_image" value="" />
                <ul>
                    <li class="clearFix">
                        <div class="title">
                            의료진 코드
                        </div>
                        <div class="content" style="width:600px;">
                            <input type="number" name="doctorCode" class="input_box_style department" style="width:160px;height:40px;background-color:#ccc;float:left;" value="<?php echo $_REQUEST['doctor_pk']; ?>" readonly />

                            <div class="comment_button">
                                댓글 관리
                            </div>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 소속진료과
                        </div>
                        <div class="content">
                            <select type="text" name="doctorDepartment" class="input_box_style" style="width:180px;height:40px;">

                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 이름
                        </div>
                        <div class="content">
                            <input type="text" name="doctorName" class="input_box_style" style="width:600px;height:40px;" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 나이
                        </div>
                        <div class="content">
                            <input type="number" name="doctorAge" class="input_box_style department" style="width:140px;height:40px;" step="1" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 성별
                        </div>
                        <div class="content">
                            <select type="text" name="doctorGender" class="input_box_style" style="width:140px;height:40px;">
                                <option value="800501">남자</option>
                                <option value="800502">여자</option>
                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 상태
                        </div>
                        <div class="content">
                            <select type="text" name="doctorStatus" class="input_box_style" style="width:140px;height:40px;">
                                <option value="800401">근무</option>
                                <option value="800402">휴직</option>
                                <option value="800403">퇴직</option>
                                <option value="800404">휴가</option>
                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 사진
                        </div>
                        <div class="content">
                            <div>
                                <input type="file" name="doctorImage" class="input_box_style" style="border:0px;padding:0px;" />
                            </div>
                            <div class="imageDisplayView">
                                <img class="department_preview" src="" alt="미리보기 이미지" title="미리보기 이미지" />
                            </div>
                        </div>
                    </li>
                    
                    <li class="clearFix">
                        <div class="title">
                            의료진 소개글 
                        </div>
                        <div class="content">
                            <textarea name="doctorContent" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 학력
                        </div>
                        <div class="content">
                            <div class="clearFix">
                                <input type="text" class="input_box_style" name="doctorSchoolPeriod" placeholder="기간" style="height:40px;float:left;" />
                                <input type="text" class="input_box_style" name="doctorSchoolContent" placeholder="내용" style="height:40px;float:left;margin-left:10px;" />
                                <div class="school_add_button">
                                    추가
                                </div>
                            </div>
                            <div class="clearFix">
                                <ul class="school_list">
                                    <!-- <li class="clearFix">
                                        <div class="s_period">
                                            1987 ~ 1991
                                        </div>
                                        <div class="s_content">
                                            서울대학교 재학
                                        </div>
                                        <div class="s_up_button">
                                            ▲
                                        </div>
                                        <div class="s_down_button">
                                            ▼
                                        </div>
                                        <div class="s_delete_button">
                                            제거
                                        </div>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                        <input type="hidden" name="schoolArray" />
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의료진 경력및연수
                        </div>
                        <div class="content">
                            <div class="clearFix">
                                <input type="text" class="input_box_style" name="doctorCareerPeriod" placeholder="기간" style="height:40px;float:left;" />
                                <input type="text" class="input_box_style" name="doctorCareerContent" placeholder="내용" style="height:40px;float:left;margin-left:10px;" />
                                <div class="career_add_button">
                                    추가
                                </div>
                            </div>
                            <div class="clearFix">
                                <ul class="career_list">
                                    <!-- <li class="clearFix">
                                        <div class="c_period">
                                            1987 ~ 1991
                                        </div>
                                        <div class="c_content">
                                            서울대학교 재학
                                        </div>
                                        <div class="c_up_button">
                                            ▲
                                        </div>
                                        <div class="c_down_button">
                                            ▼
                                        </div>
                                        <div class="c_delete_button">
                                            제거
                                        </div>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                        <input type="hidden" name="careerArray" />
                    </li>
                </ul>
                <div class="department_upload_button">
                    의료진 수정
                </div>
            </form>
        </div>









		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>