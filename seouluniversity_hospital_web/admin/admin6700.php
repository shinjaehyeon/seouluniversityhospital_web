<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - N의학정보 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin6700.js"></script>

        <style>
            .formStlyeE.admin6600 div.title {
                width:150px;
            } 
            .department_add_button {
                width:70px;
                height:40px;
                margin-left:5px;
                background-color:#008ace;
                font-size:14px;
                font-weight:normal;;
                color:#fff;
                text-align:center;
                line-height:40px;
                cursor:pointer;
            }
            .chain_department_list {
                display:block;
                margin:20px 0px 5px 0px;
                padding:0px;
                position:relative;
            }
            .chain_department_list li {
                display:block;
                margin:0px 0px 4px 0px;
                padding:2px 0px;
                position:relative;
                list-style-type:none;
                border-bottom:1px solid #d9d9d9;
            }
            #body .chain_department_list .title {
                width:auto;
                color:#888;
                font-size:14px;
                font-weight:normal;
                float:left;
            }
            #body .chain_department_list .delete_button {
                color:#f8536a;
                font-size:14px;
                font-weight:normal;
                float:right;
                cursor:pointer;
            }
        </style>
	</head>
	<body page-code="admin6700" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            N의학정보 관리
        </div>


        

       


        


        <div class="formStlyeE admin6600">
            <form name="departmentAddForm" action="./outlet.php" enctype="multipart/form-data" method="post">
                <input type="hidden" name="nmi" value="<?php echo $_REQUEST['nmi']; ?>" />
                <ul>
                    <li class="clearFix" style="font-size:14px;color:#f8536a;">
                        ※ 필수입력
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의학정보명 (한글)
                        </div>
                        <div class="content">
                            <input type="text" name="titleKo" class="input_box_style" style="width:400px;height:40px;" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            의학정보명 (영어)
                        </div>
                        <div class="content">
                            <input type="text" name="titleEn" class="input_box_style" style="width:400px;height:40px;" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            한 줄 설명
                        </div>
                        <div class="content">
                            <input type="text" name="oneDescription" class="input_box_style" style="width:400px;height:40px;" />
                        </div>
                    </li>






                    <li class="clearFix" style="font-size:14px;color:#008ace;margin-top:50px;">
                        ※ 선택입력
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            관련 진료과
                        </div>
                        <div class="content clearFix">
                            <div class="clearFix">
                                <select name="departmentSelectList" class="input_box_style" style="width:200px;height:40px;float:left;">

                                </select>
                                <div class="department_add_button" style="float:left;">
                                    추가
                                </div>
                            </div>
                            <div>
                                <ul class="chain_department_list">
                                    <!-- <li class="clearFix">
                                        <div class="title" data-code="800123">
                                            내분비내과
                                        </div>
                                        <div class="delete_button">
                                            삭제
                                        </div>
                                    </li> -->
                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            정의
                        </div>
                        <div class="content">
                            <textarea name="definition" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            증상
                        </div>
                        <div class="content">
                            <textarea name="symptom" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            원인
                        </div>
                        <div class="content">
                            <textarea name="cause" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            관련 신체기관
                        </div>
                        <div class="content">
                            <textarea name="chainBody" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            진단
                        </div>
                        <div class="content">
                            <textarea name="diagnosis" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            검사
                        </div>
                        <div class="content">
                            <textarea name="check" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            치료
                        </div>
                        <div class="content">
                            <textarea name="cure" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            경과/합병증
                        </div>
                        <div class="content">
                            <textarea name="hyperplasia" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            예방방법
                        </div>
                        <div class="content">
                            <textarea name="prevent" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            생활가이드
                        </div>
                        <div class="content">
                            <textarea name="guide" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            검사주기
                        </div>
                        <div class="content">
                            <textarea name="checkPeriod" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            소요시간
                        </div>
                        <div class="content">
                            <textarea name="checkLeadTime" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            관련검사법
                        </div>
                        <div class="content">
                            <textarea name="chainCheckWay" class="input_box_style" style="width:600px;height:120px;padding:10px;"></textarea>
                        </div>
                    </li>
                </ul>
                <div class="department_upload_button">
                    N의학정보 수정
                </div>
            </form>
        </div>









		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>