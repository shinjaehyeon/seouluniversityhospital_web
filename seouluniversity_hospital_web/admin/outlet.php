<?php	
	session_start();

	header("Content-Type: text/html; charset=UTF-8");

	$db = new mysqli("localhost", $db_user, $db_password, $db_name) or die("mysqli db connect error");
	$db->set_charset("utf8");
	$act = "none";
	if (isset($_REQUEST["act"])) {
		$act = $_REQUEST["act"];
	} else {

	}

	$newbee = 'UTF-8';
	$origin ='EUC-KR';




	switch ($act) {
		/**************************************************************************************/
		/* admin0000 (공통) */
		/**************************************************************************************/
		case 'admin0001': // 다른 곳에서의 로그인 여부 체크하기
			admin0001();
			break;





		/**************************************************************************************/
		/* admin1000 (로그인 페이지) */
		/**************************************************************************************/
		case 'admin1006': // 서버에 관리자 계정이 유효한지 요청하기
			admin1006();
			break;



		/**************************************************************************************/
		/* admin2000 (대시보드 페이지) */
		/**************************************************************************************/	
		case 'admin2002': // 로그아웃 요청하기
			admin2002();
			break;
		case 'admin2003': // 총 회원 수 가져오기
			admin2003();
			break;
		case 'admin2004': // 오늘 올라온 1:1문의 수 가져오기
			admin2004();
			break;
		case 'admin2005': // 오늘 진료예약 신청 수 가져오기
			admin2005();
			break;
		case 'admin2006': // 전체 진료과 수 가져오기
			admin2006();
			break;
		case 'admin2007': // 전체 의료진 수 가져오기
			admin2007();
			break;
		case 'admin2008': // 오늘 순번대기표 수 가져오기
			admin2008();
			break;





		/**************************************************************************************/
		/* admin3000 (순번대기표관리 페이지) */
		/**************************************************************************************/
		case 'admin3002': // 순번대기표 관리에 처음 들어왔을 때 각 접수처별로 현재번호 가져오기
			admin3002();
			break;
		case 'admin3003': // 해당 병원 접수처에 다음 번호 가져오기
			admin3003();
			break;





		/**************************************************************************************/
		/* admin4000 (푸시알림관리 페이지) */
		/**************************************************************************************/
		case 'admin4002': // 서버에서 대상 조건에 맞는 멤버 정보 및 pk array 가져오기
			admin4002();
			break;
		case 'admin4004': // 푸시 그룹 저장하기
			admin4004();
			break;
		case 'admin4006': // 개개인씩 푸시알림 보내고 결과 받기를 반복하기
			admin4006();
			break;




		/**************************************************************************************/
		/* admin5000 (회원관리 페이지) */
		/**************************************************************************************/	
		case 'admin5003': // 전체 회원 수 가져오기
			admin5003();
			break;
		case 'admin5006': // 회원 목록에서의 회원정보 수정하기
			admin5006();
			break;
		case 'admin5010': // 회원 목록 가져오기
			admin5010();
			break;
		case 'admin5016': // 회원 관리 리스트 - 페이지 맨 마지막 번호 알아내기
			admin5016();
			break;




		/**************************************************************************************/
		/* admin5100 (회원정보 상세 페이지) */
		/**************************************************************************************/	
		case 'admin5110': // 회원 상세정보 가져오기
			admin5110();
			break;
		case 'admin5112': // 서버에서 회원정보 수정하기
			admin5112();
			break;






		/**************************************************************************************/
		/* admin6000 (진료예약 관리 페이지) */
		/**************************************************************************************/
		case 'admin6005': // 의료진 전체 리스트 가져오기
			admin6005();
			break;




		/**************************************************************************************/
		/* admin6100 (진료예약 관리 페이지 - 상세) */
		/**************************************************************************************/
		case 'admin6102': // 미방문자 업데이트 하기
			admin6102();
			break;
		case 'admin6107': // 선택된 날짜/시간을 의료진 스케쥴에 추가하기
			admin6107();
			break;
		case 'admin6110': // 해당 날짜에 해당하는 의료진 스케쥴 불러오기
			admin6110();
			break;
		case 'admin6113': // 의료진 해당 시간 스케쥴 삭제하기
			admin6113();
			break;
		case 'admin6114': // 증상 정보 가져오기
			admin6114();
			break;
		case 'admin6115': // 처방약품 대분류 리스트 가져오기
			admin6115();
			break;
		case 'admin6117': // 대분류 선택된 값에 따라 다음 소분류 리스트 가져오기
			admin6117();
			break;
		case 'admin6118': // 소분류 선택된 값에 따라 다음 약품 리스트 가져오기
			admin6118();
			break;
		case 'admin6131': // 진단서 내용 서버에 저장하기
			admin6131();
			break;




		/**************************************************************************************/
		/* admin6500 (N의학정보 관리 페이지) */
		/**************************************************************************************/
		case 'admin6507': // 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
			admin6507();
			break;
		case 'admin6510': // N의학정보 리스트 가져오기
			admin6510();
			break;
		case 'admin6516': // N의학정보 페이징 - 페이지 맨 마지막 번호 알아내기
			admin6516();
			break;






		/**************************************************************************************/
		/* admin6600 (N의학정보 추가 페이지) */
		/**************************************************************************************/
		case 'admin6601': // 진료과 리스트 불러오기
			admin6601();
			break;
		case 'admin6615': // N의학정보 서버에 업로드 하기
			admin6615();
			break;





		/**************************************************************************************/
		/* admin6700 (N의학정보 상세/수정 페이지) */
		/**************************************************************************************/
		case 'admin6715': // N의학정보 상세정보 가져오기
			admin6715();
			break;
		case 'admin6716': // N의학정보 서버에 업데이트 요청하기
			admin6716();
			break;





		/**************************************************************************************/
		/* admin7000 (병원뉴스 관리 페이지) */
		/**************************************************************************************/
		case 'admin7002': // 관리자 회원 리스트 가져오기
			admin7002();
			break;
		case 'admin7007': // 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
			admin7007();
			break;
		case 'admin7010': // 병원뉴스 리스트 가져오기
			admin7010();
			break;
		case 'admin7016': // 병원뉴스 페이징 - 페이지 맨 마지막 번호 알아내기
			admin7016();
			break;




		/**************************************************************************************/
		/* admin7100 (병원뉴스 작성 페이지) */
		/**************************************************************************************/
		case 'admin7110': // 병원뉴스 서버에 업로드 하기
			admin7110();
			break;	



		/**************************************************************************************/
		/* admin7200 (병원뉴스 상세 페이지) */
		/**************************************************************************************/
		case 'admin7210': // 병원뉴스 상세정보 가져오기
			admin7210();
			break;



		/**************************************************************************************/
		/* admin7300 (병원뉴스 수정 페이지) */
		/**************************************************************************************/
		case 'admin7301': // 병원뉴스 상세정보 가져오기
			admin7301();
			break;
		case 'admin7310': // 병원뉴스 서버에서 내용 수정하기
			admin7310();
			break;




		/**************************************************************************************/
		/* admin7500 (고객의소리 관리 페이지) */
		/**************************************************************************************/
		case 'admin7510': // 고객의소리 리스트 가져오기
			admin7510();
			break;
		case 'admin7516': // 고객의소리 - 페이지 맨 마지막 번호 알아내기
			admin7516();
			break;





		/**************************************************************************************/
		/* admin7600 (고객의소리 상세페이지) */
		/**************************************************************************************/
		case 'admin7610': // 고객의소리 상세정보 가져오기
			admin7610();
			break;
		case 'admin7612': // 고객의소리 삭제하기
			admin7612();
			break;




		/**************************************************************************************/
		/* admin8000 (1:1문의 관리) */
		/**************************************************************************************/
		case 'admin8001': // 문의유형 리스트 가져오기 
			admin8001();
			break;
		case 'admin8010': // 1:1문의 리스트 가져오기
			admin8010();
			break;
		case 'admin8016': // 1:1문의 페이징 - 페이지 맨 마지막 번호 알아내기
			admin8016();
			break;




		/**************************************************************************************/
		/* admin8100 (1:1문의 관리 - 상세내용) */
		/**************************************************************************************/
		case 'admin8106': // 문의내용에 대한 답변 서버에 저장하기
			admin8106();
			break;
		case 'admin8110': // 문의 내용 가져오기
			admin8110();
			break;





		/**************************************************************************************/
		/* admin9000 (진료과 관리) */
		/**************************************************************************************/
		case 'admin9010': // 진료과 리스트 가져오기
			admin9010();
			break;




		/**************************************************************************************/
		/* admin9100 (진료과 추가 페이지) */
		/**************************************************************************************/
		case 'admin9106': // 진료과 번호 중복확인 체크하기
			admin9106();
			break;
		case 'admin9110': // 진료과 서버에 추가하기
			admin9110();
			break;





		/**************************************************************************************/
		/* admin9200 (진료과 상세/수정 페이지) */
		/**************************************************************************************/
		case 'admin9201': // 진료과 상세정보 가져오기
			admin9201();
			break;	
		case 'admin9210': // 진료과 정보 수정하기
			admin9210();
			break;






		/**************************************************************************************/
		/* admin9500 (의료진 관리) */
		/**************************************************************************************/





		/**************************************************************************************/
		/* admin9600 (의료진 추가 페이지) */
		/**************************************************************************************/
		case 'admin9604': // 진료과 리스트 가져오기
			admin9604();
			break;
		case 'admin9606': // 의료진 번호 중복확인 체크하기
			admin9606();
			break;
		case 'admin9615': // 서버에 의료진 추가하기
			admin9615();
			break;




		/**************************************************************************************/
		/* admin9700 (의료진 상세/수정 페이지) */
		/**************************************************************************************/
		case 'admin9701': // 의료진 상세정보 가져오기
			admin9701();
			break;
		case 'admin9715': // 의료진 정보 수정하기
			admin9715();
			break;




		/**************************************************************************************/
		/* admin9800 (의료진 댓글 관리 페이지) */
		/**************************************************************************************/
		case 'admin9801': // 의료진 댓글 리스트 가져오기
			admin9801();
			break;
		case 'admin9807': // 의료진 댓글 - 페이지 맨 마지막 번호 알아내기
			admin9807();
			break;
		case 'admin9809': // 댓글 삭제 버튼 클릭시 이벤트 설정하기
			admin9809();
			break;
		case 'admin9815': // 의료진 정보 가져오기
			admin9815();
			break;






		case 'test': // test
			test();
			break;



		default:

			break;



	}




	// 다른 곳에서의 로그인 여부 체크하기
	function admin0001() {
		$db = $GLOBALS["db"];
		$admin_pk = $_REQUEST["admin_pk"];
		$admin_ip = $_REQUEST["admin_ip"];

		$sql=<<<SQL
SELECT `lognumber`, `note3` FROM `suhLogs` 
WHERE `member`={$admin_pk} 
AND `lognumber`=100101
ORDER BY `log` DESC LIMIT 1
SQL;

		// echo $sql;

		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$lognumber = $row->lognumber;
			$login_ip = $row->note3;

			if ($_SERVER['REMOTE_ADDR'] != $login_ip) {
				$_SESSION['admin_login'] = '';
				$_SESSION['admin_primarykey'] = 0;
				$_SESSION['admin_id'] = '';
				$_SESSION['admin_name'] = '';
				$_SESSION['authority'] = 0;
				$_SESSION['admin_ip'] = '';

				setLog($admin_pk, 100100, 100102, 0, 0, $_SERVER['REMOTE_ADDR']);

				echo '{"result":"ok"}';
			} else {
				echo '{"result":"no"}';
			}
			
		}
	}


	// 서버에 관리자 계정이 유효한지 요청하기
	function admin1006() {
		$db = $GLOBALS["db"];
		$id = $_REQUEST["id"];
		$pw = $_REQUEST["pw"];

		$sql=<<<SQL
SELECT `suhadmin`, `id`, `name`, `authority` FROM `suhAdmin` 
WHERE `id`='{$id}' AND `pw`='{$pw}'
SQL;

		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$sql2=<<<SQL
SELECT `lognumber`, `note3` FROM `suhLogs` 
WHERE `member`={$row->suhadmin} 
ORDER BY `log` DESC LIMIT 1
SQL;
			$res2 = $db->query($sql2);

			$row2 = $res2->fetch_object();
			if ($row2->lognumber == 100101) {
				$other_login = "yes";
			} else {
				$other_login = "no";
			}


			$sql2=<<<SQL
SELECT `lognumber`, `datetime`, `note3` FROM `suhLogs` 
WHERE `member`={$row->suhadmin} 
AND `lognumber`=100101
ORDER BY `log` DESC LIMIT 1
SQL;
			$res2 = $db->query($sql2);

			if ($res2->num_rows == 1) {
				$row2 = $res2->fetch_object();


				$recent_login_exist = 'ok';
				$recent_login_addr = $row2->note3;
				$recent_login_datetime = $row2->datetime;

			} else {
				$recent_login_exist = 'no';

			}





			$_SESSION['admin_login'] = 'ok';
			$_SESSION['admin_primarykey'] = $row->suhadmin;
			$_SESSION['admin_id'] = $row->id;
			$_SESSION['admin_name'] = $row->name;
			$_SESSION['authority'] = $row->authority;
			$_SESSION['admin_ip'] = $_SERVER['REMOTE_ADDR'];
			
			$_SESSION['recent_login_exist'] = $recent_login_exist;
			$_SESSION['recent_login_addr'] = $recent_login_addr;
			$_SESSION['recent_login_datetime'] = $recent_login_datetime;


			setLog($row->suhadmin, 100100, 100101, 0, 0, $_SERVER['REMOTE_ADDR']);

			echo '{"result":"ok", "other_login":"'.$other_login.'", "recent_login_exist":"'.$recent_login_exist.'", "recent_login_addr":"'.$recent_login_addr.'", "recent_login_datetime":"'.$recent_login_datetime.'"}';
		} else {
			echo '{"result":"no"}';
		}
	}


	// 로그아웃 요청하기
	function admin2002() {
		$db = $GLOBALS["db"];
		$admin_pk = $_REQUEST["admin_pk"];

		$_SESSION['admin_login'] = '';
		$_SESSION['admin_primarykey'] = 0;
		$_SESSION['admin_id'] = '';
		$_SESSION['admin_name'] = '';
		$_SESSION['authority'] = 0;
		$_SESSION['admin_ip'] = '';

		setLog($admin_pk, 100100, 100102, 0, 0, $_SERVER['REMOTE_ADDR']);

		echo '{"result":"ok"}';
	}

	// 총 회원 수 가져오기
	function admin2003() {
		$db = $GLOBALS["db"];

		$sql='SELECT `suhmember` FROM `suhMembers`';
		$res = $db->query($sql);
		$total_num = $res->num_rows;

		echo '{"result":"ok", "total_num":'.$total_num.'}';
	}

	// 오늘 올라온 1:1문의 수 가져오기
	function admin2004() {
		$db = $GLOBALS["db"];
		$today = date("Y-m-d");

		$sql='SELECT `suhq` FROM `suhQuestions` WHERE STR_TO_DATE(`question_datetime`, \'%Y-%m-%d\')=\''.$today.'\'';
		// echo $sql; exit;
		$res = $db->query($sql);
		$total_num = $res->num_rows;

		echo '{"result":"ok", "total_num":'.$total_num.'}';
	}

	// 오늘 진료예약 신청 수 가져오기
	function admin2005() {
		$db = $GLOBALS["db"];
		$today = date("Y-m-d");

		$sql='SELECT `suhcrh` FROM `suhCRH` WHERE STR_TO_DATE(`datetime`, \'%Y-%m-%d\')=\''.$today.'\'';
		// echo $sql; exit;
		$res = $db->query($sql);
		$total_num = $res->num_rows;

		echo '{"result":"ok", "total_num":'.$total_num.'}';
	}

	// 전체 진료과 수 가져오기
	function admin2006() {
		$db = $GLOBALS["db"];
		$today = date("Y-m-d");

		$sql='SELECT `suhd` FROM `suhDepartments` WHERE `status`=900400';
		// echo $sql; exit;
		$res = $db->query($sql);
		$total_num = $res->num_rows;

		echo '{"result":"ok", "total_num":'.$total_num.'}';
	}

	// 전체 의료진 수 가져오기
	function admin2007() {
		$db = $GLOBALS["db"];
		$today = date("Y-m-d");

		$sql='SELECT `doctor` FROM `suhDoctors` WHERE `status`!=800403';
		// echo $sql; exit;
		$res = $db->query($sql);
		$total_num = $res->num_rows;

		echo '{"result":"ok", "total_num":'.$total_num.'}';
	}

	// 오늘 순번대기표 수 가져오기
	function admin2008() {
		$db = $GLOBALS["db"];
		$today = date("Y-m-d");

		$sql='SELECT `suhrtl` FROM `suhReceptionTicketLogs` WHERE STR_TO_DATE(`datetime`, \'%Y-%m-%d\')=\''.$today.'\'';
		// echo $sql; exit;
		$res = $db->query($sql);
		$total_num = $res->num_rows;

		echo '{"result":"ok", "total_num":'.$total_num.'}';
	}	

	// 순번대기표 관리에 처음 들어왔을 때 각 접수처별로 현재번호 가져오기
	function admin3002() {
		$db = $GLOBALS["db"];
		$today = date("Y-m-d");

		// 본원의 1번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700201 
AND `counter_number`=1 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700201_counter_1_number_exist = 'ok';
			$h_700201_counter_1_number = $row->ticket_number;
			$h_700201_counter_1_number_pk = $row->suhrt;

		} else {
			$h_700201_counter_1_number_exist = 'no';
			$h_700201_counter_1_number = 0;
			$h_700201_counter_1_number_pk = 0;
		}




		// 본원의 2번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700201 
AND `counter_number`=2 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700201_counter_2_number_exist = 'ok';
			$h_700201_counter_2_number = $row->ticket_number;
			$h_700201_counter_2_number_pk = $row->suhrt;

		} else {
			$h_700201_counter_2_number_exist = 'no';
			$h_700201_counter_2_number = 0;
			$h_700201_counter_2_number_pk = 0;
		}





		// 본원의 3번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700201 
AND `counter_number`=3 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700201_counter_3_number_exist = 'ok';
			$h_700201_counter_3_number = $row->ticket_number;
			$h_700201_counter_3_number_pk = $row->suhrt;

		} else {
			$h_700201_counter_3_number_exist = 'no';
			$h_700201_counter_3_number = 0;
			$h_700201_counter_3_number_pk = 0;
		}





		// 어린이병원의 1번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700202 
AND `counter_number`=1 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700202_counter_1_number_exist = 'ok';
			$h_700202_counter_1_number = $row->ticket_number;
			$h_700202_counter_1_number_pk = $row->suhrt;

		} else {
			$h_700202_counter_1_number_exist = 'no';
			$h_700202_counter_1_number = 0;
			$h_700202_counter_1_number_pk = 0;
		}





		// 어린이병원의 2번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700202 
AND `counter_number`=2 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700202_counter_2_number_exist = 'ok';
			$h_700202_counter_2_number = $row->ticket_number;
			$h_700202_counter_2_number_pk = $row->suhrt;

		} else {
			$h_700202_counter_2_number_exist = 'no';
			$h_700202_counter_2_number = 0;
			$h_700202_counter_2_number_pk = 0;
		}






		// 어린이병원의 3번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700202 
AND `counter_number`=3 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700202_counter_3_number_exist = 'ok';
			$h_700202_counter_3_number = $row->ticket_number;
			$h_700202_counter_3_number_pk = $row->suhrt;

		} else {
			$h_700202_counter_3_number_exist = 'no';
			$h_700202_counter_3_number = 0;
			$h_700202_counter_3_number_pk = 0;
		}








		// 암병원의 1번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700203 
AND `counter_number`=1 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700203_counter_1_number_exist = 'ok';
			$h_700203_counter_1_number = $row->ticket_number;
			$h_700203_counter_1_number_pk = $row->suhrt;

		} else {
			$h_700203_counter_1_number_exist = 'no';
			$h_700203_counter_1_number = 0;
			$h_700203_counter_1_number_pk = 0;
		}






		// 암병원의 2번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700203 
AND `counter_number`=2 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700203_counter_2_number_exist = 'ok';
			$h_700203_counter_2_number = $row->ticket_number;
			$h_700203_counter_2_number_pk = $row->suhrt;

		} else {
			$h_700203_counter_2_number_exist = 'no';
			$h_700203_counter_2_number = 0;
			$h_700203_counter_2_number_pk = 0;
		}







		// 암병원의 3번 접수처 현재번호 가져오기
		$sql=<<<SQL
SELECT 
`suhrtc`, `suhrt`,`hospital_type`,`counter_number`,`ticket_number` 
FROM `suhReceptionTicketCounter`
WHERE `hospital_type`=700203 
AND `counter_number`=3 
AND STR_TO_DATE(`ticket_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d') 
AND `ticket_status`=700305 
ORDER BY `suhrtc` ASC 
LIMIT 1
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$h_700203_counter_3_number_exist = 'ok';
			$h_700203_counter_3_number = $row->ticket_number;
			$h_700203_counter_3_number_pk = $row->suhrt;

		} else {
			$h_700203_counter_3_number_exist = 'no';
			$h_700203_counter_3_number = 0;
			$h_700203_counter_3_number_pk = 0;
		}




		$jsondata=<<<JSON
{
	"h_700201_counter_1_number_exist":"{$h_700201_counter_1_number_exist}",
	"h_700201_counter_1_number":{$h_700201_counter_1_number},
	"h_700201_counter_1_number_pk":{$h_700201_counter_1_number_pk},
	"h_700201_counter_2_number_exist":"{$h_700201_counter_2_number_exist}",
	"h_700201_counter_2_number":{$h_700201_counter_2_number},
	"h_700201_counter_2_number_pk":{$h_700201_counter_2_number_pk},
	"h_700201_counter_3_number_exist":"{$h_700201_counter_3_number_exist}",
	"h_700201_counter_3_number":{$h_700201_counter_3_number},
	"h_700201_counter_3_number_pk":{$h_700201_counter_3_number_pk},
	"h_700202_counter_1_number_exist":"{$h_700202_counter_1_number_exist}",
	"h_700202_counter_1_number":{$h_700202_counter_1_number},
	"h_700202_counter_1_number_pk":{$h_700202_counter_1_number_pk},
	"h_700202_counter_2_number_exist":"{$h_700202_counter_2_number_exist}",
	"h_700202_counter_2_number":{$h_700202_counter_2_number},
	"h_700202_counter_2_number_pk":{$h_700202_counter_2_number_pk},
	"h_700202_counter_3_number_exist":"{$h_700202_counter_3_number_exist}",
	"h_700202_counter_3_number":{$h_700202_counter_3_number},
	"h_700202_counter_3_number_pk":{$h_700202_counter_3_number_pk},
	"h_700203_counter_1_number_exist":"{$h_700203_counter_1_number_exist}",
	"h_700203_counter_1_number":{$h_700203_counter_1_number},
	"h_700203_counter_1_number_pk":{$h_700203_counter_1_number_pk},
	"h_700203_counter_2_number_exist":"{$h_700203_counter_2_number_exist}",
	"h_700203_counter_2_number":{$h_700203_counter_2_number},
	"h_700203_counter_2_number_pk":{$h_700203_counter_2_number_pk},
	"h_700203_counter_3_number_exist":"{$h_700203_counter_3_number_exist}",
	"h_700203_counter_3_number":{$h_700203_counter_3_number},
	"h_700203_counter_3_number_pk":{$h_700203_counter_3_number_pk},

	"result":"ok"
}
JSON;

		echo $jsondata;
		exit;

	}

	// 해당 병원 접수처에 다음 번호 가져오기
	function admin3003() {
		$db = $GLOBALS["db"];
		$number_pk = $_REQUEST["number_pk"];
		$status_checked_value = $_REQUEST["status_checked_value"];
		$hospital_type = $_REQUEST["hospital_type"];
		$counter_number = $_REQUEST["counter_number"];

		$today = date("Y-m-d");

		if ($number_pk == 0) {

			$sql=<<<SQL
SELECT `suhrtl`,`datetime`,`hospital_type`,`get_ticket_number` 
FROM `suhReceptionTicketLogs`
WHERE STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d')
AND `hospital_type`={$hospital_type} 
AND `status`=700301 
ORDER BY `suhrtl` ASC
LIMIT 1
SQL;

			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$suhrtl = $row->suhrtl;
				$get_ticket_number = $row->get_ticket_number;
				$datetime = $row->datetime;


				$sql=<<<SQL
INSERT INTO `suhReceptionTicketCounter`
(`suhrt`,`hospital_type`,`counter_number`,`ticket_number`,`ticket_datetime`,`ticket_status`)VALUES
({$suhrtl}, {$hospital_type}, {$counter_number}, {$get_ticket_number}, '{$datetime}', 700305);

UPDATE `suhReceptionTicketLogs` 
SET `status`=700305 
WHERE `suhrtl`={$row->suhrtl}
SQL;
				$res = $db->multi_query($sql);


				$jsondata=<<<JSON
{
	"number_pk":{$suhrtl},
	"number":{$get_ticket_number},
	"result":"ok"
}
JSON;
				echo $jsondata;



			} else {
				echo '{"result":"no"}';
			}



		} else {

			$sql=<<<SQL
UPDATE `suhReceptionTicketCounter` 
SET `ticket_status`={$status_checked_value} 
WHERE `suhrt`={$number_pk} ;

UPDATE `suhReceptionTicketLogs` 
SET `status`={$status_checked_value} 
WHERE `suhrtl`={$number_pk} ;

SQL;
			$res = $db->multi_query($sql);

		

			$sql=<<<SQL
SELECT `suhrtl`,`datetime`,`hospital_type`,`get_ticket_number` 
FROM `suhReceptionTicketLogs`
WHERE STR_TO_DATE(`datetime`, '%Y-%m-%d')=STR_TO_DATE('{$today}', '%Y-%m-%d')
AND `hospital_type`={$hospital_type} 
AND `status`=700301 
ORDER BY `suhrtl` ASC
LIMIT 1
SQL;
			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$suhrtl = $row->suhrtl;
				$get_ticket_number = $row->get_ticket_number;
				$datetime = $row->datetime;


				$sql=<<<SQL
INSERT INTO `suhReceptionTicketCounter`
(`suhrt`,`hospital_type`,`counter_number`,`ticket_number`,`ticket_datetime`,`ticket_status`)VALUES
({$suhrtl}, {$hospital_type}, {$counter_number}, {$get_ticket_number}, '{$datetime}', 700305);

UPDATE `suhReceptionTicketLogs` 
SET `status`=700305 
WHERE `suhrtl`={$row->suhrtl}
SQL;
				$res = $db->multi_query($sql);


				$jsondata=<<<JSON
{
	"number_pk":{$suhrtl},
	"number":{$get_ticket_number},
	"result":"ok"
}
JSON;
				echo $jsondata;



			} else {
				echo '{"result":"no"}';
			}



		}

		
	}

	// 서버에서 대상 조건에 맞는 멤버 정보 및 pk array 가져오기
	function admin4002() {
		$db = $GLOBALS["db"];
		$whoValue = $_REQUEST["whoValue"];

		switch ($whoValue) {
			case 600401: // 전체
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600402: // 10대~20대
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND (TO_DAYS(now())-(TO_DAYS(`birthday`)))/365 >= 10
AND (TO_DAYS(now())-(TO_DAYS(`birthday`)))/365 < 30
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600403: // 20대~30대
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND (TO_DAYS(now())-(TO_DAYS(`birthday`)))/365 >= 20
AND (TO_DAYS(now())-(TO_DAYS(`birthday`)))/365 < 40
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600404: // 30대~40대
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND (TO_DAYS(now())-(TO_DAYS(`birthday`)))/365 >= 30
AND (TO_DAYS(now())-(TO_DAYS(`birthday`)))/365 < 50
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600405: // 40대 이상
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND (TO_DAYS(now())-(TO_DAYS(`birthday`)))/365 >= 40
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600406: // 혈액형이 O형인 사람
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND `theBloodType`=600101
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600407: // 혈액형이 A형인 사람
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND `theBloodType`=600102
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600408: // 혈액형이 B형인 사람
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND `theBloodType`=600103
AND LENGTH(`pushkey`)>20
SQL;
				break;
			case 600409: // 혈액형이 AB형인 사람
				$sql=<<<SQL
SELECT `suhmember`,`name`,`pushkey` FROM `suhMembers` 
WHERE `status`=500101 
AND `is_get_push`!=0 
AND `theBloodType`=600104
AND LENGTH(`pushkey`)>20
SQL;
				break;
			default:
				# code...
				break;
		}



		$res = $db->query($sql);
		$all_num = $res->num_rows;

		if ($all_num >= 1) {

			$jsondata = '{"data":[';
			while($row = $res->fetch_object()) {
				$member_pk = $row->suhmember;
				$member_name = $row->name;
				$member_pushkey = $row->pushkey;

				$jsondata.=<<<JSON
{
	"member_pk":{$member_pk},
	"member_name":"{$member_name}",
	"member_pushkey":"{$member_pushkey}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok", "all_num":'.$all_num.'}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
	}

	// 푸시 그룹 저장하기
	function admin4004() {
		$db = $GLOBALS["db"];
		$pushTitle = addslashes($_REQUEST["pushTitle"]);
		$pushContent = addslashes($_REQUEST["pushContent"]);
		$linkPage = $_REQUEST["linkPage"];
		$allNum = $_REQUEST["allNum"];

		$sql=<<<SQL
INSERT INTO `suhPushGroup`
(`title`,`content`,`link_page`,`find_member`)VALUES
('{$pushTitle}', '{$pushContent}', {$linkPage}, {$allNum})
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res) {

			$sql=<<<SQL
SELECT last_insert_id() AS `recent_pk`
SQL;
			$res = $db->query($sql);
			$row = $res->fetch_object();
			$push_group_pk = $row->recent_pk;

			echo '{"result":"ok", "push_group_pk":'.$push_group_pk.'}';

		} else {
			echo '{"result":"no"}';
		}
	}


	// 개개인씩 푸시알림 보내고 결과 받기를 반복하기
	function admin4006() {
		$db = $GLOBALS["db"];
		$member_pk = $_REQUEST["member_pk"];
		$member_name = $_REQUEST["member_name"];
		$member_pushkey = $_REQUEST["member_pushkey"];
		$pushTitle = $_REQUEST["pushTitle"];
		$pushContent = $_REQUEST["pushContent"];
		$linkPage = $_REQUEST["linkPage"];
		$push_group_pk = $_REQUEST["push_group_pk"];


		$arr = array();
        $arr['push_group_pk'] = $push_group_pk;
        $arr['linkPage'] = $linkPage;


        
        $tokens = array();
        $tokens[] = $member_pushkey;
        
        $arr['title'] = str_replace("<<회원>>",$member_name,$pushTitle);
        $arr['message'] = str_replace("<<회원>>",$member_name,$pushContent);



        $message_status = androidPush($tokens, $arr);
        // echo $message_status;

        // 푸시 전송 결과 반환.
        $obj = json_decode($message_status);
        

        $sql=<<<SQL
UPDATE `suhPushGroup` SET `try_push`=`try_push`+1 WHERE `suhpg`={$push_group_pk};
SQL;
		$res = $db->query($sql);

        // 푸시 전송시 성공 수량 반환.
        $cnt = $obj->{"success"};


        




        $stats_num = 0;
        if ($cnt >= 1) {
            $stats_num = 600202; // 푸쉬 처리됨
            
            $sql=<<<SQL
UPDATE `suhPushGroup` SET `success_push`=`success_push`+1 WHERE `suhpg`={$push_group_pk};
SQL;
			$res = $db->query($sql);

        } else {
            $stats_num = 600203; // 푸쉬 실패함
        }

        $aaa = addslashes($pushTitle);
        $bbb = addslashes($pushContent);

        $sql=<<<SQL
INSERT INTO `suhPushPersonal`
(`suhpg`,`member`,`title`,`content`,`status`)VALUES
({$push_group_pk}, {$member_pk}, '{$aaa}', '{$bbb}', {$stats_num})
SQL;
        $res = $db->query($sql);
        unset($tokens);
 
        echo '{"result":"ok"}';
	}
	function androidPush($tokens, $message) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $apiKey = "AAAAj2ZeeQY:APA91bGk4eoYIG7MzMcQ7n-dCxyPzBZ7Hx1e5K2FpIa1DnvIJ9JUtSDq3x_29z3iW_hRx8GsrIoWjxB6lJMPAbzo0Q-8T9H_aAjFsE7YO0rLLDDP7KSGCPJNN1h6T5o4J2Pnzt3hx6SV38z8SsRS5oqnmPZ7hbOfhQ";

        $fields = array('registration_ids' => $tokens,'data' => $message);
        $headers = array('Authorization:key='.$apiKey,'Content-Type: application/json');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    // 전체 회원 수 가져오기
    function admin5003() {
    	$db = $GLOBALS["db"];

    	$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers`
SQL;
		$res = $db->query($sql);

		echo '{"result":"ok", "total_member_num":'.$res->num_rows.'}';

    }

    // 회원 목록에서의 회원정보 수정하기
    function admin5006() {
    	$db = $GLOBALS["db"];
		$member_pk = $_REQUEST["member_pk"];
		$mamber_name = $_REQUEST["mamber_name"];
		$member_gender = $_REQUEST["member_gender"];
		$member_birthday = $_REQUEST["member_birthday"];
		$member_bloodtype = $_REQUEST["member_bloodtype"];
		$member_status = $_REQUEST["member_status"];

		$sql=<<<SQL
UPDATE `suhMembers` 
SET 
`name`='{$mamber_name}', 
`gender`={$member_gender}, 
`birthday`='{$member_birthday}', 
`theBloodType`={$member_bloodtype}, 
`status`={$member_status} 
WHERE `suhmember`={$member_pk}
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
    }	

    // 회원 목록 가져오기
    function admin5010() {
    	$db = $GLOBALS["db"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

		$memberGender = $_REQUEST["memberGender"];
		$memberBirthday = $_REQUEST["memberBirthday"];
		$memberBloodtype = $_REQUEST["memberBloodtype"];
		$memberName = $_REQUEST["memberName"];

		$start_index = $index * $view_num;

		
		$whereGender = '';
		if ($memberGender != 0) {
			$whereGender=<<<TEXT
AND `gender`={$memberGender} 
TEXT;
		}

		$whereBirthday = '';
		if ($memberBirthday != '') {
			$whereBirthday=<<<TEXT
AND STR_TO_DATE(`birthday`, '%Y-%m-%d')=STR_TO_DATE('{$memberBirthday}', '%Y-%m-%d') 
TEXT;
		}

		$whereBloodtype = '';
		if ($memberBloodtype != 0) {
			$whereBloodtype=<<<TEXT
AND `theBloodType`={$memberBloodtype} 
TEXT;
		}

		$whereName = '';
		if ($memberName != '') {
			$whereName=<<<TEXT
AND `name` LIKE '%{$memberName}%' 
TEXT;
		}


		$sql=<<<SQL
SELECT 
`suhmember`,
`name`,
`gender`,
`birthday`,
`theBloodType`,
`status` 
FROM `suhMembers` 
WHERE `suhmember`!=0 
{$whereGender} {$whereBirthday} {$whereBloodtype} {$whereName}
ORDER BY `suhmember` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhmember = $row->suhmember;
				$name = $row->name;
				$gender = $row->gender;
				$birthday = $row->birthday;
				$theBloodType = $row->theBloodType;
				$status = $row->status;

				$jsondata.=<<<JSON
{
	"suhmember":{$suhmember},
	"name":"{$name}",
	"gender":{$gender},
	"birthday":"{$birthday}",
	"theBloodType":{$theBloodType},
	"status":{$status}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<$page_num; $ii++) {
					if ($first_indexs % $page_num == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;


				$setPageNumPlusOne = ($view_num * $page_num) + 1;

				$sql=<<<SQL
SELECT 
`suhmember`,
`name`,
`gender`,
`birthday`,
`theBloodType`,
`status` 
FROM `suhMembers` 
WHERE `suhmember`!=0 
{$whereGender} {$whereBirthday} {$whereBloodtype} {$whereName}
ORDER BY `suhmember` DESC 
LIMIT {$first_indexs}, {$setPageNumPlusOne}
SQL;
				// echo $sql; exit;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == $setPageNumPlusOne) {
					$pagenum = $page_num;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/$view_num);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
    }

    // 회원 관리 리스트 - 페이지 맨 마지막 번호 알아내기
    function admin5016() {
    	$db = $GLOBALS["db"];
    	$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$memberGender = $_REQUEST["memberGender"];
		$memberBirthday = $_REQUEST["memberBirthday"];
		$memberBloodtype = $_REQUEST["memberBloodtype"];
		$memberName = $_REQUEST["memberName"];



		$whereGender = '';
		if ($memberGender != 0) {
			$whereGender=<<<TEXT
AND `gender`={$memberGender} 
TEXT;
		}

		$whereBirthday = '';
		if ($memberBirthday != '') {
			$whereBirthday=<<<TEXT
AND STR_TO_DATE(`birthday`, '%Y-%m-%d')=STR_TO_DATE('{$memberBirthday}', '%Y-%m-%d') 
TEXT;
		}

		$whereBloodtype = '';
		if ($memberBloodtype != 0) {
			$whereBloodtype=<<<TEXT
AND `theBloodType`={$memberBloodtype} 
TEXT;
		}

		$whereName = '';
		if ($memberName != '') {
			$whereName=<<<TEXT
AND `name` LIKE '%{$memberName}%' 
TEXT;
		}


		$sql=<<<SQL
SELECT 
`suhmember`,
`name`,
`gender`,
`birthday`,
`theBloodType`,
`status` 
FROM `suhMembers` 
WHERE `suhmember`!=0 
{$whereGender} {$whereBirthday} {$whereBloodtype} {$whereName}
ORDER BY `suhmember` DESC 
SQL;
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / $view_num) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<$page_num; $i++) {
			if ($first_index % $page_num == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
    }	

    // 회원 상세정보 가져오기
    function admin5110() {
    	$db = $GLOBALS["db"];
    	$pk = $_REQUEST["pk"];

    	$sql=<<<SQL
SELECT 
`suhMembers`.`name`,
`suhMembers`.`gender`,
`suhMembers`.`theMobileCompany`,
`suhMembers`.`phoneNumber`,
`suhMembers`.`email`,
`suhMembers`.`birthday`,
`suhMembers`.`theBloodType`,
`suhMembers`.`addr`,
`suhMembers`.`signUpMethod`,
`suhCodes1`.`description` AS `signUpMethodString`,
`suhMembers`.`signUpDate`,
`suhMembers`.`status`,
`suhMembers`.`is_get_push`  
FROM `suhMembers` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhMembers`.`signUpMethod`=`suhCodes1`.`codeNumber` 
WHERE `suhMembers`.`suhmember`={$pk}
SQL;
		$res = $db->query($sql);
		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$name = $row->name;
			$gender = $row->gender;
			$theMobileCompany = $row->theMobileCompany;
			$phoneNumber = $row->phoneNumber;
			$email = $row->email;
			$birthday = $row->birthday;
			$theBloodType = $row->theBloodType;
			$addr = $row->addr;
			$signUpMethod = $row->signUpMethod;
			$signUpMethodString = $row->signUpMethodString;
			$signUpDate = $row->signUpDate;
			$status = $row->status;
			$is_get_push = $row->is_get_push;

			$jsondata=<<<JSON
{
	"name":"{$name}",
	"gender":{$gender},
	"theMobileCompany":{$theMobileCompany},
	"phoneNumber":"{$phoneNumber}",
	"email":"{$email}",
	"birthday":"{$birthday}",
	"theBloodType":{$theBloodType},
	"addr":"{$addr}",
	"signUpMethod":{$signUpMethod},
	"signUpMethodString":"{$signUpMethodString}",
	"signUpDate":"{$signUpDate}",
	"status":{$status},
	"is_get_push":{$is_get_push},
	"result":"ok"
}
JSON;
			echo $jsondata;
	
		} else {
			echo '{"result":"no"}';
		}

    }

    // 서버에서 회원정보 수정하기
    function admin5112() {
    	$db = $GLOBALS["db"];
    	$pk = $_REQUEST["pk"];

    	$memberName = $_REQUEST["memberName"];
    	$memberGender = $_REQUEST["memberGender"];
    	$memberMobileCompany = $_REQUEST["memberMobileCompany"];
    	$memberPhoneNumber = $_REQUEST["memberPhoneNumber"];
    	$memberBirthday = $_REQUEST["memberBirthday"];
    	$memberBloodtype = $_REQUEST["memberBloodtype"];
    	$memberAddr = $_REQUEST["memberAddr"];
    	$memberStatus = $_REQUEST["memberStatus"];

    	$sql=<<<SQL
UPDATE `suhMembers` SET 
`name`='{$memberName}',
`gender`={$memberGender},
`theMobileCompany`={$memberMobileCompany},
`phoneNumber`='{$memberPhoneNumber}',
`birthday`='{$memberBirthday}',
`theBloodType`={$memberBloodtype},
`addr`='{$memberAddr}',
`status`={$memberStatus} 
WHERE `suhmember`={$pk}
SQL;
		$res = $db->query($sql);
		if ($res) {
			echo '
				<script>
					alert("회원정보가 수정되었습니다.");
					location.href = "./admin5000.php";
				</script>
			';
			exit;
		} else {
			echo '
				<script>
					alert("회원정보 수정중에 오류가 발생하였습니다.");
					history.back(1);
				</script>
			';
			exit;
		}

    }

    // 의료진 전체 리스트 가져오기
    function admin6005() {
    	$db = $GLOBALS["db"];

    	$sql=<<<SQL
SELECT 
`suhDoctors`.`doctor`,
`suhDoctors`.`name`,
`suhDoctors`.`affiliation_department`, 
`suhDepartments1`.`department` AS `affiliation_department_string` 
FROM `suhDoctors` 
LEFT JOIN `suhDepartments` AS `suhDepartments1` 
ON `suhDoctors`.`affiliation_department`=`suhDepartments1`.`suhd` 
ORDER BY `suhDoctors`.`name` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$doctor_pk = $row->doctor;
				$doctor_name = htmlspecialchars($row->name);
				$doctor_department = $row->affiliation_department;
				$doctor_department_string = $row->affiliation_department_string;

				$jsondata.=<<<JSON
{
	"doctor_pk":{$doctor_pk},
	"doctor_name":"{$doctor_name}",
	"doctor_department":{$doctor_department},
	"doctor_department_string":"{$doctor_department_string}"
},
JSON;
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
    }

    // 미방문자 업데이트 하기
    function admin6102() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];


    	$sql=<<<SQL
UPDATE `suhCRH` SET `status`=500204 
WHERE `status`=500201 
AND `hope_datetime`<STR_TO_DATE(now(), '%Y-%m-%d %H:%i:%s') 
AND `doctor`={$doctor_pk} 
SQL;
		$res = $db->query($sql);

		echo '{"result":"ok"}';

    }

    // 선택된 날짜/시간을 의료진 스케쥴에 추가하기
    function admin6107() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];
    	$selected_year = $_REQUEST["selected_year"];
    	$selected_month = $_REQUEST["selected_month"];
    	$selected_date = $_REQUEST["selected_date"];
    	$selected_time = $_REQUEST["selected_time"];

    	$full_date = $selected_year.'-'.attachZero($selected_month).'-'.attachZero($selected_date).' '.$selected_time.':00';

    	// echo $full_date; exit;

    	$sql=<<<SQL
SELECT `suhds` FROM `suhDoctorSchedule` WHERE `posible_datetime`='{$full_date}'
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			echo '{"result":"already"}';
			exit;
		}





    	$sql=<<<SQL
INSERT INTO `suhDoctorSchedule`
(`doctor`,`posible_datetime`)VALUES
({$doctor_pk}, '{$full_date}')
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
    }

    // 해당 날짜에 해당하는 의료진 스케쥴 불러오기
    function admin6110() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];
    	$selected_year = $_REQUEST["selected_year"];
    	$selected_month = $_REQUEST["selected_month"];
    	$selected_date = $_REQUEST["selected_date"];

    	$full_date = $selected_year.'-'.attachZero($selected_month).'-'.attachZero($selected_date);

    	$sql=<<<SQL
SELECT 
`suhDoctorSchedule`.`suhds`, 
`suhDoctorSchedule`.`posible_datetime`, 
`suhDoctorSchedule`.`see_a_doctor_member`,
`suhDoctorSchedule`.`see_a_doctor_member_status` 
FROM `suhDoctorSchedule` 
WHERE STR_TO_DATE(`suhDoctorSchedule`.`posible_datetime`, '%Y-%m-%d')=STR_TO_DATE('{$full_date}', '%Y-%m-%d') 
AND `suhDoctorSchedule`.`doctor`={$doctor_pk} 
ORDER BY `suhDoctorSchedule`.`posible_datetime` ASC
SQL;
		

		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';


			$ppp = 0;

			// echo 'num_rows = '.$res->num_rows;
			$is_reservation = '';
			while ($row = $res->fetch_object()) {
				$suhds = $row->suhds;
				$posible_datetime = $row->posible_datetime; // ex) 2018-07-09 14:30:00 
				$see_a_doctor_member = $row->see_a_doctor_member;
				$see_a_doctor_member_status = $row->see_a_doctor_member_status;


				$hour = dateDivide($posible_datetime, 'h');
				$minute = dateDivide($posible_datetime, 'i');



				if ($see_a_doctor_member == 0) {
					$sql=<<<SQL
SELECT 
`suhCRH`.`member`,
`suhCRH`,`hope_datetime`,
`suhMembers1`.`name` AS `member_name`,
`suhMembers1`.`phoneNumber` AS `member_phoneNumber`,
`suhCRH`.`status` 
FROM `suhCRH` 
LEFT JOIN `suhMembers` AS `suhMembers1` 
ON `suhCRH`.`member`=`suhMembers1`.`suhmember` 
WHERE `suhCRH`.`hope_datetime`='{$posible_datetime}'
AND `suhCRH`.`doctor`={$doctor_pk}
AND (`suhCRH`.`status`=500201 OR `suhCRH`.`status`=500204)
ORDER BY `suhCRH`.`suhcrh` DESC LIMIT 1
SQL;
					// echo $sql;
					
					$res2 = $db->query($sql);

					if ($res2->num_rows >= 1) {
						$row2 = $res2->fetch_object();
						$is_reservation = 'ok';

						if ($row2->member == '') {
							$member_pk = 0;
						} else {
							$member_pk = $row2->member;
						}

						
						$member_name = $row2->member_name;
						$member_phoneNumber = $row2->member_phoneNumber;
						$member_status = $row2->status;

						if ($member_status == 500204) {
							$is_reservation = 'end';
						}
					} else {
						$is_reservation = 'no';
						$member_pk = 0;
						$member_name = '';
						$member_phoneNumber = '';
						$member_status = 0;
					}
				} else {

					$sql=<<<SQL
SELECT 
`name`, `phoneNumber` 
FROM `suhMembers` 
WHERE `suhmember`={$see_a_doctor_member} 
SQL;
					$res2 = $db->query($sql);
					$row2 = $res2->fetch_object();

					$is_reservation = 'end';
					$member_pk = $see_a_doctor_member;
					$member_name = $row2->name;
					$member_phoneNumber = $row2->phoneNumber;
					$member_status = $see_a_doctor_member_status;
				}


				
				$jsondata.=<<<JSON
{
	"suhds":{$suhds},
	"posible_datetime":"{$posible_datetime}",
	"hour":{$hour},
	"minute":{$minute},
	"member_pk":{$member_pk},
	"member_name":"{$member_name}",
	"member_phone":"{$member_phoneNumber}",
	"member_status":{$member_status},
	"is_reservation":"{$is_reservation}"
},
JSON;
				$ppp++;
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}

    }

    // 의료진 해당 시간 스케쥴 삭제하기
    function admin6113() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];
    	$fullDate = $_REQUEST["fullDate"];
    	$fullTime = $_REQUEST["fullTime"];

    	$fulldatetime = $fullDate.' '.$fullTime;
    	// echo 'fulldatetime = '.$fulldatetime;

    	$sql=<<<SQL
DELETE FROM `suhDoctorSchedule` WHERE `posible_datetime`='{$fulldatetime}' AND `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
    }

    // 증상 정보 가져오기
    function admin6114() {
    	$db = $GLOBALS["db"];
    	$member_pk = $_REQUEST["member_pk"];
    	$schedule_pk = $_REQUEST["schedule_pk"];

    	$sql=<<<SQL
SELECT `posible_datetime`, `doctor` FROM `suhDoctorSchedule` WHERE `suhds`={$schedule_pk}
SQL;
		$res = $db->query($sql);
		$row = $res->fetch_object();
		
		$posible_datetime = $row->posible_datetime;
		$doctor = $row->doctor;

		$sql=<<<SQL
SELECT `symptom` FROM `suhCRH` 
WHERE `member`={$member_pk} 
AND `hope_datetime`='{$posible_datetime}' 
AND `doctor`={$doctor} 
SQL;
		$res = $db->query($sql);
		$row = $res->fetch_object();
		$symptom = setCleanText($row->symptom, 'html');

		echo '{"result":"ok", "symptom":"'.$symptom.'"}';
    }

    // 처방약품 대분류 리스트 가져오기
    function admin6115() {
    	$db = $GLOBALS["db"];

    	$sql=<<<SQL
SELECT `suhdbt`, `description` FROM `suhDrugBigType` ORDER BY `suhdbt` ASC 
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$suhdbt = $row->suhdbt;
				$description = $row->description;

				$jsondata.=<<<JSON
{
	"suhdbt":{$suhdbt},
	"description":"{$description}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
    }

    // 대분류 선택된 값에 따라 다음 소분류 리스트 가져오기
    function admin6117() {
    	$db = $GLOBALS["db"];
    	$resultDrug1Value = $_REQUEST["resultDrug1Value"];

    	$sql=<<<SQL
SELECT `suhdst`,`description` FROM `suhDrugSmallType` WHERE `suhdbt`={$resultDrug1Value}
ORDER BY `suhdst` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$suhdst = $row->suhdst;
				$description = $row->description;

				$jsondata.=<<<JSON
{
	"suhdst":{$suhdst},
	"description":"{$description}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"ok"}';
		}
    }

    // 소분류 선택된 값에 따라 다음 약품 리스트 가져오기
    function admin6118() {
    	$db = $GLOBALS["db"];
    	$resultDrug1Value = $_REQUEST["resultDrug1Value"];
    	$resultDrug2Value = $_REQUEST["resultDrug2Value"];

    	$sql=<<<SQL
SELECT `suhdl`, `title` FROM `suhDrugList` WHERE `suhdbt`={$resultDrug1Value} AND `suhdst`={$resultDrug2Value}
ORDER BY `suhdl` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$suhdl = $row->suhdl;
				$title = $row->title;

				$jsondata.=<<<JSON
{
	"suhdl":{$suhdl},
	"title":"{$title}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"ok"}';
		}
    }

    // 진단서 내용 서버에 저장하기
    function admin6131() {
    	$db = $GLOBALS["db"];
    	$schedule_pk = $_REQUEST["schedule_pk"];
		$member_pk = $_REQUEST["member_pk"];
		$doctor_pk = $_REQUEST["doctor_pk"];
		$doctor_departmnet_pk = $_REQUEST["doctor_departmnet_pk"];
		$doctor_opinion = addslashes($_REQUEST["doctor_opinion"]);

		$drug_pk_array = $_REQUEST["drug_pk_array"];
		$drug_note_array = $_REQUEST["drug_note_array"];
		$drug_oneday_dose_array = $_REQUEST["drug_oneday_dose_array"];
		$oneday_number_array = $_REQUEST["oneday_number_array"];
		$dose_day_num_array = $_REQUEST["dose_day_num_array"];



		// 의료진 스케쥴 pk의 날짜 가져오기
		$sql=<<<SQL
SELECT `posible_datetime` FROM `suhDoctorSchedule` WHERE `suhds`={$schedule_pk}
SQL;
		$res = $db->query($sql);
		$row = $res->fetch_object();
		$schedule_pk_datetime = $row->posible_datetime;




		$sql=<<<SQL
INSERT INTO `suhMedicalCertificate`
(`member`,`doctor`,`department`,`doctor_comment`)VALUES
({$member_pk}, {$doctor_pk}, {$doctor_departmnet_pk}, '{$doctor_opinion}')
SQL;
		$res = $db->query($sql);

		if ($res) {

			$sql=<<<SQL
SELECT last_insert_id() AS `suhmc`
SQL;
			$res = $db->query($sql);
			$row = $res->fetch_object();
			$suhmc = $row->suhmc; // 진단서 pk


			for ($i=0; $i<count($drug_pk_array); $i++) {
				$drug_pk = $drug_pk_array[$i];
				$drug_note = addslashes($drug_note_array[$i]);
				$drug_oneday_dose = $drug_oneday_dose_array[$i];
				$oneday_number = $oneday_number_array[$i];
				$dose_day_num = $dose_day_num_array[$i];


				$sql=<<<SQL
INSERT INTO `suhPrescriptionDrug`
(`suhmc`,`drug_code`,`oneday_dose`,`oneday_number`,`dose_day_num`,`note`)VALUES
({$suhmc}, {$drug_pk}, {$drug_oneday_dose}, {$oneday_number}, {$dose_day_num}, '{$drug_note}');
SQL;
				$res = $db->query($sql);
			}

			$sql=<<<SQL
UPDATE `suhDoctorSchedule` SET `see_a_doctor_member`={$member_pk}, `see_a_doctor_member_status`=500202, `suhmc`={$suhmc}  
WHERE `suhds`={$schedule_pk};

UPDATE `suhCRH` SET `status`=500202 
WHERE `member`={$member_pk} 
AND `doctor`={$doctor_pk} 
AND `department`={$doctor_departmnet_pk} 
AND `hope_datetime`='{$schedule_pk_datetime}';
SQL;
			
			$res = $db->multi_query($sql);

			if ($res) {
				echo '{"result":"ok"}';
			} else {
				echo '{"result":"error"}';
			}


		} else {
			echo '{"result":"error"}';
			exit;
		}

    }

    // 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
    function admin6507() {
    	$db = $GLOBALS["db"];
    	$delete_pks = $_REQUEST["delete_pks"];

    	for ($i=0; $i<count($delete_pks); $i++) {
    		$pk = $delete_pks[$i];
    		$sql=<<<SQL
UPDATE `suhNMedicalInformation` SET `status`=900300 WHERE `suhnmi`={$pk}
SQL;
			$res = $db->query($sql);
    	}

    	echo '{"result":"ok"}';
    }

    // N의학정보 리스트 가져오기
    function admin6510() {
    	$db = $GLOBALS["db"];
    	$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

		$title_search_type = $_REQUEST["title_search_type"];
    	$title = $_REQUEST["title"];
    	$first_word = (int) $_REQUEST["first_word"];





    	$search_filter_array = array('','ㄱ','ㄴ','ㄷ','ㄹ','ㅁ','ㅂ','ㅅ','ㅇ','ㅈ','ㅊ','ㅋ','ㅌ','ㅍ','ㅎ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$search_filter_array2 = array('', '(ㄱ|ㄲ)', 'ㄴ', '(ㄷ|ㄸ)', 'ㄹ', 'ㅁ', 'ㅂ', '(ㅅ|ㅆ)', 'ㅇ', '(ㅈ|ㅉ)', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ');
		$search_filter_array3 = array('', '가', '나', '다', '라', '마', '바', '사', '아', '자', '차', '카', '타', '파', '하', 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');


    	$start_index = $index * $view_num;


    	if ($title_search_type == 2) {
    		$search_goal = 'title_en';
    	} else {
    		$search_goal = 'title_ko';
    	}




    	$whereTitle = '';
    	if ($title != '') {
    		$whereTitle=<<<SQL
AND `suhNMedicalInformation`.`{$search_goal}` LIKE '%{$title}%' 
SQL;
    	}





    	$whereFirstWord = '';
    	
    	if ($first_word == 0) {

		} else if ($first_word >= 1 && $first_word < 14) {
			$whereFirstWord=<<<TEXT
AND (`{$search_goal}` RLIKE '^{$search_filter_array2[$first_word]}' OR ( `{$search_goal}` >= '{$search_filter_array3[$first_word]}' AND `{$search_goal}` < '{$search_filter_array3[$first_word+1]}' ))
TEXT;
		} else if ($first_word == 14) {
			$whereFirstWord=<<<TEXT
AND (`{$search_goal}` RLIKE '^{$search_filter_array2[$first_word]}' OR ( `{$search_goal}` >= '{$search_filter_array3[$first_word]}'))
TEXT;
		} else if ($first_word >= 15 && $first_word < 41) {
			$whereFirstWord=<<<TEXT
AND (`{$search_goal}` LIKE '{$search_filter_array[$first_word]}%' OR `{$search_goal}` LIKE '{$search_filter_array3[$first_word]}%')
TEXT;
		} else {

		}



    	$sql=<<<SQL
SELECT 
`suhnmi`, 
`title_ko`, 
`title_en`, 
`one_line_description`
FROM `suhNMedicalInformation` 
WHERE `status`=900400 
{$whereTitle}
{$whereFirstWord}
ORDER BY `suhnmi` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhnmi = $row->suhnmi;
				$title_ko = setCleanText($row->title_ko, 'html');
				$title_en = setCleanText($row->title_en, 'html');
				$one_line_description = setCleanText($row->one_line_description, 'html');
				

				$jsondata.=<<<JSON
{
	"suhnmi":{$suhnmi},
	"title_ko":"{$title_ko}",
	"title_en":"{$title_en}",
	"one_line_description":"{$one_line_description}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<$page_num; $ii++) {
					if ($first_indexs % $page_num == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;



				$one_set_num_plus = ($page_num * $view_num) + 1;



				$sql=<<<SQL
SELECT 
`suhnmi`, 
`title_ko`, 
`title_en`, 
`one_line_description`
FROM `suhNMedicalInformation` 
WHERE `status`=900400 
{$whereTitle}
{$whereFirstWord}
ORDER BY `suhnmi` DESC 
LIMIT {$first_indexs}, {$one_set_num_plus}
SQL;

				// echo $sql; exit;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46



				if ($record == $one_set_num_plus) {
					$pagenum = $page_num;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/$view_num);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}

    }

    // N의학정보 페이징 - 페이지 맨 마지막 번호 알아내기
    function admin6516() {
    	$db = $GLOBALS["db"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

		$title_search_type = $_REQUEST["title_search_type"];
    	$title = $_REQUEST["title"];
    	$first_word = $_REQUEST["first_word"];


    	$search_filter_array = array('','ㄱ','ㄴ','ㄷ','ㄹ','ㅁ','ㅂ','ㅅ','ㅇ','ㅈ','ㅊ','ㅋ','ㅌ','ㅍ','ㅎ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$search_filter_array2 = array('', '(ㄱ|ㄲ)', 'ㄴ', '(ㄷ|ㄸ)', 'ㄹ', 'ㅁ', 'ㅂ', '(ㅅ|ㅆ)', 'ㅇ', '(ㅈ|ㅉ)', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ');
		$search_filter_array3 = array('', '가', '나', '다', '라', '마', '바', '사', '아', '자', '차', '카', '타', '파', '하', 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');


    	$start_index = $index * $view_num;


    	$whereTitle = '';
    	if ($title != '') {
    		$whereTitle=<<<SQL
AND `suhNMedicalInformation`.`title_ko` LIKE '%{$title}%' 
AND `suhNMedicalInformation`.`title_en` LIKE '%{$title}%' 
SQL;
    	}





    	$whereFirstWord = '';
    	if ($title_search_type == 2) {
    		$search_goal = 'title_en';
    	} else {
    		$search_goal = 'title_ko';
    	}


    	if ($search_filter == 0) {

		} else if ($search_filter >= 1 && $search_filter < 14) {
			$whereFirstWord=<<<TEXT
AND (`{$search_goal}` RLIKE '^{$search_filter_array2[$search_filter]}' OR ( `{$search_goal}` >= '{$search_filter_array3[$search_filter]}' AND `{$search_goal}` < '{$search_filter_array3[$search_filter+1]}' ))
TEXT;
		} else if ($search_filter == 14) {
			$whereFirstWord=<<<TEXT
AND (`{$search_goal}` RLIKE '^{$search_filter_array2[$search_filter]}' OR ( `{$search_goal}` >= '{$search_filter_array3[$search_filter]}'))
TEXT;
		} else if ($search_filter >= 15 && $search_filter < 41) {
			$whereFirstWord=<<<TEXT
AND (`{$search_goal}` LIKE '{$search_filter_array[$search_filter]}%' OR `{$search_goal}` LIKE '{$search_filter_array3[$search_filter]}%')
TEXT;
		} else {

		}



    	$sql=<<<SQL
SELECT 
`suhnmi`, 
`title_ko`, 
`title_en`, 
`one_line_description`
FROM `suhNMedicalInformation` 
WHERE `status`=900400 
{$whereTitle}
{$whereFirstWord}
ORDER BY `title_ko` ASC 
SQL;
		// echo $sql; exit;
		
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / $view_num) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<$page_num; $i++) {
			if ($first_index % $page_num == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
    }

    // 진료과 리스트 불러오기
    function admin6601() {
    	$db = $GLOBALS["db"];

    	$sql=<<<SQL
SELECT `suhd`,`department` FROM `suhDepartments` ORDER BY `department` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';
			while ($row = $res->fetch_object()) {
				$suhd = $row->suhd;	
				$department = $row->department;

				$jsondata.=<<<JSON
{
	"suhd":{$suhd},
	"department":"{$department}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
    }

    // N의학정보 서버에 업로드 하기
    function admin6615() {
    	$db = $GLOBALS["db"];
    	$titleKo = addslashes($_REQUEST["titleKo"]); // 의학정보명(한글)
		$titleEn = addslashes($_REQUEST["titleEn"]); // 의학정보명(영어)
		$oneDescription = addslashes($_REQUEST["oneDescription"]); // 한 줄 설명
		$chainDepartments = $_REQUEST["chainDepartments"]; // 관련진료과

		$definition = addslashes($_REQUEST["definition"]); // 정의 
		$symptom = addslashes($_REQUEST["symptom"]); // 증상
		$cause = addslashes($_REQUEST["cause"]); // 원인
		$chainBody = addslashes($_REQUEST["chainBody"]); // 관련 신체기관
		$diagnosis = addslashes($_REQUEST["diagnosis"]); // 진단
		$check = addslashes($_REQUEST["check"]); // 검사
		$cure = addslashes($_REQUEST["cure"]); // 치료
		$hyperplasia = addslashes($_REQUEST["hyperplasia"]); // 경과/합병증
		$prevent = addslashes($_REQUEST["prevent"]); // 예방방법
		$guide = addslashes($_REQUEST["guide"]); // 생활가이드
		$checkPeriod = addslashes($_REQUEST["checkPeriod"]); // 검사주기
		$checkLeadTime = addslashes($_REQUEST["checkLeadTime"]); // 소요시간
		$chainCheckWay = addslashes($_REQUEST["chainCheckWay"]); // 관련검사법


		$sql=<<<SQL
INSERT INTO `suhNMedicalInformation`
(`title_ko`, `title_en`, `one_line_description`, `chain_department`)VALUES
('{$titleKo}', '{$titleEn}', '{$oneDescription}', '{$chainDepartments}')
SQL;
		$res = $db->query($sql);

		// 삽입 성공 했으면
		if ($res) {
			$sql=<<<SQL
SELECT LAST_INSERT_ID() AS `recent_pk`
SQL;
			$res = $db->query($sql);
			$row = $res->fetch_object();
			$recent_pk = $row->recent_pk;


			$sql = '';
			$ii = 0;

			// 정의
			if ($definition != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700401, '{$definition}');

SQL;
				$ii++;
			}

			// 증상
			if ($symptom != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700402, '{$symptom}');

SQL;
				$ii++;
			}

			// 원인
			if ($cause != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700403, '{$cause}');

SQL;
				$ii++;
			}

			// 관련신체기관
			if ($chainBody != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700404, '{$chainBody}');

SQL;
				$ii++;
			}

			// 진단
			if ($diagnosis != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700405, '{$diagnosis}');

SQL;
				$ii++;
			}

			// 검사
			if ($check != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700406, '{$check}');

SQL;
				$ii++;
			}

			// 치료
			if ($cure != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700407, '{$cure}');

SQL;
				$ii++;
			}

			// 경과/합병증
			if ($hyperplasia != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700408, '{$hyperplasia}');

SQL;
				$ii++;
			}

			// 예방방법
			if ($prevent != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700409, '{$prevent}');

SQL;
				$ii++;
			}

			// 생활가이드
			if ($guide != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700410, '{$guide}');

SQL;
				$ii++;
			}

			// 검사주기
			if ($checkPeriod != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700411, '{$checkPeriod}');

SQL;
				$ii++;
			}

			// 소요시간
			if ($checkLeadTime != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700412, '{$checkLeadTime}');

SQL;
				$ii++;
			}

			// 관련검사법
			if ($chainCheckWay != '') {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`, `value_type`, `value`)VALUES
({$recent_pk}, 700413, '{$chainCheckWay}');

SQL;
				$ii++;
			}


			if ($ii >= 1) {
				$res = $db->multi_query($sql);

				if ($res) {
					echo '{"result":"ok"}';
				} else {
					echo '{"result":"no"}';
				}
			} else {
				echo '{"result":"ok"}';
			}





		} else {
			echo '{"result":"no"}';
		}
			


    }

    // N의학정보 상세정보 가져오기
    function admin6715() {
    	$db = $GLOBALS["db"];
    	$nmi = $_REQUEST["nmi"];

    	$sql=<<<SQL
SELECT 
`title_ko`,
`title_en`, 
`one_line_description`, 
`chain_department` 
FROM `suhNMedicalInformation` WHERE `suhnmi`={$nmi} AND `status`=900400
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$title_ko = setCleanText($row->title_ko, 'textarea'); 
			$title_en = setCleanText($row->title_en, 'textarea');	
			$one_line_description = setCleanText($row->one_line_description, 'textarea');
			$chain_department = setCleanText($row->chain_department, 'textarea');



			$sql=<<<SQL
SELECT `value_type`, `value`  
FROM `suhNMedicalInformationValue` WHERE `suhnmi`={$nmi}
SQL;
			$res = $db->query($sql);

			$definition_value = ''; // 700401
			$symptom_value = ''; // 700402
			$cause_value = ''; // 700403
			$chainBody_value = ''; // 700404
			$diagnosis_value = ''; // 700405
			$check_value = ''; // 700406
			$cure_value = ''; // 700407
			$hyperplasia_value = ''; // 700408
			$prevent_value = ''; // 700409
			$guide_value = ''; // 700410
			$checkPeriod_value = ''; // 700411
			$checkLeadTime_value = ''; // 700412
			$chainCheckWay_value = ''; // 700413


			if ($res->num_rows >= 1) {

				while ($row = $res->fetch_object()) {
					$value_type = $row->value_type;
					$value = $row->value;
					switch ($value_type) {
						case 700401:
							$definition_value = setCleanText($value, 'textarea');
							break;
						case 700402:
							$symptom_value = setCleanText($value, 'textarea');
							break;
						case 700403:
							$cause_value = setCleanText($value, 'textarea');
							break;
						case 700404:
							$chainBody_value = setCleanText($value, 'textarea');
							break;
						case 700405:
							$diagnosis_value = setCleanText($value, 'textarea');
							break;
						case 700406:
							$check_value = setCleanText($value, 'textarea');
							break;
						case 700407:
							$cure_value = setCleanText($value, 'textarea');
							break;
						case 700408:
							$hyperplasia_value = setCleanText($value, 'textarea');
							break;
						case 700409:
							$prevent_value = setCleanText($value, 'textarea');
							break;
						case 700410:
							$guide_value = setCleanText($value, 'textarea');
							break;
						case 700411:
							$checkPeriod_value = setCleanText($value, 'textarea');
							break;
						case 700412:
							$checkLeadTime_value = setCleanText($value, 'textarea');
							break;
						case 700413:
							$chainCheckWay_value = setCleanText($value, 'textarea');
							break;
						
						default:
							
							break;
					}
				}
			} 




			$jsondata=<<<JSON
{
	"title_ko":"{$title_ko}",
	"title_en":"{$title_en}",
	"one_line_description":"{$one_line_description}",
	"chain_department":"{$chain_department}",
	"definition_value":"{$definition_value}",
	"symptom_value":"{$symptom_value}",
	"cause_value":"{$cause_value}",
	"chainBody_value":"{$chainBody_value}",
	"diagnosis_value":"{$diagnosis_value}",
	"check_value":"{$check_value}",
	"cure_value":"{$cure_value}",
	"hyperplasia_value":"{$hyperplasia_value}",
	"prevent_value":"{$prevent_value}",
	"guide_value":"{$guide_value}",
	"checkPeriod_value":"{$checkPeriod_value}",
	"checkLeadTime_value":"{$checkLeadTime_value}",
	"chainCheckWay_value":"{$chainCheckWay_value}",
	"result":"ok"
}
JSON;

			echo $jsondata;





		} else {
			echo '{"result":"no"}';
		}
    }

    // N의학정보 서버에 업데이트 요청하기
    function admin6716() {
    	$db = $GLOBALS["db"];
    	$nmi = $_REQUEST["nmi"];

    	$titleKo = addslashes($_REQUEST["titleKo"]); // 의학정보명(한글)
		$titleEn = addslashes($_REQUEST["titleEn"]); // 의학정보명(영어)
		$oneDescription = addslashes($_REQUEST["oneDescription"]); // 한 줄 설명
		$chainDepartments = addslashes($_REQUEST["chainDepartments"]); // 관련진료과

		$definition = addslashes($_REQUEST["definition"]); // 정의 
		$symptom = addslashes($_REQUEST["symptom"]); // 증상
		$cause = addslashes($_REQUEST["cause"]); // 원인
		$chainBody = addslashes($_REQUEST["chainBody"]); // 관련 신체기관
		$diagnosis = addslashes($_REQUEST["diagnosis"]); // 진단
		$check = addslashes($_REQUEST["check"]); // 검사
		$cure = addslashes($_REQUEST["cure"]); // 치료
		$hyperplasia = addslashes($_REQUEST["hyperplasia"]); // 경과/합병증
		$prevent = addslashes($_REQUEST["prevent"]); // 예방방법
		$guide = addslashes($_REQUEST["guide"]); // 생활가이드
		$checkPeriod = addslashes($_REQUEST["checkPeriod"]); // 검사주기
		$checkLeadTime = addslashes($_REQUEST["checkLeadTime"]); // 소요시간
		$chainCheckWay = addslashes($_REQUEST["chainCheckWay"]); // 관련검사법


		
		$sql=<<<SQL
UPDATE `suhNMedicalInformation` SET 
`title_ko`='{$titleKo}', 
`title_en`='{$titleEn}', 
`one_line_description`='{$oneDescription}', 
`one_line_description`='{$oneDescription}', 
`chain_department`='{$chainDepartments}' 
WHERE `suhnmi`={$nmi} 
SQL;
		$res = $db->query($sql);



		$exist_value_type_array = array();
		$sql=<<<SQL
SELECT `value_type` FROM `suhNMedicalInformationValue` WHERE `suhnmi`={$nmi}
SQL;
		$res = $db->query($sql);
		if ($res->num_rows >= 1) {
			while ($row = $res->fetch_object()) {
				$exist_value_type_array[] = $row->value_type;
			}
		}



		$sql='';
		$value_type_array = array(700401,700402,700403,700404,700405,700406,700407,700408,700409,700410,700411,700412,700413);
		$value_array = array($definition, $symptom, $cause, $chainBody, $diagnosis, $check, $cure, $hyperplasia, $prevent, $guide, $checkPeriod, $checkLeadTime, $chainCheckWay);
		
		for ($i=0; $i<count($value_type_array); $i++) {
			if (in_array($value_type_array[$i], $exist_value_type_array)) {
				$sql.=<<<SQL
UPDATE `suhNMedicalInformationValue` 
SET `value`='{$value_array[$i]}' 
WHERE `suhnmi`={$nmi} AND `value_type`={$value_type_array[$i]};

SQL;
			} else {
				$sql.=<<<SQL
INSERT INTO `suhNMedicalInformationValue`
(`suhnmi`,`value_type`,`value`)VALUES
({$nmi}, {$value_type_array[$i]}, '{$value_array[$i]}');

SQL;
			}
		}

		$res = $db->multi_query($sql);
		
		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}





		
    }

    // 관리자 회원 리스트 가져오기
    function admin7002() {
    	$db = $GLOBALS["db"];

    	$sql=<<<SQL
		}
SELECT `suhadmin`,`name`,`id` FROM `suhAdmin` ORDER BY `suhadmin` DESC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhadmin = $row->suhadmin;
				$name = $row->name;
				$id = $row->id;

				$jsondata.=<<<JSON
{
	"suhadmin":{$suhadmin},
	"name":"{$name}",
	"id":"{$id}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
    }

    // 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
    function admin7007() {
    	$db = $GLOBALS["db"];
    	$delete_pks = $_REQUEST["delete_pks"];

    	for ($i=0; $i<count($delete_pks); $i++) {
    		$pk = $delete_pks[$i];
    		$sql=<<<SQL
UPDATE `suhNews` SET `status`=900300 WHERE `suhnews`={$pk}
SQL;
			$res = $db->query($sql);
    	}

    	echo '{"result":"ok"}';
    }

   	// 병원뉴스 리스트 가져오기
    function admin7010() {
    	$db = $GLOBALS["db"];
    	$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		
		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$writer = $_REQUEST["writer"];
		$title = $_REQUEST["title"];
		$content = $_REQUEST["content"];
 

		$start_index = $index * $view_num;


		$whereDate = '';
		if ($start_date != '' && $end_date == '') {
			// 시작일만 있는 경우
			$whereDate=<<<SQL
AND `suhNews`.`datetime`>=STR_TO_DATE('{$start_date}', '%Y-%m-%d') 
SQL;
		} else if ($start_date == '' && $end_date != '') {
			// 끝일만 있는 경우
			$whereDate=<<<SQL
AND `suhNews`.`datetime`<=STR_TO_DATE('{$end_date} 23:59:59', '%Y-%m-%d %H:%i:%s') 
SQL;
		} else if ($start_date != '' && $end_date != '') {
			// 시작일과 끝일 모두 있는 경우
			$whereDate=<<<SQL
AND `suhNews`.`datetime`>=STR_TO_DATE('{$start_date}', '%Y-%m-%d') AND `suhNews`.`datetime`<=STR_TO_DATE('{$end_date} 23:59:59', '%Y-%m-%d %H:%i:%s') 
SQL;
		} else if ($start_date == '' && $end_date == '') {
			// 시작일과 끝일 모두 없는 경우
			$whereDate = '';
		}


		$whereWriter = '';
		if ($writer != 0) {
			$whereWriter=<<<SQL
AND `suhNews`.`admin`={$writer}  
SQL;
		} else {
			$whereWriter = '';
		}


		$whereTitle = '';
		if ($title != '') {
			$whereTitle=<<<SQL
AND `suhNews`.`title` LIKE '%{$title}%' 
SQL;
		} else {
			$whereTitle = '';
		}


		$whereContent = '';
		if ($content != '') {
			$whereContent=<<<SQL
AND `suhNews`.`content` LIKE '%{$content}%' 
SQL;
		} else {
			$whereContent = '';
		}


		$sql=<<<SQL
SELECT 
`suhNews`.`suhnews`, 
`suhNews`.`admin`, 
`suhAdmin1`.`name` AS `admin_name`, 
`suhAdmin1`.`id` AS `admin_id`, 
`suhNews`.`title`, 
`suhNews`.`content`, 
`suhNews`.`datetime`, 
`suhNews`.`attach_file_flag`, 
`suhNews`.`view_index` 
FROM `suhNews` 
LEFT JOIN `suhAdmin` AS `suhAdmin1` 
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin` 
WHERE `suhNews`.`status`=900400  
{$whereDate} {$whereWriter} {$whereTitle} {$whereContent} 
ORDER BY `suhNews`.`suhnews` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhnews_pk = $row->suhnews;
				$admin = $row->admin;
				$admin_name = $row->admin_name;
				$admin_id = $row->admin_id;
				$title = htmlspecialchars($row->title);
				$content = preg_replace('/\r\n|\r|\n/',' ', htmlspecialchars($row->content));
				$datetime = $row->datetime;
				$attach_file_flag = $row->attach_file_flag;
				$view_index = $row->view_index;

				$jsondata.=<<<JSON
{
	"suhnews_pk":{$suhnews_pk},
	"admin":{$admin},
	"admin_name":"{$admin_name}",
	"admin_id":"{$admin_id}",
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<$page_num; $ii++) {
					if ($first_indexs % $page_num == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;



				$one_set_num_plus = ($page_num * $view_num) + 1;

				$sql=<<<SQL
SELECT `suhnews` FROM `suhNews` 
WHERE `suhNews`.`status`=900400 
{$whereDate} {$whereWriter} {$whereTitle} {$whereContent} 
ORDER BY `suhnews` DESC 
LIMIT {$first_indexs}, {$one_set_num_plus}
SQL;
				// echo $sql; exit;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == $one_set_num_plus) {
					$pagenum = $page_num;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/$view_num);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
    }

    // 병원뉴스 페이징 - 페이지 맨 마지막 번호 알아내기
    function admin7016() {
    	$db = $GLOBALS["db"];
    	$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		
		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$writer = $_REQUEST["writer"];
		$title = $_REQUEST["title"];
		$content = $_REQUEST["content"];


		$whereDate = '';
		if ($start_date != '' && $end_date == '') {
			// 시작일만 있는 경우
			$whereDate=<<<SQL
AND `suhNews`.`datetime`>='{$start_date}' 
SQL;
		} else if ($start_date == '' && $end_date != '') {
			// 끝일만 있는 경우
			$whereDate=<<<SQL
AND `suhNews`.`datetime`<='{$start_date}' 
SQL;
		} else if ($start_date != '' && $end_date != '') {
			// 시작일과 끝일 모두 있는 경우
			$whereDate=<<<SQL
AND (`suhNews`.`datetime`>='{$start_date}' AND `suhNews`.`datetime`<='{$end_date}')
SQL;
		} else if ($start_date == '' && $end_date == '') {
			// 시작일과 끝일 모두 없는 경우
			$whereDate = '';
		}


		$whereWriter = '';
		if ($writer != 0) {
			$whereWriter=<<<SQL
AND `suhNews`.`admin`={$writer}  
SQL;
		} else {
			$whereWriter = '';
		}


		$whereTitle = '';
		if ($title != '') {
			$whereTitle=<<<SQL
AND `suhNews`.`title` LIKE '%{$title}%' 
SQL;
		} else {
			$whereTitle = '';
		}


		$whereContent = '';
		if ($content != '') {
			$whereContent=<<<SQL
AND `suhNews`.`content` LIKE '%{$content}%' 
SQL;
		} else {
			$whereContent = '';
		}


		$sql=<<<SQL
SELECT 
`suhNews`.`suhnews`, 
`suhNews`.`admin`, 
`suhAdmin1`.`name` AS `admin_name`, 
`suhAdmin1`.`id` AS `admin_id`, 
`suhNews`.`title`, 
`suhNews`.`content`, 
`suhNews`.`datetime`, 
`suhNews`.`attach_file_flag`, 
`suhNews`.`view_index` 
FROM `suhNews` 
LEFT JOIN `suhAdmin` AS `suhAdmin1` 
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin` 
WHERE `suhNews`.`suhnews` != 0 
{$whereDate} {$whereWriter} {$whereTitle} {$whereContent} 
ORDER BY `suhNews`.`suhnews` DESC 
SQL;
		// echo $sql; exit;
		
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / $view_num) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<$page_num; $i++) {
			if ($first_index % $page_num == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
    }

    // 병원뉴스 서버에 업로드 하기
    function admin7110() {
    	$db = $GLOBALS["db"];
    	$admin = $_REQUEST["admin"];
		$title = addslashes($_REQUEST["title"]);
		$content = addslashes($_REQUEST["content"]);
		$file_ary = reArrayFiles($_FILES["attachfile"]);
		
		if ($file_ary[0]['size'] == 0) {
			$attach_file_flag = 0;
		} else {
			$attach_file_flag = 1;
		}
		// foreach ($file_ary as $file) {
	 //        // print 'File Name: ' . $file['name'];
	 //        // print 'File Type: ' . $file['type'];
	 //        // print 'File Size: ' . $file['size'];
	 //        echo 'file size = '.$file['size'];
	 //    }

		$sql=<<<SQL
INSERT INTO `suhNews`
(`admin`,`title`,`content`,`attach_file_flag`,`status`)VALUES
({$admin}, '{$title}', '{$content}', {$attach_file_flag}, 900400)
SQL;
		$res = $db->query($sql);
		if ($res) {
			$sql=<<<SQL
SELECT LAST_INSERT_ID() AS `recent_pk`
SQL;
			$res = $db->query($sql);
			$row = $res->fetch_object();
			$recent_pk = $row->recent_pk;

			// insert 성공 했을경우
			if ($attach_file_flag == 1) {
				$upload_dir = '../suhnews/suhnews'.$recent_pk.'/';
				if (!is_dir($upload_dir)) {
					mkdir($upload_dir);
				}

				$aa = 0;
				for ($i=0; $i<count($file_ary); $i++) {
					if (file_exists($file_ary[$i]['tmp_name'])) {
						$target_file = $upload_dir.basename($file_ary[$i]["name"]);
						$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
					
					    if (move_uploaded_file($file_ary[$i]['tmp_name'], $target_file)) {
					        
					    } else {
					        
					    }
					}

					$aa++;
					if ($aa >= 200) {
						break;
					}
				}

				$text=<<<TEXT
<script>
	alert("등록 되었습니다.");
	location.href="./admin7000.php";
</script>
TEXT;
				echo $text;
				exit;
			}

		} else {
			// insert 실패 했을경우
			$text=<<<TEXT
<script>
	alert("등록중에 오류가 발생하였습니다. 지속적으로 이 오류가 발생한다면 개발자에게 문의주시길 바랍니다.");
	location.href="./admin7000.php";
</script>
TEXT;
				echo $text;
				exit;
		}
    }

    // 병원뉴스 상세정보 가져오기
    function admin7210() {
    	$db = $GLOBALS["db"];
    	$news_pk = $_REQUEST["news_pk"];

    	$newbee = $GLOBALS["newbee"];
		$origin = $GLOBALS["origin"];



		$sql=<<<SQL
SELECT 
`suhNews`.`admin`,
`suhAdmin1`.`name` AS `admin_name`,
`suhAdmin1`.`id` AS `admin_id`,
`suhNews`.`title`,
`suhNews`.`content`,
`suhNews`.`datetime`,
`suhNews`.`attach_file_flag`,
`suhNews`.`view_index` 
FROM `suhNews`
LEFT JOIN `suhAdmin` AS `suhAdmin1`
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin`
WHERE `suhNews`.`suhnews`={$news_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$admin_name = $row->admin_name;
			$title = setCleanText($row->title, 'html');
			$content = setCleanText($row->content, 'html');
			$datetime = $row->datetime;
			$attach_file_flag = $row->attach_file_flag;
			$view_index = $row->view_index;

			if ($attach_file_flag == 1) {
				// 폴더명 지정
				$dir = "../suhnews/suhnews".$news_pk.'/';
				 
				// 핸들 획득
				$handle  = opendir($dir);
				 
				$files = array();
				 
				// 디렉터리에 포함된 파일을 저장한다.
				$iii = 0;
				while (false !== ($filename = readdir($handle))) {
				    if($filename == "." || $filename == ".."){
				        continue;
				    }
				 
				    // 파일인 경우만 목록에 추가한다.
				    if(is_file($dir . "/" . $filename)){
				        $files[] = mb_convert_encoding($filename,$newbee,$origin);
				    }
				    $iii++;
				    if ($iii >= 10) {
				    	break;
				    }
				}
				 
				// 핸들 해제 
				closedir($handle);
				 
				// 정렬, 역순으로 정렬하려면 rsort 사용
				sort($files);
			} else {
				$files = array();
			}


			$attach_file_array = '[';
			$oo = 0;
			for ($i=0; $i<count($files); $i++) {
				$oo++;
				$attach_file_array.=<<<JSON
{
	"filename":"{$files[$i]}"
},
JSON;
			}
			if ($oo >= 1) {
				$attach_file_array = substr($attach_file_array, 0, -1);
			}
			$attach_file_array.=']';



			$jsondata=<<<JSON
{
	"admin_name":"{$admin_name}",
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"attach_file_array":{$attach_file_array},
	"result":"ok"	
}
JSON;

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
    }

    // 병원뉴스 상세정보 가져오기
    function admin7301() {
    	$db = $GLOBALS["db"];
    	$news_pk = $_REQUEST["news_pk"];

    	$newbee = $GLOBALS["newbee"];
		$origin = $GLOBALS["origin"];



		$sql=<<<SQL
SELECT 
`suhNews`.`admin`,
`suhAdmin1`.`name` AS `admin_name`,
`suhAdmin1`.`id` AS `admin_id`,
`suhNews`.`title`,
`suhNews`.`content`,
`suhNews`.`datetime`,
`suhNews`.`attach_file_flag`,
`suhNews`.`view_index` 
FROM `suhNews`
LEFT JOIN `suhAdmin` AS `suhAdmin1`
ON `suhNews`.`admin`=`suhAdmin1`.`suhadmin`
WHERE `suhNews`.`suhnews`={$news_pk}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {

			$row = $res->fetch_object();

			$admin_name = $row->admin_name;
			$title = htmlspecialchars($row->title);
			$content = preg_replace('/\r\n|\r|\n/','\n', htmlspecialchars($row->content));
			$datetime = $row->datetime;
			$attach_file_flag = $row->attach_file_flag;
			$view_index = $row->view_index;

			if ($attach_file_flag == 1) {
				// 폴더명 지정
				$dir = "../suhnews/suhnews".$news_pk.'/';
				 
				// 핸들 획득
				$handle  = opendir($dir);
				 
				$files = array();
				 
				// 디렉터리에 포함된 파일을 저장한다.
				$iii = 0;
				while (false !== ($filename = readdir($handle))) {
				    if($filename == "." || $filename == ".."){
				        continue;
				    }
				 
				    // 파일인 경우만 목록에 추가한다.
				    if(is_file($dir . "/" . $filename)){
				        $files[] = mb_convert_encoding($filename,$newbee,$origin);
				    }
				    $iii++;
				    if ($iii >= 10) {
				    	break;
				    }
				}
				 
				// 핸들 해제 
				closedir($handle);
				 
				// 정렬, 역순으로 정렬하려면 rsort 사용
				sort($files);
			} else {
				$files = array();
			}


			$attach_file_array = '[';
			$oo = 0;
			for ($i=0; $i<count($files); $i++) {
				$oo++;
				$attach_file_array.=<<<JSON
{
	"filename":"{$files[$i]}"
},
JSON;
			}
			if ($oo >= 1) {
				$attach_file_array = substr($attach_file_array, 0, -1);
			}
			$attach_file_array.=']';



			$jsondata=<<<JSON
{
	"admin_name":"{$admin_name}",
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"attach_file_array":{$attach_file_array},
	"result":"ok"	
}
JSON;
			
			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
    }

    // 병원뉴스 서버에서 내용 수정하기
    function admin7310() {
		$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];		

		$title = addslashes($_REQUEST["title"]);
		$content = addslashes($_REQUEST["content"]);
		$file_ary = reArrayFiles($_FILES["attachfile"]);


		$fileDeleteArray = $_REQUEST["fileDeleteArray"];
		$dir = "../suhnews/suhnews".$pk.'/';

		// echo 'fileDeleteArray = '.$fileDeleteArray;
		if ($fileDeleteArray != '') {
			$file_delete_array = explode(',', $fileDeleteArray);
			for ($i=0; $i<count($file_delete_array); $i++) {
				$filename = $file_delete_array[$i];

				if (file_exists($dir.$filename)) {
					unlink($dir.$filename);
				}
			}
		}


		
		if ($file_ary[0]['size'] == 0) {
			$attach_file_flag = 0;
		} else {
			$attach_file_flag = 1;
		}
		// foreach ($file_ary as $file) {
	 //        // print 'File Name: ' . $file['name'];
	 //        // print 'File Type: ' . $file['type'];
	 //        // print 'File Size: ' . $file['size'];
	 //        echo 'file size = '.$file['size'];
	 //    }


		$sql=<<<SQL
UPDATE `suhNews` SET `title`='{$title}', `content`='{$content}' 
WHERE `suhnews`={$pk}
SQL;
		
		$res = $db->query($sql);

		if ($res) {

			// UPDATE 성공 했을경우
			if ($attach_file_flag == 1) {
				$upload_dir = '../suhnews/suhnews'.$pk.'/';
				if (!is_dir($upload_dir)) {
					mkdir($upload_dir);
				}

				$aa = 0;
				for ($i=0; $i<count($file_ary); $i++) {
					if (file_exists($file_ary[$i]['tmp_name'])) {
						$target_file = $upload_dir.basename($file_ary[$i]["name"]);
						$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
					
					    if (move_uploaded_file($file_ary[$i]['tmp_name'], $target_file)) {
					        
					    } else {
					        
					    }
					}

					$aa++;
					if ($aa >= 200) {
						break;
					}
				}
			}


			// 파일이 있는지 확인하기
			// 핸들 획득
			$handle  = opendir($dir);
			 
			$files = array();
			 
			// 디렉터리에 포함된 파일을 저장한다.
			$iii = 0;
			while (false !== ($filename = readdir($handle))) {
			    if ($filename == "." || $filename == ".."){
			        continue;
			    }
			 
			    // 파일인 경우만 목록에 추가한다.
			    if (is_file($dir.$filename)){
			        // $files[] = mb_convert_encoding($filename,$newbee,$origin);
			        $files[] = iconv ("windows-1251", "UTF-8", $filename);
			    }
			    $iii++;
			    if ($iii >= 100) {
			    	break;
			    }
			}
			 
			// 핸들 해제 
			closedir($handle);
			 
			// 정렬, 역순으로 정렬하려면 rsort 사용
			sort($files);	



			if (count($files) > 0) {
				$aa = 1;
			} else {
				$aa = 0;
			}


			$sql=<<<SQL
UPDATE `suhNews` SET `attach_file_flag`={$aa} 
WHERE `suhnews`={$pk}
SQL;
			$res = $db->query($sql);




			$text=<<<TEXT
<script>
	alert("수정 되었습니다.");
	location.href="./admin7000.php";
</script>
TEXT;
			echo $text;
			exit;
		} else {
			// insert 실패 했을경우
			$text=<<<TEXT
<script>
	alert("수정중에 오류가 발생하였습니다. 지속적으로 이 오류가 발생한다면 개발자에게 문의주시길 바랍니다.");
	location.href="./admin7000.php";
</script>
TEXT;
			echo $text;
			exit;
		}
		
    }

    // 고객의소리 리스트 가져오기
    function admin7510() {
    	$db = $GLOBALS["db"];
    	$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		
		$search_type = $_REQUEST["search_type"];

		$content_type1 = $_REQUEST["content_type1"];
		$content_type2 = $_REQUEST["content_type2"];

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$writerName = $_REQUEST["writerName"];
		$title = $_REQUEST["title"];
		$content = $_REQUEST["content"];
 

		$start_index = $index * $view_num;


		
		$whereSearchType = '';
		if ($search_type != 0) {
			$whereSearchType=<<<SQL
AND `suhCustomerBoard`.`board_type`={$search_type} 
SQL;
			if ($search_type == 800701) {
				if ($content_type1 != 0) {
					$whereSearchType.=<<<SQL
AND `suhCustomerBoard`.`content_type`={$content_type1}
SQL;
				}

			} else if ($search_type == 800702) {
				if ($content_type2 != 0) {
					$whereSearchType.=<<<SQL
AND `suhCustomerBoard`.`content_type`={$content_type2}
SQL;
				}
			}
		}



		$whereDate = '';
		if ($start_date == '' && $end_date != '') {
			$whereDate=<<<SQL
AND `suhCustomerBoard`.`datetime` <= STR_TO_DATE('{$end_date} 23:59:59', '%Y-%m-%d %H:%i:%s') 
SQL;
		} else if ($start_date != '' && $end_date == '') {
			$whereDate=<<<SQL
AND `suhCustomerBoard`.`datetime` >= STR_TO_DATE('{$start_date}', '%Y-%m-%d') 
SQL;
		} else if ($start_date != '' && $end_date != '') {
			$whereDate=<<<SQL
AND `suhCustomerBoard`.`datetime` >= STR_TO_DATE('{$start_date}', '%Y-%m-%d')  
AND `suhCustomerBoard`.`datetime` <= STR_TO_DATE('{$end_date} 23:59:59', '%Y-%m-%d %H:%i:%s') 
SQL;
		}



		$whereWriter = '';
		if ($writerName != '') {
			$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `name`='{$writerName}'
SQL;
			$res = $db->query($sql);

			if ($res->num_rows >= 1) {
				while ($row = $res->fetch_object()) {
					$suhmember = $row->suhmember;
					$writerName.=<<<SQL
AND `suhCustomerBoard`.`member`={$suhmember} 
SQL;
				}
			}
		}




		$whereTitle = '';
		if ($title != '') {
			$whereTitle=<<<SQL
AND `suhCustomerBoard`.`title` LIKE '%{$title}%' 
SQL;
		}




		$whereContent = '';
		if ($content != '') {
			$whereContent=<<<SQL
AND `suhCustomerBoard`.`content` LIKE '%{$content}%' 
SQL;
		}





		$sql=<<<SQL
SELECT 
`suhCustomerBoard`.`suhc`,
`suhCustomerBoard`.`board_type`,
`suhCodes1`.`description` AS `board_type_string`,
`suhCustomerBoard`.`content_type`,
`suhCodes2`.`description` AS `content_type_string`,
`suhCustomerBoard`.`title`,
`suhCustomerBoard`.`content`,
`suhCustomerBoard`.`member`,
`suhMembers1`.`name` AS `member_name`,
`suhMembers1`.`email` AS `member_email`,
`suhCustomerBoard`.`datetime`,
`suhCustomerBoard`.`attach_file_flag`,
`suhCustomerBoard`.`view_index` 
FROM `suhCustomerBoard` 
LEFT JOIN `suhMembers` AS `suhMembers1` 
ON `suhCustomerBoard`.`member`=`suhMembers1`.`suhmember` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhCustomerBoard`.`board_type`=`suhCodes1`.`codeNumber` 
LEFT JOIN `suhCodes` AS `suhCodes2` 
ON `suhCustomerBoard`.`content_type`=`suhCodes2`.`codeNumber` 
WHERE `suhCustomerBoard`.`status`=900400 
{$whereSearchType}
{$whereDate}
{$whereWriter}
{$whereTitle}
{$whereContent}
ORDER BY `suhCustomerBoard`.`suhc` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhc = $row->suhc;
				$board_type = $row->board_type;
				$board_type_string = $row->board_type_string;
				$content_type = $row->content_type;
				$content_type_string = $row->content_type_string;
				$title = htmlspecialchars($row->title);
				$content = preg_replace('/\r\n|\r|\n/',' ', htmlspecialchars($row->content));
				$content = preg_replace('/[\t|\s{2,}]/','  ', $content);
				$member = $row->member;
				$member_name = $row->member_name;
				$member_email = $row->member_email;
				$datetime = $row->datetime;
				$attach_file_flag = $row->attach_file_flag;
				$view_index = $row->view_index;

				$jsondata.=<<<JSON
{
	"suhc":{$suhc},
	"board_type":{$board_type},
	"board_type_string":"{$board_type_string}",
	"content_type":{$content_type},
	"content_type_string":"{$content_type_string}",
	"title":"{$title}",
	"content":"{$content}",
	"member":{$member},
	"member_name":"{$member_name}",
	"member_email":"{$member_email}",
	"datetime":"{$datetime}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<$page_num; $ii++) {
					if ($first_indexs % $page_num == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;



				$one_set_num_plus = ($page_num * $view_num) + 1;

				$sql=<<<SQL
SELECT 
`suhCustomerBoard`.`suhc` 
FROM `suhCustomerBoard` 
WHERE `suhCustomerBoard`.`status`=900400 
{$whereSearchType}
{$whereDate}
{$whereWriter}
{$whereTitle}
{$whereContent}
ORDER BY `suhCustomerBoard`.`suhc` DESC 
LIMIT {$first_indexs}, {$one_set_num_plus}
SQL;
				// echo $sql; exit;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46


				if ($record == $one_set_num_plus) {
					$pagenum = $page_num;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/$view_num);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
    }

    // 고객의소리 - 페이지 맨 마지막 번호 알아내기
    function admin7516() {
    	$db = $GLOBALS["db"];
    	
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];
		
		$search_type = $_REQUEST["search_type"];

		$content_type1 = $_REQUEST["content_type1"];
		$content_type2 = $_REQUEST["content_type2"];

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$writerName = $_REQUEST["writerName"];
		$title = $_REQUEST["title"];
		$content = $_REQUEST["content"];
 

		


		
		$whereSearchType = '';
		if ($search_type != 0) {
			$whereSearchType=<<<SQL
AND `suhCustomerBoard`.`board_type`={$search_type} 
SQL;
			if ($search_type == 800701) {
				$whereSearchType.=<<<SQL
AND `suhCustomerBoard`.`content_type`={$content_type1}
SQL;
			} else if ($search_type == 800702) {
				$whereSearchType.=<<<SQL
AND `suhCustomerBoard`.`content_type`={$content_type2}
SQL;
			}
		}



		$whereDate = '';
		if ($start_date == '' && $end_date != '') {
			$whereDate=<<<SQL
AND `suhCustomerBoard`.`datetime` <= '{$end_date}'
SQL;
		} else if ($start_date != '' && $end_date == '') {
			$whereDate=<<<SQL
AND `suhCustomerBoard`.`datetime` >= '{$start_date}'
SQL;
		} else if ($start_date != '' && $end_date != '') {
			$whereDate=<<<SQL
AND `suhCustomerBoard`.`datetime` >= '{$start_date}' 
AND `suhCustomerBoard`.`datetime` <= '{$end_date}' 
SQL;
		}



		$whereWriter = '';
		if ($writerName != '') {
			$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `name`='{$writerName}'
SQL;
			$res = $db->query($sql);

			if ($res->num_rows >= 1) {
				while ($row = $res->fetch_object()) {
					$suhmember = $row->suhmember;
					$writerName.=<<<SQL
AND `suhCustomerBoard`.`member`={$suhmember} 
SQL;
				}
			}
		}




		$whereTitle = '';
		if ($title != '') {
			$whereTitle=<<<SQL
AND `suhCustomerBoard`.`title` LIKE '%{$title}%' 
SQL;
		}




		$whereContent = '';
		if ($content != '') {
			$whereContent=<<<SQL
AND `suhCustomerBoard`.`content` LIKE '%{$content}%' 
SQL;
		}





		$sql=<<<SQL
SELECT 
`suhCustomerBoard`.`suhc` 
FROM `suhCustomerBoard` 
WHERE `suhCustomerBoard`.`status`=900400 
{$whereSearchType}
{$whereDate}
{$whereWriter}
{$whereTitle}
{$whereContent}
ORDER BY `suhCustomerBoard`.`suhc` DESC 
SQL;

    	// echo $sql; exit;
		
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / $view_num) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<$page_num; $i++) {
			if ($first_index % $page_num == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
    }

    // 
    function admin7610() {
    	$db = $GLOBALS["db"];
		$pk = $_REQUEST["pk"];
		$newbee = $GLOBALS["newbee"];
		$origin = $GLOBALS["origin"];

		// 조회수 업
		$sql=<<<SQL
UPDATE `suhCustomerBoard` SET `view_index`=`view_index`+1 WHERE `suhc`={$pk}
SQL;
		$res = $db->query($sql);





		$sql=<<<SQL
SELECT 
`suhCustomerBoard`.`board_type`,
`suhCodes1`.`description` AS `board_type_string`,
`suhCustomerBoard`.`content_type`,
`suhCodes2`.`description` AS `content_type_string`,
`suhCustomerBoard`.`title`,
`suhCustomerBoard`.`content`,
`suhCustomerBoard`.`member`,
`suhMembers1`.`name` AS `member_name`,
`suhMembers1`.`email` AS `member_email`,
`suhCustomerBoard`.`datetime`,
`suhCustomerBoard`.`attach_file_flag`,
`suhCustomerBoard`.`view_index`
FROM `suhCustomerBoard` 
LEFT JOIN `suhMembers` AS `suhMembers1`
ON `suhCustomerBoard`.`member`=`suhMembers1`.`suhmember`
LEFT JOIN `suhCodes` AS `suhCodes1`
ON `suhCustomerBoard`.`board_type`=`suhCodes1`.`codeNumber`
LEFT JOIN `suhCodes` AS `suhCodes2`
ON `suhCustomerBoard`.`content_type`=`suhCodes2`.`codeNumber`
WHERE `suhCustomerBoard`.`suhc`={$pk}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$board_type = $row->board_type;
			$board_type_string = $row->board_type_string;
			$content_type = $row->content_type;
			$content_type_string = $row->content_type_string;
			$title = htmlspecialchars($row->title);
			$content = preg_replace('/\r\n|\r|\n/','<br />', htmlspecialchars($row->content));
			$content = preg_replace('/[\t|\s{2,}]/','  ', $content);
			$member = $row->member;
			$member_name = $row->member_name;
			$member_email = $row->member_email;
			$datetime = $row->datetime;
			$attach_file_flag = $row->attach_file_flag;
			$view_index = $row->view_index;

			if ($attach_file_flag == 1) {
				// 폴더명 지정
				$dir = "../customer_board/customer".$board_type."_".$pk.'/';
				 
				// 핸들 획득
				$handle  = opendir($dir);
				 
				$files = array();
				 
				// 디렉터리에 포함된 파일을 저장한다.
				$iii = 0;
				while (false !== ($filename = readdir($handle))) {
				    if($filename == "." || $filename == ".."){
				        continue;
				    }
				 
				    // 파일인 경우만 목록에 추가한다.
				    if(is_file($dir . "/" . $filename)){
				        $files[] = mb_convert_encoding($filename,$newbee,$origin);
				    }
				    $iii++;
				    if ($iii >= 10) {
				    	break;
				    }
				}
				 
				// 핸들 해제 
				closedir($handle);
				 
				// 정렬, 역순으로 정렬하려면 rsort 사용
				sort($files);
			} else {
				$files = array();
			}


			$attach_file_array = '[';
			$oo = 0;
			for ($i=0; $i<count($files); $i++) {
				$oo++;
				$attach_file_array.=<<<JSON
{
	"filename":"{$files[$i]}"
},
JSON;
			}
			if ($oo >= 1) {
				$attach_file_array = substr($attach_file_array, 0, -1);
			}
			$attach_file_array.=']';





			// 이전글 검색
			$sql=<<<SQL
SELECT `suhc`, `title` FROM `suhCustomerBoard`
WHERE `suhc`<{$pk} AND `board_type`={$board_type} 
ORDER BY `suhc` DESC LIMIT 1;
SQL;
			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$prev_pk = $row->suhc;
				$prev_title = htmlspecialchars($row->title);
			} else {
				$prev_pk = 0;
				$prev_title = '';
			}


			// 다음글 검색
			$sql=<<<SQL
SELECT `suhc`, `title` FROM `suhCustomerBoard`
WHERE `suhc`>{$pk} AND `board_type`={$board_type} 
ORDER BY `suhc` ASC LIMIT 1;
SQL;
			$res = $db->query($sql);

			if ($res->num_rows == 1) {
				$row = $res->fetch_object();
				$next_pk = $row->suhc;
				$next_title =htmlspecialchars($row->title);
			} else {
				$next_pk = 0;
				$next_title = '';
			}





			$jsondata=<<<JSON
{
	"board_type":{$board_type},
	"board_type_string":"{$board_type_string}",
	"content_type":{$content_type},
	"content_type_string":"{$content_type_string}",
	"title":"{$title}",
	"content":"{$content}",
	"datetime":"{$datetime}",
	"member":{$member},
	"member_name":"{$member_name}",
	"member_email":"{$member_email}",
	"attach_file_flag":{$attach_file_flag},
	"view_index":{$view_index},
	"attach_file_array":{$attach_file_array},
	"prev_pk":{$prev_pk},
	"prev_title":"{$prev_title}",
	"next_pk":{$next_pk},
	"next_title":"{$next_title}",
	"result":"ok"	
}
JSON;

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
    }

    // 고객의소리 삭제하기
    function admin7612() {
    	$db = $GLOBALS["db"];
    	$pk = $_REQUEST["pk"];

    	$sql=<<<SQL
UPDATE `suhCustomerBoard` SET `status`=900300 WHERE `suhc`={$pk}
SQL;
		$res = $db->query($sql);
		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
    }

    // 문의유형 리스트 가져오기
    function admin8001() {
    	$db = $GLOBALS["db"];

    	$sql=<<<SQL
SELECT `codeNumber`,`description` FROM `suhCodes` WHERE `codeNumber`>=800601 AND `codeNumber`<800700 
ORDER BY `codeNumber` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';
			
			while ($row = $res->fetch_object()) {
				$codeNumber = $row->codeNumber;
				$description = $row->description;

				$jsondata.=<<<JSON
{
	"codeNumber":{$codeNumber},
	"description":"{$description}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';
			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}

    }

    // 1:1문의 리스트 가져오기
    function admin8010() {
    	$db = $GLOBALS["db"];
    	$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

    	$answer_whether = $_REQUEST["answer_whether"];
    	$question_type = $_REQUEST["question_type"];


    	$start_index = $index * $view_num;


    	$whereAnswerWhether = '';
    	if ($answer_whether == 1) {
    		$whereAnswerWhether=<<<SQL
AND `suhQuestions`.`answer_whether`=0 
SQL;
    	} else if ($answer_whether == 2) {
    		$whereAnswerWhether=<<<SQL
AND `suhQuestions`.`answer_whether`=1 
SQL;
    	} else {
    		$whereAnswerWhether = '';
    	}



    	$whereQuestionType = '';
    	if ($question_type != 0) {
    		$whereQuestionType=<<<SQL
AND `suhQuestions`.`question_type`={$question_type} 
SQL;
    	} else {
    		$whereQuestionType = '';
    	}



    	$sql=<<<SQL
SELECT 
`suhQuestions`.`suhq`, 
`suhQuestions`.`member`,
`suhMembers1`.`name` AS `member_name`,
`suhMembers1`.`phoneNumber` AS `member_phone_number`,
`suhQuestions`.`question_type`,
`suhCodes1`.`description` AS `question_type_string`,
`suhQuestions`.`question_title`,
`suhQuestions`.`question_content`,
`suhQuestions`.`question_datetime`,
`suhQuestions`.`answer_whether` 
FROM `suhQuestions` 
LEFT JOIN `suhMembers` AS `suhMembers1` 
ON `suhQuestions`.`member`=`suhMembers1`.`suhmember` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhQuestions`.`question_type`=`suhCodes1`.`codeNumber` 
WHERE `suhQuestions`.`suhq`!=0 
{$whereAnswerWhether} {$whereQuestionType} 
ORDER BY `suhQuestions`.`suhq` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$question_pk = $row->suhq;
				$member = $row->member;
				$member_name = htmlspecialchars(addslashes($row->member_name));
				$member_phone_number = $row->member_phone_number;
				$question_type = $row->question_type;
				$question_type_string = $row->question_type_string;
				$question_title = preg_replace('/\r\n|\r|\n/',' ', addslashes($row->question_title));
				$question_content = preg_replace('/\r\n|\r|\n/',' ',addslashes($row->question_content));
				$question_datetime = $row->question_datetime;
				$answer_whether = $row->answer_whether;
				

				$jsondata.=<<<JSON
{
	"question_pk":{$question_pk},
	"member":{$member},
	"member_name":"{$member_name}",
	"member_phone_number":"{$member_phone_number}",
	"question_type":{$question_type},
	"question_type_string":"{$question_type_string}",
	"question_title":"{$question_title}",
	"question_content":"{$question_content}",
	"question_datetime":"{$question_datetime}",
	"answer_whether":{$answer_whether}
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<$page_num; $ii++) {
					if ($first_indexs % $page_num == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;



				$one_set_num_plus = ($page_num * $view_num) + 1;



				$sql=<<<SQL
SELECT 
`member`
FROM `suhQuestions` 
WHERE `suhq`!=0 
{$whereAnswerWhether} {$whereQuestionType} 
ORDER BY `suhq` DESC 
LIMIT {$first_indexs}, {$one_set_num_plus}
SQL;

				// echo $sql; exit;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46



				if ($record == $one_set_num_plus) {
					$pagenum = $page_num;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/$view_num);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}

    }

    // 1:1문의 페이징 - 페이지 맨 마지막 번호 알아내기
    function admin8016() {
    	$db = $GLOBALS["db"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

    	$answer_whether = $_REQUEST["answer_whether"];
    	$question_type = $_REQUEST["question_type"];


    	$whereAnswerWhether = '';
    	if ($answer_whether == 1) {
    		$whereAnswerWhether=<<<SQL
AND `suhQuestions`.`answer_whether`=0 
SQL;
    	} else if ($answer_whether == 2) {
    		$whereAnswerWhether=<<<SQL
AND `suhQuestions`.`answer_whether`=1 
SQL;
    	} else {
    		$whereAnswerWhether = '';
    	}



    	$whereQuestionType = '';
    	if ($question_type != 0) {
    		$whereQuestionType=<<<SQL
AND `suhQuestions`.`question_type`={$question_type} 
SQL;
    	} else {
    		$whereQuestionType = '';
    	}



    	$sql=<<<SQL
SELECT 
`suhQuestions`.`suhq`, 
`suhQuestions`.`member`,
`suhMembers1`.`name` AS `member_name`,
`suhMembers1`.`phoneNumber` AS `member_phone_number`,
`suhQuestions`.`question_type`,
`suhCodes1`.`description` AS `question_type_string`,
`suhQuestions`.`question_title`,
`suhQuestions`.`question_content`,
`suhQuestions`.`question_datetime`,
`suhQuestions`.`answer_whether` 
FROM `suhQuestions` 
LEFT JOIN `suhMembers` AS `suhMembers1` 
ON `suhQuestions`.`member`=`suhMembers1`.`suhmember` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhQuestions`.`question_type`=`suhCodes1`.`codeNumber` 
WHERE `suhQuestions`.`suhq`!=0 
{$whereAnswerWhether} {$whereQuestionType} 
ORDER BY `suhQuestions`.`suhq` DESC 
SQL;
		// echo $sql; exit;
		
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / $view_num) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<$page_num; $i++) {
			if ($first_index % $page_num == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
    }

    // 문의내용에 대한 답변 서버에 저장하기
    function admin8106() {
    	$db = $GLOBALS["db"];
    	$question_pk = $_REQUEST["question_pk"];
		$answer_title = addslashes($_REQUEST["answer_title"]);
		$answer_content = addslashes($_REQUEST["answer_content"]);

		$sql=<<<SQL
INSERT INTO `suhAnswers`
(`suhq`,`answer_title`,`answer_content`)VALUES
({$question_pk}, '{$answer_title}', '{$answer_content}');

UPDATE `suhQuestions` SET `answer_whether`=1 WHERE `suhq`={$question_pk};
SQL;
		$res = $db->multi_query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}

    }

    // 문의 내용 가져오기
    function admin8110() {
    	$db = $GLOBALS["db"];
		$question_pk = $_REQUEST["question_pk"];

		$sql=<<<SQL
SELECT 
`suhQuestions`.`member`,
`suhMembers1`.`name` AS `member_name`, 
`suhMembers1`.`phoneNumber` AS `member_phoneNumber`, 
`suhQuestions`.`question_type`,
`suhCodes1`.`description` AS `question_type_string`,
`suhQuestions`.`question_title`, 
`suhQuestions`.`question_content`,
`suhQuestions`.`question_datetime`,
`suhQuestions`.`answer_whether` 
FROM `suhQuestions` 
LEFT JOIN `suhMembers` AS `suhMembers1` 
ON `suhQuestions`.`member`=`suhMembers1`.`suhmember` 
LEFT JOIN `suhCodes` AS `suhCodes1` 
ON `suhQuestions`.`question_type`=`suhCodes1`.`codeNumber` 
WHERE `suhQuestions`.`suhq`={$question_pk}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$member = $row->member;
			$member_name = $row->member_name;
			$member_phoneNumber = $row->member_phoneNumber;
			$question_type = $row->question_type;
			$question_type_string = $row->question_type_string;
			$question_title = preg_replace('/\r\n|\r|\n/','<br />',htmlspecialchars($row->question_title));
			$question_content = preg_replace('/\r\n|\r|\n/','<br />',htmlspecialchars($row->question_content));
			$question_datetime = $row->question_datetime;
			$answer_whether	 = $row->answer_whether;



			$answer_title = '';
			$answer_content = '';
			$answer_datetime = '';
			if ($answer_whether == 1) {
				// 답변이 있을때의 코드
				$sql=<<<SQL
SELECT `answer_title`,`answer_content`,`answer_datetime` FROM `suhAnswers` 
WHERE `suhq`={$question_pk}
SQL;
				$res2 = $db->query($sql);
				if ($res2->num_rows == 1) {
					$row2 = $res2->fetch_object();

					$answer_title = preg_replace('/\r\n|\r|\n/','<br />',htmlspecialchars($row2->answer_title));
					$answer_content = preg_replace('/\r\n|\r|\n/','<br />',htmlspecialchars($row2->answer_content));
					$answer_datetime = $row2->answer_datetime;
				} 

			} 



			$jsondata=<<<JSON
{
	"member":{$member},
	"member_name":"{$member_name}",
	"member_phoneNumber":"{$member_phoneNumber}",
	"question_type":{$question_type},
	"question_type_string":"{$question_type_string}",
	"question_title":"{$question_title}",
	"question_content":"{$question_content}",
	"question_datetime":"{$question_datetime}",
	"answer_whether":{$answer_whether},
	"answer_title":"{$answer_title}",
	"answer_content":"{$answer_content}",
	"answer_datetime":"{$answer_datetime}",
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
    }

    // 진료과 리스트 가져오기
    function admin9010() {
    	$db = $GLOBALS["db"];
    	$search_type = $_REQUEST["search_type"];
    	$search_value = $_REQUEST["search_value"];


    	$where = '';
    	if ($search_type == 1) { // 진료과 명칭

    		if ($search_value != '') {
    			$where=<<<SQL
AND `department` LIKE '%{$search_value}%'
SQL;
    		}
    		$sql=<<<SQL
SELECT 
`suhd` AS `department_pk`,
`department` AS `department_title` 
FROM `suhDepartments` 
WHERE `suhd`!=0 
{$where} 
ORDER BY `suhd` ASC
SQL;

    	} else if ($search_type == 2) { // 소속의료진 이름

    		$sql=<<<SQL
SELECT 
`affiliation_department` AS `department_pk`, 
`suhDepartments1`.`department` AS `department_title` 
FROM `suhDoctors` 
LEFT JOIN `suhDepartments` AS `suhDepartments1` 
ON `suhDoctors`.`affiliation_department`=`suhDepartments1`.`suhd` 
WHERE `name` LIKE '%{$search_value}%' GROUP BY `affiliation_department`
ORDER BY `affiliation_department` ASC 
SQL;

    	}




    	$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$department_pk = $row->department_pk;
				$department_title = $row->department_title;

				$jsondata.=<<<JSON
{
	"department_pk":{$department_pk},
	"department_title":"{$department_title}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
			exit;
		}

    }

    // 진료과 번호 중복확인 체크하기
    function admin9106() {
    	$db = $GLOBALS["db"];
    	$department_pk = $_REQUEST["department_pk"];

    	$sql=<<<SQL
SELECT `suhd` FROM `suhDepartments` WHERE `suhd`={$department_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			echo '{"result":"already"}';
		} else {
			echo '{"result":"use_possible"}';
		}
    }

    // 진료과 서버에 추가하기
    function admin9110() {
		$db = $GLOBALS["db"];
		$departmentCode = $_REQUEST["departmentCode"];
		$departmentTitle = addslashes($_REQUEST["departmentTitle"]);
		$departmentContent = addslashes($_REQUEST["departmentContent"]);


		echo 'departmentCode = '.$departmentCode.'<br />';
		echo 'departmentTitle = '.$departmentTitle.'<br />';
		echo 'departmentImageSize = '.$_FILES['departmentImage']['size'].'<br />';
		echo 'departmentContent = '.$departmentContent.'<br />';



		if ($_FILES['departmentImage']['size'] == 0) {
			// 파일이 없는 경우
			$sql=<<<SQL
INSERT INTO `suhDepartments`
(`suhd`,`department`,`description`,`note1`)VALUES
({$departmentCode}, '{$departmentTitle}', '{$departmentContent}', 0)
SQL;

		} else {
			// 파일이 있는 경우
			$array = explode('.', $_FILES['departmentImage']['name']);
			$arrayLength = count($array);
			$imageType = $array[$arrayLength-1];

			if ($imageType != 'png' && $imageType != 'PNG' && $imageType != 'gif' && $imageType != 'GIF' && $imageType != 'jpg' && $imageType != 'JPG' && $imageType != 'jpeg' && $imageType != 'JPEG') {
				echo '
					<script>
						alert("png, jpg, gif 파일만 업로드 가능합니다.");
						history.back(1);
					</script>
				';
				exit;
			}

			$base64EncodeFile = base64_encode_image($_FILES['departmentImage']['tmp_name'], $imageType);


			$sql=<<<SQL
INSERT INTO `suhDepartments`
(`suhd`,`image`,`department`,`description`, `note1`)VALUES
({$departmentCode}, '{$base64EncodeFile}', '{$departmentTitle}', '{$departmentContent}', 1)
SQL;

		}


		$res = $db->query($sql);

		if ($res) {
			echo '
				<script>
					alert("업로드 되었습니다.");
					location.href = "./admin9000.php";
				</script>
			';
			exit;
		} else {
			echo '
				<script>
					alert("업로드 중에 오류가 발생하였습니다.");
					history.back(1);
				</script>
			';
		}

	
    }

    // 진료과 상세정보 가져오기
    function admin9201() {
    	$db = $GLOBALS["db"];
		$first_department_code = $_REQUEST["first_department_code"];


		$sql=<<<SQL
SELECT `image`,`department`,`description` 
FROM `suhDepartments` WHERE `suhd`={$first_department_code}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$image = $row->image;
			$department = $row->department;
			$description = htmlspecialchars(preg_replace('/\r\n|\r|\n/','\n',$row->description));


			$jsondata=<<<JSON
{
	"image":"{$image}",
	"department":"{$department}",
	"description":"{$description}",
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}

    }

    // 진료과 정보 수정하기
    function admin9210() {
    	$db = $GLOBALS["db"];
		$department_first_pk = $_REQUEST["department_first_pk"];

		$departmentTitle = addslashes($_REQUEST["departmentTitle"]);
		$departmentContent = addslashes($_REQUEST["departmentContent"]);
		$is_first_image = $_REQUEST["is_first_image"];



		if ($_FILES['departmentImage']['size'] == 0) {
			// 파일이 없는 경우


			if ($is_first_image == 1) {
				$sql=<<<SQL
UPDATE `suhDepartments` SET 
`department`='{$departmentTitle}', 
`description`='{$departmentContent}'  
WHERE `suhd`={$department_first_pk}
SQL;
			} else {
				$sql=<<<SQL
UPDATE `suhDepartments` SET 
`image`='',
`department`='{$departmentTitle}', 
`description`='{$departmentContent}', 
`note1`=0 
WHERE `suhd`={$department_first_pk}
SQL;
			}


		

		} else {
			// 파일이 있는 경우
			$array = explode('.', $_FILES['departmentImage']['name']);
			$arrayLength = count($array);
			$imageType = $array[$arrayLength-1];

			if ($imageType != 'png' && $imageType != 'PNG' && $imageType != 'gif' && $imageType != 'GIF' && $imageType != 'jpg' && $imageType != 'JPG' && $imageType != 'jpeg' && $imageType != 'JPEG') {
				echo '
					<script>
						alert("png, jpg, gif 파일만 업로드 가능합니다.");
						history.back(1);
					</script>
				';
				exit;
			}

			$base64EncodeFile = base64_encode_image($_FILES['departmentImage']['tmp_name'], $imageType);

			$sql=<<<SQL
UPDATE `suhDepartments` SET 
`image`='{$base64EncodeFile}',
`department`='{$departmentTitle}', 
`description`='{$departmentContent}',
`note1`=1 
WHERE `suhd`={$department_first_pk}
SQL;
		
		}


		$res = $db->query($sql);

		if ($res) {
			echo '
				<script>
					alert("수정 되었습니다.");
					location.href = "./admin9000.php";
				</script>
			';
			exit;
		} else {
			echo '
				<script>
					alert("수정 중에 오류가 발생하였습니다.");
					history.back(1);
				</script>
			';
			exit;
		}

    }

    // 진료과 리스트 가져오기
    function admin9604() {
    	$db = $GLOBALS["db"];

    	$sql=<<<SQL
SELECT `suhd`,`department` FROM `suhDepartments` ORDER BY `department` ASC
SQL;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {

			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhd = $row->suhd;
				$department = $row->department;
				
				$jsondata.='{"suhd":'.$suhd.', "department":"'.$department.'"},';
			}

			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], "result":"ok"}';

			echo $jsondata;
		} else {
			echo '{"result":"no"}';
		}
    }

    // 의료진 번호 중복확인 체크하기
    function admin9606() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];

    	$sql=<<<SQL
SELECT `doctor` FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			echo '{"result":"already"}';
		} else {
			echo '{"result":"use_possible"}';
		}
		exit;
    }

    // 의료진 서버에 추가하기
    function admin9615() {
    	$db = $GLOBALS["db"];

    	$doctorCode = $_REQUEST["doctorCode"];
    	$doctorDepartment = $_REQUEST["doctorDepartment"];
    	$doctorName = addslashes($_REQUEST["doctorName"]);
    	$doctorAge = addslashes($_REQUEST["doctorAge"]);
    	$doctorGender = $_REQUEST["doctorGender"];
    	$doctorStatus = $_REQUEST["doctorStatus"];
    	$doctorContent = addslashes($_REQUEST["doctorContent"]);

    	$schoolArray = addslashes($_REQUEST["schoolArray"]);
    	$careerArray = addslashes($_REQUEST["careerArray"]);

    	if ($_FILES['doctorImage']['size'] == 0) {
			// 파일이 없는 경우
			$sql=<<<SQL
INSERT INTO `suhDoctors`
(`doctor`,`name`,`age`,`gender`,`profile_info`,`affiliation_department`,`status`,`school_history`,`career`)VALUES
({$doctorCode}, '{$doctorName}', {$doctorAge}, {$doctorGender}, '{$doctorContent}', {$doctorDepartment}, {$doctorStatus}, '{$schoolArray}', '{$careerArray}')
SQL;

		} else {
			// 파일이 있는 경우
			$array = explode('.', $_FILES['doctorImage']['name']);
			$arrayLength = count($array);
			$imageType = $array[$arrayLength-1];

			if ($imageType != 'png' && $imageType != 'PNG' && $imageType != 'gif' && $imageType != 'GIF' && $imageType != 'jpg' && $imageType != 'JPG' && $imageType != 'jpeg' && $imageType != 'JPEG') {
				echo '
					<script>
						alert("png, jpg, gif 파일만 업로드 가능합니다.");
						history.back(1);
					</script>
				';
				exit;
			}

			$base64EncodeFile = base64_encode_image($_FILES['doctorImage']['tmp_name'], $imageType);


			$sql=<<<SQL
INSERT INTO `suhDoctors`
(`doctor`,`profile_image`,`image_exist`,`name`,`age`,`gender`,`profile_info`,`affiliation_department`,`status`,`school_history`,`career`)VALUES
({$doctorCode}, '{$base64EncodeFile}', 1, '{$doctorName}', {$doctorAge}, {$doctorGender}, '{$doctorContent}', {$doctorDepartment}, {$doctorStatus}, '{$schoolArray}', '{$careerArray}')
SQL;

		}


		$res = $db->query($sql);

		if ($res) {
			echo '
				<script>
					alert("업로드 되었습니다.");
					location.href = "./admin9500.php";
				</script>
			';
			exit;
		} else {
			echo '
				<script>
					alert("업로드 중에 오류가 발생하였습니다.");
					history.back(1);
				</script>
			';
		}
    }

    // 의료진 상세정보 가져오기
    function admin9701() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];


    	$sql=<<<SQL
SELECT `name`,`age`,`gender`,`profile_image`,`image_exist`,`profile_info`,`affiliation_department`,`status`,`school_history`,`career`   
FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$name = setCleanText($row->name, 'textarea');
			$age = setCleanText($row->age, 'textarea');
			$gender = $row->gender;
			$profile_image = $row->profile_image;
			$image_exist = $row->image_exist;
			$profile_info = setCleanText($row->profile_info, 'textarea');
			// $profile_info = $row->profile_info;
			$affiliation_department = $row->affiliation_department;
			$status = $row->status;
			$school_history = setCleanText($row->school_history, 'html');
			$career = setCleanText($row->career, 'html');


			$jsondata=<<<JSON
{
	"name":"{$name}",
	"age":{$age},
	"gender":{$gender},
	"profile_image":"{$profile_image}",
	"image_exist":{$image_exist},
	"profile_info":"{$profile_info}",
	"affiliation_department":{$affiliation_department},
	"status":{$status},
	"school_history":"{$school_history}",
	"career":"{$career}",
	"result":"ok"
}
JSON;
			echo $jsondata;


		} else {
			echo '{"result":"no"}';
		}
    }

    // 의료진 정보 수정하기
    function admin9715() {
    	$db = $GLOBALS["db"];
		$doctor_pk = $_REQUEST["doctor_pk"];

		$doctorDepartment = $_REQUEST["doctorDepartment"];
		$doctorName = addslashes($_REQUEST["doctorName"]);
		$doctorAge = addslashes($_REQUEST["doctorAge"]);
		$doctorGender = $_REQUEST["doctorGender"];
		$doctorStatus = $_REQUEST["doctorStatus"];
		$doctorContent = addslashes($_REQUEST["doctorContent"]);

		$schoolArray = $_REQUEST["schoolArray"];
    	$careerArray = $_REQUEST["careerArray"];

		$is_first_image = $_REQUEST["is_first_image"];



		if ($_FILES['doctorImage']['size'] == 0) {
			// 파일이 없는 경우


			if ($is_first_image == 1) {
				$sql=<<<SQL
UPDATE `suhDoctors` SET 
`name`='{$doctorName}', 
`age`={$doctorAge}, 
`gender`={$doctorGender},
`profile_info`='{$doctorContent}',
`affiliation_department`={$doctorDepartment},
`status`={$doctorStatus},
`school_history`='{$schoolArray}',
`career`='{$careerArray}' 
WHERE `doctor`={$doctor_pk}
SQL;
			} else {
				$sql=<<<SQL
UPDATE `suhDoctors` SET 
`name`='{$doctorName}', 
`age`={$doctorAge}, 
`gender`={$doctorGender},
`profile_image`='',
`image_exist`=0,
`profile_info`='{$doctorContent}',
`affiliation_department`={$doctorDepartment},
`status`={$doctorStatus}, 
`school_history`='{$schoolArray}',
`career`='{$careerArray}' 
WHERE `doctor`={$doctor_pk}
SQL;
			}


		

		} else {
			// 파일이 있는 경우
			$array = explode('.', $_FILES['doctorImage']['name']);
			$arrayLength = count($array);
			$imageType = $array[$arrayLength-1];

			if ($imageType != 'png' && $imageType != 'PNG' && $imageType != 'gif' && $imageType != 'GIF' && $imageType != 'jpg' && $imageType != 'JPG' && $imageType != 'jpeg' && $imageType != 'JPEG') {
				echo '
					<script>
						alert("png, jpg, gif 파일만 업로드 가능합니다.");
						history.back(1);
					</script>
				';
				exit;
			}

			$base64EncodeFile = base64_encode_image($_FILES['doctorImage']['tmp_name'], $imageType);

			$sql=<<<SQL
UPDATE `suhDoctors` SET 
`name`='{$doctorName}', 
`age`={$doctorAge}, 
`gender`={$doctorGender},
`profile_image`='$base64EncodeFile',
`image_exist`=1,
`profile_info`='{$doctorContent}',
`affiliation_department`={$doctorDepartment},
`status`={$doctorStatus}, 
`school_history`='{$schoolArray}',
`career`='{$careerArray}' 
WHERE `doctor`={$doctor_pk}
SQL;
		
		}


		$res = $db->query($sql);

		if ($res) {
			echo '
				<script>
					alert("수정 되었습니다.");
					location.href = "./admin9500.php";
				</script>
			';
			exit;
		} else {
			echo '
				<script>
					alert("수정 중에 오류가 발생하였습니다.");
					history.back(1);
				</script>
			';
			exit;
		}
    }

    // 의료진 댓글 리스트 가져오기
    function admin9801() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];
		$index = $_REQUEST["index"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

		$search_type = $_REQUEST["search_type"];
		$search_value = $_REQUEST["search_value"];


		$start_index = $index * $view_num;


		$whereText = '';
		if ($search_value != '') {
			if (((int) $search_type) == 1) {
				// 작성자 검색일 경우
				$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `name`='{$search_value}'
SQL;
				$res = $db->query($sql);

				if ($res->num_rows >= 1) {
					while ($row = $res->fetch_object()) {
						$suhmember = $row->suhmember;

						$whereText.=<<<TEXT
AND `suhDoctorComments`.`member`={$suhmember} 
TEXT;
					}

				} else {
					echo '{"result":"no"}';
					exit;
				}



			} else if (((int) $search_type) == 2) {
				// 내용 검색일 경우
				$whereText=<<<TEXT
AND `suhDoctorComments`.`comment` LIKE '%{$search_value}%' 
TEXT;

			}
		}


		$sql=<<<SQL
SELECT 
`suhDoctorComments`.`suhdc`,
`suhDoctorComments`.`member`,
`suhMembers1`.`name` AS `member_name`,
`suhMembers1`.`email` AS `member_email`,
`suhDoctorComments`.`datetime`,
`suhDoctorComments`.`comment` 
FROM `suhDoctorComments` 
LEFT JOIN `suhMembers` AS `suhMembers1` 
ON `suhDoctorComments`.`member`=`suhMembers1`.`suhmember` 
WHERE `suhDoctorComments`.`status`=900400 
AND `suhDoctorComments`.`doctor`={$doctor_pk} 
{$whereText} 
ORDER BY `suhDoctorComments`.`suhdc` DESC 
LIMIT {$start_index}, {$view_num}
SQL;
		// echo $sql; exit;
		$res = $db->query($sql);

		if ($res->num_rows >= 1) {
			$jsondata = '{"data":[';

			while ($row = $res->fetch_object()) {
				$suhdc = $row->suhdc;
				$member = $row->member;
				$member_name = $row->member_name;
				$member_email = $row->member_email;
				$datetime = $row->datetime;
				$comment = preg_replace('/\r\n|\r|\n/','<br />', htmlspecialchars($row->comment));
				

				$jsondata.=<<<JSON
{
	"suhdc":{$suhdc},
	"member":{$member},
	"member_name":"{$member_name}",
	"member_email":"{$member_email}",
	"datetime":"{$datetime}",
	"comment":"{$comment}"
},
JSON;
			}
			$jsondata = substr($jsondata, 0, -1);
			$jsondata.='], ';

			if ($page_reckoning == 1) {
				$first_indexs = $index;

				for ($ii=0; $ii<$page_num; $ii++) {
					if ($first_indexs % $page_num == 0) {
						break;
					}
					$first_indexs--;
				}
				$first_indexs = $first_indexs * $view_num;



				$one_set_num_plus = ($page_num * $view_num) + 1;



				$sql=<<<SQL
SELECT 
`suhdc`,
`member`,
`datetime`,
`comment` 
FROM `suhDoctorComments` 
WHERE `status`=900400 
AND `doctor`={$doctor_pk} 
{$whereText} 
ORDER BY `suhdc` DESC 
LIMIT {$first_indexs}, {$one_set_num_plus}
SQL;

				// echo $sql; exit;
				$res = $db->query($sql);
				$record = $res->num_rows; // ex) 46



				if ($record == $one_set_num_plus) {
					$pagenum = $page_num;
					$nextpageflag = 1;
				} else {
					$pagenum = ceil($record/$view_num);
					$nextpageflag = 0;
				}

				$jsondata.='"pagenum":'.$pagenum.', "nextpageflag":'.$nextpageflag.', ';
			}



			$jsondata.='"result":"ok"}';
			
			echo $jsondata;	
		} else {
			echo '{"result":"no"}';
		}
    }

    // 의료진 댓글 - 페이지 맨 마지막 번호 알아내기
    function admin9807() {
		$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];
		$view_num = $_REQUEST["view_num"];
		$page_num = $_REQUEST["page_num"];
		$page_reckoning = $_REQUEST["page_reckoning"];

		$search_type = $_REQUEST["search_type"];
		$search_value = $_REQUEST["search_value"];



		$whereText = '';
		if ($search_value != '') {
			if (((int) $search_type) == 1) {
				// 작성자 검색일 경우
				$sql=<<<SQL
SELECT `suhmember` FROM `suhMembers` WHERE `name`='{$search_value}'
SQL;
				$res = $db->query($sql);

				if ($res->num_rows >= 1) {
					while ($row = $res->fetch_object()) {
						$suhmember = $row->suhmember;

						$whereText.=<<<TEXT
AND `member`={$suhmember} 
TEXT;
					}

				}



			} else if (((int) $search_type) == 2) {
				// 내용 검색일 경우
				$whereText=<<<TEXT
AND `comment` LIKE '%{$search_value}%' 
TEXT;

			}
		}


		$sql=<<<SQL
SELECT 
`suhdc`,
`member`,
`datetime`,
`comment` 
FROM `suhDoctorComments` 
WHERE `status`=900400 
AND `doctor`={$doctor_pk} 
{$whereText} 
ORDER BY `suhdc` DESC
SQL;
		$res = $db->query($sql);

		$record_num = $res->num_rows;

		$pagenum = ceil($record_num / $view_num) - 1;

		$first_index = $pagenum;
		for ($i=0; $i<$page_num; $i++) {
			if ($first_index % $page_num == 0) {
				break;
			}
			$first_index--;
		}

		echo '{"result":"ok", "last_index":'.$pagenum.', "first_index":'.$first_index.'}';
    }

    // 댓글 삭제 버튼 클릭시 이벤트 설정하기
    function admin9809() {
    	$db = $GLOBALS["db"];
    	$willDeleteCommentPk = $_REQUEST["willDeleteCommentPk"];

    	$sql=<<<SQL
UPDATE `suhDoctorComments` SET `status`=900300 WHERE `suhdc`={$willDeleteCommentPk}
SQL;
		$res = $db->query($sql);

		if ($res) {
			echo '{"result":"ok"}';
		} else {
			echo '{"result":"no"}';
		}
    }

    // 의료진 정보 가져오기
    function admin9815() {
    	$db = $GLOBALS["db"];
    	$doctor_pk = $_REQUEST["doctor_pk"];

    	$sql=<<<SQL
SELECT `name` FROM `suhDoctors` WHERE `doctor`={$doctor_pk}
SQL;
		$res = $db->query($sql);

		if ($res->num_rows == 1) {
			$row = $res->fetch_object();

			$name = $row->name;

			$jsondata=<<<JSON
{
	"name":"{$name}",
	"result":"ok"
}
JSON;
			echo $jsondata;

		} else {
			echo '{"result":"no"}';
		}
    }












    // test
    function test() {
    	$db = $GLOBALS["db"];

//     	for ($i=0; $i<100; $i++) {
//     		$title = '실험제목 '.$i.'번째';

//     		$sql=<<<SQL
// INSERT INTO `suhNMedicalInformation`(`title_ko`,`title_en`,`one_line_description`)VALUES('{$title}', 'abc', '테스트')
// SQL;
// 			$res = $db->query($sql);
//     	}

    	for ($i=0; $i<100; $i++) {
    		$title = '실험제목 '.$i.'번째';

    		$sql=<<<SQL
INSERT INTO `suhNews`(`admin`,`title`,`content`)VALUES(1, '{$title}', '테스트')
SQL;
			$res = $db->query($sql);
    	}

    }






















	// 로그 남기는 함수
	function setLog($member, $logType, $logNumber, $note1, $note2, $note3) {
		$db = $GLOBALS["db"];

		$sql=<<<SQL
INSERT INTO `suhLogs` 
(`member`,`logtype`,`lognumber`,`note1`,`note2`,`note3`)VALUES
({$member}, {$logType}, {$logNumber}, {$note1}, {$note2}, '{$note3}')
SQL;
		$res = $db->query($sql);
		if ($res) {
			return true;
		} else {
			return false;
		}
	}

	function attachZero($numbers) {
		$value = ''.$numbers;
		if (((int) $numbers) < 10) {
			$value = '0'.$numbers;
		}
		return $value;
	}

	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }

    function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}

	function base64_encode_image($filename=string, $filetype=string) {
		if ($filename) {
			$imgbinary = fread(fopen($filename, "r"), filesize($filename));
			return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
		}
	}

	// html, android에 출력될 수 있도록 치환하기
	function setCleanText($str, $mode) {
		$return_str = '';
		switch ($mode) {
			case 'html':
				$return_str = htmlspecialchars($str);
				$return_str = preg_replace('/\r\n|\r|\n/','<br />', $return_str);
				$return_str = preg_replace('/\t+/',' ', $return_str);
				break;
			case 'textarea':
				$return_str = htmlspecialchars($str); 
				$return_str = preg_replace('/\r\n|\r|\n/','\n', $return_str);
				$return_str = preg_replace('/\t+/',' ', $return_str);
				break;
			case 'android':
				$return_str = addslashes($str);
				$return_str = preg_replace('/\r\n|\r|\n/','\n', $return_str);
				$return_str = preg_replace('/\t+/',' ', $return_str);
				break;
			default:
				$return_str = htmlspecialchars($str);
				$return_str = preg_replace('/\t+/',' ', $return_str);
				$return_str = preg_replace('/\r\n|\r|\n/','<br />', $return_str);
				break;
		}
		return $return_str;
	}
?>