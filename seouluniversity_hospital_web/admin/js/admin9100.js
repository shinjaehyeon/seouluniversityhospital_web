var global_start_code = 800101;
var global_end_code = 800199;

var global_checked_department_code = 0;






$(document).ready(function(){
	
	admin9101(); // 진료과 추가 버튼 클릭시 이벤트 설정하기

	admin9102(); // 이미지 파일 업로드시 미리보기 이미지 보여주기

	admin9103(); // 진료과 코드 값 변경시 이벤트 설정하기

	admin9105(); // 진료과 번호 중복확인 버튼 클릭시 이벤트 설정하기

});

// 진료과 추가 버튼 클릭시 이벤트 설정하기
function admin9101() {
	$(".formStlyeE .department_upload_button").on("click", function(){
		var department_pk = $("input[name=departmentCode]").val();
		var department_title = $.trim($("input[name=departmentTitle]").val());
		var department_image = $("input[name=departmentImage]").val();

		if (department_pk == '') {
			alert("진료과 코드를 입력해주세요.");

			global_checked_department_code = 0;
			$(".member_index_display").text("진료과 코드를 입력해주세요.");
			$(".member_index_display").css({"color":"#f8536a"});

			return false;
		}

		if (Number(department_pk) < global_start_code || Number(department_pk) > global_end_code) {
			alert(global_start_code+" ~ "+global_end_code+" 사이의 숫자로 진료과 코드를 입력해주세요.");
			
			global_checked_department_code = 0;
			$(".member_index_display").text("범위에 맞는 진료과 코드를 입력해주세요.");
			$(".member_index_display").css({"color":"#f8536a"});

			return false;
		}


		if (global_checked_department_code == 0) {
			alert("진료과 코드 중복확인을 해주세요.");
			$(".member_index_display").text("진료과 코드 중복확인을 해주세요.");
			$(".member_index_display").css({"color":"#f8536a"});
			return false;
		}


		if (department_title == '') {
			alert("진료과 명칭을 입력해주세요.");
			return false;
		}


		console.log("department_image = "+department_image);



		$("form[name=departmentAddForm]").submit();
	});
}

// 이미지 파일 업로드시 미리보기 이미지 보여주기
function admin9102() {
	$("input[name=departmentImage]").on('change', function(){
		if ($(this).val() == '') {
			$(".formStlyeE .imageDisplayView").css({"display":"none"});
		} else {
			$(".formStlyeE .imageDisplayView").css({"display":"block"});
        	readURL(this);
        }
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$(".department_preview").attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
    }
}

// 진료과 코드 값 변경시 이벤트 설정하기
function admin9103() {
	$("input[name=departmentCode]").on("change", function(){
		global_checked_department_code = 0;
		$(".member_index_display").text("진료과 코드 중복확인을 해주세요.");
		$(".member_index_display").css({"color":"#f8536a"});
	});
}





// 진료과 번호 중복확인 버튼 클릭시 이벤트 설정하기
function admin9105() {
	$(".formStlyeE .overlap_check_button").on("click", function(){
		if (isLoadingFalse()) {
			var department_pk = $("input[name=departmentCode]").val();

			if (department_pk == '') {
				alert("진료과 코드를 입력해주세요.");

				global_checked_department_code = 0;
				$(".member_index_display").text("진료과 코드를 입력해주세요.");
				$(".member_index_display").css({"color":"#f8536a"});

				return false;
			} 
			
			if (Number(department_pk) < global_start_code || Number(department_pk) > global_end_code) {
				alert(global_start_code+" ~ "+global_end_code+" 사이의 숫자로 진료과 코드를 입력해주세요.");
				
				global_checked_department_code = 0;
				$(".member_index_display").text("범위에 맞는 진료과 코드를 입력해주세요.");
				$(".member_index_display").css({"color":"#f8536a"});

				return false;
			}

			admin9106(department_pk); // 진료과 번호 중복확인 체크하기
		}
	});
}

// 진료과 번호 중복확인 체크하기
function admin9106(department_pk) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9106",
			department_pk:department_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
			global_checked_department_code = 0;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "use_possible") {
					
					$(".member_index_display").text("이용 가능한 코드번호 입니다.");
					$(".member_index_display").css({"color":"#008ace"});

					global_checked_department_code = department_pk;

				} else if (result == "already") {

					$(".member_index_display").text("이미 사용중인 코드번호 입니다.");
					$(".member_index_display").css({"color":"#f8536a"});

				} else {

					$(".member_index_display").text('');

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}