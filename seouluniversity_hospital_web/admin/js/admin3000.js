var global_h_700201_counter_1_number = 0;
var global_h_700201_counter_1_number_pk = 0;

var global_h_700201_counter_2_number = 0;
var global_h_700201_counter_2_number_pk = 0;

var global_h_700201_counter_3_number = 0;
var global_h_700201_counter_3_number_pk = 0;

var global_h_700202_counter_1_number = 0;
var global_h_700202_counter_1_number_pk = 0;

var global_h_700202_counter_2_number = 0;
var global_h_700202_counter_2_number_pk = 0;

var global_h_700202_counter_3_number = 0;
var global_h_700202_counter_3_number_pk = 0;

var global_h_700203_counter_1_number = 0;
var global_h_700203_counter_1_number_pk = 0;

var global_h_700203_counter_2_number = 0;
var global_h_700203_counter_2_number_pk = 0;

var global_h_700203_counter_3_number = 0;
var global_h_700203_counter_3_number_pk = 0;





$(document).ready(function(){
	admin3001(); // 병원 종류 탭 버튼 클릭시 이벤트 설정하기

	admin3002(); // 순번대기표 관리에 처음 들어왔을 때 각 접수처별로 현재번호 가져오기

	admin3005(); // 다음 번호 받기 버튼 눌렀을 때 이벤트 설정하기
});

// 병원 종류 탭 버튼 클릭시 이벤트 설정하기
function admin3001() {
	$(".admin3000.ticketadmin ul.tab_box li").on("click", function(){
		$(".admin3000.ticketadmin ul.tab_box li.active").removeClass("active");
		$(this).addClass("active");



		var hospital_type = Number($(this).attr("hospital-type"));
		$(".admin3000.ticketadmin > ul.tab_content > li.show").removeClass("show");
		$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").addClass("show");
	});
}

// 순번대기표 관리에 처음 들어왔을 때 각 접수처별로 현재번호 가져오기
function admin3002() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin3002"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var h_700201_counter_1_number_exist = jsonObj.h_700201_counter_1_number_exist;
					var h_700201_counter_1_number = jsonObj.h_700201_counter_1_number;
					var h_700201_counter_1_number_pk = jsonObj.h_700201_counter_1_number_pk;

					var h_700201_counter_2_number_exist = jsonObj.h_700201_counter_2_number_exist;
					var h_700201_counter_2_number = jsonObj.h_700201_counter_2_number;
					var h_700201_counter_2_number_pk = jsonObj.h_700201_counter_2_number_pk;

					var h_700201_counter_3_number_exist = jsonObj.h_700201_counter_3_number_exist;
					var h_700201_counter_3_number = jsonObj.h_700201_counter_3_number;
					var h_700201_counter_3_number_pk = jsonObj.h_700201_counter_3_number_pk;



					var h_700202_counter_1_number_exist = jsonObj.h_700202_counter_1_number_exist;
					var h_700202_counter_1_number = jsonObj.h_700202_counter_1_number;
					var h_700202_counter_1_number_pk = jsonObj.h_700202_counter_1_number_pk;

					var h_700202_counter_2_number_exist = jsonObj.h_700202_counter_2_number_exist;
					var h_700202_counter_2_number = jsonObj.h_700202_counter_2_number;
					var h_700202_counter_2_number_pk = jsonObj.h_700202_counter_2_number_pk;

					var h_700202_counter_3_number_exist = jsonObj.h_700202_counter_3_number_exist;
					var h_700202_counter_3_number = jsonObj.h_700202_counter_3_number;
					var h_700202_counter_3_number_pk = jsonObj.h_700202_counter_3_number_pk;



					var h_700203_counter_1_number_exist = jsonObj.h_700203_counter_1_number_exist;
					var h_700203_counter_1_number = jsonObj.h_700203_counter_1_number;
					var h_700203_counter_1_number_pk = jsonObj.h_700203_counter_1_number_pk;

					var h_700203_counter_2_number_exist = jsonObj.h_700203_counter_2_number_exist;
					var h_700203_counter_2_number = jsonObj.h_700203_counter_2_number;
					var h_700203_counter_2_number_pk = jsonObj.h_700203_counter_2_number_pk;

					var h_700203_counter_3_number_exist = jsonObj.h_700203_counter_3_number_exist;
					var h_700203_counter_3_number = jsonObj.h_700203_counter_3_number;
					var h_700203_counter_3_number_pk = jsonObj.h_700203_counter_3_number_pk;


					if (h_700201_counter_1_number_exist == 'no') {
						global_h_700201_counter_1_number = 0;
						global_h_700201_counter_1_number_pk = 0;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700201+"]").find(".current_number_box").eq(0).children("div").eq(1).text('-');
					} else {
						global_h_700201_counter_1_number = h_700201_counter_1_number;
						global_h_700201_counter_1_number_pk = h_700201_counter_1_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700201+"]").find(".current_number_box").eq(0).children("div").eq(1).text(global_h_700201_counter_1_number);
					}

					if (h_700201_counter_2_number_exist == 'no') {
						global_h_700201_counter_2_number = 0;
						global_h_700201_counter_2_number_pk = 0;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700201+"]").find(".current_number_box").eq(1).children("div").eq(1).text('-');
					} else {
						global_h_700201_counter_2_number = h_700201_counter_2_number;
						global_h_700201_counter_2_number_pk = h_700201_counter_2_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700201+"]").find(".current_number_box").eq(1).children("div").eq(1).text(global_h_700201_counter_2_number);
					}

					if (h_700201_counter_3_number_exist == 'no') {
						global_h_700201_counter_3_number = 0;
						global_h_700201_counter_3_number_pk = 0;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700201+"]").find(".current_number_box").eq(2).children("div").eq(1).text('-');
					} else {
						global_h_700201_counter_3_number = h_700201_counter_3_number;
						global_h_700201_counter_3_number_pk = h_700201_counter_3_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700201+"]").find(".current_number_box").eq(2).children("div").eq(1).text(global_h_700201_counter_3_number);
					}





					if (h_700202_counter_1_number_exist == 'no') {
						global_h_700202_counter_1_number = 0;
						global_h_700202_counter_1_number_pk = 0;
					} else {
						global_h_700202_counter_1_number = h_700202_counter_1_number;
						global_h_700202_counter_1_number_pk = h_700202_counter_1_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700202+"]").find(".current_number_box").eq(0).children("div").eq(1).text(global_h_700202_counter_1_number);
					}

					if (h_700202_counter_2_number_exist == 'no') {
						global_h_700202_counter_2_number = 0;
						global_h_700202_counter_2_number_pk = 0;
					} else {
						global_h_700202_counter_2_number = h_700202_counter_2_number;
						global_h_700202_counter_2_number_pk = h_700202_counter_2_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700202+"]").find(".current_number_box").eq(1).children("div").eq(1).text(global_h_700202_counter_2_number);
					}

					if (h_700202_counter_3_number_exist == 'no') {
						global_h_700202_counter_3_number = 0;
						global_h_700202_counter_3_number_pk = 0;
					} else {
						global_h_700202_counter_3_number = h_700202_counter_3_number;
						global_h_700202_counter_3_number_pk = h_700202_counter_3_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700202+"]").find(".current_number_box").eq(2).children("div").eq(1).text(global_h_700202_counter_3_number);
					}






					if (h_700203_counter_1_number_exist == 'no') {
						global_h_700203_counter_1_number = 0;
						global_h_700203_counter_1_number_pk = 0;
					} else {
						global_h_700203_counter_1_number = h_700203_counter_1_number;
						global_h_700203_counter_1_number_pk = h_700203_counter_1_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700203+"]").find(".current_number_box").eq(0).children("div").eq(1).text(global_h_700203_counter_1_number);
					}

					if (h_700203_counter_2_number_exist == 'no') {
						global_h_700203_counter_2_number = 0;
						global_h_700203_counter_2_number_pk = 0;
					} else {
						global_h_700203_counter_2_number = h_700203_counter_2_number;
						global_h_700203_counter_2_number_pk = h_700203_counter_2_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700203+"]").find(".current_number_box").eq(1).children("div").eq(1).text(global_h_700203_counter_2_number);
					}

					if (h_700203_counter_3_number_exist == 'no') {
						global_h_700203_counter_3_number = 0;
						global_h_700203_counter_3_number_pk = 0;
					} else {
						global_h_700203_counter_3_number = h_700203_counter_3_number;
						global_h_700203_counter_3_number_pk = h_700203_counter_3_number_pk;

						$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+700203+"]").find(".current_number_box").eq(2).children("div").eq(1).text(global_h_700203_counter_3_number);
					}


				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 해당 병원 접수처에 다음 번호 가져오기
function admin3003(number_pk, status_checked_value, hospital_type, counter_number) {
	console.log("number_pk = "+number_pk);
	console.log("status_checked_value = "+status_checked_value);
	console.log("hospital_type = "+hospital_type);
	console.log("counter_number = "+counter_number);

	if (number_pk == null || number_pk == undefined) {
		number_pk = 0;
	}
	if (status_checked_value == null || status_checked_value == undefined) {
		status_checked_value = 0;
	}
	if (hospital_type == null || hospital_type == undefined) {
		hospital_type = 0;
	}
	if (counter_number == null || counter_number == undefined) {
		counter_number = 0;
	}

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin3003",
			number_pk:number_pk,
			status_checked_value:status_checked_value,
			hospital_type:hospital_type,
			counter_number:counter_number
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				var en_number = '';
				switch (counter_number) {
					case 1: en_number = 'one'; break;
					case 2: en_number = 'two'; break;
					case 3: en_number = 'three'; break;
					default: break;
				}

				$("input:radio[name=h"+hospital_type+en_number+"]:checked").prop('checked', false);

				if (result == "ok") {

					var number_pk = jsonObj.number_pk;
					var number = jsonObj.number;
					
					switch(hospital_type) {
						case 700201: // 본원 
							if (counter_number == 1) {
								global_h_700201_counter_1_number = number;
								global_h_700201_counter_1_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700201_counter_1_number);
									
							} else if (counter_number == 2) {
								global_h_700201_counter_2_number = number;
								global_h_700201_counter_2_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700201_counter_2_number);
							} else if (counter_number == 3) {
								global_h_700201_counter_3_number = number;
								global_h_700201_counter_3_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700201_counter_3_number);
							}
							break;
						case 700202: // 어린이병원 
							if (counter_number == 1) {
								global_h_700202_counter_1_number = number;
								global_h_700202_counter_1_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700202_counter_1_number);
									
							} else if (counter_number == 2) {
								global_h_700202_counter_2_number = number;
								global_h_700202_counter_2_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700202_counter_2_number);
							} else if (counter_number == 3) {
								global_h_700202_counter_3_number = number;
								global_h_700202_counter_3_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700202_counter_3_number);
							}
							break;
						case 700203: // 어린이병원 
							if (counter_number == 1) {
								global_h_700203_counter_1_number = number;
								global_h_700203_counter_1_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700203_counter_1_number);
									
							} else if (counter_number == 2) {
								global_h_700203_counter_2_number = number;
								global_h_700203_counter_2_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700203_counter_2_number);
							} else if (counter_number == 3) {
								global_h_700203_counter_3_number = number;
								global_h_700203_counter_3_number_pk = number_pk;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text(global_h_700203_counter_3_number);
							}
							break;

						default:

							break;
					}

				} else {
					alert("다음 대기중인 번호가 없습니다.");

					switch(hospital_type) {
						case 700201: // 본원 
							if (counter_number == 1) {
								global_h_700201_counter_1_number = 0;
								global_h_700201_counter_1_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
								
							} else if (counter_number == 2) {
								global_h_700201_counter_2_number = 0;
								global_h_700201_counter_2_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
							} else if (counter_number == 3) {
								global_h_700201_counter_3_number = 0;
								global_h_700201_counter_3_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
							}
							break;
						case 700202: // 어린이병원 
							if (counter_number == 1) {
								global_h_700202_counter_1_number = 0;
								global_h_700202_counter_1_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
									
							} else if (counter_number == 2) {
								global_h_700202_counter_2_number = 0;
								global_h_700202_counter_2_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
							} else if (counter_number == 3) {
								global_h_700202_counter_3_number = 0;
								global_h_700202_counter_3_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
							}
							break;
						case 700203: // 어린이병원 
							if (counter_number == 1) {
								global_h_700203_counter_1_number = 0;
								global_h_700203_counter_1_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
									
							} else if (counter_number == 2) {
								global_h_700203_counter_2_number = 0;
								global_h_700203_counter_2_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
							} else if (counter_number == 3) {
								global_h_700203_counter_3_number = 0;
								global_h_700203_counter_3_number_pk = 0;

								$(".admin3000.ticketadmin > ul.tab_content > li[hospital-type="+hospital_type+"]").find(".current_number_box").eq(counter_number-1).children("div").eq(1).text('-');
							}
							break;

						default:

							break;
					}	

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 다음 번호 받기 버튼 눌렀을 때 이벤트 설정하기
function admin3005() {
	$(".admin3000.ticketadmin .next_number_button").on("click", function(){


		if (isLoading == false) {
			var hospital_type = Number($(this).attr("hospital-type"));
			var counter_number = Number($(this).attr("counter-number"));

			console.log("hospital_type = "+hospital_type);
			console.log("counter_number = "+counter_number);

			var en_number = '';
			if (counter_number == 1) {
				en_number = 'one';
			} else if (counter_number == 2) {
				en_number = 'two';
			} else {
				en_number = 'three';
			}

			var status_checked_value = $(this).siblings(".current_number_check").find("input:radio[name=h"+hospital_type+en_number+"]:checked").val();
			




			switch (hospital_type) {
				case 700201: // 본원 
					if (counter_number == 1) {
						if (global_h_700201_counter_1_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700201_counter_1_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					} else if (counter_number == 2) {
						if (global_h_700201_counter_2_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700201_counter_2_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					} else if (counter_number == 3) {
						if (global_h_700201_counter_3_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700201_counter_3_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					}
					break;
				case 700202: // 어린이병원 
					if (counter_number == 1) {
						if (global_h_700202_counter_1_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700202_counter_1_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					} else if (counter_number == 2) {
						if (global_h_700202_counter_2_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700202_counter_2_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					} else if (counter_number == 3) {
						if (global_h_700202_counter_3_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700202_counter_3_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					}
					break;
				case 700203: // 암병원

					if (counter_number == 1) {
						if (global_h_700203_counter_1_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700203_counter_1_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					} else if (counter_number == 2) {
						if (global_h_700203_counter_2_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700203_counter_2_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					} else if (counter_number == 3) {
						if (global_h_700203_counter_3_number != 0) {
							if (status_checked_value == undefined) {
								alert("번호표 상태를 체크해주세요.");
							} else {
								admin3003(global_h_700203_counter_3_number_pk, status_checked_value, hospital_type, counter_number);
							}
						} else {
							admin3003(0, 0, hospital_type, counter_number);
						}
					}
					break;
				default:

					break;
			}


		} else {
			alert("작업이 진행중입니다.");
		} 
			





	});
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}