var normalLoginTry = false;
var refreshTry = false;

$(document).ready(function(){
	// admin0001(); // 다른 곳에서의 로그인 여부 체크하기

	admin1101(); // 윈도우 너비, 높이에 따라 메뉴 영역, 내용 영역 사이즈 조정하기
	admin1102(); // 윈도우 창이 리사이즈 될 때마다 이벤트 설정하기
	
	admin1103(); // 관리자 메뉴를 클릭했을 때 이벤트 설정하기

	admin1104(); // 처음 관리자 메인화면 접속시 보여줄 첫 화면 설정하기


	// $(window).bind("unload", function() { 
	// 	console.log("onunload 실행됨");

	// 	beforeunloads();
	// });

	setPushLoadingDisplaySetting();

});

function setPushLoadingDisplaySetting() {
	var width = $(window).width();
	var height = $(window).height();
	$(".push_loading_bar").css({"width":width+"px", "height":height+"px"});
	var marginTop = (height - $(".push_loading_bar .contents").height()) / 2;
	$(".push_loading_bar .contents").css({"margin-top":marginTop+"px"});

	$(window).resize(function(){
		var width = $(window).width();
		var height = $(window).height();
		$(".push_loading_bar").css({"width":width+"px", "height":height+"px"});
		var marginTop = (height - $(".push_loading_bar .contents").height()) / 2;
		$(".push_loading_bar .contents").css({"margin-top":marginTop+"px"});
	});
}

function showPushLoading() {
	$(".push_loading_bar").css({"display":"block"});
}

function hidePushLoading() {
	$(".push_loading_bar").css({"display":"none"});
}






function beforeunloads() {
	// admin2002();

	if (window.innerHeight <= 0 || window.innerWidth <= 0 || self.screenTop > 9000 || window.length == 0) {
		refreshTry = false;

		console.log("닫기 이벤트 발생");
	} else if (document.readyState == "complete"){

		refreshTry = true;
		console.log("새로고침 이벤트 발생");
	} else if (document.readyState=="loading"){

	    //다른 페이지 이동
	    console.log("다른페이지로이동 이벤트 발생");
	} else {
		alert("other");
		refreshTry = false;
		console.log("다 해당하지 않는 이벤트 발생");
	}

	if (normalLoginTry == false) {
		if (refreshTry == false) {
		    admin2002();
		}
	}
}


function onloads() {
	console.log("현재 refreshTry = "+refreshTry);
}

// 로그아웃 요청하기
function admin2002() {
	var admin_pk = Number($("input[name=admin_pk]").val());

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2002",
			admin_pk:admin_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			// console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("로그아웃 되었습니다.");
					location.href = "./admin1000.php";

				} else {

					alert("로그아웃중에 오류가 발생하였습니다.");
					window.parent.location.href = "./admin1000.php";

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}



// 윈도우 너비, 높이에 따라 메뉴 영역, 내용 영역 사이즈 조정하기
function admin1101() {

	var windowWidth = $(window).width();
	var windowHeight = $(window).height();

	var menuWindowheight = $(".admin1100.menu_window").height();
	var contentWindowHeight = windowHeight - menuWindowheight;

	$(".admin1100.content_window").css({"height":contentWindowHeight+"px"});
	$(".admin1100.content_window > iframe").css({"height":contentWindowHeight+"px"});

}

// 윈도우 창이 리사이즈 될 때마다 이벤트 설정하기
function admin1102() {
	$(window).resize(function(){
		admin1101();
	});
}

// 관리자 메뉴를 클릭했을 때 이벤트 설정하기
function admin1103() {
	$(".admin1100.menu_window ul.menu_list_box li a").on("click", function(){
		$(".admin1100.menu_window ul.menu_list_box li a.active").removeClass("active");
		$(this).addClass("active");

	});
}

// 처음 관리자 메인화면 접속시 보여줄 첫 화면 설정하기
function admin1104() {
	var pagecode = "admin2000";

	$(".admin1100.content_window > iframe").attr("src", "./"+pagecode+".php");
	$(".admin1100.menu_window ul.menu_list_box li a").each(function(){
		var aHref = $(this).attr("href");
		var onlyCdoe = aHref.split("/")[1];
		var onlyCode2 = onlyCdoe.split(".")[0];

		if (pagecode == onlyCode2) {
			$(this).click();
		}
	});
}





// 다른 곳에서의 로그인 여부 체크하기
function admin0001() {
	var admin_pk = Number($("input[name=admin_pk]").val());
	var admin_ip = $("input[name=admin_ip]").val();

	// console.log("admin_pk = "+admin_pk);
	// console.log("admin_ip = "+admin_ip);

	if (admin_ip != '' && admin_ip != undefined) {
		$.ajax({
			type: "POST",
			url: "./outlet.php",
			data:{
				act:"admin0001",
				admin_pk:admin_pk,
				admin_ip:admin_ip
			},
			timeout: 2000,
			beforeSend: function() {
				isLoading = true;
			},
			complete: function() {
				isLoading = false;
			},
			success: function(data) {
				// console.log("data = "+data);

				if (isJSON(data)) {
					var jsonObj = $.parseJSON(data);
					var result = jsonObj.result;

					if (result == "ok") {
						
						alert("다른 pc에서 로그인을 시도하여 로그아웃 됩니다.");
						window.parent.location.href = "./admin1000.php";

					} else {

						setTimeout(admin0001, 1500);

					}
				} else {

				}
			},
			fail: function() {
				
			}
		});
	}
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}