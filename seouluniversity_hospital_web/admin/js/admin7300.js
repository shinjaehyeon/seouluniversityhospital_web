var global_news_pk = 0;








$(document).ready(function(){
	getNewsPk();

	admin7301(); // 병원뉴스 상세정보 가져오기





	admin7305(); // 수정 버튼 클릭시 이벤트 설정하기


});

function getNewsPk() {
	global_news_pk = Number($("input[name=pk]").val());
}

// 병원뉴스 상세정보 가져오기
function admin7301() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7301",
			news_pk:global_news_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var admin_name = jsonObj.admin_name;
					var admin_id = jsonObj.admin_id;
					var title = htmlspecialchar_decode(jsonObj.title);
					console.log("title = "+title);
					var content = htmlspecialchar_decode(jsonObj.content);
					var datetime = jsonObj.datetime;
					var attach_file_flag = jsonObj.attach_file_flag;
					var view_index = jsonObj.view_index;
					var attach_file_array = jsonObj.attach_file_array;


					$("input[name=title]").val(title);

					$(".viewStyleD > ul > li > ul.original_files").empty();
					$.each(attach_file_array, function(key, value){
						var filename = value.filename;
						var fileurl = '../suhnews/suhnews'+global_news_pk+'/'+filename;

						var text = '';
						text += '<li class="clearFix" data-delete="0">';
                        text += '    <div class="filename">  ';
                        text += '        '+filename;
                        text += '    </div>';
                        text += '    <div class="delete_button" onclick="admin7302(this);">';
                        text += '        삭제';
                        text += '    </div>';
                        text += '</li>';

						$("ul.original_files").append(text);
					});

					

					$("textarea[name=content]").html(content);



				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}



function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}









// 수정하기전 업로드했던 파일 삭제버튼 클릭시 이벤트 설정하기
function admin7302(obj) {
	$(obj).parent().attr("data-delete", 1);
	$(obj).parent().css({"display":"none"});
}

// 뉴스 수정하기 버튼 클릭시 이벤트 설정하기
function admin7305() {
	$(".writeStyleA > .upload_button").on("click", function(){
		var title = $.trim($("input[name=title]").val());
		var content = $.trim($("textarea[name=content]").val());

		if (title == '') {
			alert("제목을 입력해주세요.");
			return false;
		}

		if (content == '') {
			alert("내용을 입력해주세요.");
			return false;
		}


		var arrayText = '';
		var num = $("ul.original_files li").length;
		for (var i=0; i<num; i++) {
			var isDelete = Number($("ul.original_files li").eq(i).attr("data-delete"));

			if (isDelete == 1) {
				arrayText+=$.trim($("ul.original_files li").eq(i).find(".filename").text());

				if (i != num-1) {
					arrayText+=',';
				}
			}
		}
		$("input[name=fileDeleteArray]").val(arrayText);





		$("form[name=newsForm]").submit();
	});
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}