var global_start_code = 800201;
var global_end_code = 800399;

var global_doctor_pk = 0;
var global_is_first_image = 0;

var totalThreadSuccess = 0;

$(document).ready(function(){
	getDoctorPk();
	admin9716(); // 댓글 관리 버튼 클릭시 이벤트 설정하기
	
	admin9705(); // 의료진 수정 버튼 클릭시 이벤트 설정하기

	admin9602(); // 이미지 파일 업로드시 미리보기 이미지 보여주기



	admin9604(); // 진료과 리스트 가져오기



	// 추가된 부분
	admin9607(); // 의료진 학력 추가 버튼 클릭시 이벤트 설정하기

	admin9611(); // 의료진 경력및연수 추가 버튼 클릭시 이벤트 설정하기

	
});

function getDoctorPk() {
	global_doctor_pk = Number($("input[name=doctor_pk]").val());
}

// 댓글 관리 버튼 클릭시 이벤트 설정하기
function admin9716() {
	$(".comment_button").on("click", function(){
		var pop = window.open("./admin9800.php?pk="+global_doctor_pk,"pop","width=780,height=600,top=130,left=600, scrollbars=yes, resizable=yes"); 
	});
}

// 의료진 수정 버튼 클릭시 이벤트 설정하기
function admin9705() {
	$(".formStlyeE.admin9600 .department_upload_button").on("click", function(){
		var doctor_department = $("select[name=doctorDepartment]").val();
		var doctor_name = $.trim($("input[name=doctorName]").val());
		var doctor_age = $.trim($("input[name=doctorAge]").val());
		var doctor_gender = $("select[name=doctorGender]").val();
		var doctor_image = $("input[name=doctorImage]").val();
		var doctor_content = $("textarea[name=doctorContent]").val();


		if (doctor_name == '') {
			alert("의료진 이름을 입력해주세요.");
			return false;
		}

		if (doctor_age == '') {
			alert("의료진 나이를 입력해주세요.");
			return false;
		}

		if (doctor_age < 0 || doctor_age > 120) {
			alert("나이는 0 ~ 120 사이의 값을 입력해주세요.");
			return false;
		}


		// console.log("doctor_image = "+doctor_image);



		// 의료진 학력
		var schoolList = '';
		var schoolNum = $("ul.school_list li").length;
		for (var i=0; i<schoolNum; i++) {
			var period = $.trim($("ul.school_list li").eq(i).children(".s_period").text());
			var content = $.trim($("ul.school_list li").eq(i).children(".s_content").text());

			var one_set = period+":"+content;

			schoolList+=one_set;
			if (i != schoolNum-1) {
				schoolList+=',';
			}
		}
		$("input[name=schoolArray]").val(schoolList);
			


		// 의료진 경력 및 연수
		var careerList = '';
		var careerNum = $("ul.career_list li").length;
		for (var i=0; i<careerNum; i++) {
			var period = $.trim($('ul.career_list li').eq(i).children(".c_period").text());
			var content = $.trim($('ul.career_list li').eq(i).children(".c_content").text());

			var one_set = period+":"+content;

			careerList+=one_set;
			if (i != careerNum-1) {
				careerList+=',';
			}
		}
		$("input[name=careerArray]").val(careerList);



		$("form[name=doctorAddForm]").submit();
	});
}

// 이미지 파일 업로드시 미리보기 이미지 보여주기
function admin9602() {
	$("input[name=doctorImage]").on('change', function(){
		if ($(this).val() == '') {
			$(".formStlyeE.admin9600 .imageDisplayView").css({"display":"none"});

			global_is_first_image = 0;
			$("input[name=is_first_image]").val(global_is_first_image);
		} else {
			$(".formStlyeE.admin9600 .imageDisplayView").css({"display":"block"});
        	readURL(this);
        }
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$(".department_preview").attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
    }
}



// 진료과 리스트 가져오기
function admin9604() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9604"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			
			isLoading = false;
		},
		success: function(data) {
			totalThreadSuccess++;

			// console.log("data = "+data);

			if (isJSON(data)) {
				// console.log("json이 맞아요~");
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					$.each(jsonObj.data, function(key, value){
						var suhd = value.suhd;
						var department = value.department;

						var text = '<option value="'+suhd+'">'+department+'</option>';

						$("select[name=doctorDepartment]").append(text);
					});

					// console.log("totalThreadSuccess = "+totalThreadSuccess);
					
				} else {


				}
			} else {
				// console.log("json이 아니에요~");
			}

			if (totalThreadSuccess >= 1) {
				admin9701(); // 의료진 상세정보 가져오기
			}


		},
		fail: function() {
			
		}
	});
}


// 의료진 상세정보 가져오기
function admin9701() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9701",
			doctor_pk:global_doctor_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			// console.log("admin9701 data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var name = jsonObj.name;
					console.log("name = "+name);
					var age = jsonObj.age;
					var gender = jsonObj.gender;
					var profile_image = jsonObj.profile_image;
					var image_exist = jsonObj.image_exist;
					var profile_info = jsonObj.profile_info;
					var affiliation_department = jsonObj.affiliation_department;
					var status = jsonObj.status;
					var result = jsonObj.result;
					var school_history = jsonObj.school_history;
					var career = jsonObj.career;

					console.log("school_history = "+school_history);
					console.log("career = "+career);

					$("select[name=doctorDepartment]").children("option").each(function(){
						var department_code = Number($(this).attr("value"));

						if (affiliation_department == department_code) {
							$(this).prop("selected", true);
						}
					});

					$("input[name=doctorName]").val(htmlspecialchar_decode(name));
					$("input[name=doctorAge]").val(htmlspecialchar_decode(age));

					$("select[name=doctorGender]").children("option").each(function(){
						var gender_code = Number($(this).attr("value"));

						if (gender == gender_code) {
							$(this).prop("selected", true);
						}
					});

					$("select[name=doctorStatus]").children("option").each(function(){
						var status_code = Number($(this).attr("value"));

						if (status == status_code) {
							$(this).prop("selected", true);
						}
					});

					if (profile_image == '') {
						$(".formStlyeE.admin9600 .imageDisplayView").css({"display":"none"});
						$(".formStlyeE.admin9600 .imageDisplayView img").attr("src", "");

						global_is_first_image = 0;
						$("input[name=is_first_image]").val(global_is_first_image);
					} else {
						$(".formStlyeE.admin9600 .imageDisplayView").css({"display":"block"});
						$(".formStlyeE.admin9600 .imageDisplayView img").attr("src", profile_image);

						global_is_first_image = 1;
						$("input[name=is_first_image]").val(global_is_first_image);
					}

					$("textarea[name=doctorContent]").val(htmlspecialchar_decode(profile_info));



					if (school_history != '') {
						var school_array = school_history.split(",");
						for (var i=0; i<school_array.length; i++) {
							var set = school_array[i];
							var set_array = set.split(":");
							var period = set_array[0];
							var content = set_array[1];


							var text = '';
							text += '<li class="clearFix">';
					        text += '    <div class="s_period">';
					        text += '        '+htmlspecialchar_decode(period);
					        text += '    </div>';
					        text += '    <div class="s_content">';
					        text += '        '+htmlspecialchar_decode(content);
					        text += '    </div>';
					        text += '    <div class="s_up_button" onclick="admin9608(this);">';
					        text += '        ▲';
					        text += '    </div>';
					        text += '    <div class="s_down_button" onclick="admin9609(this);">';
					        text += '        ▼';
					        text += '    </div>';
					        text += '    <div class="s_delete_button" onclick="admin9610(this);">';
					        text += '        제거';
					        text += '    </div>';
					        text += '</li>';

					        $("ul.school_list").append(text);
						}
					}



					if (career != '') {
						var career_array = career.split(",");
						for (var i=0; i<career_array.length; i++) {
							var set = career_array[i];
							var set_array = set.split(":");
							var period = set_array[0];
							var content = set_array[1];

							
							var text = '';
							text += '<li class="clearFix">';
					        text += '    <div class="c_period">';
					        text += '        '+htmlspecialchar_decode(period);
					        text += '    </div>';
					        text += '    <div class="c_content">';
					        text += '        '+htmlspecialchar_decode(content);
					        text += '    </div>';
					        text += '    <div class="c_up_button" onclick="admin9612(this);">';
					        text += '        ▲';
					        text += '    </div>';
					        text += '    <div class="c_down_button" onclick="admin9613(this);">';
					        text += '        ▼';
					        text += '    </div>';
					        text += '    <div class="c_delete_button" onclick="admin9614(this);">';
					        text += '        제거';
					        text += '    </div>';
					        text += '</li>';

					        $("ul.career_list").append(text);
						}
					}

					

				} else {


				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}



function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}




// 의료진 학력 추가 버튼 클릭시 이벤트 설정하기
function admin9607() {
	$(".school_add_button").on("click", function(){
		var period = $("input[name=doctorSchoolPeriod]").val();
		var content = $("input[name=doctorSchoolContent]").val();

		if (period == '') {
			alert("학력의 기간을 입력해주세요.");
			return false;
		}
		if (period.indexOf(':') >= 0 || period.indexOf(',') >= 0) {
			alert("' : ', ' , ' 문자는 사용할 수 없습니다.");
			return false;	
		}


		if (content == '') {
			alert("학력의 내용을 입력해주세요.");
			return false;
		}
		if (content.indexOf(':') >= 0 || period.indexOf(',') >= 0) {
			alert("' : ', ' , ' 문자는 사용할 수 없습니다.");
			return false;
		}

		var text = '';
		text += '<li class="clearFix">';
        text += '    <div class="s_period">';
        text += '        '+period;
        text += '    </div>';
        text += '    <div class="s_content">';
        text += '        '+content;
        text += '    </div>';
        text += '    <div class="s_up_button" onclick="admin9608(this);">';
        text += '        ▲';
        text += '    </div>';
        text += '    <div class="s_down_button" onclick="admin9609(this);">';
        text += '        ▼';
        text += '    </div>';
        text += '    <div class="s_delete_button" onclick="admin9610(this);">';
        text += '        제거';
        text += '    </div>';
        text += '</li>';

        $("ul.school_list").append(text);
	});
}

// 의료진 학력 리스트 업 버튼 클릭시 이벤트
function admin9608(obj) {
	var currentIndex = $(obj).parent().index();
	var lastIndex = $(obj).parent().parent().children("li").length - 1;

	if (currentIndex == 0) {
		alert("맨 처음입니다.");
	} else {
		var temp = $(obj).parent().detach();
		$("ul.school_list li").eq(currentIndex-1).before(temp);
	}
}

// 의료진 학력 리스트 다운 버튼 클릭시 이벤트
function admin9609(obj) {
	var currentIndex = $(obj).parent().index();
	var lastIndex = $(obj).parent().parent().children("li").length - 1;

	if (currentIndex == lastIndex) {
		alert("맨 마지막입니다.");
	} else {
		var temp = $(obj).parent().detach();
		$("ul.school_list li").eq(currentIndex).after(temp);
	}
}

// 의료진 학력 리스트 삭제 버튼 클릭시 이벤트
function admin9610(obj) {
	$(obj).parent().remove();
}



/////////////////////////////////////////////////


// 의료진 경력및연수 추가 버튼 클릭시 이벤트 설정하기
function admin9611() {
	$(".career_add_button").on("click", function(){
		var period = $("input[name=doctorCareerPeriod]").val();
		var content = $("input[name=doctorCareerContent]").val();

		if (period == '') {
			alert("경력 및 연수의 기간을 입력해주세요.");
			return false;
		}
		if (period.indexOf(':') >= 0 || period.indexOf(',') >= 0) {
			alert("' : ', ' , ' 문자는 사용할 수 없습니다.");
			return false;	
		}


		if (content == '') {
			alert("경력 및 연수의 내용을 입력해주세요.");
			return false;
		}
		if (content.indexOf(':') >= 0 || period.indexOf(',') >= 0) {
			alert("' : ', ' , ' 문자는 사용할 수 없습니다.");
			return false;
		}

		var text = '';
		text += '<li class="clearFix">';
        text += '    <div class="c_period">';
        text += '        '+period;
        text += '    </div>';
        text += '    <div class="c_content">';
        text += '        '+content;
        text += '    </div>';
        text += '    <div class="c_up_button" onclick="admin9612(this);">';
        text += '        ▲';
        text += '    </div>';
        text += '    <div class="c_down_button" onclick="admin9613(this);">';
        text += '        ▼';
        text += '    </div>';
        text += '    <div class="c_delete_button" onclick="admin9614(this);">';
        text += '        제거';
        text += '    </div>';
        text += '</li>';

        $("ul.career_list").append(text);
	});
}

// 의료진 경력및연수 리스트 업 버튼 클릭시 이벤트
function admin9612(obj) {
	var currentIndex = $(obj).parent().index();
	var lastIndex = $(obj).parent().parent().children("li").length - 1;

	if (currentIndex == 0) {
		alert("맨 처음입니다.");
	} else {
		var temp = $(obj).parent().detach();
		$("ul.career_list li").eq(currentIndex-1).before(temp);
	}
}

// 의료진 경력및연수 리스트 다운 버튼 클릭시 이벤트
function admin9613(obj) {
	var currentIndex = $(obj).parent().index();
	var lastIndex = $(obj).parent().parent().children("li").length - 1;

	if (currentIndex == lastIndex) {
		alert("맨 마지막입니다.");
	} else {
		var temp = $(obj).parent().detach();
		$("ul.career_list li").eq(currentIndex).after(temp);
	}
}

// 의료진 경력및연수 리스트 삭제 버튼 클릭시 이벤트
function admin9614(obj) {
	$(obj).parent().remove();
}






/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}