
var global_start_date = '';
var global_end_date = '';
var global_writer = 0;
var global_title = '';
var global_content = '';

var global_index = 0;
var global_view_num = 20;
var global_page_num = 10;
var global_page_reckoning = 1;




var global_delete_pks = new Array();
var global_delete_mode = false;






$(document).ready(function(){
	

	admin7001(); // 병원뉴스 검색필터의 검색버튼 클릭시 이벤트 설정하기
	admin7002(); // 관리자 회원 리스트 가져오기

	admin7010(); // 병원뉴스 리스트 가져오기
	admin7011(); // 회원 관리 리스트 - 다음 페이지 클릭시 이벤트 설정
	admin7012(); // 회원 관리 리스트 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
	admin7013(); // 회원 관리 리스트 - 이전 페이지 클릭시 이벤트 설정
	admin7014(); // 회원 관리 리스트 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정


	setDeleteModeButtonClickEvent(); // 삭제모드 버튼 클릭시 이벤트 설정하기
	deleteAllSelectButtonEvent(); // 전체선택 클릭시 이벤트 설정하기
	selectedListDeleteButtonEvent(); // 선택된 리스트 삭제 버튼 클릭시 이벤트 설정하기

});

// 삭제모드 버튼 클릭시 이벤트 설정하기
function setDeleteModeButtonClickEvent() {
	$(".delete_mode").on("click", function(){
		if (global_delete_mode == false) {
			global_delete_mode = true;
			$(this).text("삭제모드 닫기");
			$(".delete_mode_comment_box").css({"display":"block"});
			$(".all_select").css({"display":"block"});
			$(".selected_list_delete_button").css({"display":"block"});

			$(".boardTypeC ul li").off();
			$(".boardTypeC ul li .delete_check_box").css({"display":"block"});
		} else {
			global_delete_mode = false;
			$(this).text("삭제모드 열기");
			$(".delete_mode_comment_box").css({"display":"none"});
			$(".all_select").css({"display":"none"});
			$(".selected_list_delete_button").css({"display":"none"});

			admin7017();
			$(".boardTypeC ul li .delete_check_box").css({"display":"none"});
		}
	});
}

// 삭제박스 클릭시 이벤트 설정하기
function deleteCheckBoxClickEvent() {
	$(".delete_check_box").on("click", function(){
		var currentStatus = Number($(this).attr("data-status"));
		var currentPk = Number($(this).parent().attr("data-pk"));

		if (currentStatus == 0) {
			global_delete_pks.push(currentPk);
			$(this).attr("data-status", 1);
			$(this).css({"opacity":"1"});
		} else {
			global_delete_pks.splice(global_delete_pks.indexOf(currentPk),1);
			$(this).attr("data-status", 0);
			$(this).css({"opacity":"0"});
		}

		console.log(global_delete_pks);
		$(".selected_list_delete_button").text("선택된 리스트 삭제("+global_delete_pks.length+")");
	});
}

// 전체선택 클릭시 이벤트 설정하기
function deleteAllSelectButtonEvent() {
	$(".all_select").on("click", function(){
		var currentStatus = Number($(this).attr("data-status"));
		if (currentStatus == 0) {
			$(this).attr("data-status", 1);
			$(this).text("전체해제");
			$(".boardTypeC ul li .delete_check_box").attr("data-status", 1);
			$(".boardTypeC ul li .delete_check_box").css({"opacity":"1"});
			$(".boardTypeC ul li .delete_check_box").each(function(){
				var pk = Number($(this).parent().attr("data-pk"));
				if (global_delete_pks.indexOf(pk) == -1) {
					global_delete_pks.push(pk);
				}
			});

		} else {
			$(this).attr("data-status", 0);
			$(this).text("전체선택");
			$(".boardTypeC ul li .delete_check_box").attr("data-status", 0);
			$(".boardTypeC ul li .delete_check_box").css({"opacity":"0"});
			$(".boardTypeC ul li .delete_check_box").each(function(){
				var pk = Number($(this).parent().attr("data-pk"));
				if (global_delete_pks.indexOf(pk) != -1) {
					global_delete_pks.splice(global_delete_pks.indexOf(pk),1);
				}
			});
		}
		console.log(global_delete_pks);
		$(".selected_list_delete_button").text("선택된 리스트 삭제("+global_delete_pks.length+")");
	});
}

// 선택된 리스트 삭제 버튼 클릭시 이벤트 설정하기
function selectedListDeleteButtonEvent() {
	$(".selected_list_delete_button").on("click", function(){
		if (isLoading == false) {
			if(global_delete_pks.length == 0) {
				alert("선택된 리스트가 없습니다.");
			} else {
				admin7007(); // 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
			}
		} else {
			alert("작업이 진행 중입니다. 잠시 기다려주세요.");
		}
	});
}

// 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
function admin7007() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7007",
			delete_pks:global_delete_pks
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("선택한 리스트들이 삭제되었습니다.")
					location.href = "./admin7000.php";

				} else {

					alert("선택한 리스트를 삭제중에 오류가 발생하였습니다.");

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}



















// 병원뉴스 검색필터의 검색버튼 클릭시 이벤트 설정하기
function admin7001() {
	$(".searchFilterTypeA .finalSearchButton").on("click", function(){
		var start_date = $("input[name=startDate]").val();
		var end_date = $("input[name=endDate]").val();
		var wirter = $("select[name=writer]").val();
		var title = $("input[name=title]").val();
		var content = $("input[name=content]").val();

		global_start_date = start_date;
		global_end_date = end_date;
		global_writer = wirter;
		global_title = title;
		global_content = content;

		console.log("global_title = "+global_title);

		global_page_reckoning = 1;
		admin7010(); // 병원뉴스 리스트 가져오기

	});
}

// 관리자 회원 리스트 가져오기
function admin7002() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7002"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("admin7002 data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$("select[name=writer]").children("option").not(".default").remove();

					$.each(jsonObj.data, function(key, value){
						var suhadmin = value.suhadmin;
						var name = value.name;
						var id = value.id;

						var text = '<option value="'+suhadmin+'" class="default">'+name+'('+id+')</option>';
						$("select[name=writer]").append(text);
					});

				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}



// 병원뉴스 리스트 가져오기
function admin7010() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7010",
			index:global_index,
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			start_date:global_start_date,
			end_date:global_end_date,
			writer:global_writer,
			title:global_title,
			content:global_content
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$(".all_select").attr("data-status", 0);
				$(".all_select").text("전체선택");

				if (result == "ok") {
					$(".boardTypeC > ul").empty();

					$.each(jsonObj.data, function(key, value){
						var suhnews_pk = value.suhnews_pk;
						var admin = value.admin;
						var admin_name = value.admin_name;
						var admin_id = value.admin_id;
						var title = cutString(value.title, 24);
						var content = cutString(value.content, 80);
						var datetime = value.datetime;
						var attach_file_flag = value.attach_file_flag;
						var view_index = value.view_index;


						var text = '';
						text += '<li data-pk="'+suhnews_pk+'">';
		                text += '    <div class="datetime_writer_area clearFix">';
		                text += '		 <div>';
		                text += '		 	 '+suhnews_pk;
		                text += '		 </div>';
		                text += '        <div>';
		                text += '            '+datetime;
		                text += '        </div>';
		                text += '        <div>';
		                text += '            '+admin_name+'('+admin_id+') 작성';
		                text += '        </div>';
		                text += '    </div>';
		                text += '    <div class="title">';
		                text += '        '+title;
		                text += '    </div>';
		                text += '    <div class="content">';
		                text += '        '+content;
		                text += '    </div>';
		                text += '    <div class="view_index">';
		                text += '        <div>';
		                text += '            조회수';
		                text += '        </div>';
		                text += '        <div>';
		                text += '            '+view_index;
		                text += '        </div>';
		                text += '    </div>';

		                var styleText = '';
		                var dataStatus = 0;
		                if (global_delete_pks.indexOf(suhnews_pk) != -1) {
			            	styleText+='opacity:1;'; 
			            	dataStatus = 1;	
			            } else {
			            	styleText+='opacity:0;';
			            	dataStatus = 0;
			            }
			            if (global_delete_mode == false) {
			              	styleText+='display:none;';
			            } else {
			            	styleText+='display:block;';
			            }

			            text += '    <div class="delete_check_box" data-status="'+dataStatus+'" style="'+styleText+'">';
		                text += '        ';
		                text += '    </div>';

		                text += '</li>';


						

						$(".boardTypeC > ul").append(text);
					});

					if (global_delete_mode == false) {
						admin7017(); // 병원뉴스 리스트 클릭시 이벤트 설정하기
					}
					deleteCheckBoxClickEvent(); // 삭제박스 클릭시 이벤트 설정하기	




					if (global_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (global_index == 0) {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
							$(".pagingControllerBox > div > div.prev").css({"display":"none"});
						} else {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"block"});
							$(".pagingControllerBox > div > div.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".pagingControllerBox > div > div.best_next").css({"display":"block"});
							$(".pagingControllerBox > div > div.next").css({"display":"block"});	
						} else {
							$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
							$(".pagingControllerBox > div > div.next").css({"display":"none"});
						}


						$(".pagingControllerBox > div > ul").empty();



						var first_page_index = global_index;
						for (var ii=0; ii<global_page_num; ii++) {
							if (first_page_index % global_page_num == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("global_index = "+global_index);
							console.log("page_number-1 = "+(page_number-1));
							if (global_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".pagingControllerBox > div > ul").append(text);
						}


						admin7015(); // 페이지 번호 클릭시 이벤트 설정



						global_page_reckoning = 0;
					}
				} else {
					$(".boardTypeC > ul").empty();
				

					$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
					$(".pagingControllerBox > div > div.next").css({"display":"none"});

					$(".pagingControllerBox > div > ul").empty();
					$(".pagingControllerBox > div > ul").append('<li class="active">1</li>');
				}
			}
		},
		fail: function() {
			
		}
	});
}	

// 병원뉴스 페이징 - 다음 페이지 클릭시 이벤트 설정
function admin7011() {
	$(".pagingControllerBox > div > div.next").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + global_page_num;
		global_index = next_first_index;
		admin7010();
	});
}

// 병원뉴스 페이징 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function admin7012() {
	$(".pagingControllerBox > div > div.best_next").on("click", function(){
		if (isLoading == false) {
			admin7016(); // 병원뉴스 페이징 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 병원뉴스 페이징 - 이전 페이지 클릭시 이벤트 설정
function admin7013() {
	$(".pagingControllerBox > div > div.prev").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - global_page_num;
		global_index = prev_first_index;
		admin7010();
	});
}

// 병원뉴스 페이징 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function admin7014() {
	$(".pagingControllerBox > div > div.best_prev").on("click", function(){
		global_page_reckoning = 1;
		global_index = 0;
		admin7010();
	});
}


// 병원뉴스 페이징 - 페이지 번호 클릭시 이벤트 설정
function admin7015() {
	$(".pagingControllerBox > div > ul > li").on("click", function(){
		if (isLoading == false) {	
			$(".pagingControllerBox > div > ul > li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			global_index = selected_index;
			admin7010();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 병원뉴스 페이징 - 페이지 맨 마지막 번호 알아내기
function admin7016() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7016",
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			start_date:global_start_date,
			end_date:global_end_date,
			writer:global_writer,
			title:global_title,
			content:global_content
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					global_index = last_index;

					console.log("last_index = "+last_index);

					global_page_reckoning = 1;
					admin7010();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 병원뉴스 리스트 클릭시 이벤트 설정하기
function admin7017() {
	$(".boardTypeC > ul > li").on("click", function(){
		var pk = Number($(this).attr("data-pk"));

		location.href = "./admin7200.php?pk="+pk;
	});
}






/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}