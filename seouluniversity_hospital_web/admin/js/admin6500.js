var global_title_search_type = 1;
var global_title = '';
var global_first_word = 0;

var global_index = 0;
var global_view_num = 20;
var global_page_num = 10;
var global_page_reckoning = 1;


var global_delete_pks = new Array();
var global_delete_mode = false;



$(document).ready(function(){
	
	

	admin6505(); // 검색 버튼 클릭시 이벤트 설정하기

	admin6510(); // N의학정보 리스트 가져오기
	admin6511();
	admin6512();
	admin6513();
	admin6514();

	setDeleteModeButtonClickEvent(); // 삭제모드 버튼 클릭시 이벤트 설정하기
	deleteAllSelectButtonEvent(); // 전체선택 클릭시 이벤트 설정하기
	selectedListDeleteButtonEvent(); // 선택된 리스트 삭제 버튼 클릭시 이벤트 설정하기
});

// 삭제모드 버튼 클릭시 이벤트 설정하기
function setDeleteModeButtonClickEvent() {
	$(".delete_mode").on("click", function(){
		if (global_delete_mode == false) {
			global_delete_mode = true;
			$(this).text("삭제모드 닫기");
			$(".delete_mode_comment_box").css({"display":"block"});
			$(".all_select").css({"display":"block"});
			$(".selected_list_delete_button").css({"display":"block"});

			$(".listTypeD ul li").off();
			$(".listTypeD ul li .delete_check_box").css({"display":"block"});
		} else {
			global_delete_mode = false;
			$(this).text("삭제모드 열기");
			$(".delete_mode_comment_box").css({"display":"none"});
			$(".all_select").css({"display":"none"});
			$(".selected_list_delete_button").css({"display":"none"});

			admin6517();
			$(".listTypeD ul li .delete_check_box").css({"display":"none"});
		}
	});
}

// 삭제박스 클릭시 이벤트 설정하기
function deleteCheckBoxClickEvent() {
	$(".delete_check_box").on("click", function(){
		var currentStatus = Number($(this).attr("data-status"));
		var currentPk = Number($(this).parent().attr("data-pk"));

		if (currentStatus == 0) {
			global_delete_pks.push(currentPk);
			$(this).attr("data-status", 1);
			$(this).css({"opacity":"1"});
		} else {
			global_delete_pks.splice(global_delete_pks.indexOf(currentPk),1);
			$(this).attr("data-status", 0);
			$(this).css({"opacity":"0"});
		}

		console.log(global_delete_pks);
		$(".selected_list_delete_button").text("선택된 리스트 삭제("+global_delete_pks.length+")");
	});
}

// 전체선택 클릭시 이벤트 설정하기
function deleteAllSelectButtonEvent() {
	$(".all_select").on("click", function(){
		var currentStatus = Number($(this).attr("data-status"));
		if (currentStatus == 0) {
			$(this).attr("data-status", 1);
			$(this).text("전체해제");
			$(".listTypeD ul li .delete_check_box").attr("data-status", 1);
			$(".listTypeD ul li .delete_check_box").css({"opacity":"1"});
			$(".listTypeD ul li .delete_check_box").each(function(){
				var pk = Number($(this).parent().attr("data-pk"));
				if (global_delete_pks.indexOf(pk) == -1) {
					global_delete_pks.push(pk);
				}
			});

		} else {
			$(this).attr("data-status", 0);
			$(this).text("전체선택");
			$(".listTypeD ul li .delete_check_box").attr("data-status", 0);
			$(".listTypeD ul li .delete_check_box").css({"opacity":"0"});
			$(".listTypeD ul li .delete_check_box").each(function(){
				var pk = Number($(this).parent().attr("data-pk"));
				if (global_delete_pks.indexOf(pk) != -1) {
					global_delete_pks.splice(global_delete_pks.indexOf(pk),1);
				}
			});
		}
		console.log(global_delete_pks);
		$(".selected_list_delete_button").text("선택된 리스트 삭제("+global_delete_pks.length+")");
	});
}

// 선택된 리스트 삭제 버튼 클릭시 이벤트 설정하기
function selectedListDeleteButtonEvent() {
	$(".selected_list_delete_button").on("click", function(){
		if (isLoading == false) {
			if(global_delete_pks.length == 0) {
				alert("선택된 리스트가 없습니다.");
			} else {
				admin6507(); // 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
			}
		} else {
			alert("작업이 진행 중입니다. 잠시 기다려주세요.");
		}
	});
}

// 선택된 리스트의 pk 값으로 해당 리스트 삭제처리 하기
function admin6507() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6507",
			delete_pks:global_delete_pks
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("선택한 리스트들이 삭제되었습니다.")
					location.href = "./admin6500.php";

				} else {

					alert("선택한 리스트를 삭제중에 오류가 발생하였습니다.");

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}






// 검색 버튼 클릭시 이벤트 설정하기
function admin6505() {
	$(".searchFilterTypeA .finalSearchButton").on("click", function(){
		if (isLoading == false) {
			var title_search_type = Number($("select[name=titleSearchType]").val());
			var title = $("input[name=title]").val();
			var first_word = Number($("select[name=firstWord]").val());

			console.log(first_word);

			global_title_search_type = title_search_type;
			global_title = title;
			global_first_word = first_word;

			global_page_reckoning = 1;
			admin6510(); // N의학정보 리스트 가져오기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// N의학정보 리스트 가져오기
function admin6510() {
	console.log("global_first_word = "+global_first_word);

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6510",
			index:global_index,
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			title_search_type:global_title_search_type,
			title:global_title,
			first_word:global_first_word
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$(".all_select").attr("data-status", 0);
				$(".all_select").text("전체선택");

				if (result == "ok") {
					
					$(".listTypeD > ul").empty();

					$.each(jsonObj.data, function(key, value){
						var suhnmi = value.suhnmi;
						var title_ko = value.title_ko;
						var title_en = value.title_en;
						var one_line_description = value.one_line_description;
						

						var text = '';
						text += '<li data-pk="'+suhnmi+'" class="setTopVirtualBox">';
		                text += '    <div class="title clearFix">';
		                text += '        <div class="pk">';
		                text += '            '+suhnmi;
		                text += '        </div>';
		                text += '        <div class="real_title">';
		                text += '            '+cutString(title_ko, 15)+' ['+cutString(title_en, 15)+']';
		                text += '        </div>';
		                text += '    </div>';
		                text += '    <div class="one_description">';
		                text += '        '+one_line_description;
		                text += '    </div>';

		                var styleText = '';
		                var dataStatus = 0;
		                if (global_delete_pks.indexOf(suhnmi) != -1) {
			            	styleText+='opacity:1;'; 
			            	dataStatus = 1;	
			            } else {
			            	styleText+='opacity:0;';
			            	dataStatus = 0;
			            }
			            if (global_delete_mode == false) {
			              	styleText+='display:none;';
			            } else {
			            	styleText+='display:block;';
			            }

			            text += '    <div class="delete_check_box rounded" data-status="'+dataStatus+'" style="'+styleText+'">';
		                text += '        ';
		                text += '    </div>';

		                text += '</li>';
						


						

						$(".listTypeD > ul").append(text);
					});

					if (global_delete_mode == false) {
						admin6517(); // N의학정보 리스트 클릭시 이벤트 설정하기
					}

					deleteCheckBoxClickEvent(); // 삭제박스 클릭시 이벤트 설정하기



					if (global_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (global_index == 0) {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
							$(".pagingControllerBox > div > div.prev").css({"display":"none"});
						} else {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"block"});
							$(".pagingControllerBox > div > div.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".pagingControllerBox > div > div.best_next").css({"display":"block"});
							$(".pagingControllerBox > div > div.next").css({"display":"block"});	
						} else {
							$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
							$(".pagingControllerBox > div > div.next").css({"display":"none"});
						}


						$(".pagingControllerBox > div > ul").empty();



						var first_page_index = global_index;
						for (var ii=0; ii<global_page_num; ii++) {
							if (first_page_index % global_page_num == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("global_index = "+global_index);
							console.log("page_number-1 = "+(page_number-1));
							if (global_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".pagingControllerBox > div > ul").append(text);
						}


						admin6515(); // 페이지 번호 클릭시 이벤트 설정



						global_page_reckoning = 0;
					}

				} else {

					$(".listTypeD > ul").empty();
				

					$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
					$(".pagingControllerBox > div > div.next").css({"display":"none"});

					$(".pagingControllerBox > div > ul").empty();
					$(".pagingControllerBox > div > ul").append('<li class="active">1</li>');

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}


// N의학정보 - 다음 페이지 클릭시 이벤트 설정
function admin6511() {
	$(".pagingControllerBox > div > div.next").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + global_page_num;
		global_index = next_first_index;
		admin6510();
	});
}

// N의학정보 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function admin6512() {
	$(".pagingControllerBox > div > div.best_next").on("click", function(){
		if (isLoading == false) {
			admin6516(); // N의학정보 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// N의학정보 - 이전 페이지 클릭시 이벤트 설정
function admin6513() {
	$(".pagingControllerBox > div > div.prev").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - global_page_num;
		global_index = prev_first_index;
		admin6510();
	});
}

// N의학정보 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function admin6514() {
	$(".pagingControllerBox > div > div.best_prev").on("click", function(){
		global_page_reckoning = 1;
		global_index = 0;
		admin6510();
	});
}


// N의학정보 - 페이지 번호 클릭시 이벤트 설정
function admin6515() {
	$(".pagingControllerBox > div > ul > li").on("click", function(){
		if (isLoading == false) {	
			$(".pagingControllerBox > div > ul > li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			global_index = selected_index;
			admin6510();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// N의학정보 - 페이지 맨 마지막 번호 알아내기
function admin6516() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6516",
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			title_search_type:global_title_search_type,
			title:global_title,
			first_word:global_first_word
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					global_index = last_index;

					console.log("last_index = "+last_index);

					global_page_reckoning = 1;
					admin6510();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// N의학정보 리스트 클릭시 이벤트 설정하기
function admin6517() {
	$(".listTypeD > ul > li").on("click", function(){
		var pk = Number($(this).attr("data-pk"));

		
		location.href = "./admin6700.php?nmi="+pk;
	});
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}