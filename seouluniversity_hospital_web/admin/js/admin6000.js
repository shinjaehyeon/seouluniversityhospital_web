



$(document).ready(function(){
	
	admin6001(); // 검색 버튼 클릭시 이벤트 설정하기
	admin6005(); // 의료진 전체 리스트 가져오기

});


// 검색 버튼 클릭시 이벤트 설정하기
function admin6001() {
	$(".searchBoxTypeA .searchButton").on("click", function(){
		var searchType = Number($("select[name=doctorSearchType]").val());
		var searchValue = $("input[name=searchValue]").val();
		var ii = 1;

		if (searchType == 1) {
			// 의료진 코드 검색시
			if (searchValue != '') {
				var searchCode = Number(searchValue);

				$(".doctor_list_area ul li").each(function(){
					var doctor_pk = Number($(this).attr("data-pk"));

					if (doctor_pk == searchCode) {
						$(this).removeClass("hide");
						$(this).css({"margin-right":"2%"});
						if (ii%5 == 0) {
							$(this).css({"margin-right":"0px"});
						}
						ii++;
					} else {
						$(this).addClass("hide");
					}
				});
			} else {

				$(".doctor_list_area ul li").each(function(){
					var doctor_pk = Number($(this).attr("data-pk"));

					
					$(this).removeClass("hide");
					$(this).css({"margin-right":"2%"});
					if (ii%5 == 0) {
						$(this).css({"margin-right":"0px"});
					}
					ii++;
					
				});
			}


		} else if (searchType == 2) {
			// 소속진료과 명칭

			$(".doctor_list_area ul li").each(function(){
				var doctor_department = $(this).attr("data-deaparment")+"";

				if (doctor_department.indexOf(searchValue) != -1) {
					$(this).removeClass("hide");
					$(this).css({"margin-right":"2%"});
					if (ii%5 == 0) {
						$(this).css({"margin-right":"0px"});
					}
					ii++;
				} else {
					$(this).addClass("hide");
				}
			});

		}
	});
}


// 의료진 전체 리스트 가져오기
function admin6005() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6005"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					$(".doctor_list_area ul").empty();

					$.each(jsonObj.data, function(key, value){
						var doctor_pk = value.doctor_pk;
						var doctor_name = value.doctor_name;
						var doctor_department = value.doctor_department;
						var doctor_department_string = value.doctor_department_string;

						var text = '';
						text += '<li class="setTopVirtualBox" data-pk="'+doctor_pk+'" data-deaparment="'+doctor_department_string+'" data-departmentint="'+doctor_department+'">';
		                text += '    <div class="department">'+doctor_department_string+'</div>';
		                text += '    <div class="doctor_name">'+doctor_name+'</div>';
		                text += '</li>';

		                $(".doctor_list_area ul").append(text);	
					});

					admin6006(); // 의료진 클릭시 이벤트 설정하기

				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}	

// 의료진 클릭시 이벤트 설정하기
function admin6006() {
	$(".doctor_list_area ul li").on("click", function(){
		var doctor_pk = Number($(this).attr("data-pk"));
		var doctor_department = $(this).attr("data-deaparment");
		var doctor_departmentint = Number($(this).attr("data-departmentint"));
		var doctor_name = $(this).children(".doctor_name").text();

		location.href = "./admin6100.php?doctor_pk="+doctor_pk+"&doctor_name="+doctor_name+"&doctor_department="+doctor_department+"&doctor_departmentint="+doctor_departmentint;
	});
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}