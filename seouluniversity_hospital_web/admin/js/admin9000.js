var global_search_type = 1;
var global_search_value = '';








$(document).ready(function(){
	
	
	admin9004(); // 검색 타입 값이 바뀔때 이벤트 설정하기
	admin9005(); // 검색 버튼 클릭시 이벤트 설정하기

	admin9010(); // 진료과 리스트 가져오기

});


// 검색 타입 값이 바뀔때 이벤트 설정하기
function admin9004() {
	$(".searchBoxTypeA .searchType").on("change", function(){
		global_search_type = $(this).val();
	});
}

// 검색 버튼 클릭시 이벤트 설정하기
function admin9005() {
	$(".searchBoxTypeA .searchButton").on("click", function(){
		global_search_value = $(".searchBoxTypeA > .inputArea > input").val();

		admin9010(); // 진료과 리스트 가져오기
	});
}

// 진료과 리스트 가져오기
function admin9010() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9010",
			search_type:global_search_type,
			search_value:global_search_value
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			


			if (isJSON(data)) {
				
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;


				$(".department_list_area > ul").empty();

				if (result == "ok") {
					
					$.each(jsonObj.data, function(key, value){
						var department_pk = value.department_pk;
						var department_title = value.department_title;

						var text = '';
						text += '<li data-pk="'+department_pk+'">';
		                text += '    <div class="department_code">';
		                text += '        '+department_pk;
		                text += '    </div>';
		                text += '    <div class="department_title">';
		                text += '        '+department_title;
		                text += '    </div>';
		                text += '</li>';

		                $(".department_list_area > ul").append(text);
					});

					admin9011(); // 진료과 리스트 아이템 클릭시 이벤트 설정하기
					
				} else {


				}
			} else {
				
			}
		},
		fail: function() {
			
		}
	});
}

// 진료과 리스트 아이템 클릭시 이벤트 설정하기
function admin9011() {
	$(".department_list_area > ul > li").on("click", function(){
		var department_pk = Number($(this).attr("data-pk"));

		console.log("department_pk = "+department_pk);

		location.href = "./admin9200.php?department_pk="+department_pk;
	});
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}