var globalDoctorPrimarykey = 0;
var globalDoctorName = '';
var globalDoctorDepartment = '';
var globalDoctorDepartmentInt = 0;



var currentYear = null;
var currentMonth = null;

var currentSelectedYear = null;
var currentSelectedMonth = null;
var currentSelectedDate = null;
var dayArray = ['일', '월', '화', '수', '목', '금', '토'];

var selected_year = null;
var selected_month = null;
var selected_date = null;
var selected_day = null;

var selected_time = '';



$(document).ready(function(){

	getDoctorInfo(); // 의사 정보 저장하기
	startCalendar(); // 캘린더 시작하기

	admin6101(); // 미방문자 업데이트 하기 버튼 클릭시 이벤트 설정하기
	admin6105(); // 진료가능시간 추가하기 버튼 클릭시 이벤트 설정하기

	admin6115(); // 처방약품 대분류 리스트 가져오기
	admin6116(); // 처방약품 선택박스 값을 바꿀 때마다 이벤트 설정하기
	admin6119(); // 처방약을 선택한 후 추가하기를 눌렀을 때 이벤트 설정하기
	admin6130(); // 진단서 작성하기 버튼 클릭 시 이벤트 설정하기
});

// 의사 정보 저장하기
function getDoctorInfo() {
	globalDoctorPrimarykey = Number($("input[name=doctor_pk]").val());
	globalDoctorName = $("input[name=doctor_name]").val();
	globalDoctorDepartment = $("input[name=doctor_department]").val();
	globalDoctorDepartmentInt = $("input[name=doctor_departmentint]").val();
}

// 미방문자 업데이트 하기 버튼 클릭시 이벤트 설정하기
function admin6101() {
	$(".not_enter_update").on("click", function(){
		if (isLoading == false) {
			admin6102(); // 미방문자 업데이트 하기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 미방문자 업데이트 하기
function admin6102() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6102",
			doctor_pk:globalDoctorPrimarykey
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;	
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("미방문자가 업데이트 되었습니다.");

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 진료가능시간 추가하기 버튼 클릭시 이벤트 설정하기
function admin6105() {
	$(".group1 .clinic_time_list_box .time_add_area .add_button").on("click", function(){
		if (admin6106()) {

			admin6107(); // 선택된 날짜/시간을 의료진 스케쥴에 추가히기

		}
	});
}

// 날짜/시간이 선택되었는지 체크하기
function admin6106() {
	if (selected_year == null || selected_month == null || selected_date == null) {
		alert("날짜를 선택해주세요.");
		return false;
	}

	var timeValue = $("input[name=clinicTime]").val();
	console.log("timeValue = "+timeValue);
	if (timeValue == '') {
		alert("시간을 설정해주세요.")
		return false;
	}
	selected_time = timeValue;

	return true;
}


// 선택된 날짜/시간을 의료진 스케쥴에 추가하기
function admin6107() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6107",
			doctor_pk:globalDoctorPrimarykey,
			selected_year:selected_year,
			selected_month:selected_month,
			selected_date:selected_date,
			selected_time:selected_time
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;	
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					admin6110(); // 해당 날짜에 해당하는 의료진 스케쥴 불러오기

				} else if (result == "already") {
						
					alert('이미 등록된 시간입니다. 다른 시간을 선택해주세요.');
					
				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 해당 날짜에 해당하는 의료진 스케쥴 불러오기
function admin6110() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6110",
			doctor_pk:globalDoctorPrimarykey,
			selected_year:selected_year,
			selected_month:selected_month,
			selected_date:selected_date
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;	
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$(".group1 .clinic_time_list_box .list_area ul").empty();

				if (result == "ok") {
					
					console.log("여기 실행 됬잖아");

					$.each(jsonObj.data, function(key, value){
						var suhds = value.suhds;
						var posible_datetime = value.posible_datetime;
						var hour = attachZero(value.hour);
						var minute = attachZero(value.minute);
						var member_pk = value.member_pk;
						var member_name = value.member_name;
						var member_phone = value.member_phone;
						var member_status = value.member_status;
						var is_reservation = value.is_reservation;



						var name_phone_content = '';
						var reservationClass = '';
						var cancel_button_class = '';
						if (is_reservation == 'ok') {
							// 예약중이면
							reservationClass = ' reservation';
							cancel_button_class = ' hide';

							name_phone_content += '    <div class="member_name">';
	                        name_phone_content += '        '+member_name+' 예약함';
	                        name_phone_content += '    </div>';
	                        name_phone_content += '    <div class="member_phone">';
	                        name_phone_content += '        ('+member_phone+')';
	                        name_phone_content += '    </div>';
						} else if (is_reservation == 'no') {
							reservationClass = '';
							cancel_button_class = '';

							name_phone_content += '    <div class="member_name">';
	                        name_phone_content += '        예약된 내역이';
	                        name_phone_content += '    </div>';
	                        name_phone_content += '    <div class="member_phone">';
	                        name_phone_content += '        없습니다.';
	                        name_phone_content += '    </div>';
						} else if (is_reservation == 'end') {
							reservationClass = '';
							cancel_button_class = ' hide';

							name_phone_content += '    <div class="member_name">';
	                        
							if (member_status == 500202) {
	                       		name_phone_content += '        '+member_name+' 진료완료';
							} else if (member_status == 500204) {
								name_phone_content += '        '+member_name+' 미방문';
							}
	                        

	                        name_phone_content += '    </div>';
	                        name_phone_content += '    <div class="member_phone">';
	                        name_phone_content += '        ('+member_phone+')';
	                        name_phone_content += '    </div>';

						}



						var text = '';
						text += '<li class="clearFix setTopVirtualBox'+reservationClass+'" member-pk="'+member_pk+'" member-name="'+member_name+'" schedule-pk="'+suhds+'">';
                        text += '    <div class="time">';
                        text += '        '+hour+':'+minute;
                        text += '    </div>';
                        text += name_phone_content;
                        text += '    <div class="cancel_button'+cancel_button_class+'" onclick="admin6111(this)">';
                        text += '        취소';
                        text += '    </div>';
                        text += '</li>';


                        $(".group1 .clinic_time_list_box .list_area ul").append(text);
					});

					admin6112(); // 진료예약된 시간을 클릭했을 시 이벤트 설정하기

				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}


// 진료예약된 시간 리스트에서 취소 버튼 클릭했을 때 이벤트 설정하기
function admin6111(obj) {
	var fullDate = selected_year+'-'+attachZero(selected_month)+'-'+attachZero(selected_date);
	var time = $.trim($(obj).siblings(".time").text())+':00';

	if (confirm("정말 해당 일정을 삭제하시겠습니까?")) {
		admin6113(fullDate, time); // 의료진 해당 시간 스케쥴 삭제하기
	}
}


// 진료예약된 시간을 클릭했을 시 이벤트 설정하기
function admin6112() {
	$(".group1 .clinic_time_list_box .list_area ul li.reservation").on("click", function(){
		var schedule_pk = Number($(this).attr("schedule-pk"));
		var member_pk = Number($(this).attr("member-pk"));
		var member_name = $(this).attr("member-name");

		console.log("schedule_pk = "+schedule_pk);
		console.log("member_pk = "+member_pk);
		console.log("member_name = "+member_name);

		$(".medical_certificate_box").addClass("show");
		$(".medical_certificate_box").attr("schedule-pk", schedule_pk);
		$(".medical_certificate_box").attr("member-pk", member_pk);
		$(".medical_certificate_box ul.medical_certificate_form_box li input[name=resultName]").val(member_name);
		
		$("select[name=resultDrug2]").children("option").not(".default").remove();
		$("select[name=resultDrug3]").children("option").not(".default").remove();
		$(".medical_certificate_box ul.medical_certificate_form_box li ul.drug_list_ul").empty();
		$(".medical_certificate_box ul.medical_certificate_form_box li textarea[name=doctor_opinion]").val("");

		// 증상 정보 가져오기
		$.ajax({
			type: "POST",
			url: "./outlet.php",
			data:{
				act:"admin6114",
				member_pk:member_pk,
				schedule_pk:schedule_pk
			},
			timeout: 2000,
			beforeSend: function() {
				
			},
			complete: function() {
				
			},
			success: function(data) {
				console.log("data = "+data);

				if (isJSON(data)) {
					var jsonObj = $.parseJSON(data);
					var result = jsonObj.result;

					if (result == "ok") {
						
						var symptom = jsonObj.symptom;
						if (symptom == "") {
							$(".symptom").html("증상 미입력");
						} else {
							$(".symptom").html(symptom);
						}	

					} else {
						

					}
				}
			},
			fail: function() {
				
			}
		});
	});	
}

// 의료진 해당 시간 스케쥴 삭제하기
function admin6113(fullDate, time) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6113",
			doctor_pk:globalDoctorPrimarykey,
			fullDate:fullDate,
			fullTime:time
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("삭제되었습니다.");
					admin6110(); // 해당 날짜에 해당하는 의료진 스케쥴 불러오기

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});

}

// 처방약품 대분류 리스트 가져오기
function admin6115() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6115"
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			// console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$("select[name=resultDrug1]").children("option").not(".default").remove();

					$.each(jsonObj.data, function(key, value){
						var suhdbt = value.suhdbt;
						var description = value.description;

						var text = '<option value="'+suhdbt+'">'+description+'</option>';

						$("select[name=resultDrug1]").append(text);
					});

					

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}


// 처방약품 선택박스 값을 바꿀 때마다 이벤트 설정하기
function admin6116() {
	// 대분류 값을 변경했을 경우
	$("select[name=resultDrug1]").on("change", function(){
		if (isLoading == false) {
			var value = Number($(this).val());

			if (value == 0) {
				$("select[name=resultDrug2]").children("option").not(".default").remove();
				$("select[name=resultDrug3]").children("option").not(".default").remove();
			} else {
				admin6117(value); // 대분류 선택된 값에 따라 다음 소분류 리스트 가져오기
			}
		} else {
			alert("작업이 진행중입니다.");
		}

	});


	// 소분류 값을 변경했을 경우
	$("select[name=resultDrug2]").on("change", function(){
		if (isLoading == false) {
			var value = Number($(this).val());
			var resultDrug1 = $("select[name=resultDrug1]").val();

			if (value == 0) {
				$("select[name=resultDrug3]").children("option").not(".default").remove();
			} else {
				admin6118(resultDrug1, value); // 소분류 선택된 값에 따라 다음 약품 리스트 가져오기
			}
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 대분류 선택된 값에 따라 다음 소분류 리스트 가져오기
function admin6117(resultDrug1Value) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6117",
			resultDrug1Value:resultDrug1Value
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;	
		},
		success: function(data) {
			// console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$("select[name=resultDrug2]").children("option").not(".default").remove();

					$.each(jsonObj.data, function(key, value){
						var suhdst = value.suhdst;
						var description = value.description;

						var text = '<option value="'+suhdst+'">'+description+'</option>';

						$("select[name=resultDrug2]").append(text);
					});

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 소분류 선택된 값에 따라 다음 약품 리스트 가져오기
function admin6118(resultDrug1Value, resultDrug2Value) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6118",
			resultDrug1Value:resultDrug1Value,
			resultDrug2Value:resultDrug2Value
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;	
		},
		success: function(data) {
			// console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$("select[name=resultDrug3]").children("option").not(".default").remove();

					$.each(jsonObj.data, function(key, value){
						var suhdl = value.suhdl;
						var title = value.title;

						var text = '<option value="'+suhdl+'">'+title+'</option>';

						$("select[name=resultDrug3]").append(text);
					});

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 처방약을 선택한 후 추가하기를 눌렀을 때 이벤트 설정하기
function admin6119() {
	$(".medical_certificate_box ul.medical_certificate_form_box li .drug_add_button").on("click", function(){
		if (isLoading == false) {
			var drugPrimarykey = Number($("select[name=resultDrug3]").val());
			console.log("drugPrimarykey = "+drugPrimarykey);


			if (drugPrimarykey == 0) {
				alert("처방약품이 선택되지 않았습니다.");
			} else {

				var checkNum = 0;
				$(".medical_certificate_box ul.medical_certificate_form_box li ul.drug_list_ul li").each(function(){
					var exist_drug_pk = Number($(this).find(".only_pk").text());
					if (exist_drug_pk == drugPrimarykey) {
						checkNum++;
					}
				});


				if (checkNum == 0) {

					var target = document.getElementById("resultDrug3");
					var drug_name = target.options[target.selectedIndex].text;

					var text = '';

	                text += '<li class="clearFix setTopVirtualBox">';
	                text += '    <div class="clearFix">';
	                text += '        <div class="drug_pk">';
	                text += '            No.<span class="only_pk">'+drugPrimarykey+'</span>';
	                text += '        </div>';
	                text += '        <div class="drug_name">';
	                text += '            '+drug_name;
	                text += '        </div>  ';
	                text += '        <div class="drug_note">';
	                text += '            <input type="text" placeholder="약품 비고 설명(선택)" />';
	                text += '        </div>';
	                text += '        <div class="drug_remove" onclick="admin6120(this)">';
	                text += '            제거';
	                text += '        </div>';
	                text += '    </div>';
	                text += '    <div class="clearFix">';
	                text += '        <input class="oneday_dose" type="number" step="0.1" placeholder="하루 투여량" />';
	                text += '        <input class="oneday_number" type="number" step="1" placeholder="하루 투여 횟수" />';
	                text += '        <input class="dose_day_num" type="number" step="1" placeholder="투여 일수" />';
	                text += '    </div>';
	                text += '</li>';

	                $(".medical_certificate_box ul.medical_certificate_form_box li ul.drug_list_ul").append(text);
	            
	            } else {
	            	alert("이미 리스트에 존재하는 약품입니다. 같은 약을 여러개 처방하는 것을 원할 경우 약품비고 설명에 기재해주세요.");
	            }
			}
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}	

// 처방약 리스트의 삭제를 눌렀을 때 이벤트 설정하기
function admin6120(obj) {
	$(obj).parent().parent().remove();
}


// 진단서 작성하기 버튼 클릭 시 이벤트 설정하기
function admin6130() {
	$(".medical_certificate_box .upload_button").on("click", function(){
		var schedule_pk = Number($(".medical_certificate_box").attr("schedule-pk"));
		var member_pk = Number($(".medical_certificate_box").attr("member-pk"));
		var doctor_pk = globalDoctorPrimarykey;
		var doctor_departmnet_pk = globalDoctorDepartmentInt;

		var doctor_opinion = $("textarea[name=doctor_opinion]").val();


		var drug_pk_array = new Array();
		var drug_note_array = new Array();
		var drug_oneday_dose_array = new Array();
		var oneday_number_array = new Array();
		var dose_day_num_array = new Array();

		$(".medical_certificate_box ul.medical_certificate_form_box li ul.drug_list_ul li").each(function(){
			var drug_pk = Number($(this).find(".only_pk").text());
			var drug_note = $(this).find(".drug_note").children("input").val();
			var drug_oneday_dose = $(this).find(".oneday_dose").val();
			var oneday_number = $(this).find(".oneday_number").val();
			var dose_day_num = $(this).find(".dose_day_num").val();

			drug_pk_array.push(drug_pk);
			drug_note_array.push(drug_note);
			drug_oneday_dose_array.push(drug_oneday_dose);
			oneday_number_array.push(oneday_number);
			dose_day_num_array.push(dose_day_num);
		});


		if (confirm("입력하신 내용대로 진단서를 작성하시겠습니까?")) {
			admin6131(schedule_pk, member_pk, doctor_pk, doctor_departmnet_pk, doctor_opinion, drug_pk_array, drug_note_array, drug_oneday_dose_array, oneday_number_array, dose_day_num_array); // 진단서 내용 서버에 저장하기
		}

	});
}

// 진단서 내용 서버에 저장하기
function admin6131(schedule_pk, member_pk, doctor_pk, doctor_departmnet_pk, doctor_opinion, drug_pk_array, drug_note_array, drug_oneday_dose_array, oneday_number_array, dose_day_num_array) {
	console.log("admin6131 실행됨");

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6131",
			schedule_pk:schedule_pk,
			member_pk:member_pk,
			doctor_pk:doctor_pk,
			doctor_departmnet_pk:doctor_departmnet_pk,
			doctor_opinion:doctor_opinion,
			drug_pk_array:drug_pk_array,
			drug_note_array:drug_note_array,
			drug_oneday_dose_array:drug_oneday_dose_array,
			oneday_number_array:oneday_number_array,
			dose_day_num_array:dose_day_num_array
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;	
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					$(".medical_certificate_box").removeClass("show");
					$(".medical_certificate_box").attr("schedule-pk", "");
					$(".medical_certificate_box").attr("member-pk", "");
					$(".medical_certificate_box ul.medical_certificate_form_box li input[name=resultName]").val("");
				
					$("select[name=resultDrug2]").children("option").not(".default").remove();
					$("select[name=resultDrug3]").children("option").not(".default").remove();
					$(".medical_certificate_box ul.medical_certificate_form_box li ul.drug_list_ul").empty();
					$(".medical_certificate_box ul.medical_certificate_form_box li textarea[name=doctor_opinion]").val("");

					alert("진단서가 업로드 되었습니다.");

					admin6110(); // 해당 날짜에 해당하는 의료진 스케쥴 불러오기

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}






























// 캘린더 시작하기
function startCalendar() {
	var today = new Date();

    // 오늘 년도 구하기
    currentYear = today.getFullYear();

    // 오늘 월 구하기
    currentMonth = today.getMonth();

    // 파라미터 year년 month월의 캘린더 그리기
    fillCalendar(currentYear, currentMonth);

    // 이전달/다음달 버튼 이벤트 설정하기
    setPrevNextMonthButtonEvent();

    // 날짜를 선택하면 활성화 표시 하기
    setClickDateEvent();
}  

// 파라미터 year년 month월의 캘린더 그리기
function fillCalendar(year, month) {
	// 오늘 정보 가져오기
	var today = new Date();

    todayYear = today.getFullYear();
    todayMonth = today.getMonth();
    todayDate = today.getDate();


    // year년 month월 1일의 요일 index 구하기
    var dates = new Date(year+'-'+(month+1)+'-01').getDay(); // 일요일 0, 월요일 1, ... , 토요일 6
    // console.log("dates = "+dates);

    var startIndex = dates;

    var lastDate = Number(lastDay(year+'-'+(month+1)+'-01').split("-")[2]);
    // console.log("lastDate = "+lastDate);

    $(".year_month_display .year").text(currentYear+"년");
    $(".year_month_display .month").text((currentMonth+1)+"월");
    

    // 효과 모두 지우기
    $(".date_display_area ul li").find(".date").removeClass("prevnext");
    $(".date_display_area ul li.active").removeClass("active");
    $(".date_display_area ul li.normal1").removeClass("normal1");
    $(".date_display_area ul li.normal2").removeClass("normal2");
    $(".date_display_area ul li").attr("data-year", "");
    $(".date_display_area ul li").attr("data-month", "");
    $(".date_display_area ul li").attr("data-date", "");
    $(".date_display_area ul li .today").css({"display":"none"});

    var jj = 1;
    for (var i=0; i<42; i++) {
        if (i < startIndex) {
            continue;
        }

        if (jj <= lastDate) {
            var classname = "";
            if (i % 2 == 0) {
                classname = "normal1";
            } else {
                classname = "normal2";
            }
            $(".date_display_area ul li").eq(i).attr("data-year", currentYear);
            $(".date_display_area ul li").eq(i).attr("data-month", currentMonth + 1);
            $(".date_display_area ul li").eq(i).attr("data-date", jj);
            $(".date_display_area ul li").eq(i).addClass(classname);
            $(".date_display_area ul li").eq(i).find(".date").text(jj+"");
            
            if (Number(currentSelectedYear) == Number(currentYear) && Number(currentSelectedMonth)-1 == Number(currentMonth) && Number(currentSelectedDate) == Number(jj)) {
                $(".date_display_area ul li").eq(i).addClass("active");
            }

            if (Number(currentYear) == Number(todayYear) && Number(currentMonth) == Number(todayMonth) && Number(todayDate) == Number(jj)) {
            	$(".date_display_area ul li").eq(i).children(".today").css({"display":"block"});
            }

            jj++;
        }

    }

    // 이전달 일 뿌리기

    // 이전달의 마지막날 구하기
    var prev_year = null;
    var prev_month = null;
    if (currentMonth - 1 < 0) {
        prev_year = currentYear - 1;
        prev_month = 11;
    } else {
        prev_year = currentYear;
        prev_month = currentMonth - 1;
    }

    var prev_lastDate = Number(lastDay(prev_year+'-'+(prev_month+1)+'-01').split("-")[2]);
    
    for (var i=startIndex-1; i>=0; i--) {
        $(".date_display_area ul li").eq(i).find(".date").addClass("prevnext");
        $(".date_display_area ul li").eq(i).find(".date").text(prev_lastDate+"");
        prev_lastDate--;
    }



    // 댜음달 일 뿌리기
    var ll = 1;
    for (var i=startIndex+jj-1; i<42; i++) {
        $(".date_display_area ul li").eq(i).find(".date").addClass("prevnext");
        $(".date_display_area ul li").eq(i).find(".date").text(ll+"");
        ll++;
    }   
}

//현재달의 마지막날(일자) 구하기
function lastDay(date_str){
    var date = new Date(date_str);
    date.setMonth( date.getMonth() + 1);
    date.setDate(0);
    return converDateString(date); 
}

function converDateString(dt){
    return dt.getFullYear() + "-" + addZero(eval(dt.getMonth()+1)) + "-" + addZero(dt.getDate());
}
function addZero(i){
    var rtn = i + 100;
    return rtn.toString().substring(1,3);
}



// 이전달/다음달 버튼 이벤트 설정하기
function setPrevNextMonthButtonEvent() {
    var p = ".month_and_arrow_button_area .prev_month_button";
    var n = ".month_and_arrow_button_area .next_month_button";

    // 이전달 버튼 이벤트 설정
    $(p).on("click", function(){
        var prev_year = null;
        var prev_month = null;
        if (currentMonth - 1 < 0) {
            prev_year = currentYear - 1;
            prev_month = 11;
        } else {
            prev_year = currentYear;
            prev_month = currentMonth - 1;
        }

        currentYear = prev_year;
        currentMonth = prev_month;
        fillCalendar(currentYear, currentMonth);
    });

    // 다음달 버튼 이벤트 설정
    $(n).on("click", function(){
        var next_year = null;
        var next_month = null;
        if (currentMonth + 1 > 11) {
            next_year = currentYear + 1;
            next_month = 0;
        } else {
            next_year = currentYear;
            next_month = currentMonth + 1;
        }

        currentYear = next_year;
        currentMonth = next_month;
        fillCalendar(currentYear, currentMonth);
    });
}



// 날짜를 선택하면 활성화 표시 하기
function setClickDateEvent() {
    $(".date_display_area ul li").on("click", function(){
    	if (isLoading == false) {
	        if ($(this).hasClass("normal1") === true || $(this).hasClass("normal2") === true) {
	        	$(".date_display_area ul li.active").removeClass("active");
	            $(this).addClass("active");



	            currentSelectedYear = Number($(this).attr("data-year"));
	            currentSelectedMonth = Number($(this).attr("data-month"));
	            currentSelectedDate = Number($(this).attr("data-date"));


	            var today = new Date(); // 오늘 날짜
	            today.setHours(0, 0, 0, 0);
	            var select_day = new Date(currentSelectedMonth+"-"+currentSelectedDate+"-"+currentSelectedYear+" 00:00:00"); // 선택된 날짜
	            var differTime = select_day - today;
	            var differDay = differTime / (1000*60*60*24);
	            
	            if (today.getTime() == select_day.getTime()) {
	            	// alert("같은 날짜 입니다");
	            }


	        	selected_year = currentSelectedYear; // 선택된 년도
	            selected_month = currentSelectedMonth; // 선택된 월
	            selected_date = currentSelectedDate; // 선택된 일
	            selected_day = select_day.getDay(); // 선택된 요일 인덱스

	            admin6110(); // 해당 날짜에 해당하는 의료진 스케쥴 불러오기


	            

	            var fullDate = currentSelectedYear+"-"+currentSelectedMonth+"-"+currentSelectedDate+" "+dayArray[selected_day]+"요일";
	            console.log("현재 선택된 날짜 = "+fullDate);
	            $(".fullDate").text("("+fullDate+")");
	        }
	    } else {
	    	alert("작업이 진행중입니다.");
	    }
    });
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}