



$(document).ready(function(){
	admin2001(); // 로그아웃 버튼 클릭시 이벤트 설정하기

	admin2003(); // 총 회원 수 가져오기
	admin2004(); // 오늘 올라온 1:1문의 수 가져오기
	admin2005(); // 오늘 진료예약 신청 수 가져오기
	admin2006(); // 전체 진료과 수 가져오기
	admin2007(); // 전체 의료진 수 가져오기
	admin2008(); // 오늘 순번대기표 수 가져오기

});

// 로그아웃 버튼 클릭시 이벤트 설정하기
function admin2001() {
	$(".admin_profile_info .logout_button").on("click", function(){
		if (confirm("정말 로그아웃 하시겠습니까?")) {
			admin2002(); // 로그아웃 요청하기
		}
	});
}

// 로그아웃 요청하기
function admin2002() {
	var admin_pk = Number($("input[name=admin_pk]").val());

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2002",
			admin_pk:admin_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("로그아웃 되었습니다.");
					window.parent.normalLoginTry = true;
					window.parent.location.href = "./admin1000.php";

				} else {

					alert("로그아웃중에 오류가 발생하였습니다.");
					window.parent.normalLoginTry = true;
					window.parent.location.href = "./admin1000.php";

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 총 회원 수 가져오기
function admin2003() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2003"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var total_num = jsonObj.total_num;
					$(".total_member_amount").text(total_num);

				} else {



				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 오늘 올라온 1:1문의 수 가져오기
function admin2004() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2004"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var total_num = jsonObj.total_num;
					$(".today_oneone_question_total_num").text(total_num);

				} else {



				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 오늘 진료예약 신청 수 가져오기
function admin2005() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2005"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var total_num = jsonObj.total_num;
					$(".today_clinic_reservation_total_num").text(total_num);

				} else {



				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 전체 진료과 수 가져오기
function admin2006() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2006"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var total_num = jsonObj.total_num;
					$(".department_total_num").text(total_num);

				} else {



				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 전체 의료진 수 가져오기
function admin2007() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2007"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var total_num = jsonObj.total_num;
					$(".doctor_total_num").text(total_num);

				} else {



				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 오늘 순번대기표 수 가져오기
function admin2008() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin2008"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var total_num = jsonObj.total_num;
					$(".today_waiting_ticket_total_num").text(total_num);

				} else {



				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}



/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}