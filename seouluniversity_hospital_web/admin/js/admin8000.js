var global_answer_whether = 0;
var global_question_type = 0;

var global_index = 0;
var global_view_num = 20;
var global_page_num = 10;
var global_page_reckoning = 1;







$(document).ready(function(){
	
	admin8001(); // 문의유형 리스트 가져오기

	admin8005(); // 검색 버튼 클릭시 이벤트 설정하기

	admin8010(); // 1:1문의 리스트 가져오기
	admin8011();
	admin8012();
	admin8013();
	admin8014();

});

// 문의유형 리스트 가져오기
function admin8001() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin8001"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$("select[name=questionType]").children("option").not(".default").remove();

				if (result == "ok") {
					
					$.each(jsonObj.data, function(key, value){
						var codeNumber = value.codeNumber;
						var description = value.description;

						var text = '<option value="'+codeNumber+'">'+description+'</option>';
						$("select[name=questionType]").append(text);
					});

					

				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 검색 버튼 클릭시 이벤트 설정하기
function admin8005() {
	$(".searchFilterTypeA .finalSearchButton").on("click", function(){
		if (isLoading == false) {
			var answer_whether = $("select[name=answerWhether]").val();
			var question_type = $("select[name=questionType]").val();

			global_answer_whether = answer_whether;
			global_question_type = question_type;

			global_page_reckoning = 1;
			admin8010(); // 1:1문의 리스트 가져오기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 1:1문의 리스트 가져오기
function admin8010() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin8010",
			index:global_index,
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			answer_whether:global_answer_whether,
			question_type:global_question_type
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				

				if (result == "ok") {
					
					$(".listTypeD > ul").empty();

					$.each(jsonObj.data, function(key, value){
						var question_pk = value.question_pk;
						var member = value.member;
						var member_name = value.member_name;
						var member_phone_number = value.member_phone_number;
						var question_type = value.question_type;
						var question_type_string = value.question_type_string;
						var question_title = cutString(value.question_title, 24);
						var question_content = value.question_content;
						var question_datetime = value.question_datetime;
						var answer_whether = value.answer_whether;


						var text = '';
						text += '<li class="clearFix" data-pk="'+question_pk+'">';
		                text += '    <div class="question_type">';
		                text += '        '+question_type_string;
		                text += '    </div>';
		                text += '    <div class="group1">';
		                text += '        <div class="datetime">';
		                text += '            '+question_datetime;
		                text += '        </div>';
		                text += '        <div class="title">';
		                text += '            '+question_title;
		                text += '        </div>';
		                text += '        <div class="member_info">';
		                text += '            <span class="member_name">'+member_name+'</span>';
		                
		                if (member_phone_number == '' || member_phone_number == null || member_phone_number == undefined) {
		                	
		                } else {
		                	text += '            <span class="member_phone">('+member_phone_number+')</span>';
		                }
		                
		                text += '        </div>';
		                text += '    </div>';
		                
		                
		                if (answer_whether == 1) {
		                	text += '    <div class="answer_whether ok">';
		                	text += '        답변';
		                	text += '    </div>';
		                } else {
		                	text += '    <div class="answer_whether">';
		                	text += '        미답변';
		                	text += '    </div>';
		                }
		                
		                text += '</li>';


						

						$(".listTypeD > ul").append(text);
					});

					admin8017(); // 병원뉴스 리스트 클릭시 이벤트 설정하기


					if (global_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (global_index == 0) {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
							$(".pagingControllerBox > div > div.prev").css({"display":"none"});
						} else {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"block"});
							$(".pagingControllerBox > div > div.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".pagingControllerBox > div > div.best_next").css({"display":"block"});
							$(".pagingControllerBox > div > div.next").css({"display":"block"});	
						} else {
							$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
							$(".pagingControllerBox > div > div.next").css({"display":"none"});
						}


						$(".pagingControllerBox > div > ul").empty();



						var first_page_index = global_index;
						for (var ii=0; ii<global_page_num; ii++) {
							if (first_page_index % global_page_num == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("global_index = "+global_index);
							console.log("page_number-1 = "+(page_number-1));
							if (global_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".pagingControllerBox > div > ul").append(text);
						}


						admin8015(); // 페이지 번호 클릭시 이벤트 설정



						global_page_reckoning = 0;
					}

				} else {

					$(".listTypeD > ul").empty();
				

					$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
					$(".pagingControllerBox > div > div.next").css({"display":"none"});

					$(".pagingControllerBox > div > ul").empty();
					$(".pagingControllerBox > div > ul").append('<li class="active">1</li>');

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}


// 1:1문의 페이징 - 다음 페이지 클릭시 이벤트 설정
function admin8011() {
	$(".pagingControllerBox > div > div.next").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + global_page_num;
		global_index = next_first_index;
		admin8010();
	});
}

// 1:1문의 페이징 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function admin8012() {
	$(".pagingControllerBox > div > div.best_next").on("click", function(){
		if (isLoading == false) {
			admin8016(); // 1:1문의 페이징 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 1:1문의 페이징 - 이전 페이지 클릭시 이벤트 설정
function admin8013() {
	$(".pagingControllerBox > div > div.prev").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - global_page_num;
		global_index = prev_first_index;
		admin8010();
	});
}

// 1:1문의 페이징 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function admin8014() {
	$(".pagingControllerBox > div > div.best_prev").on("click", function(){
		global_page_reckoning = 1;
		global_index = 0;
		admin8010();
	});
}


// 1:1문의 페이징 - 페이지 번호 클릭시 이벤트 설정
function admin8015() {
	$(".pagingControllerBox > div > ul > li").on("click", function(){
		if (isLoading == false) {	
			$(".pagingControllerBox > div > ul > li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			global_index = selected_index;
			admin8010();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 1:1문의 페이징 - 페이지 맨 마지막 번호 알아내기
function admin8016() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin8016",
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			answer_whether:global_answer_whether,
			question_type:global_question_type
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					global_index = last_index;

					console.log("last_index = "+last_index);

					global_page_reckoning = 1;
					admin8010();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 1:1문의 리스트 클릭시 이벤트 설정하기
function admin8017() {
	$(".listTypeD > ul > li").on("click", function(){
		var pk = Number($(this).attr("data-pk"));

		
		window.open("./admin8100.php?pk="+pk,"1:1문의 상세내용","width=800 height=600 top=120 left=120 menubar=no status=no");

	});
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}