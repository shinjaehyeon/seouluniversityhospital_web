var global_question_pk = 0;







$(document).ready(function(){
	
	getQuestionPrimarykey(); // 1:1문의 pk 가져오기

	admin8110(); // 문의 내용 가져오기

});

// 1:1문의 pk 가져오기
function getQuestionPrimarykey() {
	global_question_pk = Number($("input[name=question_pk]").val());
}

// 답변 등록 버튼 클릭시 이벤트 설정하기
function admin8105() {
	$(".answer_input_box .answer_upload_button").on("click", function(){
		if (isLoading == false) {
			var answer_title = $.trim($(".answer_input_box .box_content input[name=answerTitle]").val());
			var answer_content = $.trim($(".answer_input_box .box_content textarea[name=answerContent]").val());
		
			if (answer_title == '') {
				alert("답변 제목을 입력해주세요.");
				return false;
			}

			if (answer_content == '') {
				alert("답변 내용을 입력해주세요.");
				return false;	
			}


			admin8106(answer_title, answer_content); // 문의내용에 대한 답변 서버에 저장하기
		} else {
			alert("작업이 진행중입니다.");
		}

	});
}

// 문의내용에 대한 답변 서버에 저장하기
function admin8106(answer_title, answer_content) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin8106",
			question_pk:global_question_pk,
			answer_title:answer_title,
			answer_content:answer_content
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("admin8106 data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("답변이 정상적으로 등록되었습니다.");
					location.reload();

				} else {

					alert("답변 등록중에 오류가 발생하였습니다.");

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 문의 내용 가져오기
function admin8110() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin8110",
			question_pk:global_question_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("admin8110 data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var member = jsonObj.member;
					var member_name = jsonObj.member_name;
					var member_phoneNumber = jsonObj.member_phoneNumber;
					if (member_phoneNumber != '') {
						member_phoneNumber = ' ('+member_phoneNumber+')';
					} 


					var question_type = jsonObj.question_type;
					var question_type_string = jsonObj.question_type_string;
					var question_title = jsonObj.question_title;
					var question_content = jsonObj.question_content;
					var question_datetime = jsonObj.question_datetime;
					var answer_whether = jsonObj.answer_whether;
					var answer_title = jsonObj.answer_title;
					var answer_content = jsonObj.answer_content;
					var answer_datetime = jsonObj.answer_datetime;


					$(".viewStyleD.admin8100 > ul > li > .question_type").text(question_type_string);
					$(".viewStyleD > ul > li > .writer").text('문의자 : '+member_name+member_phoneNumber);
					$(".viewStyleD > ul > li > .datetime").text('작성일 : '+question_datetime);
					$(".viewStyleD > ul > li > .title").html(question_title);
					$(".viewStyleD > ul > li > .content").html(question_content);


					if (answer_whether == 1) {
						$(".answer_input_box").remove();
						$(".answer_view_box .answer_title").text(question_title);
						$(".answer_view_box .answer_content").html(answer_content);
						$(".answer_view_box .answer_datetime").html(answer_datetime);
					} else {
						$(".answer_view_box").remove();
						admin8105(); // 답변 등록 버튼 클릭시 이벤트 설정하기
					}



				} else {


				}
			}
		},
		fail: function() {
			
		}
	});
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}