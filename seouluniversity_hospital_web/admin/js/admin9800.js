var doctor_pk = 0;

var global_search_type = 1;
var global_search_value = '';

var global_index = 0;
var global_view_num = 10;
var global_page_num = 10;
var global_page_reckoning = 1;







$(document).ready(function(){
	getDoctorPk();
	

	admin9815(); // 의료진 정보 가져오기

	admin9810(); // 검색 버튼 클릭시 이벤트 설정하기

	admin9801(); // 의료진 댓글 리스트 가져오기
	admin9802();
	admin9803();
	admin9804();
	admin9805();

});


function getDoctorPk() {
	doctor_pk = $("input[name=pk]").val();
}

// 의료진 정보 가져오기
function admin9815() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9815",
			doctor_pk:doctor_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var name = jsonObj.name;

					$(".doctor_info_area").text(name+" 의료진 댓글");
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}




// 검색 버튼 클릭시 이벤트 설정하기
function admin9810() {
	$(".comment_search_style_box .search_button").on("click", function(){
		if (isLoading == false) {
			global_search_type = $("select[name=searchType]").val();
			global_search_value = $("input[name=searchValue]").val();

			global_page_reckoning = 1;
			admin9801(); // 1:1문의 리스트 가져오기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 의료진 댓글 리스트 가져오기
function admin9801() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9801",
			doctor_pk:doctor_pk,
			index:global_index,
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			search_type:global_search_type,
			search_value:global_search_value
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$("ul.comment_list").empty();

				if (result == "ok") {

					$.each(jsonObj.data, function(key, value){
						var suhdc = value.suhdc;
						var member = value.member;
						var member_name = value.member_name;
						var member_email = value.member_email;
						var datetime = value.datetime;
						var comment = value.comment;


						var text = '';
						text += '<li class="clearFix">';
		                text += '    <div class="member_info float_left">';
		                text += '        <div class="member_name">';
		                text += '            '+member_name;
		                text += '        </div>';
		                text += '        <div class="member_email">';
		                text += '            ('+member_email+')';
		                text += '        </div>';
		                text += '    </div>';
		                text += '    <div class="comment_set float_left">';
		                text += '        <div class="datetime">';
		                text += '            '+datetime;
		                text += '        </div>';
		                text += '        <div class="comment">';
		                text += '            '+comment;
		                text += '        </div>';
		                text += '    </div>';
		                text += '    <div class="comment_delete_button float_right" comment-pk="'+suhdc+'" onclick="admin9809(this);">';
		                text += '        삭제';
		                text += '    </div>';
		                text += '</li>';
						

						$("ul.comment_list").append(text);
					});


					if (global_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (global_index == 0) {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
							$(".pagingControllerBox > div > div.prev").css({"display":"none"});
						} else {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"block"});
							$(".pagingControllerBox > div > div.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".pagingControllerBox > div > div.best_next").css({"display":"block"});
							$(".pagingControllerBox > div > div.next").css({"display":"block"});	
						} else {
							$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
							$(".pagingControllerBox > div > div.next").css({"display":"none"});
						}


						$(".pagingControllerBox > div > ul").empty();



						var first_page_index = global_index;
						for (var ii=0; ii<global_page_num; ii++) {
							if (first_page_index % global_page_num == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("global_index = "+global_index);
							console.log("page_number-1 = "+(page_number-1));
							if (global_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".pagingControllerBox > div > ul").append(text);
						}


						admin9806(); // 페이지 번호 클릭시 이벤트 설정



						global_page_reckoning = 0;
					}

				} else {

					$("ul.comment_list").append('<li class="no_comment">댓글이 없습니다.</li>');
				

					$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
					$(".pagingControllerBox > div > div.next").css({"display":"none"});

					$(".pagingControllerBox > div > ul").empty();
					$(".pagingControllerBox > div > ul").append('<li class="active">1</li>');

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}


// 의료진 댓글 - 다음 페이지 클릭시 이벤트 설정
function admin9802() {
	$(".pagingControllerBox > div > div.next").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + global_page_num;
		global_index = next_first_index;
		admin9801();
	});
}

// 의료진 댓글 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function admin9803() {
	$(".pagingControllerBox > div > div.best_next").on("click", function(){
		if (isLoading == false) {
			admin9807(); // 의료진 댓글 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 의료진 댓글 - 이전 페이지 클릭시 이벤트 설정
function admin9804() {
	$(".pagingControllerBox > div > div.prev").on("click", function(){
		global_page_reckoning = 1;

		var minimum_index = global_index;
		for (var i=0; i<global_page_num; i++) {
			if (minimum_index % global_page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - global_page_num;
		global_index = prev_first_index;
		admin9801();
	});
}

// 의료진 댓글 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function admin9805() {
	$(".pagingControllerBox > div > div.best_prev").on("click", function(){
		global_page_reckoning = 1;
		global_index = 0;
		admin9801();
	});
}


// 의료진 댓글 - 페이지 번호 클릭시 이벤트 설정
function admin9806() {
	$(".pagingControllerBox > div > ul > li").on("click", function(){
		if (isLoading == false) {	
			$(".pagingControllerBox > div > ul > li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			global_index = selected_index;
			admin9801();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 의료진 댓글 - 페이지 맨 마지막 번호 알아내기
function admin9807() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9807",
			doctor_pk:doctor_pk,
			view_num:global_view_num,
			page_num:global_page_num,
			page_reckoning:global_page_reckoning,
			search_type:global_search_type,
			search_value:global_search_value
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					global_index = last_index;

					console.log("last_index = "+last_index);

					global_page_reckoning = 1;
					admin9801();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 댓글 삭제 버튼 클릭시 이벤트 설정하기
function admin9809(obj) {
	if (isLoading == false) {
		if (confirm("정말 삭제하시겠습니까?")) {
			var willDeleteElement = $(obj).parent();
			var willDeleteCommentPk = Number($(obj).attr("comment-pk"));

			$.ajax({
				type: "POST",
				url: "./outlet.php",
				data:{
					act:"admin9809",
					willDeleteCommentPk:willDeleteCommentPk
				},
				timeout: 2000,
				beforeSend: function() {
					isLoading = true;
				},
				complete: function() {
					isLoading = false;
				},
				success: function(data) {
					console.log("data = "+data);

					if (isJSON(data)) {
						var jsonObj = $.parseJSON(data);
						var result = jsonObj.result;

						if (result == "ok") {

							$(willDeleteElement).remove();

						} else {
							alert("삭제 도중 오류가 발생하였습니다.");
						}
					}
				},
				fail: function() {
					
				}
			});
		}

	} else {
		alert("작업이 진행중입니다.");
	}
}







/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}