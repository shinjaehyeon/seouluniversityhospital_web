var global_pk = null;











$(document).ready(function(){
	getPk();


	admin5110(); // 회원 상세정보 가져오기
	admin5111(); // 수정 버튼 클릭시 이벤트 설정하기
	
});


function getPk() {
	global_pk = Number($("input[name=pk]").val());
}

// 회원 상세정보 가져오기
function admin5110() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin5110",
			pk:global_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var name = jsonObj.name;
					var gender = jsonObj.gender;
					var theMobileCompany = jsonObj.theMobileCompany;
					var phoneNumber = jsonObj.phoneNumber;
					var email = jsonObj.email;
					var birthday = jsonObj.birthday;
					var theBloodType = jsonObj.theBloodType;
					var addr = jsonObj.addr;
					var signUpMethod = jsonObj.signUpMethod;
					var signUpMethodString = jsonObj.signUpMethodString;
					var signUpDate = jsonObj.signUpDate;
					var status = jsonObj.status;
					var is_get_push = jsonObj.is_get_push;
					
					// 이름
					$("input[name=memberName]").val(name);

					// 성별
					$("select[name=memberGender] option[value="+gender+"]").prop("selected", true);

					// 휴대폰 통신사
					$("select[name=memberMobileCompany] option[value="+theMobileCompany+"]").prop("selected", true);

					// 휴대폰 번호
					$("input[name=memberPhoneNumber]").val(phoneNumber);

					// 이메일
					$(".member_email").text(email);

					// 생일
					$("input[name=memberBirthday]").val(birthday.split(" ")[0]);

					// 혈액형
					$("select[name=memberBloodtype] option[value="+theBloodType+"]").prop("selected", true);

					// 주소
					$("textarea[name=memberAddr]").val(addr);

					// 가입일
					$(".member_join_datetime").text(signUpDate);

					// 가입방법
					$(".member_join_method").text(signUpMethodString);

					// 현재상태
					$("select[name=memberStatus] option[value="+status+"]").prop("selected", true);

					// 푸시알림 수신여부
					if (is_get_push == 1) {
						$(".member_is_get_push").text("ON");
					} else {
						$(".member_is_get_push").text("OFF");
					}

				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 수정 버튼 클릭시 이벤트 설정하기
function admin5111() {
	$(".edit_button").on("click", function(){
		var memberBirthday = $.trim($("input[name=memberBirthday]").val());
		if (memberBirthday == '') {
			alert("생년월일의 날짜를 정확히 입력해주세요.");
			return false;
		}

		$("form[name=memberForm]").submit();
	});
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}