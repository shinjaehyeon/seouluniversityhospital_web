
$(document).ready(function(){
	
});





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}