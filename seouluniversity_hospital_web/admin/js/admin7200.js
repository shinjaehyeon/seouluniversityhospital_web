var global_news_pk = 0;








$(document).ready(function(){
	

	getNewsPk(); // 병원뉴스 pk 글로벌 전역변수에 저장하기

	admin7210(); // 병원뉴스 상세정보 가져오기 


});

function getNewsPk() {
	global_news_pk = Number($("input[name=pk]").val());
}

// 병원뉴스 상세정보 가져오기 
function admin7210() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7210",
			news_pk:global_news_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var admin_name = jsonObj.admin_name;
					var admin_id = jsonObj.admin_id;

					var title = htmlspecialchar_decode(jsonObj.title);
					console.log("title = "+title);


					var content = jsonObj.content;
					console.log("content = "+content);

					
					var datetime = jsonObj.datetime;
					var attach_file_flag = jsonObj.attach_file_flag;
					var view_index = jsonObj.view_index;
					var attach_file_array = jsonObj.attach_file_array;


					$(".viewStyleD > ul > li > .title").text(title);
					$(".viewStyleD > ul > li > .writer").text('작성자 : '+admin_name);
					$(".viewStyleD > ul > li > .view_index").text('조회수 : '+view_index);
					$(".viewStyleD > ul > li > .datetime").text('작성일 : '+datetime);

					$(".viewStyleD > ul > li > ul.file_list").empty();
					$.each(attach_file_array, function(key, value){
						var filename = value.filename;
						var fileurl = '../suhnews/suhnews'+global_news_pk+'/'+filename;

						var text = '';
						text += '<li>';
						text += '	-';
						text += '	<a href="'+fileurl+'" download>';
						text += '		'+filename;
						text += '	</a>';
						text += '</li>';

						$(".viewStyleD > ul > li > ul.file_list").append(text);
					});

					if (attach_file_flag == 0) {
						$(".viewStyleD > ul > li > ul.file_list").append('<li class="file_no">- 첨부파일이 없습니다.</li>');
					}

					$(".viewStyleD > ul > li > .content").html(content);



				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}





function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}








/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}

