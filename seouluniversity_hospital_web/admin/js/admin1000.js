var isLoading = false;

var windowWidth;
var windowHeight;

$(document).ready(function(){
	windowWidth = $(window).width();
	windowHeight = $(window).height();

	admin1001(); // 로그인 창 가운데로 위치시키기
	admin1002(); // 윈도우 창이 리사이즈 될 때마다 이벤트 설정하기
	admin1005(); // 로그인 버튼 클릭시 이벤트 설정하기

	setEnterEvent();
});

function setEnterEvent() {
	$('input.idpwform').keypress(function(event){
		if (event.keyCode == 13) {
			var id = $.trim($("input[name=adminId]").val());
			if (id == "") {
				alert("아이디를 입력하세요.");
				return 0;
			}

			var pw = $.trim($("input[name=adminPw]").val());
			if (pw == "") {
				alert("비밀번호를 입력하세요.");
				return 0;
			}
			admin1006(id, pw); // 서버에 관리자 계정이 유효한지 요청하기
		}
	});
}

// 로그인 창 가운데로 위치시키기
function admin1001() {
	var loginWindowHeight = $(".admin1000.login_box").height();
	var marginTop = (windowHeight - loginWindowHeight) / 2;
	$(".admin1000.login_box").stop().animate({"margin-top":marginTop+"px"}, 500, 'easeOutQuint', function(){

	});
}

// 윈도우 창이 리사이즈 될 때마다 이벤트 설정하기
function admin1002() {
	$(window).resize(function(){
		windowWidth = $(window).width();
		windowHeight = $(window).height();

		admin1001(); // 로그인 창 가운데로 위치시키기
	});
}

// 로그인 버튼 클릭시 이벤트 설정하기
function admin1005() {
	$(".admin1000.login_box form .login_button").on("click", function(){
		if (isLoading == false) {
			var id = $.trim($("input[name=adminId]").val());
			if (id == "") {
				alert("아이디를 입력하세요.");
				return 0;
			}

			var pw = $.trim($("input[name=adminPw]").val());
			if (pw == "") {
				alert("비밀번호를 입력하세요.");
				return 0;
			}


			admin1006(id, pw); // 서버에 관리자 계정이 유효한지 요청하기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 서버에 관리자 계정이 유효한지 요청하기
function admin1006(id, pw) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin1006",
			id:id,
			pw:pw
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var other_login = jsonObj.other_login;

					if (other_login == 'no') {
						alert("로그인 되었습니다. 관리자 페이지로 이동합니다.");
					} else {
						alert("다른 기기에서 로그인되어 있습니다. 다른 기기에 있는 계정을 로그아웃하고 메인으로 이동합니다.");
					}
					location.href = "./admin1100.php";
				} else {
					alert("일치하는 계정이 없습니다.");
				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}