var global_pk = 0;







$(document).ready(function(){
	
	getPrimarykey(); // 1:1문의 pk 가져오기

	admin7610(); // 고객의소리 상세정보 가져오기
	admin7611(); // 고객의소리 삭제 버튼 클릭시 이벤트 설정하기

});

// 1:1문의 pk 가져오기
function getPrimarykey() {
	global_pk = Number($("input[name=pk]").val());
}



// 고객의소리 상세정보 가져오기
function admin7610() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7610",
			pk:global_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var board_type = jsonObj.board_type;
					var board_type_string = $.trim(jsonObj.board_type_string);
					var content_type = jsonObj.content_type;
					var content_type_string = jsonObj.content_type_string;
					var title = jsonObj.title;
					var content = jsonObj.content;
					var datetime = jsonObj.datetime;
					var member = jsonObj.member;
					var member_name = jsonObj.member_name;
					var member_email = jsonObj.member_email;
					var attach_file_flag = jsonObj.attach_file_flag;
					var view_index = jsonObj.view_index;
					var attach_file_array = jsonObj.attach_file_array;

					var prev_pk = jsonObj.prev_pk;
					var prev_title = jsonObj.prev_title;
					var next_pk = jsonObj.next_pk;
					var next_title = jsonObj.next_title;

							
					$(".board_type").text(board_type_string);	
					$(".content_type").text(content_type_string);
					$(".title").html(title);
					$(".writer").text("작성자 : "+member_name+"("+member_email+")");
					$(".view_indexs").text("조회수 : "+view_index);
					$(".datetime").text("작성일 : "+datetime);

					$("ul.attach_file_list").empty();
					$.each(attach_file_array, function(key, value){
						var filename = value.filename;
						var fileurl = '../customer_board/customer'+board_type+'_'+global_pk+'/'+filename;

						var text = '';
						text += '<li class="clearFix">';
                        text += '    <a href="'+fileurl+'" class="file_name" download>';
                        text += '        '+filename;
                        text += '    </a>';
                        text += '    <div class="file_delete_button">';
                        text += '        삭제';
                        text += '    </div>';
                        text += '</li>';

						$("ul.attach_file_list").append(text);
					});

					if (attach_file_flag == 0) {
						$(".no_result").css({"display":"block"});
					}

					$(".content").html(content);



				} else {


				}
			}
		},
		fail: function() {
			
		}
	});
}

// 고객의소리 삭제 버튼 클릭시 이벤트 설정하기
function admin7611() {
	$(".content_delete_button").on("click", function(){
		if (confirm("정말 삭제하시겠습니까?")) {
			admin7612(); // 고객의소리 삭제하기
		}
	});
}

// 고객의소리 삭제하기
function admin7612() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7612",
			pk:global_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					// alert("삭제되었습니다.");
					$(opener.location).attr("href", "javascript:admin7521("+global_pk+");");
					window.close();


				} else {


				}
			}
		},
		fail: function() {
			
		}
	});
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}