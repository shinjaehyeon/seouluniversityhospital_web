var globalMemberGender = 0;
var globalMemberBirthday = '';
var globalMemberBloodtype = 0;
var globalMemberName = '';

var page_reckoning = 1;

var index = 0;
var view_num = 50;
var page_num = 10;



$(document).ready(function(){
	
	admin5001(); // 검색 버튼 눌렀을 때 이벤트 설정하기
	admin5002(); // 생일에서 전체 체크박스 클릭했을 때 이벤트 설정하기

	admin5003(); // 전체 회원 수 가져오기

	admin5010(); // 회원목록 가져오기 
	admin5011(); // 회원 관리 리스트 - 다음 페이지 클릭시 이벤트 설정
	admin5012(); // 회원 관리 리스트 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
	admin5013(); // 회원 관리 리스트 - 이전 페이지 클릭시 이벤트 설정
	admin5014(); // 회원 관리 리스트 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
});


// 검색 버튼 눌렀을 때 이벤트 설정하기
function admin5001() {
	$(".searchFilterTypeA .finalSearchButton").on("click", function(){
		if (searchFormCheck()) {
			page_reckoning = 1;
			admin5010(); // 회원 목록 가져오기
		}
	});
}

// 생일에서 전체 체크박스 클릭했을 때 이벤트 설정하기
function admin5002() {
	$("input[name=searchMemberBirthdayUse]").on("change", function(){
		var isCheceked = $(this).prop("checked");

		if (isCheceked) {
			$("input[name=searchMemberBirthday]").attr("readonly", true);
			$("input[name=searchMemberBirthday]").addClass("lock");

		} else {
			$("input[name=searchMemberBirthday]").attr("readonly", false);
			$("input[name=searchMemberBirthday]").removeClass("lock");
		}
	});
}

// 전체 회원 수 가져오기
function admin5003() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin5003"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var total_member_num = jsonObj.total_member_num;
					$(".member_total_num").text("전체 회원 수 : "+total_member_num+"명");

				} else {

					
				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 검색 폼 체크하기
function searchFormCheck() {
	// 성별 체크
	var gender = $("input[name=searchMemberGender]:checked").val();
	globalMemberGender = gender;

	// 생일 체크
	var isBirthdayAll = $("input[name=searchMemberBirthdayUse]").prop("checked");
	
	if (isBirthdayAll) {
		// 전체에 체크가 되어 있으면
		globalMemberBirthday = '';
	} else {
		// 전체에 체크가 되어 있지 않으면
		var birthday = $("input[name=searchMemberBirthday]").val();
		if (birthday == '') {
			alert("검색할 생일을 입력해주세요.");
			return false;
		} else {
			globalMemberBirthday = birthday;
		}
	}


	// 혈액형 체크
	var bloodType = $("select[name=searchMemberBloodtype]").val();
	globalMemberBloodtype = bloodType;
	console.log("globalMemberBloodtype = "+globalMemberBloodtype);


	// 이름 체크 
	var name = $("input[name=searchMemberName]").val();
	globalMemberName = name;


	return true;

}


// 회원 목록에서 수정버튼 클릭했을 때 이벤트 설정하기
function admin5005() {
	$(".boardTypeB .memberListBox ul li > div .member_edit_button").on("click", function(){
		var member_pk = Number($(this).parent().siblings(".member_pk").text());
		var mamber_name = $.trim($(this).parent().siblings(".mamber_name").children("input").val());
		if (mamber_name == "") {
			alert("이름에는 공백이 들어갈 수 없습니다.");
			return false;
		}

		var member_gender = Number($(this).parent().siblings(".member_gender").children("select").val());
		var member_birthday = $(this).parent().siblings(".member_birthday").children("input").val();
		if (member_birthday == '') {
			alert("생일을 올바르게 입력해주세요.");
			return false;
		}

		var member_bloodtype = Number($(this).parent().siblings(".member_bloodtype").children("select").val());
		var member_status = Number($(this).parent().siblings(".member_status").children("select").val());

		if (confirm("수정하시겠습니까?")) {
			admin5006(member_pk, mamber_name, member_gender, member_birthday, member_bloodtype, member_status); // 회원 목록에서의 회원정보 수정하기
		}
	});
}

// 회원 목록에서의 회원정보 수정하기
function admin5006(member_pk, mamber_name, member_gender, member_birthday, member_bloodtype, member_status) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin5006",
			member_pk:member_pk,
			mamber_name:mamber_name,
			member_gender:member_gender,
			member_birthday:member_birthday,
			member_bloodtype:member_bloodtype,
			member_status:member_status
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("해당 회원 정보가 수정되었습니다.")

				} else {

					alert("회원 정보 수정중에 오류가 발생하였습니다.");

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 회원 목록에서 상세버튼 클릭했을 때 이벤트 설정하기
function admin5007() {
	$(".member_detail_button").on("click", function(){
		var pk = Number($(this).parent().parent().attr("data-pk"));
		location.href = "./admin5100.php?pk="+pk;
	});
}




// 회원 목록 가져오기
function admin5010() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin5010",
			index:index,
			view_num:view_num,
			page_num:page_num,
			page_reckoning:page_reckoning,
			memberGender:globalMemberGender,
			memberBirthday:globalMemberBirthday,
			memberBloodtype:globalMemberBloodtype,
			memberName:globalMemberName
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {

					$(".boardTypeB .memberListBox ul li").remove();

					$.each(jsonObj.data, function(key, value){
						var suhmember = value.suhmember;
						var name = value.name;
						var gender = value.gender;
						var birthday = value.birthday;
						var only_birthday = birthday.split(" ")[0];
						var theBloodType = value.theBloodType;
						var status = value.status;


						var text = '';
						text += '<li class="clearFix" data-pk="'+suhmember+'">';
	                    text += '    <div class="col1 member_pk">';
	                    text += '        '+suhmember;
	                    text += '    </div>';
	                    text += '    <div class="col2 mamber_name">';
	                    text += '        <input class="memberNameInput" type="text" name="memberName" value="'+name+'" />';
	                    text += '    </div>';
	                    text += '    <div class="col3 member_gender">';
	                    text += '        <select class="memberGenderSelect" name="memberGender">';
	                    text += '            <option value="900900">미등록</option>';
	                    text += '            <option value="800501">남자</option>';
	                    text += '            <option value="800502">여자</option>';
	                    text += '        </select>';
	                    text += '    </div>';
	                    text += '    <div class="col4 member_birthday">';
	                    text += '        <input class="memberBirthdayInput" type="date" name="memberBirthday" value="'+only_birthday+'" />';
	                    text += '    </div>';
	                    text += '    <div class="col5 member_bloodtype">';
	                    text += '        <select class="memberBloodtypeSelect" name="memberBloodtype">';
	                    text += '            <option value="900900">미등록</option>';
	                    text += '            <option value="600101">O형</option>';
	                    text += '            <option value="600102">A형</option>';
	                    text += '            <option value="600103">B형</option>';
	                    text += '            <option value="600104">AB형</option>';
	                    text += '        </select>';
	                    text += '    </div>';
	                    text += '    <div class="col6 member_status">';
	                    text += '        <select class="memberStatusSelect" name="memberStatus">';
	                    text += '            <option value="500101">일반상태</option>';
	                    text += '            <option value="500102">탈퇴상태</option>';
	                    text += '            <option value="500103">휴먼상태</option>';
	                    text += '            <option value="500104">정지상태</option>';
	                    text += '        </select>';
	                    text += '    </div>';
	                    text += '    <div class="col7 member_edit_button_area">';
	                    text += '        <div class="member_edit_button">';
	                    text += '            수정';
	                    text += '        </div>';
	                    text += '        <div class="member_detail_button">';
	                    text += '            상세';
	                    text += '        </div>';
	                    text += '    </div>';
	                    text += '</li>';

						$(".boardTypeB .memberListBox ul").append(text);


						$(".boardTypeB .memberListBox ul li[data-pk="+suhmember+"]").find(".memberGenderSelect").children("option").each(function(){
							var value = Number($(this).attr("value"));
							if (gender == value) {
								$(this).prop("selected", true);
							}
						});

						$(".boardTypeB .memberListBox ul li[data-pk="+suhmember+"]").find(".memberBloodtypeSelect").children("option").each(function(){
							var value = Number($(this).attr("value"));
							if (theBloodType == value) {
								$(this).prop("selected", true);
							}
						});

						$(".boardTypeB .memberListBox ul li[data-pk="+suhmember+"]").find(".memberStatusSelect").children("option").each(function(){
							var value = Number($(this).attr("value"));
							if (status == value) {
								$(this).prop("selected", true);
							}
						});

					});

					admin5005(); // 회원 목록에서 수정버튼 클릭했을 때 이벤트 설정하기
					admin5007(); // 회원 목록에서 상세버튼 클릭했을 때 이벤트 설정하기


					if (page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (index == 0) {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
							$(".pagingControllerBox > div > div.prev").css({"display":"none"});
						} else {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"block"});
							$(".pagingControllerBox > div > div.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".pagingControllerBox > div > div.best_next").css({"display":"block"});
							$(".pagingControllerBox > div > div.next").css({"display":"block"});	
						} else {
							$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
							$(".pagingControllerBox > div > div.next").css({"display":"none"});
						}


						$(".pagingControllerBox > div > ul").empty();



						var first_page_index = index;
						for (var ii=0; ii<page_num; ii++) {
							if (first_page_index % page_num == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("index = "+index);
							console.log("page_number-1 = "+(page_number-1));
							if (index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".pagingControllerBox > div > ul").append(text);
						}


						admin5015(); // 회원 관리 리스트 - 페이지 번호 클릭시 이벤트 설정



						page_reckoning = 0;
					}
					
					

				} else {

					$(".boardTypeB .memberListBox ul li").remove();
				
					$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
					$(".pagingControllerBox > div > div.next").css({"display":"none"});

					$(".pagingControllerBox > div > ul").empty();
					$(".pagingControllerBox > div > ul").append('<li class="active">1</li>');

					alert("검색된 회원이 없습니다.");
				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}


// 회원 관리 리스트 - 다음 페이지 클릭시 이벤트 설정
function admin5011() {
	$(".pagingControllerBox > div > div.next").on("click", function(){
		page_reckoning = 1;

		var minimum_index = index;
		for (var i=0; i<page_num; i++) {
			if (minimum_index % page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + page_num;
		index = next_first_index;
		admin5010();
	});
}

// 회원 관리 리스트 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function admin5012() {
	$(".pagingControllerBox > div > div.best_next").on("click", function(){
		if (isLoading == false) {
			admin5016(); // 회원 관리 리스트 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 회원 관리 리스트 - 이전 페이지 클릭시 이벤트 설정
function admin5013() {
	$(".pagingControllerBox > div > div.prev").on("click", function(){
		page_reckoning = 1;

		var minimum_index = index;
		for (var i=0; i<page_num; i++) {
			if (minimum_index % page_num == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - page_num;
		index = prev_first_index;
		admin5010();
	});
}

// 회원 관리 리스트 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function admin5014() {
	$(".pagingControllerBox > div > div.best_prev").on("click", function(){
		page_reckoning = 1;
		index = 0;
		admin5010();
	});
}


// 회원 관리 리스트 - 페이지 번호 클릭시 이벤트 설정
function admin5015() {
	$(".pagingControllerBox > div > ul > li").on("click", function(){
		console.log("ㅇㅇ");
		if (isLoading == false) {	
			$(".pagingControllerBox > div > ul > li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			index = selected_index;
			admin5010();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 회원 관리 리스트 - 페이지 맨 마지막 번호 알아내기
function admin5016() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin5016",
			view_num:view_num,
			page_num:page_num,
			memberGender:globalMemberGender,
			memberBirthday:globalMemberBirthday,
			memberBloodtype:globalMemberBloodtype,
			memberName:globalMemberName
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					index = last_index;

					console.log("last_index = "+last_index);

					page_reckoning = 1;
					admin5010();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}