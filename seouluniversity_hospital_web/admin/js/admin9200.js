var global_start_code = 800101;
var global_end_code = 800199;

var global_first_department_code = 0;
var global_checked_department_code = 0;

var global_is_first_image = 0;




$(document).ready(function(){
	getDepartmentPk();


	admin9201(); // 진료과 상세정보 가져오기

	admin9202(); // 이미지 파일 업로드시 미리보기 이미지 보여주기

	admin9205(); // 진료과 추가 버튼 클릭시 이벤트 설정하기

	//admin9103(); // 진료과 코드 값 변경시 이벤트 설정하기

	//admin9105(); // 진료과 번호 중복확인 버튼 클릭시 이벤트 설정하기

});


function getDepartmentPk() {
	global_first_department_code = Number($("input[name=department_first_pk]").val());
}


// 진료과 상세정보 가져오기
function admin9201() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin9201",
			first_department_code:global_first_department_code
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			//console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var image = jsonObj.image;
					var department = jsonObj.department;
					var description = jsonObj.description;

					if (image == '') {
						$(".formStlyeE.admin9100 .imageDisplayView").css({"display":"none"});
						$(".formStlyeE.admin9100 .imageDisplayView img").attr("src", "");

						global_is_first_image = 0;
						$("input[name=is_first_image]").val(global_is_first_image);
					} else {
						$(".formStlyeE.admin9100 .imageDisplayView").css({"display":"block"});
						$(".formStlyeE.admin9100 .imageDisplayView img").attr("src", image);

						global_is_first_image = 1;
						$("input[name=is_first_image]").val(global_is_first_image);
					}




					$("input[name=departmentTitle]").val(htmlspecialchar_decode(department));
					$("textarea[name=departmentContent]").val(htmlspecialchar_decode(description));


				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}



function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}




// 이미지 파일 업로드시 미리보기 이미지 보여주기
function admin9202() {
	$("input[name=departmentImage]").on('change', function(){
		if ($(this).val() == '') {
			$(".formStlyeE.admin9100 .imageDisplayView").css({"display":"none"});
			$(".department_preview").attr("src", "");
			global_is_first_image = 0;
			$("input[name=is_first_image]").val(global_is_first_image);
		} else {
			$(".formStlyeE.admin9100 .imageDisplayView").css({"display":"block"});
        	readURL(this);
        }
    });
}


// 진료과 수정 버튼 클릭시 이벤트 설정하기
function admin9205() {
	$(".formStlyeE.admin9100 .department_upload_button").on("click", function(){
		var department_title = $.trim($("input[name=departmentTitle]").val());
		var department_image = $("input[name=departmentImage]").val();


		if (department_title == '') {
			alert("진료과 명칭을 입력해주세요.");
			return false;
		}



		$("form[name=departmentAddForm]").submit();
	});
}


function readURL(input) {
    if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$(".department_preview").attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
    }
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}