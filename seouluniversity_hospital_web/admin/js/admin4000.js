var globaWhoValue = 600401; // 초기값은 전체
var globalMemberPk = new Array();
var globalMemberName = new Array();
var globalMemberPushKey = new Array();

var globalI = 0;
var globalMemberPkLength = 0;

var isPushSending = false;

$(document).ready(function(){
	admin4001(); // 대상 값 변경시 이벤트 설정하기
	admin4002(); // 서버에서 대상 조건에 맞는 멤버 정보 및 pk array 가져오기

	admin4005(); // 푸시 알림 전송 클릭시 이벤트 설정하기

	$("textarea[name=pushContent]").on("keyup", function(){
		$(".word_num").text($(this).val().length+"");
	});
});

// 대상 값 변경시 이벤트 설정하기
function admin4001() {
	$("select[name=who]").on("change", function(){
		if (isLoading == false) {
			globaWhoValue = $(this).val();
			admin4002(); // 서버에서 대상 조건에 맞는 멤버 정보 및 pk array 가져오기
		} else {
			alert("요청한 작업을 진행중입니다.");
		}
	});
}

// 서버에서 대상 조건에 맞는 멤버 정보 및 pk array 가져오기
function admin4002() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin4002",
			whoValue:globaWhoValue
		},
		timeout: 2000,
		beforeSend: function() {
			globalMemberPk = [];
			globalMemberName = [];
			globalMemberPushKey = [];

			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var all_num = jsonObj.all_num;
					$(".formStlyeE > form > ul > li .member_index_display").html('총 <b>'+all_num+'명</b> 검색됨');

					
					$.each(jsonObj.data, function(key, value){
						var member_pk = value.member_pk;
						var member_name = value.member_name;
						var member_pushkey = value.member_pushkey;

						globalMemberPk.push(member_pk);
						globalMemberName.push(member_name);
						globalMemberPushKey.push(member_pushkey);
					});

					


				} else {

					$(".formStlyeE > form > ul > li .member_index_display").html('검색된 회원 없음');

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 푸시 그룹 저장하기
function admin4004(pushTitle, pushContent, linkPage) {
	var allNums = globalMemberPk.length;
	window.parent.showPushLoading();
	$(".total_num", parent.document).text(allNums);
	$(".send_num", parent.document).text("0");

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin4004",
			pushTitle:pushTitle,
			pushContent:pushContent,
			linkPage:linkPage,
			allNum:allNums
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
			isPushSending = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var push_group_pk = jsonObj.push_group_pk;

					// 개개인씩 푸시알림 보내고 결과 받기를 반복하기
					globalMemberPkLength = globalMemberPk.length;
					admin4006(pushTitle, pushContent, linkPage, push_group_pk);

				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 푸시 알림 전송 클릭시 이벤트 설정하기
function admin4005() {
	$(".formStlyeE > form > .push_send_button").on("click", function(){
		if (isPushSending == false) {
			var who = $("select[name=who]").val();
			var linkPage = $("select[name=linkPage]").val();
			var pushTitle = $.trim($("input[name=pushTitle]").val());
			var pushContent = $.trim($("textarea[name=pushContent]").val());

			console.log("pushContent = "+pushContent);

			if (pushTitle == "") {
				alert("푸시 제목을 입력해주세요.");
				return 0;
			} 

			if (pushContent == "") {
				alert("푸시 내용을 입력해주세요.");
				return 0;
			}

			if (pushContent.length >= 180) {
				alert("푸시 내용은 180자를 넘을 수 없습니다.");
				return 0;
			}

			if (globalMemberPk.length == 0) {
				alert("검색된 회원이 없어 푸시 알림을 보낼 수 없습니다.");
				return 0;
			}


			// 푸쉬 그룹 저장하기
			admin4004(pushTitle, pushContent, linkPage);

		} else {
			alert("푸시 전송 작업이 진행중입니다. 잠시후 시도해주세요.");
		}

	});
}

// 개개인씩 푸시알림 보내고 결과 받기를 반복하기
function admin4006(pushTitle, pushContent, linkPage, push_group_pk) {
	if (globalI < globalMemberPkLength) {
		$.ajax({
			type: "POST",
			url: "./outlet.php",
			data:{
				act:"admin4006",
				member_pk:globalMemberPk[globalI],
				member_name:globalMemberName[globalI],
				member_pushkey:globalMemberPushKey[globalI],
				pushTitle:pushTitle,
				pushContent:pushContent,
				linkPage:linkPage,
				push_group_pk:push_group_pk
			},
			timeout: 2000,
			beforeSend: function() {
				isLoading = true;
			},
			complete: function() {
				isLoading = false;
			},
			success: function(data) {
				globalI++;

				$(".send_num", parent.document).text(globalI);

				console.log("data = "+data);

				if (isJSON(data)) {
					var jsonObj = $.parseJSON(data);
					var result = jsonObj.result;

					if (result == "ok") {
						

						admin4006(pushTitle, pushContent, linkPage, push_group_pk);


					} else {

						

					}
				} else {

				}
			},
			fail: function() {
				
			}
		});
	} else {
		isPushSending = false;
		setTimeout(pushComplete, 500);
	}
}

function pushComplete() {
	alert("푸시 알림 전송이 완료되었습니다.");
	window.parent.location.href="./admin1100.php";
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}