var global_nmi_pk = 0;


var global_department_int_array = new Array();
var global_department_string_array = new Array();




$(document).ready(function(){
	getNmiPk(); // N의학정보 pk 가져오기


	admin6601(); // 진료과 리스트 불러오기
	admin6602(); // 관련 진료과 추가 버튼 클릭시 이벤트 설정하기

	admin6610(); // N의학정보 추가 버튼 클릭시 이벤트 설정하기
});

// N의학정보 pk 가져오기
function getNmiPk() {
	global_nmi_pk = Number($("input[name=nmi]").val());
}

// 진료과 리스트 불러오기
function admin6601() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6601"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					$.each(jsonObj.data, function(key, value){
						var suhd = value.suhd;
						var department = value.department;

						global_department_int_array.push(suhd);
						global_department_string_array.push(department);

						var text = '<option value="'+suhd+'">'+department+'</option>';

                        $("select[name=departmentSelectList]").append(text);
					});


					admin6715(); // N의학정보 상세정보 가져오기


				} else {

					
				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 관련 진료과 추가 버튼 클릭시 이벤트 설정하기
function admin6602() {
	$(".department_add_button").on("click", function(){
		var d_code = $("select[name=departmentSelectList]").val();
		var d_string = $.trim($("select[name=departmentSelectList]").children("option[value="+d_code+"]").text());


		var checkFlag = 0;
		$("ul.chain_department_list li").each(function(){
			var existString = $.trim(Number($(this).children("div.title").text()));

			if (d_string == existString) {
				checkFlag++;
				return 0;
			}
		});
		if (checkFlag == 1) {
			alert("이미 등록되어 있는 진료과 입니다.");
			return false;
		}


		var text = '';
		text += '<li class="clearFix">';
        text += '    <div class="title" data-code="'+d_code+'">';
        text += '        '+d_string;
        text += '    </div>';
        text += '    <div class="delete_button" onclick="admin6603(this);">';
        text += '        삭제';
        text += '    </div>';
        text += '</li>';

        $("ul.chain_department_list").append(text);

	});
}

// 관련 진료과 삭제 버튼 클릭시 이벤트 설정하기
function admin6603(obj) {
	$(obj).parent().remove();
}

// N의학정보 추가 버튼 클릭시 이벤트 설정하기
function admin6610() {
	$(".department_upload_button").on("click", function(){
		if (admin6611()) { // 입력된 N의학정보 폼 체크하기
			admin6716(); // N의학정보 서버에 업데이트 요청하기
		}
	});
}



var checked_titleKo = null;
var checked_titleEn = null;
var checked_oneDescription = null;
var checked_chainDepartments = new Array();
var checked_definition = null;
var checked_symptom = null;
var checked_cause = null;
var checked_chainBody = null;
var checked_diagnosis = null;
var checked_check = null;
var checked_cure = null;
var checked_hyperplasia = null;
var checked_prevent = null;
var checked_guide = null;
var checked_checkPeriod = null;
var checked_checkLeadTime = null;
var checked_chainCheckWay = null;

// 입력된 N의학정보 폼 체크하기
function admin6611() {
	// 의학정보명 (한글) 체크
	var titleKo = $("input[name=titleKo]").val();
	if (titleKo == '') {
		alert("의학정보명(한글)을 입력해주세요.");
		return false;
	}
	checked_titleKo = titleKo;



	// 의학정보명 (영어) 체크
	var titleEn = $("input[name=titleEn]").val();
	if (titleEn == '') {
		alert("의학정보명(영어)을 입력해주세요.");
		return false;
	}
	checked_titleEn = titleEn;



	// 한 줄 설명
	var oneDescription = $("input[name=oneDescription]").val();
	if (oneDescription == '') {
		alert("한 줄 설명을 입력해주세요.");
		return false;
	}
	checked_oneDescription = oneDescription;



	
	// 관련 진료과
	var chainDepartment = new Array();
	$("ul.chain_department_list li").each(function(){
		chainDepartment.push($.trim($(this).children("div.title").text()));
	});

	var chainDepartmentString = '';
	for (var i=0; i<chainDepartment.length; i++) {
		chainDepartmentString += chainDepartment[i];
		if (i != chainDepartment.length-1) {
			chainDepartmentString += ', ';
		}
	}
	checked_chainDepartments = chainDepartmentString;




	// 정의
	var definition = $("textarea[name=definition]").val();
	checked_definition = definition;

	// 증상
	var symptom = $("textarea[name=symptom]").val();
	checked_symptom = symptom;

	// 원인
	var cause = $("textarea[name=cause]").val();
	checked_cause = cause;

	// 관련 신체기관
	var chainBody = $("textarea[name=chainBody]").val();
	checked_chainBody = chainBody;

	// 진단
	var diagnosis = $("textarea[name=diagnosis]").val();
	checked_diagnosis = diagnosis;

	// 검사
	var check = $("textarea[name=check]").val();
	checked_check = check;

	// 치료
	var cure = $("textarea[name=cure]").val();
	checked_cure = cure;

	// 경과/합병증
	var hyperplasia = $("textarea[name=hyperplasia]").val();
	checked_hyperplasia = hyperplasia;

	// 예방방법
	var prevent = $("textarea[name=prevent]").val();
	checked_prevent = prevent;

	// 생활가이드
	var guide = $("textarea[name=guide]").val();
	checked_guide = guide;

	// 검사주기
	var checkPeriod = $("textarea[name=checkPeriod]").val();
	checked_checkPeriod = checkPeriod;

	// 소요시간
	var checkLeadTime = $("textarea[name=checkLeadTime]").val();
	checked_checkLeadTime = checkLeadTime;

	// 관련검사법
	var chainCheckWay = $("textarea[name=chainCheckWay]").val();
	checked_chainCheckWay = chainCheckWay;


	return true;
}

// N의학정보 서버에 업데이트 요청하기
function admin6716() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6716",
			nmi:global_nmi_pk,
			titleKo:checked_titleKo,
			titleEn:checked_titleEn,
			oneDescription:checked_oneDescription,
			chainDepartments:checked_chainDepartments,
			definition:checked_definition,
			symptom:checked_symptom,
			cause:checked_cause,
			chainBody:checked_chainBody,
			diagnosis:checked_diagnosis,
			check:checked_check,
			cure:checked_cure,
			hyperplasia:checked_hyperplasia,
			prevent:checked_prevent,
			guide:checked_guide,
			checkPeriod:checked_checkPeriod,
			checkLeadTime:checked_checkLeadTime,
			chainCheckWay:checked_chainCheckWay
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					
					alert("수정되었습니다.");
					location.href = "./admin6500.php";


				} else {

					
				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}


// N의학정보 상세정보 가져오기
function admin6715() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin6715",
			nmi:global_nmi_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var title_ko = jsonObj.title_ko;
					var title_en = jsonObj.title_en;
					var one_line_description = jsonObj.one_line_description;
					var chain_department = jsonObj.chain_department;
					var definition_value = jsonObj.definition_value;
					var symptom_value = jsonObj.symptom_value;
					var cause_value = jsonObj.cause_value;
					var chainBody_value = jsonObj.chainBody_value;
					var diagnosis_value = jsonObj.diagnosis_value;
					var check_value = jsonObj.check_value;
					var cure_value = jsonObj.cure_value;
					var hyperplasia_value = jsonObj.hyperplasia_value;
					var prevent_value = jsonObj.prevent_value;
					var guide_value = jsonObj.guide_value;
					var checkPeriod_value = jsonObj.checkPeriod_value;
					var checkLeadTime_value = jsonObj.checkLeadTime_value;
					var chainCheckWay_value = jsonObj.chainCheckWay_value;


					$("input[name=titleKo]").val(htmlspecialchar_decode(title_ko));
					$("input[name=titleEn]").val(htmlspecialchar_decode(title_en));
					$("input[name=oneDescription]").val(htmlspecialchar_decode(one_line_description));

					console.log("chain_department = "+chain_department);
					if (chain_department != '') {
						var arrays = chain_department.split(", ");
						for (var i=0; i<arrays.length; i++) {
							var department_string = $.trim(arrays[i]);
							var department_int = 0;
							$("select[name=departmentSelectList]").children("option").each(function(){
								var department_str = $.trim($(this).text());

								if (department_str == department_string) {
									department_int = Number($(this).attr("value"));
								}
							});



							var text = '';
							text += '<li class="clearFix">';
					        text += '    <div class="title" data-code="'+department_int+'">';
					        text += '        '+department_string;
					        text += '    </div>';
					        text += '    <div class="delete_button" onclick="admin6603(this);">';
					        text += '        삭제';
					        text += '    </div>';
					        text += '</li>';

					        $("ul.chain_department_list").append(text);
						}
					}


					$("textarea[name=definition]").val(htmlspecialchar_decode(definition_value));
					$("textarea[name=symptom]").val(htmlspecialchar_decode(symptom_value));
					$("textarea[name=cause]").val(htmlspecialchar_decode(cause_value));
					$("textarea[name=chainBody]").val(htmlspecialchar_decode(chainBody_value));
					$("textarea[name=diagnosis]").val(htmlspecialchar_decode(diagnosis_value));
					$("textarea[name=check]").val(htmlspecialchar_decode(check_value));
					$("textarea[name=cure]").val(htmlspecialchar_decode(cure_value));
					$("textarea[name=hyperplasia]").val(htmlspecialchar_decode(hyperplasia_value));
					$("textarea[name=prevent]").val(htmlspecialchar_decode(prevent_value));
					$("textarea[name=guide]").val(htmlspecialchar_decode(guide_value));
					$("textarea[name=checkPeriod]").val(htmlspecialchar_decode(checkPeriod_value));
					$("textarea[name=checkLeadTime]").val(htmlspecialchar_decode(checkLeadTime_value));
					$("textarea[name=chainCheckWay]").val(htmlspecialchar_decode(chainCheckWay_value));
					

				} else {

					
				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}


// &amp; 등의 형태의 html 문자기호를 html코드로 디코드화 하여 문자열 반환하기
function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}




/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}