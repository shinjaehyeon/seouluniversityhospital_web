var search_values = [0, '', '', '', '', '', 0, 0];
var page_values = [0, 10, 10, 1];





$(document).ready(function(){
	setBoardTypeSelectBoxEvent(); // 게시글 종류 값 선택할 때마다 이벤트 설정하기

	
	admin7510();
	admin7511();
	admin7512();
	admin7513();
	admin7514();


	admin7520(); // 검색 버튼 클릭시 이벤트 설정하기
});

// 게시글 종류 값 선택할 때마다 이벤트 설정하기
function setBoardTypeSelectBoxEvent() {
	$("select[name=boardType]").on("change", function(){
		var boardType = Number($(this).val());

		if (boardType == 800701) {
			$(".boardtype_1_content_type").show();
			$(".boardtype_2_content_type").hide();
		} else if (boardType == 800702) {
			$(".boardtype_1_content_type").hide();
			$(".boardtype_2_content_type").show();
		} else {
			$(".boardtype_1_content_type").hide();
			$(".boardtype_2_content_type").hide();
		}
	});
}

// 고객의소리 리스트 가져오기
function admin7510() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7510",
			index:    		page_values[0],
			view_num: 		page_values[1],
			page_num: 		page_values[2],
			page_reckoning: page_values[3],
			search_type: 	search_values[0],
			content_type1:  search_values[6],
			content_type2:  search_values[7],
			start_date: 	search_values[1],
			end_date: 		search_values[2],
			writerName: 	search_values[3],
			title: 			search_values[4],
			content: 		search_values[5]
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$(".listTypeD > ul").empty();

					$.each(jsonObj.data, function(key, value){


						var suhc = value.suhc;
						var board_type = value.board_type;
						var board_type_string = value.board_type_string;
						var content_type = value.content_type;
						var content_type_string = value.content_type_string;
						var title = cutString(value.title);

						var content = cutString(value.content);
						var member = value.member;
						var member_name = value.member_name;
						var member_email = value.member_email;
						var datetime = value.datetime;
						var attach_file_flag = value.attach_file_flag;
						var view_index = value.view_index;


						var text = '';
						text += '<li class="clearFix" data-pk="'+suhc+'">';
		                text += '    <div class="question_type board_content_type">';
		                text += '        <div class="board_type">';
		                text += '            '+board_type_string;
		                text += '        </div>';
		                text += '        <div class="content_type">';
		                text += '            '+content_type_string;
		                text += '        </div>';
		                text += '    </div>';
		                text += '    <div class="group1">';
		                text += '        <div class="datetime">';
		                text += '            '+datetime;
		                text += '        </div>';
		                text += '        <div class="title">';
		                text += '            '+title;
		                text += '        </div>';
		                text += '        <div class="member_info">';
		                text += '            <span class="member_name">'+member_name+'</span>';
		                text += '            <span class="member_phone member_email">('+member_email+')</span>';
		                text += '        </div>';
		                text += '    </div>';
		                text += '</li>';


						
		                console.log("실행됨");
						$(".listTypeD > ul").append(text);
					});

					admin7517(); // 병원뉴스 리스트 클릭시 이벤트 설정하기


					if (page_values[3] == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (page_values[0] == 0) {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
							$(".pagingControllerBox > div > div.prev").css({"display":"none"});
						} else {
							$(".pagingControllerBox > div > div.best_prev").css({"display":"block"});
							$(".pagingControllerBox > div > div.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".pagingControllerBox > div > div.best_next").css({"display":"block"});
							$(".pagingControllerBox > div > div.next").css({"display":"block"});	
						} else {
							$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
							$(".pagingControllerBox > div > div.next").css({"display":"none"});
						}


						$(".pagingControllerBox > div > ul").empty();



						var first_page_index = page_values[0];
						for (var ii=0; ii<page_values[2]; ii++) {
							if (first_page_index % page_values[2] == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("page_values[0] = "+page_values[0]);
							console.log("page_number-1 = "+(page_number-1));
							if (page_values[0] == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".pagingControllerBox > div > ul").append(text);
						}


						admin7515(); // 페이지 번호 클릭시 이벤트 설정



						page_values[3] = 0;
					}
				} else {
					$(".listTypeD > ul").empty();
				

					$(".pagingControllerBox > div > div.best_prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.prev").css({"display":"none"});
					$(".pagingControllerBox > div > div.best_next").css({"display":"none"});
					$(".pagingControllerBox > div > div.next").css({"display":"none"});

					$(".pagingControllerBox > div > ul").empty();
					$(".pagingControllerBox > div > ul").append('<li class="active">1</li>');
				}
			} else {
				console.log("JSON이 아님!");
			}
		},
		fail: function() {
			
		}
	});

}




function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}




// 고객의소리 - 다음 페이지 클릭시 이벤트 설정
function admin7511() {
	$(".pagingControllerBox > div > div.next").on("click", function(){
		page_values[3] = 1;

		var minimum_index = page_values[0];
		for (var i=0; i<page_values[2]; i++) {
			if (minimum_index % page_values[2] == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + page_values[2];
		page_values[0] = next_first_index;
		admin7510();
	});
}

// 고객의소리 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function admin7512() {
	$(".pagingControllerBox > div > div.best_next").on("click", function(){
		if (isLoading == false) {
			admin7516(); // 고객의소리 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 고객의소리 - 이전 페이지 클릭시 이벤트 설정
function admin7513() {
	$(".pagingControllerBox > div > div.prev").on("click", function(){
		page_values[3] = 1;

		var minimum_index = page_values[0];
		for (var i=0; i<page_values[2]; i++) {
			if (minimum_index % page_values[2] == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - page_values[2];
		page_values[0] = prev_first_index;
		admin7510();
	});
}

// 고객의소리 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function admin7514() {
	$(".pagingControllerBox > div > div.best_prev").on("click", function(){
		page_values[3] = 1;
		page_values[0] = 0;
		admin7510();
	});
}


// 고객의소리 - 페이지 번호 클릭시 이벤트 설정
function admin7515() {
	$(".pagingControllerBox > div > ul > li").on("click", function(){
		if (isLoading == false) {	
			$(".pagingControllerBox > div > ul > li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			page_values[0] = selected_index;
			admin7510();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 고객의소리 - 페이지 맨 마지막 번호 알아내기
function admin7516() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"admin7516",
			view_num: 		page_values[1],
			page_num: 		page_values[2],
			page_reckoning: page_values[3],
			search_type: 	search_values[0],
			content_type1:  search_values[6],
			content_type2:  search_values[7],
			start_date: 	search_values[1],
			end_date: 		search_values[2],
			writerName: 	search_values[3],
			title: 			search_values[4],
			content: 		search_values[5]
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					page_values[0] = last_index;

					console.log("last_index = "+last_index);

					page_values[3] = 1;
					admin7510();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 병원뉴스 리스트 클릭시 이벤트 설정하기
function admin7517() {
	$(".listTypeD > ul > li").on("click", function(){
		var pk = Number($(this).attr("data-pk"));

		console.log("./admin7600.php?pk="+pk);

		window.open("./admin7600.php?pk="+pk,"고객의소리 상세내용","width=800 height=600 top=120 left=120 menubar=no status=no");
	});
}






// 검색 버튼 클릭시 이벤트 설정하기
function admin7520() {
	$(".finalSearchButton").on("click", function(){
		search_values[0] = Number($("select[name=boardType]").val()); // 게시글 종류
		search_values[1] = $("input[name=startDate]").val(); // 작성날짜 시작
		search_values[2] = $("input[name=endDate]").val(); // 작성날짜 종료
		search_values[3] = $("input[name=writerName]").val(); // 작성자 이름
		search_values[4] = $("input[name=title]").val(); // 제목
		search_values[5] = $("input[name=content]").val(); // 내용
		search_values[6] = $("select[name=contentType1]").val(); // 감사합니다 내용종류
		search_values[7] = $("select[name=contentType2]").val(); // 건의합니다 내용종류

		page_values[3] = 1;

		admin7510(); // 고객의소리 리스트 가져오기
	});
}

// 상세페이지에서 삭제한 글의 il 태그 삭제하기
function admin7521(pk_num) {
	$("li[data-pk="+pk_num+"]").remove();
}





/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

var isLoading = false;

// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}


function consoles(str, value) {
	console.log(str+" = "+value);
}

function isLoadingFalse() {
	if (isLoading == false) {
		return true;
	} else {
		alert("작업이 진행중입니다.");
		return false;
	}
}