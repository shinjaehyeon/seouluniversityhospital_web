<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 의료진 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin9500.js"></script>
	</head>
	<body page-code="admin9500" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 의료진 관리
        </div>


        

       




        <div class="searchBoxTypeA clearFix">
            <div class="searchTypeEgg">
                <select name="doctorSearchType" class="searchType">
                    <option value="3">의료진 이름</option>
                    <option value="1">의료진 코드</option>
                    <option value="2">소속진료과 명칭</option>
                </select>
            </div>
            <div class="inputArea" style="float:left;width:80%;">
                <input type="text" name="searchValue" placeholder="의료진 이름, 코드 또는 소속진료과명칭으로 검색하세요." maxlength="6" />
            </div>
            <div class="searchButton">

            </div>
        </div>






        <div class="group3">
            <a href="./admin9600.php" class="doctor_add_button">
                의료진 추가
            </a>
        </div>





        <div class="doctor_list_area">
            <ul class="clearFix">
                <li data-pk="800262">
                    <div class="department">내분비내과</div>
                    <div class="doctor_name">강진환</div>
                </li>
                <li data-pk="800262">
                    <div class="department">내분비내과</div>
                    <div class="doctor_name">강진환</div>
                </li>
                <li data-pk="800262">
                    <div class="department">내분비내과</div>
                    <div class="doctor_name">강진환</div>
                </li>
                <li data-pk="800262">
                    <div class="department">내분비내과</div>
                    <div class="doctor_name">강진환</div>
                </li>
                <li data-pk="800262">
                    <div class="department">내분비내과</div>
                    <div class="doctor_name">강진환</div>
                </li>
            </ul>
        </div>
        







		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>