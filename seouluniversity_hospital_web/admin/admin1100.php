<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자 페이지</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin1100.js"></script>
	</head>
	<body page-code="admin1100" class="clearFix" onload="onloads();">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>
		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />



		<div class="push_loading_bar">
			<div class="contents">
				<div class="ment">
					푸쉬를 보내고 있습니다. 잠시만 기다려주세요.
				</div>
				<div class="progress_display">
					<span class="send_num"></span>
					<span>/</span>
					<span class="total_num"></span>
				</div>
				<div class="loading_area">
					<img src="./images/loading_animation.gif" alt="로딩바" title="로딩바" />
				</div>
			</div>
		</div>



		
		
		
		<div class="admin1100 menu_window setTopVirtualBox">
			<div class="logo_box">
				<img src="./images/logo_white.png" alt="로고" title="로고" />
			</div>
			<div class="clearFix">
				<div class="logo_areas">
					<img src="./images/logo_white.png" alt="로고" title="로고" />
					<div>
						<관리자페이지>
					</div>
				</div>
				<div class="menu_list_box_parent">
					<ul class="menu_list_box">
						<li>
							<a href="./admin2000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_01.png);"></div>
								<div class="text">대시<br />보드</div>
							</a>
						</li>
						<li>
							<a href="./admin5000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_02.png);"></div>
								<div class="text">회원<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin3000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_03.png);"></div>
								<div class="text">순번대기표<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin4000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_04.png);"></div>
								<div class="text">푸시알림<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin6000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_05.png);"></div>
								<div class="text">진료예약<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin6500.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_06.png);"></div>
								<div class="text">N의학<br />정보관리</div>
							</a>
						</li>
						<li>
							<a href="./admin7000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_07.png);"></div>
								<div class="text">병원뉴스<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin7500.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_08.png);"></div>
								<div class="text">고객의소리<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin8000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_09.png);"></div>
								<div class="text">1:1문의<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin9000.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_10.png);"></div>
								<div class="text">진료과<br />관리</div>
							</a>
						</li>
						<li>
							<a href="./admin9500.php" target="admin_content" class="setTopVirtualBox">
								<div class="icon" style="background-image:url(./images/admin_menu_icon_11.png);"></div>
								<div class="text">의료진<br />관리</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="admin1100 content_window">
			<iframe name="admin_content"></iframe>
		</div>
		


	
	</body>
</html>