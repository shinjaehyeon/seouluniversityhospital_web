<!DOCTYPE html>
<html class="admin1000" lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자 로그인</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin1000.js"></script>
	</head>
	<body page-code="admin1000">
		<?php
			session_start();
			
			if ($_SESSION['admin_login'] == 'ok') {
				echo '
				<script>
					alert("이미 로그인 되어 있습니다. 관리자 메인 화면으로 이동합니다.");
					location.href = "./admin1100.php";
				</script>';
				exit;
			}

		?>

		




		<div class="admin1000 login_box">
			<div class="admin_text">
				관리자페이지
			</div>

			<div class="logo_box clearFix">
				<img width="160" src="./images/logo_color.png" alt="로고" title="로고" />
			</div>
			
			<form>
				<ul>
					<li>
						<input class="idpwform" type="text" name="adminId" placeholder="ID" />
					</li>
					<li>
						<input class="idpwform" type="password" name="adminPw" placeholder="PW" />
					</li>
				</ul>	
				<div class="login_button">
					로그인
				</div>
			</form>
		</div>
		
	</body>
</html>