<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>서울대학교병원 관리자페이지 - 푸시알림 관리</title>

		<link href="./css/admin.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/admin4000.js"></script>
	</head>
	<body page-code="admin4000" id="body">
		<?php
			session_start();

			if ($_SESSION['admin_login'] == '') {	
				echo '
					<script>
						alert("잘못된 접근입니다.");
						window.parent.location.href="./admin1000.php";
					</script>
				';
				exit;
			}
		?>

		<input type="hidden" name="admin_pk" value="<?php echo $_SESSION['admin_primarykey']; ?>" />
		<input type="hidden" name="admin_ip" value="<?php echo $_SESSION['admin_ip']; ?>" />






        <div class="big_title">
            ■ 푸시알림 관리
        </div>


        





        <div class="formStlyeE">
            <form>
                <ul>
                    <li class="clearFix">
                        <div class="title">
                            대상
                        </div>
                        <div class="content">
                            <select class="input_box_style" name="who" style="width:300px;height:40px;">
                                <option value="600401">전체</option>
                                <option value="600402">10대~20대</option>
                                <option value="600403">20대~30대</option>
                                <option value="600404">30대~40대</option>
                                <option value="600405">40대 이상</option>
                                <option value="600406">혈액형이 O형인 사람</option>
                                <option value="600407">혈액형이 A형인 사람</option>
                                <option value="600408">혈액형이 B형인 사람</option>
                                <option value="600409">혈액형이 AB형인 사람</option>
                            </select>
                        </div>
                        <div class="member_index_display">
                            
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            연결 페이지
                        </div>
                        <div class="content">
                            <select class="input_box_style" name="linkPage" style="width:300px;height:40px;">
                                <option value="600501">없음</option>
                                <option value="600502">병원뉴스</option>
                                <option value="600503">프로필정보업데이트</option>
                                <option value="600504">진료예약신청</option>
                                <option value="600505">진료예약내역</option>
                                <option value="600506">진료과</option>
                                <option value="600507">의료진</option>
                            </select>
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            제목
                        </div>
                        <div class="content">
                            <input type="text" name="pushTitle" class="input_box_style" style="width:600px;height:40px;" />
                        </div>
                    </li>
                    <li class="clearFix">
                        <div class="title">
                            내용
                        </div>
                        <div class="content">
                            <textarea name="pushContent" class="input_box_style" style="width:600px;height:120px;padding:10px;display:block;"></textarea>
                            <div>
                                <span class="word_num">0</span><span>/</span><span>180</span>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="push_send_button">
                    푸시 알림 전송
                </div>
            </form>
        </div>







		
	</body>
</html>

<?php 
	// yyyy-dd-mm hh:ii:ss 형태의 데이터날짜를 년,월,일,분,시,초로 뽑아내기
    function dateDivide($date, $type) {
        $day = array('일','월','화','수','목','금','토');

        $temp_date = explode(" ", $date);
        $only_date = $temp_date[0]; // ex) 2018-05-07
        $only_time = $temp_date[1]; // ex) 13:11:30

        $dates = explode('-', $only_date); // [0] = 2018, [1] = 06, [2] = 13
        $times = explode(':', $only_time); // [0] = 15, [1] = 23, [2] = 35

        $return_str = '';

        switch ($type) {
            case 'y':
            case 'yy':
                $return_str = substr($dates[0], 2, 2);
                break;
            case 'Y':
            case 'YY':
            case 'yyyy':
                $return_str = $dates[0];
                break;
            case 'm':
                $return_str = substr($dates[1], 1, 1); 
                break;
            case 'M':
            case 'MM':
            case 'mm':
                $return_str = $dates[1];
                break;
            case 'd':
                $return_str = $dates[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'D':
            case 'DD':
            case 'dd':
                $return_str = $dates[2];
                break;
            case 'h':
                $return_str = $times[0];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'hh':
            case 'H':
            case 'HH':
                $return_str = $times[0];
                break;
            case 'i':
                $return_str = $times[1];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'I':
            case 'II':
            case 'ii':
                $return_str = $times[1];
                break;

            case 's':
                $return_str = $times[2];
                $temp_int = (int) $return_str;
                if ($temp_int < 10) {
                	$return_str = ''.$temp_int;
                } 
                break;
            case 'S':
            case 'SS':
            case 'ss':
                $return_str = $times[2];
                break;
            case 'date':
                $return_str = $only_date;
                break;
            case 'time':
                $return_str = $only_time;
                break;
            case 'day':
                $return_str = $day[date('w', strtotime($date))];
                break;
            default:
                # code...
                break;
        }

        return $return_str;
    }
?>