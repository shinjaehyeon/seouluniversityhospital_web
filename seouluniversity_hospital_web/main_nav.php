<nav class="nav">
	<div class="nav_1200">
		<ul class="main_nav_box clearFix">
			<li>
				<a href="#" menu-index="0">
					진료안내
				</a>
			</li>
			<li>
				<a href="#" menu-index="1">
					이용안내
				</a>
			</li>
			<li>
				<a href="#" menu-index="2">
					건강정보
				</a>
			</li>
			<li>
				<a href="#" menu-index="3">
					고객참여
				</a>
			</li>
			<li>
				<a href="#" menu-index="4">
					병원소개
				</a>
			</li>
		</ul>
	</div>

	<div class="menu_1_child_menu_box child_menu_box" child-menu-index="0">
		<div class="child_menu_1200_box">
			<ul class="child_menu_cols">
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								진료과/의료진
							</a>
						</li>
						<li>
							<a href="./web2100.php">
								진료과 소개
							</a>
						</li>
						<li>
							<a href="./web2300.php">
								의료진 소개
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="./web2000.php">
								진료예약 신청
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								외래진료안내
							</a>
						</li>
						<li>
							<a href="#">
								진료안내
							</a>
						</li>
						<li>
							<a href="#">
								예약안내
							</a>
						</li>
						<li>
							<a href="#">
								진료비하이패스
							</a>
						</li>
						<li>
							<a href="#">
								지역채혈안내
							</a>
						</li>
						<li>
							<a href="#">
								비급여진료비용
							</a>
						</li>
					</ul>
					
				</li>
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								입/퇴원 안내
							</a>
						</li>
						<li>
							<a href="#">
								입원절차
							</a>
						</li>
						<li>
							<a href="#">
								퇴원절차
							</a>
						</li>
						<li>
							<a href="#">
								입원생활안내
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								가정간호 안내
							</a>
						</li>
						<li>
							<a href="#">
								가정간호소개
							</a>
						</li>
						<li>
							<a href="#">
								가정간호내용
							</a>
						</li>
						<li>
							<a href="#">
								가정간호이용안내
							</a>
						</li>
					</ul>
					
					
				</li>
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								응급의료센터
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								건강검진센터
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								공단건강검진센터
							</a>
						</li>
					</ul>
					
					
				</li>
			</ul>
		</div>
	</div>

	<div class="menu_2_child_menu_box child_menu_box" child-menu-index="1">
		<div class="child_menu_1200_box">
			<ul class="child_menu_cols">
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								위치안내
							</a>
						</li>
						<li>
							<a href="./web3000.php">
								오시는길
							</a>
						</li>
						<li>
							<a href="#">
								원내위치도
							</a>
						</li>
						<li>
							<a href="#">
								주차안내
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								편의시설
							</a>
						</li>
						<li>
							<a href="#">
								원내편의시설
							</a>
						</li>
						<li>
							<a href="#">
								주변편의시설
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								문병안내
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								위임장/동의서 안내
							</a>
						</li>
					</ul>
					
				</li>
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								신청/발급 안내
							</a>
						</li>
						<li>
							<a href="#">
								진료의뢰서
							</a>
						</li>
						<li>
							<a href="#">
								진단서발급
							</a>
						</li>
						<li>
							<a href="#">
								장애/병사/취업진단서
							</a>
						</li>
						<li>
							<a href="#">
								진료비계산서
							</a>
						</li>
						<li>
							<a href="#">
								처방전
							</a>
						</li>
						<li>
							<a href="#">
								의무기록 열람/사본발급
							</a>
						</li>
						<li>
							<a href="#">
								연말정산 진료비영수증
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								전화번호 안내
							</a>
						</li>
					</ul>
					
					
				</li>
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								증명서발급사이트
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								FAQ
							</a>
						</li>
					</ul>
					
				</li>
			</ul>
		</div>
	</div>

	<div class="menu_3_child_menu_box child_menu_box" child-menu-index="2">
		<div class="child_menu_1200_box">
			<ul class="child_menu_cols">
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								건강TV
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								자가진단서비스
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								질병/의학정보
							</a>
						</li>
						<li>
							<a href="#">
								의학백과사전
							</a>
						</li>
						<li>
							<a href="#">
								종합질병정보
							</a>
						</li>
						<li>
							<a href="./web4000.php">
								N의학정보
							</a>
						</li>
					</ul>
					
				</li>
				<li>
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								의료기기정보
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								포스트/카드뉴스
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>

	<div class="menu_4_child_menu_box child_menu_box" child-menu-index="3">
		<div class="child_menu_1200_box">
			<ul class="child_menu_cols">
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								고객의소리
							</a>
						</li>
						<li>
							<a href="./web5000.php">
								고객의소리 게시판
							</a>
						</li>
						<li>
							<a href="#">
								고객상담실 업무안내
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								감사이야기
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								자원봉사센터
							</a>
						</li>
						<li>
							<a href="#">
								모집공지
							</a>
						</li>
						<li>
							<a href="#">
								갤러리
							</a>
						</li>
						<li>
							<a href="#">
								자원봉사란?
							</a>
						</li>
					</ul>
					
				</li>
				<li>

					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								후원
							</a>
						</li>
						<li>
							<a href="#">
								발전후원회
							</a>
						</li>
						<li>
							<a href="#">
								어린이후원회
							</a>
						</li>
					</ul>
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								환자권리장전
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								정보공개제도
							</a>
						</li>
						<li>
							<a href="#">
								정보공개제도안내
							</a>
						</li>
						<li>
							<a href="#">
								정보공개목록
							</a>
						</li>
					</ul>
					
					
				</li>
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								파견/용역업체 정규직전환
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								사업실명제
							</a>
						</li>
						<li>
							<a href="#">
								실명제개요
							</a>
						</li>
						<li>
							<a href="#">
								실명제목록
							</a>
						</li>
						<li>
							<a href="#">
								클린센터
							</a>
						</li>
					</ul>
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								병원보퀴즈
							</a>
						</li>
					</ul>
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								설문조사
							</a>
						</li>
					</ul>
					
				</li>
			</ul>
		</div>
	</div>

	<div class="menu_5_child_menu_box child_menu_box" child-menu-index="4">
		<div class="child_menu_1200_box">
			<ul class="child_menu_cols">
				<li>
					
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								병원역사
							</a>
						</li>
						<li>
							<a href="#">
								사진으로 보는 역사
							</a>
						</li>
						<li>
							<a href="#">
								연혁
							</a>
						</li>
						<li>
							<a href="#">
								제중원
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								병원장소개
							</a>
						</li>
						<li>
							<a href="#">
								인사말
							</a>
						</li>
						<li>
							<a href="#">
								역대병원장
							</a>
						</li>
					</ul>
					<ul class="child_menu_group">
						<li class="title">
							<a href="#">
								조직도
							</a>
						</li>
						<li>
							<a href="#">
								조직도
							</a>
						</li>
						<li>
							<a href="#">
								현황 및 통계
							</a>
						</li>
						<li>
							<a href="#">
								국내외 유관기관
							</a>
						</li>
					</ul>
					
				</li>
				<li>

					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								병원소식
							</a>
						</li>
						<li>
							<a href="./web6000.php">
								병원뉴스
							</a>
						</li>
						<li>
							<a href="#">
								강좌안내
							</a>
						</li>
						<li>
							<a href="#">
								언론보도
							</a>
						</li>
						<li>
							<a href="#">
								병원신문
							</a>
						</li>
						<li>
							<a href="#">
								입찰정보
							</a>
						</li>
						<li>
							<a href="#">
								채용공고
							</a>
						</li>
					</ul>
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								정기간행물
							</a>
						</li>
						<li>
							<a href="#">
								웹진
							</a>
						</li>
						<li>
							<a href="#">
								매거진 VOM
							</a>
						</li>
					</ul>
					
				</li>
				<li>

					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								HI/로고
							</a>
						</li>
						<li>
							<a href="#">
								HI 소개
							</a>
						</li>
						<li>
							<a href="#">
								HI / 캐릭터 / 기타
							</a>
						</li>
					</ul>
					<ul class="child_menu_group first">
						<li class="title">
							<a href="#">
								미션 / 비전
							</a>
						</li>
					</ul>
					
				</li>
			</ul>
		</div>
	</div>
</nav>