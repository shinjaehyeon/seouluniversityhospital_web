<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 메인" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원에 오신것을 환영합니다.</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />



		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web1000.js"></script>
	</head>
	<body id="body" page-code="web1000">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>

		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>

		
		
		<section class="main_visual_box">
			<h2 class="blind">
				메인 비주얼 영역
			</h2>

			<div class="black_shading_effect">
				<!-- 검정 음영 효과 -->
			</div>

			<div class="main_visual_1200 setTopVirtualBox">
				<!-- <section class="main_search_box">
					<h2 class="blind">
						메인 검색 영역
					</h2>
					<input class="main_search" type="text" name="main_search" placeholder="검색어를 입력하세요." />
					<div class="main_search_submit_button">

					</div>
				</section> -->


				<section class="main_tag_box">
					<h2 class="blind">
						메인 검색 영역
					</h2>
					
					<div class="ment">
						원하는 태그를 선택하세요!
					</div>
					<ul class="tag_list clearFix">
						<li>
							<a href="./web2100.php">
								#진료과
							</a>
						</li>
						<li>
							<a href="./web2300.php">
								#의료진
							</a>
						</li>
						<li>
							<a href="./web2000.php">
								#진료예약
							</a>
						</li>
						<li>
							<?php
								if ($_SESSION['is_login'] == 'ok') {
									echo '<a href="./web1500.php">#내정보</a>';
								} else {
									echo '<a href="./web1200.php">#회원가입</a>';
								}
							?>
						</li>
						<br />
						<li>
							<a href="./web4000.php">
								#N의학정보
							</a>
						</li>
						<li>
							<a href="./web3000.php">
								#오시는길
							</a>
						</li>
						<li>
							<a href="./web6000.php">
								#병원뉴스
							</a>
						</li>
						<li>
							<a href="./web5000.php">
								#고객의소리
							</a>
						</li>
						<li>
							<a href="./index.php">
								#서울대학교병원
							</a>
						</li>
					</ul>
				</section>



				<section class="main_major_menu_box">
					<h2 class="blind">
						메인 주요 메뉴 영역
					</h2>
					<ul class="main_major_menu_ul clearFix">
						<li>
							<a href="./web2000.php" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_1_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									진료예약
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
						<li>
							<a href="./web2100.php" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_2_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									진료과
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
						<li>
							<a href="./web2300.php" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_3_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									의료진
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
						<li>
							<a href="./web5000.php" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_4_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									고객의소리
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
					</ul>
					<ul class="main_major_menu_ul clearFix">
						<li>
							<a href="./web4000.php" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_5_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									N의학정보
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
						<li>
							<a href="./web6000.php" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_6_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									병원뉴스
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
						<li>
							<a href="./web3000.php" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_7_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									오시는길
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
						<li>
							<a href="https://play.google.com/store/apps/details?id=org.snuh.www.app" class="setTopVirtualBox">
								<div class="main_major_menu_thick_bg_box">

								</div>
								<div class="main_major_menu_icon_box">
									<img src="./images/web1000_main_major_menu_8_icon.png" />
								</div>
								<div class="main_major_menu_title_box">
									앱 설치
								</div>
								<div class="main_major_menu_linktext_box">
									바로가기
								</div>
							</a>
						</li>
					</ul>
				</section>

				<div class="main_major_menu_slide_controller_box">
					<ul class="clearFix">
						
					</ul>
					<div class="stop_play_button">
						<img src="./images/stop_icon.png" alt="재생/정지 아이콘" title="재생/정지 아이콘" />
					</div>
				</div>

			</div>
		</section>


		<section class="hospital_board_box setTopVirtualBox setBottomVirtualBox">
			<h2 class="blind">
				병원 소식 영역
			</h2>

			
			<div class="main_section_title_box">
				<!-- 병원소식 -->
				병원뉴스
			</div>
			<div class="main_section_title_comment_box">
				서울대학교병원에 대한 병원뉴스를 전해드립니다.
			</div>
			<div class="board_tab_box" style="display:none;">
				<div class="board_tab_box_1200">
					<ul class="board_tab clearFix">
						<li class="active" menu-index="0">
							병원뉴스
						</li>
						<li menu-index="1">
							채용안내
						</li>
						<li menu-index="2">
							언론보도
						</li>
						<li menu-index="3">
							강좌안내
						</li>
					</ul>
				</div>
				<div class="board_tab_position_line_box">
					<div class="board_tab_position_line_box_1200">
						<div class="board_tab_position menu_index_0">

						</div>
					</div>
				</div>
			</div>
			<div class="board_box">
				<ul class="tab_index_0_board board show">
					<li class="board_title clearFix">
						<div>
							번호
						</div>
						<div>
							제목
						</div>
						<div>
							작성일
						</div>
						<div>
							첨부파일
						</div>
						<div>
							조회수
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="attach_file">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="attach_file">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="attach_file">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="attach_file">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="attach_file">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="attach_file">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
						</div>
						<div class="center">
							2018-05-18
						</div>
						<div class="attach_file">
							
						</div>
						<div class="center">
							34
						</div>
					</li>
				</ul>
				<ul class="tab_index_1_board board">
					<li class="board_title clearFix">
						<div>
							번호
						</div>
						<div>
							제목
						</div>
						<div>
							모집기간
						</div>
						<div>
							구분
						</div>
						<div>
							진행상황
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">서울대학교병원 블라인드 직원채용 (단시간근로자) 공고</a>
						</div>
						<div class="center">
							2018-06-29 ~ 2018-07-06
						</div>
						<div class="center">
							신입	
						</div>
						<div class="center">
							접수준비중
						</div>
					</li>
				</ul>
				<ul class="tab_index_2_board board">
					<li class="board_title clearFix">
						<div>
							번호
						</div>
						<div>
							제목
						</div>
						<div>
							의료진
						</div>
						<div>
							보도일자
						</div>
						<div>
							언론사
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">불면증에 잠 못 이루는 밤…과식·과음은 금물!</a>
						</div>
						<div class="center">
							이유진
						</div>
						<div class="center">
							2018-06-29
						</div>
						<div class="center">
							농민신문
						</div>
					</li>
				</ul>
				<ul class="tab_index_3_board board">
					<li class="board_title clearFix">
						<div>
							번호
						</div>
						<div>
							제목
						</div>
						<div>
							강좌기간
						</div>
						<div>
							조회수
						</div>
						<div>
							첨부파일
						</div>
					</li>
					<li class="clearFix">
						<div class="center">
							2018
						</div>
						<div class="padding_left_30">
							<a href="#">2018년 7월 암정보교육센터 교육프로그램 안내</a>
						</div>
						<div class="center">
							2018년 9월 14일 금요일
						</div>
						<div class="center">
							414
						</div>
						<div class="attach_file">
							
						</div>
					</li>
				</ul>
			</div>
			<div class="more_view_button" onclick="location.href='./web6000.php'">
				더보기
			</div>

		</section>


		<section class="banner_box setTopVirtualBox">
			<h2 class="blind">
				메인 서울대학교 배너 영역
			</h2>
			<div class="black_shading_effect">

			</div>
			<div class="banner_box_1200">
				<div class="big">
					최상의 진료로 가장 신뢰받는 병원
				</div>
				<div class="small">
					세계적 첨단 진료영역을 지속적으로 확보함으로써<br />
					국민과 의료전문가들이 믿고 선택하는 병원이 되겠습니다.
				</div>
			</div>
		</section>


		<section class="health_info_box setTopVirtualBox setBottomVirtualBox">
			<h2 class="blind">
				건강 정보 영역
			</h2>

			
			<div class="main_section_title_box">
				건강정보
			</div>
			<div class="main_section_title_comment_box">
				질병, 영양, 운동, 생활 속 관리 등 건강에 대한 모든 것을 알려드립니다.
			</div>
			<div class="health_info_menu_box">
				<ul class="health_info_menu clearFix">
					<li>
						<a href="#" class="menu_index_0 setTopVirtualBox">
							<div class="shading_effect">

							</div>
							<div class="title">
								건강 TV
							</div>
							<div class="link_text">
								바로가기
							</div>
						</a>	
					</li>
					<li>
						<a href="#" class="menu_index_1 setTopVirtualBox">
							<div class="shading_effect">

							</div>
							<div class="title">
								포스트/카드뉴스
							</div>
							<div class="link_text">
								바로가기
							</div>
						</a>	
					</li>	
					<li>
						<a href="#" class="menu_index_2 setTopVirtualBox">
							<div class="shading_effect">

							</div>
							<div class="title">
								질병/의학정보
							</div>
							<div class="link_text">
								바로가기
							</div>
						</a>	
					</li>	
					<li>
						<a href="#" class="menu_index_3 setTopVirtualBox">
							<div class="shading_effect">

							</div>
							<div class="title">
								의료기기정보
							</div>
							<div class="link_text">
								바로가기
							</div>
						</a>	
					</li>		
				</ul>
			</div>

		</section>


		<?php include "footer.php"; ?>
	</body>
</html>