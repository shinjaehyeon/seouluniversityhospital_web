$(document).ready(function(){
	web4010(); // 오늘의 의학정보 - 랜덤으로 N의학정보 4개 가져오기
	// web4011(); // 오늘의 의학정보 - 마우스 오버시 이벤트 모션주기

	web4015(); // N의학정보 가져오기
	web4016(); // N의학정보 - 다음 페이지 클릭시 이벤트 설정
	web4017(); // N의학정보 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
	web4018(); // N의학정보 - 이전 페이지 클릭시 이벤트 설정
	web4019(); // N의학정보 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정

	web4022(); // N의학정보 검색버튼 클릭시 이벤트 설정하기
	web4023(); // N의학정보 검색필터 항목값 클릭시 이벤트 설정하기
});

// 오늘의 의학정보 - 랜덤으로 N의학정보 4개 가져오기
function web4010() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web4010"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$(".today_medical_information_box ul.list").empty();

					$.each(jsonObj.data, function(key, value){
						var suhnmi = value.suhnmi;
						var title_ko = cutString2(value.title_ko, 15);
						var title_en = cutString2(value.title_en, 22);
						var one_line_description = cutString2(value.one_line_description, 36);

						var text = '';
						text += '<li>';
						text += '	<a href="./web4100.php?pk='+suhnmi+'" class="setTopVirtualBox">';
						text += '		<div class="ko common">';
						text += '			'+title_ko;
						text += '		</div>';
						text += '		<div class="en common">';
						text += '			['+title_en+']';
						text += '		</div>';
						text += '		<div class="description">';
						text += '			'+one_line_description;
						text += '		</div>';
						text += '	</a>';
						text += '</li>';
						
						$(".today_medical_information_box ul.list").append(text);
					});
					

				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}




var web4015_index = 0;
var web4015_view_num = 10;
var web4015_page_reckoning = 1;
var web4015_search_word = '';
var web4015_search_filter = 0;

// N의학정보 가져오기
function web4015() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web4015",
			search_word:web4015_search_word,
			search_filter:web4015_search_filter,
			index:web4015_index,
			view_num:web4015_view_num,
			page_reckoning:web4015_page_reckoning
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("N의학정보 가져오기 data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$(".web4000.board_list ul").empty();

				if (result == "ok") {
					$.each(jsonObj.data, function(key, value){
						var suhnmi = value.suhnmi;
						var title_ko = cutString2(value.title_ko, 40);
						var title_en = cutString2(value.title_en, 30);
						var description = value.description;


						var text = '';
						text += '<li class="setTopVirtualBox">';
						text += '	<a href="./web4100.php?pk='+suhnmi+'" class="setTopVirtualBox">';
						text += '		<div class="title">';
						text += '			'+title_ko+' ['+title_en+']';
						text += '		</div>';
						text += '		<div class="description">';
						text += '			'+description;
						text += '		</div>';
						text += '	</a>';
						text += '</li>';


						$(".web4000.board_list ul").append(text);
					});



					if (web4015_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;
						var all_num = jsonObj.all_num;


						$(".web4000.all_board_num .right").text(all_num);

						// 이전 페이지 버튼 여부
						if (web4015_index == 0) {
							$(".web4000.board_paging_controller_box .best_prev").css({"display":"none"});
							$(".web4000.board_paging_controller_box .line").eq(0).css({"display":"none"});
							$(".web4000.board_paging_controller_box .prev").css({"display":"none"});
						} else {
							$(".web4000.board_paging_controller_box .best_prev").css({"display":"block"});
							$(".web4000.board_paging_controller_box .line").eq(0).css({"display":"block"});
							$(".web4000.board_paging_controller_box .prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".web4000.board_paging_controller_box .best_next").css({"display":"block"});
							$(".web4000.board_paging_controller_box .line").eq(1).css({"display":"block"});
							$(".web4000.board_paging_controller_box .next").css({"display":"block"});	
						} else {
							$(".web4000.board_paging_controller_box .best_next").css({"display":"none"});
							$(".web4000.board_paging_controller_box .line").eq(1).css({"display":"none"});
							$(".web4000.board_paging_controller_box .next").css({"display":"none"});
						}


						$(".web4000.board_paging_controller_box ul.page_num").empty();



						var first_page_index = web4015_index;
						for (var ii=0; ii<10; ii++) {
							if (first_page_index % 10 == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("web4015_index = "+web4015_index);
							console.log("page_number-1 = "+(page_number-1));
							if (web4015_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".web4000.board_paging_controller_box ul.page_num").append(text);
						}


						web4020(); // 페이지 번호 클릭시 이벤트 설정



						web4015_page_reckoning = 0;
					}

				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}


// N의학정보 - 다음 페이지 클릭시 이벤트 설정
function web4016() {
	$(".web4000.board_paging_controller_box .next").on("click", function(){
		web4015_page_reckoning = 1;

		var minimum_index = web4015_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + 10;
		web4015_index = next_first_index;
		web4015();
	});
}

// N의학정보 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function web4017() {
	$(".web4000.board_paging_controller_box .best_next").on("click", function(){
		if (isLoading == false) {
			web4021(); // N의학정보 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// N의학정보 - 이전 페이지 클릭시 이벤트 설정
function web4018() {
	$(".web4000.board_paging_controller_box .prev").on("click", function(){
		web4015_page_reckoning = 1;

		var minimum_index = web4015_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - 10;
		web4015_index = prev_first_index;
		web4015();
	});
}

// N의학정보 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function web4019() {
	$(".web4000.board_paging_controller_box .best_prev").on("click", function(){
		web4015_page_reckoning = 1;
		web4015_index = 0;
		web4015();
	});
}


// N의학정보 - 페이지 번호 클릭시 이벤트 설정
function web4020() {
	$(".web4000.board_paging_controller_box ul.page_num li").on("click", function(){
		if (isLoading == false) {	
			$(".web4000.board_paging_controller_box ul.page_num li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			web4015_index = selected_index;
			web4015();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// N의학정보 - 페이지 맨 마지막 번호 알아내기
function web4021() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web4021",
			search_word:web4015_search_word,
			search_filter:web4015_search_filter,
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					web4015_index = last_index;

					console.log("last_index = "+last_index);

					web4015_page_reckoning = 1;
					web4015();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// N의학정보 검색버튼 클릭시 이벤트 설정하기
function web4022() {
	$(".web4000.search_box .search_button").on("click", function(){
		var inputed_search_word = $.trim($("input[name=web4000search]").val());

		web4015_search_filter = 0;
		$(".web4000.search_filter_box ul li.active").removeClass("active");
		$(".web4000.search_filter_box ul li").eq(0).addClass("active");

		web4015_index = 0;
		web4015_search_word = inputed_search_word;
		web4015_page_reckoning = 1;

		web4015(); // N의학정보 가져오기
		
	});


	$("input[name=web4000search]").keypress(function(event){
		if (event.keyCode == 13) {
			var inputed_search_word = $.trim($("input[name=web4000search]").val());

			web4015_search_filter = 0;
			$(".web4000.search_filter_box ul li.active").removeClass("active");
			$(".web4000.search_filter_box ul li").eq(0).addClass("active");

			web4015_index = 0;
			web4015_search_word = inputed_search_word;
			web4015_page_reckoning = 1;

			web4015(); // N의학정보 가져오기
		}
	});

}

// N의학정보 검색필터 항목값 클릭시 이벤트 설정하기
function web4023() {
	$(".web4000.search_filter_box ul li").on("click", function(){
		$(".web4000.search_filter_box ul li.active").removeClass("active");
		$(this).addClass("active");

		web4015_index = 0;


		var filter_value = Number($(this).attr("data-value"));

		web4015_search_filter = filter_value;
		web4015_search_word = '';
		$("input[name=web4000search]").val('');

		web4015_page_reckoning = 1;
		web4015(); // N의학정보 가져오기
	});
}













// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}



function cutString2(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}