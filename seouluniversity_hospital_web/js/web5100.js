var currentBoardType = null;

$(document).ready(function(){
	web5101(); // 페이지에 처음 들어왔을 때 설정되어 있는 게시판 타입 코드를 전역변수에 저장하기


	web5110(); // 게시판 분류 내용이 바뀔때마다 이벤트 설정하기
	web5111(); // 내용 종류 리스트 가져오기


	web5120(); // 등록하기 버튼 클릭시 이벤트 설정하기
});

// 페이지에 처음 들어왔을 때 설정되어 있는 게시판 타입 코드를 전역변수에 저장하기
function web5101() {
	if (Number($("input[name=first_tab_index]").val()) == 0) {
		currentBoardType = 800701;
	} else {
		currentBoardType = 800702;
	}
	console.log("페이지에 처음currentBoardType = "+currentBoardType);
}

// 게시판 분류 내용이 바뀔때마다 이벤트 설정하기
function web5110() {
	$("select[name=board_type]").on("change", function(){
		if (isLoading == false) {
			currentBoardType = Number($(this).val());

			console.log("currentBoardType = "+currentBoardType);
			web5111(); // 내용 종류 리스트 가져오기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 내용 종류 리스트 가져오기
function web5111() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web5111",
			board_type:currentBoardType
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;	
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$("select[name=content_type]").empty();

					var i = 0;
					$.each(jsonObj.data, function(key, value){
						var code_number = value.code_number;
						var description = value.description;

						var text = '<option value="'+code_number+'">'+description+'</option>';
						if (i == 0) {
							text = '<option value="'+code_number+'" selected="selected">'+description+'</option>';
						}

						$("select[name=content_type]").append(text);

						i++;
					});

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 등록하기 버튼 클릭시 이벤트 설정하기
function web5120() {
	$(".web5100.form_box form .submit_button").on("click", function(){
		// 제목 체크
		var title = $.trim($("input[name=title]").val())+"";
		if (title == "") {
			alert("제목을 입력해주세요.");
			return 0;
		}

		// 내용 체크
		var content = $("textarea[name=content]").val()+"";
		if (content == "") {
			alert("내용을 입력해주세요.");
			return 0;
		}		



		$("form[name=web5100form]").submit();
	});
}	







// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}