

$(document).ready(function(){
	web3010(); // 노선 표 높이 설정하기
});

// 노선 표 높이 설정하기
function web3010() {
	$(".route_group .list li div.right").each(function(){
		var height = $(this).height();
		var paddingTop = $(this).css("padding-top");
		paddingTop = Number(paddingTop.split("px")[0]);
		var paddingBottom = $(this).css("padding-bottom");
		paddingBottom = Number(paddingBottom.split("px")[0]);

		var realHeight = height + paddingTop + paddingBottom;

		$(this).siblings().css({"height":realHeight+"px", "line-height":realHeight+"px"});
	});
}