var global_member_pk = 0;

var crh_index = 0;
var crh_view_num = 10;
var crh_page_reckoning = 1;






$(document).ready(function(){
	getMemberPk();


	web1501(); // 탭 버튼 클릭 시 이벤트 설정하기

	web1510(); // 진료예약 내역 가져오기
	web1511(); // 진료예약 내역 페이징 - 다음 페이지 클릭시 이벤트 설정
	web1512(); // 진료예약 내역 페이징 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
	web1513(); // 진료예약 내역 페이징 - 이전 페이지 클릭시 이벤트 설정
	web1514(); // 진료예약 내역 페이징 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정



	web1530(); // 회원 상세정보 가져오기
	web1531(); // 회원정보 수정하기 버튼 클릭시 이벤트 설정하기

});


function getMemberPk() {
	global_member_pk = Number($("input[name=member_pk]").val());
}


// 탭 버튼 클릭 시 이벤트 설정하기
function web1501() {
	$(".tab_button_box.web1500 ul li").on("click", function(){
		var index = $(this).index();
		
		$(".tab_button_box.web1500 ul li.active").removeClass("active");
		$(this).addClass("active");

		$(".tab_line_box.web1500 .line").stop().animate({"left":(index*200)+"px"}, 400, function(){

		});

		$(".tabcontent").removeClass("show");
		$(".tab"+(index+1)+"_content").addClass("show");
	});
}

// 진료예약 내역 가져오기
function web1510() {
	var member_pk = Number($("input[name=member_pk]").val());

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web1510",
			member:member_pk,
			index:crh_index,
			view_num:crh_view_num,
			page_reckoning:crh_page_reckoning
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("web1510 data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$(".web1500.tab_content_box .tab1_content .crhlist_box ul li").not(".title").remove();

				if (result == "ok") {
					

					$.each(jsonObj.data, function(key, value){
						var suhcrh = value.suhcrh;
						var hope_datetime = value.hope_datetime;
						var department = value.department;
						var department_string = value.department_string;
						var doctor = value.doctor;
						var doctor_name = value.doctor_name;
						var status = value.status;


						var status_content = '';
						if (status == 500201) {
							// 예약중
							status_content += '<span class="reservation">';
							status_content += '		예약됨';
							status_content += '</span>';
							status_content += '<span class="empty_box" style="width:10px;">';
							status_content += '		';
							status_content += '</span>';
							status_content += '<span class="cancel" data-pk="'+suhcrh+'">';
							status_content += ' 	취소하기';
							status_content += '</span>';
						} else if (status == 500202) {
							// 진료완료
							status_content += '<span class="blur">';
							status_content += ' 	진료완료';
							status_content += '</span>';
						} else if (status == 500203) {
							// 취소됨
							status_content += '<span class="blur">';
							status_content += ' 	취소됨';
							status_content += '</span>';
						} else if (status == 500204) {
							// 미방문
							status_content += '<span class="blur">';
							status_content += ' 	미방문';
							status_content += '</span>';
						}

						

						var text = '';
						text += '<li class="clearFix">';
						text += '	<div class="col">';
						text += '		'+hope_datetime;
						text += '	</div>';
						text += '	<div class="col">';
						text += '		'+department_string;
						text += '	</div>';
						text += '	<div class="col">';
						text += '		'+doctor_name;
						text += '	</div>';
						text += '	<div class="col">';
						text += '		'+status_content;
						text += '	</div>';
						text += '</li>';

						$(".web1500.tab_content_box .tab1_content .crhlist_box ul").append(text);
					});


					if (crh_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (crh_index == 0) {
							$(".allboard_paging_controller_box .best_prev").css({"display":"none"});
							$(".allboard_paging_controller_box .prev").css({"display":"none"});
							$(".allboard_paging_controller_box .line.prev").css({"display":"none"});
						} else {
							$(".allboard_paging_controller_box .best_prev").css({"display":"block"});
							$(".allboard_paging_controller_box .prev").css({"display":"block"});
							$(".allboard_paging_controller_box .line.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".allboard_paging_controller_box .best_next").css({"display":"block"});
							$(".allboard_paging_controller_box .next").css({"display":"block"});
							$(".allboard_paging_controller_box .line.next").css({"display":"block"});	
						} else {
							$(".allboard_paging_controller_box .best_next").css({"display":"none"});
							$(".allboard_paging_controller_box .next").css({"display":"none"});
							$(".allboard_paging_controller_box .line.next").css({"display":"none"});
						}


						$(".allboard_paging_controller_box ul.page_num").empty();



						var first_page_index = crh_index;
						for (var ii=0; ii<10; ii++) {
							if (first_page_index % 10 == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("crh_index = "+crh_index);
							console.log("page_number-1 = "+(page_number-1));
							if (crh_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".allboard_paging_controller_box ul.page_num").append(text);
						}


						web1515(); // 진료예약 내역 페이징 - 페이지 번호 클릭시 이벤트 설정



						crh_page_reckoning = 0;
					}
					web1517(); // 진료예약 취소하기 버튼 클릭시 이벤트 설정
				} else {
					$(".web1500.tab_content_box .tab1_content .crhlist_box ul").append('<li class="not_result_text">진료예약내역이 없습니다.</li>');

					$(".allboard_paging_controller_box ul.page_num").empty();

					$(".allboard_paging_controller_box .best_prev").css({"display":"none"});
					$(".allboard_paging_controller_box .prev").css({"display":"none"});
					$(".allboard_paging_controller_box .line.prev").css({"display":"none"});
					$(".allboard_paging_controller_box .best_next").css({"display":"none"});
					$(".allboard_paging_controller_box .next").css({"display":"none"});
					$(".allboard_paging_controller_box .line.next").css({"display":"none"});
				}
			}
		},
		fail: function() {
			
		}
	});
}

// 진료예약 내역 페이징 - 다음 페이지 클릭시 이벤트 설정
function web1511() {
	$(".allboard_paging_controller_box .next").on("click", function(){
		crh_page_reckoning = 1;

		var minimum_index = crh_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + 10;
		crh_index = next_first_index;
		web1510();
	});
}

// 진료예약 내역 페이징 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function web1512() {
	$(".allboard_paging_controller_box .best_next").on("click", function(){
		if (isLoading == false) {
			web1516(); // 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 진료예약 내역 페이징 - 이전 페이지 클릭시 이벤트 설정
function web1513() {
	$(".allboard_paging_controller_box .prev").on("click", function(){
		crh_page_reckoning = 1;

		var minimum_index = crh_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - 10;
		crh_index = prev_first_index;
		web1510();
	});
}

// 진료예약 내역 페이징 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function web1514() {
	$(".allboard_paging_controller_box .best_prev").on("click", function(){
		crh_page_reckoning = 1;

		crh_index = 0;
		web1510();
	});
}


// 진료예약 내역 페이징 - 페이지 번호 클릭시 이벤트 설정
function web1515() {
	$(".allboard_paging_controller_box ul.page_num li").on("click", function(){
		$(".allboard_paging_controller_box ul.page_num li.active").removeClass("active");
		$(this).addClass("active");


		var selected_index = Number($(this).attr("data-index"));

		crh_index = selected_index;
		web1510();
	});
}

// 진료예약 내역 페이징 - 페이지 맨 마지막 번호 알아내기
function web1516() {
	var member_pk = Number($("input[name=member_pk]").val());

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web1516",
			member:member_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					crh_index = last_index;

					console.log("last_index = "+last_index);

					crh_page_reckoning = 1;
					web1510();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 진료예약 취소하기 버튼 클릭시 이벤트 설정
function web1517() {
	$(".web1500.tab_content_box .tab1_content .crhlist_box ul li .cancel").on("click", function(){
		if (isLoading == false) {
			var datetime = $.trim($(this).parent().parent().children(".col").eq(0).text());
			

			var year = Number(dateDivide(datetime, 'yyyy'));
			var month = Number(dateDivide(datetime, 'm')) - 1;
			var date = Number(dateDivide(datetime, 'd'));
			var hour = Number(dateDivide(datetime, 'h'));
			var minute = Number(dateDivide(datetime, 'i'));
			var second = Number(dateDivide(datetime, 's'));

			var today = new Date();
			var hopeDatetime = new Date(year, month, date, hour, minute, second);




			if (today.getTime() < hopeDatetime.getTime()) {
				if (confirm("정말 취소하시겠습니까?")) {
					var pk = Number($(this).attr("data-pk"));
					web1520(pk, $(this).parent());
				}
			} else {
				alert("진료 미방문 변환 대기중입니다. 취소는 불가능합니다.");
			}

		} else {
			alert("작업 중입니다.");
		}
	});
}

// 진료예약 취소 요청 보내기
function web1520(pk, object) {
	var member_pk = Number($("input[name=member_pk]").val());

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web1520",
			member:member_pk,
			crh_pk:pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			// console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$(object).empty();
					$(object).html('<span class="blur">취소됨</span>');
				} else {
					alert("진료예약 취소중 오류가 발생하였습니다. 다시 시도해주세요.");
				}
			}
		},
		fail: function() {
			
		}
	});
}






// 회원 상세정보 가져오기
function web1530() {
	console.log("global_member_pk = "+global_member_pk);

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web1530",
			member:global_member_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("web1530 data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var name = jsonObj.name;
					var gender = jsonObj.gender;
					var theMobileCompany = jsonObj.theMobileCompany;
					var phoneNumber = jsonObj.phoneNumber;
					var email = jsonObj.email;
					var birthday = jsonObj.birthday;
					var theBloodType = jsonObj.theBloodType;
					var addr = jsonObj.addr;

					$("select[name=gender] option[value="+gender+"]").prop("selected", true);
					$("select[name=mobileCompany] option[value="+theMobileCompany+"]").prop("selected", true);
					$("input[name=phoneNumber]").val(phoneNumber);
					$("input[name=birthday]").val(birthday.split(" ")[0]);
					$("select[name=bloodtype] option[value="+theBloodType+"]").prop("selected", true);
					$("textarea[name=addr]").html(addr);
				} else {
					


				}
			}
		},
		fail: function() {
			
		}
	});
}

// 회원정보 수정하기 버튼 클릭시 이벤트 설정하기
function web1531() {
	$(".edit_button").on("click", function(){
		var gender = Number($("select[name=gender]").val());
		if (gender == 900900) {
			alert("성별은 미지정으로 수정할 수 없습니다.");
			return false;
		}

		var mobileCompany = Number($("select[name=mobileCompany]").val());
		if (mobileCompany == 900900) {
			alert("휴대폰통신사는 미지정으로 수정할 수 없습니다.");
			return false;
		}

		var phoneNumber = $("input[name=phoneNumber]").val();
		if (phoneNumber == '') {
			alert("휴대폰 번호는 공백으로 수정할 수 없습니다.");
			return false;
		}

		var birthday = $("input[name=birthday]").val();
		if (birthday == '') {
			alert("생일 값을 정확히 입력주세요.");
			return false;
		}

		var bloodtype = Number($("select[name=bloodtype]").val());
		if (bloodtype == 900900) {
			alert("혈액형은 미지정으로 수정할 수 없습니다.");
			return false;
		}

		var addr = $("textarea[name=addr]").val();

		web1532(gender, mobileCompany, phoneNumber, birthday, bloodtype, addr);

	});
}

// 회원정보 수정하기
function web1532(gender, mobileCompany, phoneNumber, birthday, bloodtype, addr) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web1532",
			member:global_member_pk,
			gender:gender,
			mobileCompany:mobileCompany,
			phoneNumber:phoneNumber,
			birthday:birthday,
			bloodtype:bloodtype,
			addr:addr
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("web1530 data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					alert("회원정보가 수정되었습니다.");
					location.href = "./web1500.php";

				} else {
					
					alert("회원정보 수정중에 오류가 발생하였습니다.");

				}
			}
		},
		fail: function() {
			
		}
	});
}






// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}



// Y-m-d H:i:s 형태의 데이터날짜를 년,월,일,분,시,초,요일로 뽑아내기
function dateDivide(datetimes, types) {
	var finalDate = datetimes instanceof Date ? changeDateForm(datetimes) : datetimes;

    var day = ['일','월','화','수','목','금','토'];

    var temp_date = finalDate.split(' ');
    var only_date = temp_date[0]; // ex) 2018-05-07
    var only_time = temp_date[1]; // ex) 13:11:30

    var dates = only_date.split('-'); // [0] = 2018, [1] = 06, [2] = 13
    var times = only_time.split(':'); // [0] = 15, [1] = 23, [2] = 35

    var return_str = '';

    switch (types) {
        case 'y':
        case 'yy':
            return_str = dates[0].substring(2, 4);
            break;
        case 'Y':
        case 'YY':
        case 'yyyy':
            return_str = dates[0];
            break;
        case 'm':
            return_str = Number(dates[1]); 
            break;
        case 'M':
        case 'MM':
        case 'mm':
            return_str = dates[1];
            break;
        case 'd':
            return_str = Number(dates[2]);
            break;
        case 'D':
        case 'DD':
        case 'dd':
            return_str = dates[2];
            break;
        case 'h':
            return_str = Number(times[0]);
            break;
        case 'hh':
        case 'H':
        case 'HH':
            return_str = times[0];
            break;
        case 'i':
            return_str = Number(times[1]);
            break;
        case 'I':
        case 'II':
        case 'ii':
            return_str = times[1];
            break;
        case 's':
            return_str = Number(times[2]);
            break;
        case 'S':
        case 'SS':
        case 'ss':
            return_str = times[2];
            break;
        case 'date':
            return_str = only_date;
            break;
        case 'time':
            return_str = only_time;
            break;
        case 'day':
            return_str = day[new Date(dates).getDay()];
            break;
        case 'day_int':
            return_str = Number(new Date(dates).getDay());
            break;
        default:

            break;
    }

    return return_str;
}
