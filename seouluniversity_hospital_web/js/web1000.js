var global_current_slide_index = 0;
var global_slide_num = 0;
var global_one_set_width = 1200;

var global_interval_obj = null;
var global_is_stopping = false;

var global_slide_duration = 5000;





$(document).ready(function(){
	web1001(); // 메인 비주얼 큰 메뉴 슬라이드 효과 시작하기
	web1002();

	web1010(); // 메인에서 병원소식 탭 메뉴 이벤트 주기
	web1015();
});

// 메인 비주얼 큰 메뉴 슬라이드 효과 시작하기
function web1001() {
	var one_set_width = $(".main_major_menu_box ul").eq(0).width();
	global_one_set_width = one_set_width;
	var slide_num = $(".main_major_menu_box ul").length;
	global_slide_num = slide_num;

	console.log("slide_num = "+slide_num);

	for (var i=0; i<slide_num; i++) {
		$(".main_major_menu_box ul").eq(i).css({"left":(i*one_set_width)+"px"});
			
		if (i == 0) {
			$(".main_major_menu_slide_controller_box ul").append('<li class="active"></li>');
		} else {
			$(".main_major_menu_slide_controller_box ul").append('<li></li>');
		}

	}

	global_interval_obj = setInterval(web1002, global_slide_duration);

	web1003(); // 메인 비주얼 큰 메뉴 슬라이드컨트롤러 버튼 클릭시 이벤트 설정하기
}


// 메인 비주얼 큰 메뉴 슬라이드 하기
function web1002() {

	
	// 모든 슬라이드 인덱스의 left 값 조정
	if (global_current_slide_index == 0) {
		$(".main_major_menu_box ul").eq(0).css({"left":"0px"});
		$(".main_major_menu_box ul").eq(1).css({"left":"1200px"});
	} else if (global_current_slide_index == 1) {
		$(".main_major_menu_box ul").eq(0).css({"left":"-1200px"});
		$(".main_major_menu_box ul").eq(1).css({"left":"0px"});
	}

	// 슬라이드컨트롤러 조정
	$(".main_major_menu_slide_controller_box ul li.active").removeClass("active");
	$(".main_major_menu_slide_controller_box ul li").eq(global_current_slide_index).addClass("active");


	global_current_slide_index++;
	if (global_current_slide_index >= global_slide_num) {
		global_current_slide_index = 0;
	}

}

// 메인 비주얼 큰 메뉴 슬라이드컨트롤러 버튼 클릭시 이벤트 설정하기
function web1003() {
	$(".main_major_menu_slide_controller_box ul li").on("click", function(){
		clearInterval(global_interval_obj);
		$(".main_major_menu_slide_controller_box ul li.active").removeClass("active");
		$(this).addClass("active");

		var index = $(this).index();
		global_current_slide_index = index;
		web1002();
		
		if (global_is_stopping == false) {
			global_interval_obj = setInterval(web1002, global_slide_duration);
		}

	});

	// 재생/정지 버튼 이벤트 주기
	$(".stop_play_button img").on("click", function(){
		if (global_is_stopping == false) {
			global_is_stopping = true;
			clearInterval(global_interval_obj);
			$(this).attr("src", "./images/play_icon.png");
		} else {
			global_is_stopping = false;
			global_interval_obj = setInterval(web1002, global_slide_duration);
			$(this).attr("src", "./images/stop_icon.png");
		}
	});
}


// 메인에서 병원소식 탭 메뉴 이벤트 주기
function web1010() {
	// 병원뉴스 ~ 강좌안내 메뉴 탭 버튼 클릭시 이벤트
	$("section.hospital_board_box ul.board_tab li").on("click", function() {
		var menuIndex = $(this).attr("menu-index");

		$("section.hospital_board_box ul.board_tab li.active").removeClass("active");
		$(this).addClass("active");

		
		$(".hospital_board_box .board_tab_position.menu_index_0").removeClass("menu_index_0");
		$(".hospital_board_box .board_tab_position.menu_index_1").removeClass("menu_index_1");
		$(".hospital_board_box .board_tab_position.menu_index_2").removeClass("menu_index_2");
		$(".hospital_board_box .board_tab_position.menu_index_3").removeClass("menu_index_3");
		$(".hospital_board_box .board_tab_position").addClass("menu_index_"+menuIndex);

		$(".hospital_board_box .board_box .board.show").removeClass("show");
		$(".hospital_board_box .board_box .tab_index_"+menuIndex+"_board").addClass("show");
	});
}


// 병원뉴스 최신 10개 리스트 가져오기
function web1015() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web1015"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
						
					$("ul.tab_index_0_board li").not(".board_title").remove();

					$.each(jsonObj.data, function(key, value){
						var suhnews_pk = value.suhnews_pk;
						var admin = value.admin;
						var admin_name = value.admin_name;
						var title = value.title;
						var datetime = value.datetime;
						var attach_file_flag = value.attach_file_flag;
						var view_index = value.view_index;

						var attachClass = '';
						if (attach_file_flag == 1) {
							attachClass = 'attach_file';
						}

						var text = '';
						text += '<li class="clearFix">';
						text += '	<div class="center">';
						text += '		'+suhnews_pk;
						text += '	</div>';
						text += '	<div class="padding_left_30">';
						text += '		<a href="./web6100.php?pk='+suhnews_pk+'">'+title+'</a>';
						text += '	</div>';
						text += '	<div class="center">';
						text += '		'+datetime.split(" ")[0];
						text += '	</div>';
						text += '	<div class="'+attachClass+'">';
						text += '		';
						text += '	</div>';
						text += '	<div class="center">';
						text += '		'+view_index;
						text += '	</div>';
						text += '</li>';

						$("ul.tab_index_0_board").append(text);
					});

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}







// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}