var checked_name = null;
var checked_gender = 800501;
var checked_themobilecompany = 700101;
var checked_phone_first = '010';
var checked_phone_second = null;
var checked_phone_third = null;

var isOverlapChecked = false;
var checked_email_before = null;
var checked_email_after1 = '@naver.com';
var checked_email_after2 = null;
var checked_email_full = null;

var checked_password = null;
var checked_password_check = null;
var checked_birthday_year = null;
var checked_birthday_month = null;
var checked_birthday_date = null;
var checked_blood_type = 600101;

var checked_addr_post = null;
var checked_addr_basic = null;
var checked_addr_detail = null;

$(document).ready(function(){
	checkJoinForm(); // 회원가입 입력 폼 유효성 체크하기
});

// 회원가입 입력 폼 유효성 체크하기
function checkJoinForm() {
	checkName();
	checkGender();
	checkTheMobileCompany();
	checkPhone();

	checkEmail();
	checkPassword();
	checkBirthday();
	checkBloodType();

	setJoinButtonEvent();
	setOverlapCheckButtonEvent();
	// setPostFindButtonEvent();
}

function checkName() {
	$("input[name=name]").on("keyup", function(){
		var inputed_value = $(this).val();

		if (inputed_value == '') {
			checked_name = null;
			$(this).addClass("error");
			$(this).siblings(".error_text").css({"display":"block"});
		} else {
			checked_name = inputed_value;
			$(this).removeClass("error");
			$(this).siblings(".error_text").css({"display":"none"});
		}
	});
}

function checkGender() {
	$("select[name=gender]").on("change", function(){
		var value = $(this).val();
		checked_gender = value;
	});
}

function checkTheMobileCompany() {
	$("select[name=theMobileCompany]").on("change", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		checked_themobilecompany = inputed_value;
	});
}

function checkPhone() {
	$("select[name=phoneFirst]").on("change", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		checked_phone_first = inputed_value+"";
	});

	$("input[name=phoneSecond]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_phone_second = null;
			$(this).addClass("error");
			$(this).siblings(".error_text").css({"display":"block"});
		} else {
			checked_phone_second = inputed_value;
			$(this).removeClass("error");
			$(this).siblings(".error_text").css({"display":"none"});
		}
	});

	$("input[name=phoneThird]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_phone_third = null;
			$(this).addClass("error");
			$(this).siblings(".error_text").css({"display":"block"});
		} else {
			checked_phone_third = inputed_value;
			$(this).removeClass("error");
			$(this).siblings(".error_text").css({"display":"none"});
		}
	});
}

function checkEmail() {
	$("input[name=emailBefore]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			isOverlapChecked = false;
			checked_email_before = null;

			$(this).addClass("error");
			$(this).siblings(".error_text").css({"display":"none"});
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
		} else {
			checked_email_before = inputed_value;
			isOverlapChecked = false;

			$(this).siblings().removeClass("error");
			$(this).removeClass("error");
			$(this).siblings(".error_text").css({"display":"none"});

			if (isOverlapChecked == false) {
				$(this).siblings(".error_text").eq(1).css({"display":"block"});
			}
		}
	});

	$("select[name=emailAfter1]").on("change", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		checked_email_after1 = inputed_value+"";

		$(this).siblings(".error_text").css({"display":"none"});
		$(this).siblings(".error_text").eq(1).css({"display":"block"});
		isOverlapChecked = false;

		if (inputed_value == '0') {
			checked_email_after1 = null;
			$("input[name=emailAfter2]").css({"display":"block"});
		} else {
			checked_email_after2 = null;
			$("input[name=emailAfter2]").css({"display":"none"});
		}
	});

	$("input[name=emailAfter2]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			isOverlapChecked = false;
			checked_email_after2 = null;
			$(this).addClass("error");
			$(this).siblings(".error_text").css({"display":"none"});
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
		} else {
			isOverlapChecked = false;
			checked_email_after2 = inputed_value;
			$(this).removeClass("error");
			$(this).siblings().removeClass("error");

			$(this).siblings(".error_text").css({"display":"none"});

			if (isOverlapChecked == false) {
				$(this).siblings(".error_text").eq(1).css({"display":"block"});
			}
		}
	});
}

function checkPassword() {
	$("input[name=pw]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_password = null;
			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_password = inputed_value;
			$(this).removeClass("error");
			$("input[name=pwcheck]").removeClass("error");
			$(this).siblings(".error_text").css({"display":"none"});
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_password != checked_password_check) {
				$(this).siblings(".error_text").eq(1).css({"display":"block"});
			} else {
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
				$("input[name=pwcheck]").siblings(".error_text").eq(1).css({"display":"none"});
			}
		}
	});

	$("input[name=pwcheck]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_password_check = null;
			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_password_check = inputed_value;
			$(this).removeClass("error");
			$("input[name=pw]").removeClass("error");
			$(this).siblings(".error_text").css({"display":"none"});
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_password != checked_password_check) {
				$(this).siblings(".error_text").eq(1).css({"display":"block"});
			} else {
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
				$("input[name=pw]").siblings(".error_text").eq(1).css({"display":"none"});
			}
		}
	});
}

function checkBirthday() {
	$("input[name=birthdayYear]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_birthday_year = null;

			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_birthday_year = inputed_value;
			
			$(this).removeClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_birthday_year == null || checked_birthday_month == null || checked_birthday_date == null) {
				$(this).addClass("error");
				$(this).siblings(".error_text").eq(0).css({"display":"block"});
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
			} else {
				var fullDate = checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date);
				if (isValidDate(fullDate)) {
					console.log("날짜가 맞습니다.");
					$("input[name=birthdayYear]").removeClass("error");
					$("input[name=birthdayMonth]").removeClass("error");
					$("input[name=birthdayDate]").removeClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"none"});

					console.log("remoevClass 실행됨");
				} else {
					$("input[name=birthdayYear]").addClass("error");
					$("input[name=birthdayMonth]").addClass("error");
					$("input[name=birthdayDate]").addClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"block"});
				}
			}	
		}
	});
	$("input[name=birthdayYear]").on("change", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_birthday_year = null;

			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_birthday_year = inputed_value;
			
			$(this).removeClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_birthday_year == null || checked_birthday_month == null || checked_birthday_date == null) {
				$(this).addClass("error");
				$(this).siblings(".error_text").eq(0).css({"display":"block"});
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
			} else {
				var fullDate = checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date);
				if (isValidDate(fullDate)) {
					console.log("날짜가 맞습니다.");
					$("input[name=birthdayYear]").removeClass("error");
					$("input[name=birthdayMonth]").removeClass("error");
					$("input[name=birthdayDate]").removeClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"none"});

					console.log("remoevClass 실행됨");
				} else {
					$("input[name=birthdayYear]").addClass("error");
					$("input[name=birthdayMonth]").addClass("error");
					$("input[name=birthdayDate]").addClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"block"});
				}
			}	
		}
	});

	$("input[name=birthdayMonth]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_birthday_month = null;

			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_birthday_month = inputed_value;
			
			$(this).removeClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_birthday_year == null || checked_birthday_month == null || checked_birthday_date == null) {
				$(this).addClass("error");
				$(this).siblings(".error_text").eq(0).css({"display":"block"});
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
			} else {
				var fullDate = checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date);
				if (isValidDate(fullDate)) {
					console.log("날짜가 맞습니다.");
					$("input[name=birthdayYear]").removeClass("error");
					$("input[name=birthdayMonth]").removeClass("error");
					$("input[name=birthdayDate]").removeClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"none"});

					console.log("remoevClass 실행됨");
				} else {
					$("input[name=birthdayYear]").addClass("error");
					$("input[name=birthdayMonth]").addClass("error");
					$("input[name=birthdayDate]").addClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"block"});
				}
			}	
		}
	});
	$("input[name=birthdayMonth]").on("change", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_birthday_month = null;

			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_birthday_month = inputed_value;
			
			$(this).removeClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_birthday_year == null || checked_birthday_month == null || checked_birthday_date == null) {
				$(this).addClass("error");
				$(this).siblings(".error_text").eq(0).css({"display":"block"});
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
			} else {
				var fullDate = checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date);
				if (isValidDate(fullDate)) {
					console.log("날짜가 맞습니다.");
					$("input[name=birthdayYear]").removeClass("error");
					$("input[name=birthdayMonth]").removeClass("error");
					$("input[name=birthdayDate]").removeClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"none"});

					console.log("remoevClass 실행됨");
				} else {
					$("input[name=birthdayYear]").addClass("error");
					$("input[name=birthdayMonth]").addClass("error");
					$("input[name=birthdayDate]").addClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"block"});
				}
			}		
		}
	});

	$("input[name=birthdayDate]").on("keyup", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_birthday_date = null;

			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_birthday_date = inputed_value;
			
			$(this).removeClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_birthday_year == null || checked_birthday_month == null || checked_birthday_date == null) {
				$(this).addClass("error");
				$(this).siblings(".error_text").eq(0).css({"display":"block"});
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
			} else {
				var fullDate = checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date);
				if (isValidDate(fullDate)) {
					console.log("날짜가 맞습니다.");
					$("input[name=birthdayYear]").removeClass("error");
					$("input[name=birthdayMonth]").removeClass("error");
					$("input[name=birthdayDate]").removeClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"none"});

					console.log("remoevClass 실행됨");
				} else {
					$("input[name=birthdayYear]").addClass("error");
					$("input[name=birthdayMonth]").addClass("error");
					$("input[name=birthdayDate]").addClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"block"});
				}
			}	
		}
	});
	$("input[name=birthdayDate]").on("change", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		if (inputed_value == '') {
			checked_birthday_date = null;

			$(this).addClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"block"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});
		} else {
			checked_birthday_date = inputed_value;
			
			$(this).removeClass("error");
			$(this).siblings(".error_text").eq(0).css({"display":"none"});
			$(this).siblings(".error_text").eq(1).css({"display":"none"});

			if (checked_birthday_year == null || checked_birthday_month == null || checked_birthday_date == null) {
				$(this).addClass("error");
				$(this).siblings(".error_text").eq(0).css({"display":"block"});
				$(this).siblings(".error_text").eq(1).css({"display":"none"});
			} else {
				var fullDate = checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date);
				if (isValidDate(fullDate)) {
					console.log("날짜가 맞습니다.");
					$("input[name=birthdayYear]").removeClass("error");
					$("input[name=birthdayMonth]").removeClass("error");
					$("input[name=birthdayDate]").removeClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"none"});

					console.log("remoevClass 실행됨");
				} else {
					$("input[name=birthdayYear]").addClass("error");
					$("input[name=birthdayMonth]").addClass("error");
					$("input[name=birthdayDate]").addClass("error");

					$(this).siblings(".error_text").eq(0).css({"display":"none"});
					$(this).siblings(".error_text").eq(1).css({"display":"block"});
				}
			}	
		}
	});
}

function isValidDate(param) {
    try {
        param = param.replace(/-/g,'');

        // 자리수가 맞지않을때
        if( isNaN(param) || param.length!=8 ) {
            return false;
        }
         
        var year = Number(param.substring(0, 4));
        var month = Number(param.substring(4, 6));
        var day = Number(param.substring(6, 8));

        var dd = day / 0;

        if( month<1 || month>12 ) {
            return false;
        }
         
        var maxDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        var maxDay = maxDaysInMonth[month-1];
         
        // 윤년 체크
        if( month==2 && ( year%4==0 && year%100!=0 || year%400==0 ) ) {
            maxDay = 29;
        }
         
        if( day<=0 || day>maxDay ) {
            return false;
        }
        return true;

    } catch (err) {
        return false;
    }                       
}

function checkBloodType() {
	$("input[name=bloodType]").on("change", function(){
		var inputed_value = $(this).val();
		console.log("inputed_value = "+inputed_value);

		checked_blood_type = inputed_value;
	});
}

function setJoinButtonEvent() {
	$("form.form_box .submit_button").on("click", function(){
		if (isLoading == false) {
			if (theFormAllCheck()) {
				web1220();
			} 
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청
function web1220() {
	var fullPhone = checked_phone_first+"-"+checked_phone_second+"-"+checked_phone_third;
	var fullBirthday = checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date);
	var fullAddr = "("+checked_addr_post+") "+checked_addr_basic+", "+checked_addr_detail;
	if (checked_addr_post == null || checked_addr_post == '') {
		fullAddr = '';
	}

	console.log("checked_phone_first = "+checked_phone_first);
	console.log("fullPhone = "+fullPhone);
	console.log("fullBirthday = "+fullBirthday);
	console.log("fullAddr = "+fullAddr);
	console.log("checkedBloodTypeNumber = "+checked_blood_type);

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web1220",
			checkedName:checked_name,
			checkedGender:checked_gender,
			checkedMobileCompanyNumber:checked_themobilecompany,
			checkedPhoneNumber:fullPhone,
			checkedEmail:checked_email_full,
			checkedPassword:checked_password,
			checkedBirthday:fullBirthday,
			checkedBloodTypeNumber:checked_blood_type,
			checkedAddress:fullAddr
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("web1210 data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					alert("회원가입이 완료되었습니다.");
					location.href = "./web1100.php";
				} else {
					alert("회원가입중 오류가 발생하였습니다. 다시 시도해 주세요.");	
				}
			}
		},
		fail: function() {
			
		}
	});
}

function setOverlapCheckButtonEvent() {
	$("#overlap_check_button").on("click", function(){
		if (isLoading == false) {
			// 이메일 체크
			if (checked_email_before == null) {
				console.log("aaa1");
				alert("이메일을 입력해주세요.");
				return false;
			}

			if (checked_email_after1 == null && checked_email_after2 == null) {
				console.log("aaa2");
				alert("이메일을 입력해주세요.");
				return false;	
			}


			var fullEmail = checked_email_before + checked_email_after1;
			if (checked_email_after1 == null) {
				fullEmail = checked_email_before + '@' + checked_email_after2.replace('@', '');
			}

			// 이메일 유효성 체크
			if (!inValidEmail2(fullEmail)) {
				alert("이메일 형식이 유효하지 않습니다. 다시 입력해주세요.");
				$('input[name=emailBefore]').addClass("error");
				$('input[name=emailAfter1]').addClass("error");
				$('input[name=emailAfter2]').addClass("error");
				$('input[name=emailBefore]').siblings(".error_text").css({"display":"none"});
				$('input[name=emailBefore]').siblings(".error_text").eq(3).css({"display":"block"});
				return false;
			} 


			// 서버에 이메일 중복체크 요청
			$.ajax({
				type: "POST",
				url: "./outlet.php",
				data:{
					act:"web1210",
					checkedEmail:fullEmail
				},
				timeout: 2000,
				beforeSend: function() {
					isLoading = true;
				},
				complete: function() {
					isLoading = false;
				},
				success: function(data) {
					console.log("web1210 data = "+data);

					if (isJSON2(data)) {
						var jsonObj = $.parseJSON(data);
						var emailUse = jsonObj.emailUse;

						if (emailUse == "ok") {
							alert("이용 가능한 이메일 입니다.");
							$("input[name=emailAfter2]").siblings(".error_text").css({"display":"none"});

							isOverlapChecked = true;
						} else {
							alert("이미 사용중인 이메일 입니다. 다른 이메일을 입력해주세요.");
							isOverlapChecked = false;
						}
					}
				},
				fail: function() {
					
				}
			});
		} else {
			alert("요청 처리중입니다.");
		}
	});
}

function theFormAllCheck() {
	// 서비스 이용약관 동의여부 체크
	if (!$("input[id=service_t]").is(":checked")) {
		alert("서비스 이용약관에 동의해주세요.");
		$('.web1200.terms_box ul').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		return false;
	}

	// 개인정보취급방침 동의여부 체크
	if (!$("input[id=personal_t]").is(":checked")) {
		alert("개인정보취급방침에 동의해주세요.");
		$('.web1200.terms_box ul').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		return false;
	}


	// 이름 체크
	if (checked_name == null) {
		alert("이름을 입력해주세요.");
		$('input[name=name]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=name]').addClass("error");
		$('input[name=name]').siblings(".error_text").css({"display":"block"});
		// $('input[name=name]').focus();
		return false;
	}

	// 성별 체크
	checked_gender = Number($("select[name=gender]").val());

	// 통신사 체크
	if (checked_themobilecompany == null) {
		alert("통신사를 선택해주세요.");
		$('input[name=theMobileCompany]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		return false;
	}

	// 휴대폰 체크 
	if (checked_phone_first == null) {
		alert("휴대폰번호를 입력해주세요.");
		$('input[name=phoneFirst]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$("input[name=phoneFirst]").addClass("error");
		$('input[name=phoneFirst]').siblings(".error_text").css({"display":"block"});
		return false;
	}
	if (checked_phone_second == null) {
		alert("휴대폰번호를 입력해주세요.");
		$('input[name=phoneSecond]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$("input[name=phoneSecond]").addClass("error");
		$('input[name=phoneSecond]').siblings(".error_text").css({"display":"block"});
		return false;
	}
	if (checked_phone_third == null) {
		alert("휴대폰번호를 입력해주세요.");
		$('input[name=phoneThird]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$("input[name=phoneThird]").addClass("error");
		$('input[name=phoneThird]').siblings(".error_text").css({"display":"block"});
		return false;
	}

	// 이메일 체크
	if (checked_email_before == null) {
		console.log("aaa1");
		alert("이메일을 입력해주세요.");
		$('input[name=emailBefore]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=emailBefore]').addClass("error");
		$('input[name=emailBefore]').siblings(".error_text").eq(0).css({"display":"block"});
		$('input[name=emailBefore]').siblings(".error_text").eq(1).css({"display":"none"});
		$('input[name=emailBefore]').siblings(".error_text").eq(2).css({"display":"none"});
		return false;
	}

	if (checked_email_after1 == null && checked_email_after2 == null) {
		console.log("aaa2");
		alert("이메일을 입력해주세요.");
		$('input[name=emailAfter2]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=emailAfter2]').addClass("error");
		$('input[name=emailAfter2]').siblings(".error_text").eq(0).css({"display":"block"});
		$('input[name=emailAfter2]').siblings(".error_text").eq(1).css({"display":"none"});
		$('input[name=emailAfter2]').siblings(".error_text").eq(2).css({"display":"none"});
		return false;	
	}

	checked_email_full = checked_email_before + checked_email_after1;
	if (checked_email_after1 == null) {
		checked_email_full = checked_email_before + "@" + checked_email_after2;
	}

	console.log("checked_email_full = "+checked_email_full);
	if (!inValidEmail2(checked_email_full)) {
		alert("이메일 양식이 맞지 않습니다. 다시 입력해주세요.");
		$('select[name=emailAfter1]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=emailBefore]').addClass("error");
		$('select[name=emailAfter1]').addClass("error");
		$('input[name=emailAfter2]').addClass("error");

		$('input[name=emailBefore]').siblings(".error_text").css({"display":"none"});
		$('input[name=emailBefore]').siblings(".error_text").eq(3).css({"display":"block"});

		checked_email_full = null;
		return false;
	}

	if (isOverlapChecked == false) {
		alert("이메일 중복확인을 해주세요.");
		$('select[name=emailAfter1]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		checked_email_full = null;
		return false;
	}



	// 비밀번호 체크
	if (checked_password == null) {
		alert("비밀번호를 입력해주세요.");
		$('input[name=pw]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=pw]').addClass("error");
		$('input[name=pw]').siblings(".error_text").css({"display":"none"});
		$('input[name=pw]').siblings(".error_text").eq(0).css({"display":"block"});
		return false;
	}

	if (checked_password_check == null) {
		alert("비밀번호 확인을 입력해주세요.");
		$('input[name=pwcheck]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=pwcheck]').addClass("error");
		$('input[name=pwcheck]').siblings(".error_text").css({"display":"none"});
		$('input[name=pwcheck]').siblings(".error_text").eq(0).css({"display":"block"});
		return false;
	}

	if (checked_password != checked_password_check) {
		alert("비밀번호와 비밀번호확인을 동일하게 입력해주세요.");
		$('input[name=pwcheck]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=pw]').addClass("error");
		$('input[name=pwcheck]').addClass("error");
		$('input[name=pw]').siblings(".error_text").css({"display":"none"});
		$('input[name=pw]').siblings(".error_text").eq(1).css({"display":"block"});
		$('input[name=pwcheck]').siblings(".error_text").css({"display":"none"});
		$('input[name=pwcheck]').siblings(".error_text").eq(1).css({"display":"block"});
		return false;
	}

	// 생년월일 체크
	if (checked_birthday_year == null) {
		alert("생년월일을 입력해주세요.");
		$('input[name=birthdayYear]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=birthdayYear]').addClass("error");
		$('input[name=birthdayYear]').siblings(".error_text").css({"display":"none"});
		$('input[name=birthdayYear]').siblings(".error_text").eq(0).css({"display":"block"});
		return false;	
	}
	if (checked_birthday_month == null) {
		alert("생년월일을 입력해주세요.");
		$('input[name=birthdayMonth]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=birthdayMonth]').addClass("error");
		$('input[name=birthdayMonth]').siblings(".error_text").css({"display":"none"});
		$('input[name=birthdayMonth]').siblings(".error_text").eq(0).css({"display":"block"});
		return false;	
	}
	if (checked_birthday_date == null) {
		alert("생년월일을 입력해주세요.");
		$('input[name=birthdayDate]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=birthdayDate]').addClass("error");
		$('input[name=birthdayDate]').siblings(".error_text").css({"display":"none"});
		$('input[name=birthdayDate]').siblings(".error_text").eq(0).css({"display":"block"});
		return false;	
	}

	if (!isValidDate(checked_birthday_year+"-"+attachZeroA(checked_birthday_month)+"-"+attachZeroA(checked_birthday_date))) {
		alert("생년월일이 유효하지 않습니다. 날짜 유형에 맞게 입력해주세요.");
		$('input[name=birthdayYear]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		$('input[name=birthdayDate]').siblings(".error_text").css({"display":"none"});
		$('input[name=birthdayDate]').siblings(".error_text").eq(1).css({"display":"block"});

		return false;	
	}

	// 혈액형 체크
	if (checked_blood_type == null) {
		alert("혈액형을 선택해주세요.");
		$('input[name=bloodType]').get(0).scrollIntoView({ 
			behavior: 'smooth' 
		});
		return false;		
	}


	// 주소체크
	checked_addr_post = $("input[name=postNumber]").val();
	checked_addr_basic = $("input[name=basicAddr]").val();
	checked_addr_detail = $("input[name=detailAddr]").val();



	return true;

}






// 이메일 형식인지 체크하는 함수
function inValidEmail2(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}






// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}





function attachZeroA(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}