var global_pk = 0;





$(document).ready(function(){
	getPk(); // 의료진 pk 가져오기

	web2401(); // 의료진 상세정보 가져오기
	web2402(); // 의료진 이미지 가져오기
});

function getPk() {
	global_pk = Number($("input[name=pk]").val());
}


// 의료진 상세정보 가져오기
function web2401() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2401",
			pk:global_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				console.log("json 맞아");

				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var name = jsonObj.name;
					var affiliation_department = jsonObj.affiliation_department;
					var affiliation_department_string = jsonObj.affiliation_department_string;
					var profile_info = jsonObj.profile_info;
					var school_history = jsonObj.school_history;
					var career = jsonObj.career;

					$(".doctor_name_title.web2400").html(name);
					$(".doctor_one_word").html(profile_info);
					$(".doctor_chain_department").text(affiliation_department_string);



					if (school_history != '') {
						var lines = school_history.split(",");

						for (var i=0; i<lines.length; i++) {
							var set = lines[i].split(":");
							var period = set[0];
							var content = set[1];

							var text = '';
							text += '<li class="clearFix">';
							text += '	<div class="period">';
							text += '		'+period;
							text += '	</div>';
							text += '	<div class="content">';
							text += '		'+content;
							text += '	</div>';
							text += '</li>';

							$(".school_list.list").append(text);
						}
					} else {
						$(".school_list_area").css("display", "none");
					}	


					if (career != '') {
						var lines = career.split(",");

						for (var i=0; i<lines.length; i++) {
							var set = lines[i].split(":");
							var period = set[0];
							var content = set[1];

							var text = '';
							text += '<li class="clearFix">';
							text += '	<div class="period">';
							text += '		'+period;
							text += '	</div>';
							text += '	<div class="content">';
							text += '		'+content;
							text += '	</div>';
							text += '</li>';

							$(".career_list.list").append(text);
						}
					} else {
						$(".career_list_area").css("display", "none");
					}


				} else {

					

				}
			} else {
				console.log("json 아니야");
			}
		},
		fail: function() {
			
		}
	});
}

// 의료진 이미지 가져오기
function web2402() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2402",
			pk:global_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			// console.log("data = "+data);

			if (data == 'no') {

			} else {
				$(".doctor_image_box").css("background-image", "url("+data+")");
			}
		},
		fail: function() {
			
		}
	});
}











// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}