var global_pk = 0;





$(document).ready(function(){
	getPk(); // 진료과 pk 가져오기

	web2201(); // 진료과 상세정보 가져오기
	web2202(); // 진료과 이미지 가져오기
});

function getPk() {
	global_pk = Number($("input[name=pk]").val());
}


// 진료과 상세정보 가져오기
function web2201() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2201",
			pk:global_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				console.log("json 맞아");

				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var department = jsonObj.department;
					var description = jsonObj.description;

					$(".department_title").text(department);
					$(".department_description").html(description);


					$.each(jsonObj.data, function(key, value){
						var doctor = value.doctor;
						var name = value.name;


						var text = '';
						text += '<li>';
						text += '	<a href="./web2400.php?pk='+doctor+'">';
						text += '		<div class="overlay_display">';
						text += '			상세정보 바로가기';
						text += '		</div>';
						text += '		<div class="doctor_photo" datas-pk="'+doctor+'">';
						text += '		</div>';
						text += '		<div class="doctor_name setTopVirtualBox">';
						text += '			<div>';
						text += '				'+name;
						text += '			</div>';
						text += '		</div>';
						text += '	</a>';
						text += '</li>';

						$(".chain_doctor_list ul").append(text);

						web2203($("div[datas-pk="+doctor+"]"), doctor);
					});
					

				} else {

					

				}
			} else {
				console.log("json 아니야");
			}
		},
		fail: function() {
			
		}
	});
}

// 진료과 이미지 가져오기
function web2202() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2202",
			pk:global_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			// console.log("data = "+data);

			if (data == 'no') {

			} else {
				$(".department_image_box").css("background-image", "url("+data+")");
			}
		},
		fail: function() {
			
		}
	});
}


function web2203(obj, doctor_pk) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2203",
			doctor_pk:doctor_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+cutString(data, 10));

			if ($.trim(data) == "no") {
				console.log("no 실행");
			} else {
				$(obj).css({"background-image":"url("+data+")", "background-size":"cover"});
			}
		},
		fail: function() {
			
		}
	});
}











// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}