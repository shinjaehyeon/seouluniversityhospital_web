$(document).ready(function(){
	web4101(); // N의학정보 상세정보 가져오기
});

// N의학정보 상세정보 가져오기
function web4101() {
	var pk = Number($("input[name=pk]").val());

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web4101",
			pk:pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var title_ko = jsonObj.title_ko;
					var title_en = jsonObj.title_en;
					var one_line_description = jsonObj.one_line_description;
					var chain_department = jsonObj.chain_department;
					var chain_body_simple = jsonObj.chain_body_simple;
					var chain_symptom = jsonObj.chain_symptom;

					$(".web4100.title .ko").html(title_ko);
					$(".web4100.title .en").html('['+title_en+']');


					$(".web4100.simple_info_box > ul").empty();

					if (one_line_description != "") {
						$(".web4100.simple_info_box ul").append('<li class="clearFix"><div class="title common">한 줄 설명</div><div class="description common">'+one_line_description+'</div></li>');
					}

					if (chain_department != "") {
						$(".web4100.simple_info_box ul").append('<li class="clearFix"><div class="title common">관련 진료과</div><div class="description common">'+chain_department+'</div></li>');		
					}

					if (chain_body_simple != "") {
						$(".web4100.simple_info_box ul").append('<li class="clearFix"><div class="title common">관련 신체기관</div><div class="description common">'+chain_body_simple+'</div></li>');		
					}

					if (chain_symptom != "") {
						$(".web4100.simple_info_box ul").append('<li class="clearFix"><div class="title common">관련 증상</div><div class="description common">'+chain_symptom+'</div></li>');			
					}


					$(".web4100.accordion_list ul").empty();
					$.each(jsonObj.more_info_array, function(key, value){
						var value_type = value.value_type;
						var value_type_string = value.value_type_string;
						var value = value.value;

						if (value != "") {

							var text = '';
							text += '<li class="close">';
							text += '	<div class="title">';
							text += '		'+value_type_string;
							text += '	</div>';
							text += '	<div class="open_close_button">';
							text += '		<img src="./images/web0000_bottom_arrow_black_icon.png" alt="아래방향 검은색 아이콘" title="아래방향 검은색 아이콘" />';
							text += '	</div>';
							text += '	<div class="content">';
							text += '		'+value;
							text += '	</div>';
							text += '</li>';

							$(".web4100.accordion_list ul").append(text);

						}
					});

					web4110(); // 아코디언 메뉴 클릭시 발생할 이벤트 설정하기
					web4111(); // 전체펼치기, 전체접기 버튼 이벤트 설정하기
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}


// 아코디언 메뉴 클릭시 발생할 이벤트 설정하기
function web4110() {
	$(".web4100.accordion_list ul li").on("click", function(){
		var boolean = $(this).hasClass("close");
		if (boolean) {
			$(this).removeClass("close");
			$(this).addClass("open");

			var li_closed_height = 74;
			var li_content_height = $(this).children(".content").height();
			var li_content_height_margin_bottom = Number($(this).children(".content").css("margin-bottom").split("px")[0]) + 20;
			var full_height = li_closed_height + li_content_height + li_content_height_margin_bottom;

			$(this).stop().animate({"height":full_height+"px"}, 100, 'easeOutQuint', function(){

			});
		} else {
			$(this).removeClass("open");
			$(this).addClass("close");

			$(this).stop().animate({"height":"74px"}, 100, 'easeOutQuint', function(){
				
			});
		}
	});
}

// 전체펼치기, 전체접기 버튼 이벤트 설정하기
function web4111() {
	$(".web4100.all_open_all_close_button_box ul li.all_open").on("click", function(){
		// 모든 항목 열기
		$(".web4100.accordion_list ul li").each(function(){
			$(this).removeClass("close");
			$(this).addClass("open");

			var li_closed_height = 74;
			var li_content_height = $(this).children(".content").height();
			var li_content_height_margin_bottom = Number($(this).children(".content").css("margin-bottom").split("px")[0]) + 20;
			var full_height = li_closed_height + li_content_height + li_content_height_margin_bottom;

			$(this).stop().animate({"height":full_height+"px"}, 100, 'easeOutQuint', function(){

			});
		});
	});


	$(".web4100.all_open_all_close_button_box ul li.all_close").on("click", function(){
		// 모든 항목 닫기
		$(".web4100.accordion_list ul li").each(function(){
			$(this).removeClass("open");
			$(this).addClass("close");

			$(this).stop().animate({"height":"74px"}, 100, 'easeOutQuint', function(){
			
			});
		});
	});
}






// &amp; 등의 형태의 html 문자기호를 html코드로 디코드화 하여 문자열 반환하기
function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}






// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}