





$(document).ready(function(){
	web2101(); // 진료과 리스트 가져오기
	web2102(); // 진료과명칭 검색 버튼 클릭시 이벤트 설정하기
});

// 진료과 리스트 가져오기
function web2101() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2101"
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					var ii = 1;
					$.each(jsonObj.data, function(key, value){
						var suhd = value.suhd;
						var department = value.department;

						var text = '';
						if (ii%4 == 0) {
							text += '<li class="margin_right_zero">';
						} else {
							text += '<li>';
						}

						text += '	<a href="./web2200.php?pk='+suhd+'" class="setTopVirtualBox">';
						text += '		<div>';
						text += '			'+department;
						text += '		</div>';
						text += '	</a>';
						text += '</li>';

						$("ul.department_list_area").append(text);
						ii++;
					});

					

				} else {

					

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 진료과명칭 검색 버튼 클릭시 이벤트 설정하기
function web2102() {
	$(".search_area_type_a .search_button").on("click", function(){
		var value = $("input[name=search_value]").val();

		if (value == '') {
			$("ul.department_list_area li.hide").removeClass("hide");
			$("ul.department_list_area li.margin_right_zero").removeClass("margin_right_zero");
			
			var total_num = $("ul.department_list_area li").length;
			for (var i=0; i<total_num; i++) {
				if ((i+1) % 4 == 0) {
					$("ul.department_list_area li").eq(i).addClass("margin_right_zero");
				}
			}
		} else {
			$("ul.department_list_area li.hide").removeClass("hide");
			$("ul.department_list_area li.margin_right_zero").removeClass("margin_right_zero");

			var total_num = $("ul.department_list_area li").length;
			var kk = 1;
			for (var i=0; i<total_num; i++) {
				var department = $.trim($("ul.department_list_area li").eq(i).find("div").text());

				if (department.indexOf(value) >= 0) {
					if (kk%4 == 0) {
						$("ul.department_list_area li").eq(i).addClass("margin_right_zero");
					}
					kk++;
				} else {
					$("ul.department_list_area li").eq(i).addClass("hide");
				}
			}
		}
	});


	$("input[name=search_value]").keypress(function(event){
		if (event.keyCode == 13) {
			var value = $("input[name=search_value]").val();

			if (value == '') {
				$("ul.department_list_area li.hide").removeClass("hide");
				$("ul.department_list_area li.margin_right_zero").removeClass("margin_right_zero");
				
				var total_num = $("ul.department_list_area li").length;
				for (var i=0; i<total_num; i++) {
					if ((i+1) % 4 == 0) {
						$("ul.department_list_area li").eq(i).addClass("margin_right_zero");
					}
				}
			} else {
				$("ul.department_list_area li.hide").removeClass("hide");
				$("ul.department_list_area li.margin_right_zero").removeClass("margin_right_zero");

				var total_num = $("ul.department_list_area li").length;
				var kk = 1;
				for (var i=0; i<total_num; i++) {
					var department = $.trim($("ul.department_list_area li").eq(i).find("div").text());

					if (department.indexOf(value) >= 0) {
						if (kk%4 == 0) {
							$("ul.department_list_area li").eq(i).addClass("margin_right_zero");
						}
						kk++;
					} else {
						$("ul.department_list_area li").eq(i).addClass("hide");
					}
				}
			}
		}
	});
}







// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}