var currentYear = null;
var currentMonth = null;

var currentSelectedYear = null;
var currentSelectedMonth = null;
var currentSelectedDate = null;

var currentSelectedTime = null;

var selected_department_pk = null;
var selected_department_string = null;

var selected_doctor_pk = null;
var selected_doctor_string = null;

var selected_year = null;
var selected_month = null;
var selected_date = null;
var selected_day = null;
var selected_hour = null;
var selected_minute = null;

var currentSTEP = 1;

var dayArray = ['일', '월', '화', '수', '목', '금', '토'];

$(document).ready(function(){
	web2001(); // 현재 보이는 STEP에 따라 이전, 다음 버튼 클릭시 이벤트 설정하기	
	setSTEP01searchEvent(); // STEP01 진료과 선택 검색박스에 키보드가 눌릴때마다 발생될 이벤트 설정하기

	setSTEP02searchEvent(); // STEP02 의료진 선택 검색박스에 키보드가 눌릴때마다 발생될 이벤트 설정하기
	web2003(); // 진료과 목록 요청하고 보여주기 web2004();
	
	web2010(); // 날짜/시간 선택 페이지에서 캘린더 뿌리기
});

// 의료진 이미지 가져오기
function web2009(obj, doctor_pk) {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2009",
			doctor_pk:doctor_pk
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			if (data == "no") {
				

			} else {

				$(obj).css({"background-image":"url("+data+")", "background-size":"cover"});

			}
		},
		fail: function() {
			
		}
	});
}

// 현재 보이는 STEP에 따라 이전, 다음 버튼 클릭시 이벤트 설정하기
function web2001() {
	// 다음 버튼 눌렀을 때
	$(".controll_button_box_1200 .button_box .positive_button").on("click", function(){
		switch(currentSTEP) {
			case 1: // 현재 단계가 STEP 01 일 경우
				// 진료과가 선택되어 있는지 확인하기
				if (selected_department_pk != null) {
					// 선택되어 있으면
					currentSTEP = 2;
					activeStep(currentSTEP);
					$(".clinic_reservation_content_box_1200 .step_content.show").removeClass("show");
					$(".clinic_reservation_content_box_1200 .step_content.step02_content").addClass("show");
					$(".controll_button_box_1200 .button_box .nagative_button").css({"display":"block"});
				} else {
					// 선택되어 있지 않으면
					alert("진료과를 선택해주세요.");
				}
				break;
			case 2: // 현재 단계가 STEP 02 일 경우
				// 의료진이 선택되어 있는지 확인하기
				if (selected_doctor_pk != null) {
					// 선택되어 있으면
					currentSTEP = 3;
					activeStep(currentSTEP);
					$(".clinic_reservation_content_box_1200 .step_content.show").removeClass("show");
					$(".clinic_reservation_content_box_1200 .step_content.step03_content").addClass("show");
					$(".controll_button_box_1200 .button_box .nagative_button").css({"display":"block"});
				} else {
					// 선택되어 있지 않으면
					alert("의료진을 선택해주세요.");
				}
				break;
			case 3: // 현재 단계가 STEP 03 일 경우
				// 시간이 선택되어 있으면
				if (selected_hour != null) {
					currentSTEP = 4;
					activeStep(currentSTEP);
					$(".clinic_reservation_content_box_1200 .step_content.show").removeClass("show");
					$(".clinic_reservation_content_box_1200 .step_content.step04_content").addClass("show");
					$(".controll_button_box_1200 .button_box .nagative_button").css({"display":"block"});
				} else {
					alert("날짜 및 시간을 선택해주세요.");
				}
				break;
			case 4: // 현재 단계가 STEP 04 일 경우
				if (isLoading == false) {
					web2020(); // 서버에 입력받은 예약정보를 DB에 저장 요청 
				}

				break;
			case 5: // 현재 단계가 STEP 01 일 경우

				break;
		}
	});

	// 이전 버튼을 눌렀을 때
	$(".controll_button_box_1200 .button_box .nagative_button").on("click", function(){
		switch(currentSTEP) {
			case 1: // 현재 단계가 STEP 01 일 경우

				break;
			case 2: // 현재 단계가 STEP 02 일 경우
				currentSTEP = 1;
				activeStep(currentSTEP);

				$(".clinic_reservation_content_box_1200 .step_content.show").removeClass("show");
				$(".clinic_reservation_content_box_1200 .step_content.step01_content").addClass("show");
				$(".controll_button_box_1200 .button_box .nagative_button").css({"display":"none"});
				break;
			case 3: // 현재 단계가 STEP 03 일 경우
				currentSTEP = 2;
				activeStep(currentSTEP);

				$(".clinic_reservation_content_box_1200 .step_content.show").removeClass("show");
				$(".clinic_reservation_content_box_1200 .step_content.step02_content").addClass("show");
				break;
			case 4: // 현재 단계가 STEP 04 일 경우
				currentSTEP = 3;
				activeStep(currentSTEP);

				$(".clinic_reservation_content_box_1200 .step_content.show").removeClass("show");
				$(".clinic_reservation_content_box_1200 .step_content.step03_content").addClass("show");
				break;
			case 5: // 현재 단계가 STEP 01 일 경우

				break;
		}
	});
		
}

function activeStep(number) {
	$("ul.clinic_reservation_step_display li.active").removeClass("active");
	$("ul.clinic_reservation_step_display li").not(".icon").eq(number-1).addClass("active");

	$(".step_position_line_box .step_position_line.step01").removeClass("step01");
	$(".step_position_line_box .step_position_line.step02").removeClass("step02");
	$(".step_position_line_box .step_position_line.step03").removeClass("step03");
	$(".step_position_line_box .step_position_line.step04").removeClass("step04");
	$(".step_position_line_box .step_position_line.step05").removeClass("step05");

	$(".step_position_line_box .step_position_line").addClass("step0"+number);	
}

// 진료과 목록 요청하고 보여주기
function web2003() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2003"
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$.each(jsonObj.data, function(key, value){
						var department_pk = value.suhd;
						var department_string = value.department;

						var text = '<li department-pk="'+department_pk+'">'+department_string+'</li>';

						$(".step01_content .right_window ul.department_list_box").append(text)
					});

					web2004(); // 진료과 선택시 이벤트 설정하기

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 진료과 선택시 이벤트 설정하기
function web2004() {
	$(".clinic_reservation_content_box .step01_content ul.department_list_box li").on("click", function(){
		console.log("??");
		var department_pk = Number($(this).attr("department-pk"));
		var department_string = $(this).text();

		console.log("department_pk = "+department_pk);
		console.log("department_string = "+department_string);

		selected_department_pk = department_pk;
		selected_department_string = department_string;

		$(".clinic_reservation_content_box .step01_content ul.department_list_box li.active").removeClass("active");
		$(this).addClass("active");

		web2005(); // STEP 02에 관한 내용 전부 초기화 하기
		web2006(); // STEP 03에 관한 내용 전부 초기화 하기

		$(".current_status .step01_selected").empty();
		$(".current_status .step02_selected").empty();
		$(".current_status .step03_selected_1").empty();
		$(".current_status .step03_selected_2").empty();
		$(".current_status .step01_selected").text("▣ 진료과 : "+selected_department_string);

		web2007(); // 진료과 pk에 맞는 의료진 리스트 가져와 뿌리기
	});
}

// STEP01 진료과 선택 검색박스에 키보드가 눌릴때마다 발생될 이벤트 설정하기
function setSTEP01searchEvent() {
	$(".step_content.step01_content .left_window .step_search_box .step_search").on("keyup", function(){
		var inputed_value = $(this).val();

		if (inputed_value == "") {
			$(".clinic_reservation_content_box .step01_content ul.department_list_box li.hide").removeClass("hide");
			$(".clinic_reservation_content_box .step01_content ul.department_list_box li").addClass("show");
		} else {
			$(".clinic_reservation_content_box .step01_content ul.department_list_box li.show").removeClass("show");
			$(".clinic_reservation_content_box .step01_content ul.department_list_box li").addClass("hide");

			$(".clinic_reservation_content_box .step01_content ul.department_list_box li").each(function(){
				var department = $(this).text();
				
				if (department.indexOf(inputed_value) != -1) {
					$(this).removeClass("hide");
					$(this).addClass("show");
				}
			});		

		}

	});
}

// STEP02 의료진 선택 검색박스에 키보드가 눌릴때마다 발생될 이벤트 설정하기
function setSTEP02searchEvent() {
	$(".step_content.step02_content .left_window .step_search_box .step_search").on("keyup", function(){
		var inputed_value = $(this).val();

		if (inputed_value == "") {
			$(".clinic_reservation_content_box .step02_content ul.doctor_list_box li.hide").removeClass("hide");
			$(".clinic_reservation_content_box .step02_content ul.doctor_list_box li").addClass("show");
		} else {
			$(".clinic_reservation_content_box .step02_content ul.doctor_list_box li.show").removeClass("show");
			$(".clinic_reservation_content_box .step02_content ul.doctor_list_box li").addClass("hide");

			$(".clinic_reservation_content_box .step02_content ul.doctor_list_box li").each(function(){
				var name = $(this).find(".name").text();
				
				if (name.indexOf(inputed_value) != -1) {
					$(this).removeClass("hide");
					$(this).addClass("show");
				}
			});		

		}

	});
}


// STEP 02에 관한 내용 전부 초기화 하기
function web2005() {
	selected_doctor_pk = null;
	selected_doctor_string = null;

	$(".clinic_reservation_content_box .step02_content ul.doctor_list_box").empty();
}

// STEP 03에 관한 내용 전부 초기화 하기
function web2006() {
	selected_year = null;
	selected_month = null;
	selected_date = null;
	selected_day = null;
	selected_hour = null;
	selected_minute = null;

	$(".step03_content .time_box ul").empty();
}

// 진료과 pk에 맞는 의료진 리스트 가져와 뿌리기
function web2007() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2007",
			department_pk:selected_department_pk,
			department_string:selected_department_string
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$(".clinic_reservation_content_box .step02_content ul.doctor_list_box").empty();

					$.each(jsonObj.data, function(key, value){
						var doctor_pk = value.doctor;
						var doctor_name = value.name;
						var doctor_department = value.department_string;

						var text = '';
						text+= '<li doctor-pk="'+doctor_pk+'" doctor-name="'+doctor_name+'">';
						text+= '	<a class="doctor_detail_page_button" href="./web2400.php?pk='+doctor_pk+'" target="_blank">';
						text+= ' 		';
						text+= '	</a>';
						text+= '	<div class="doctor_image" datas-doctorpk="'+doctor_pk+'">';
						text+= ' 		';
						text+= '	</div>';
						text+= '	<div class="doctor_info setTopVirtualBox">';
						text+= '		<div class="name_and_department clearFix">';
						text+= '			<div class="name">';
						text+= ' 				'+doctor_name;
						text+= '			</div>';
						text+= '			<div class="department">';
						text+= ' 				'+doctor_department;
						text+= '			</div>';
						text+= '		</div>';
						text+= '		<div class="detail_info">';
						text+= ' 			';									
						text+= '		</div>';
						text+= '	</div>';
						text+= '</li>';

						$(".clinic_reservation_content_box .step02_content ul.doctor_list_box").append(text);

						web2009($("div[datas-doctorpk="+doctor_pk+"]"), doctor_pk);
					});

					web2008(); // 의료진 선택시 이벤트 설정하기
				} else {
					
				}
			}
		},
		fail: function() {
			
		}
	});
}

// 의료진 선택시 이벤트 설정하기
function web2008() {
	$(".clinic_reservation_content_box .step02_content ul.doctor_list_box li").on("click", function(){
		$(".clinic_reservation_content_box .step02_content ul.doctor_list_box li.active").removeClass("active");
		$(this).addClass("active");


		var doctor_pk = Number($(this).attr("doctor-pk"));
		var doctor_name = $(this).attr("doctor-name");

		selected_doctor_pk = doctor_pk;
		selected_doctor_string = doctor_name;

		web2006(); // STEP 03에 관한 내용 전부 초기화 하기


		$(".current_status .step02_selected").empty();
		$(".current_status .step03_selected_1").empty();
		$(".current_status .step03_selected_2").empty();
		$(".current_status .step02_selected").text("▣ 의료진 : "+selected_doctor_string);
	});
}

// 날짜/시간 선택 페이지에서 캘린더 뿌리기
function web2010() {

	var today = new Date();

    // 오늘 년도 구하기
    currentYear = today.getFullYear();

    // 오늘 월 구하기
    currentMonth = today.getMonth();

    // 파라미터 year년 month월의 캘린더 그리기
    fillCalendar(currentYear, currentMonth);

    // 이전달/다음달 버튼 이벤트 설정하기
    setPrevNextMonthButtonEvent();

    // 날짜를 선택하면 활성화 표시 하기
    setClickDateEvent();
}  


// 파라미터 year년 month월의 캘린더 그리기
function fillCalendar(year, month) {
	// 오늘 정보 가져오기
	var today = new Date();

    todayYear = today.getFullYear();
    todayMonth = today.getMonth();
    todayDate = today.getDate();


    // year년 month월 1일의 요일 index 구하기
    var dates = new Date(year, month, 1).getDay(); // 일요일 0, 월요일 1, ... , 토요일 6
    // console.log("dates = "+dates);

    var startIndex = dates;

    var lastDate = lastDay(year, month, 1);
    // console.log("lastDate = "+lastDate);

    $(".year_month_display .year").text(currentYear+"년");
    $(".year_month_display .month").text((currentMonth+1)+"월");
    

    // 효과 모두 지우기
    $(".date_display_area ul li").find(".date").removeClass("prevnext");
    $(".date_display_area ul li.active").removeClass("active");
    $(".date_display_area ul li.normal1").removeClass("normal1");
    $(".date_display_area ul li.normal2").removeClass("normal2");
    $(".date_display_area ul li").attr("data-year", "");
    $(".date_display_area ul li").attr("data-month", "");
    $(".date_display_area ul li").attr("data-date", "");
    $(".date_display_area ul li .today").css({"display":"none"});

    var jj = 1;
    for (var i=0; i<42; i++) {
        if (i < startIndex) {
            continue;
        }

        if (jj <= lastDate) {
            var classname = "";
            if (i % 2 == 0) {
                classname = "normal1";
            } else {
                classname = "normal2";
            }
            $(".date_display_area ul li").eq(i).attr("data-year", currentYear);
            $(".date_display_area ul li").eq(i).attr("data-month", currentMonth + 1);
            $(".date_display_area ul li").eq(i).attr("data-date", jj);
            $(".date_display_area ul li").eq(i).addClass(classname);
            $(".date_display_area ul li").eq(i).find(".date").text(jj+"");

            
            if (Number(currentSelectedYear) == Number(currentYear) && Number(currentSelectedMonth)-1 == Number(currentMonth) && Number(currentSelectedDate) == Number(jj)) {
                $(".date_display_area ul li").eq(i).addClass("active");
            }

            if (Number(currentYear) == Number(todayYear) && Number(currentMonth) == Number(todayMonth) && Number(todayDate) == Number(jj)) {
            	$(".date_display_area ul li").eq(i).children(".today").css({"display":"block"});
            }

            jj++;
        }

    }

    // 이전달 일 뿌리기

    // 이전달의 마지막날 구하기
    var prev_year = null;
    var prev_month = null;
    if (currentMonth - 1 < 0) {
        prev_year = currentYear - 1;
        prev_month = 11;
    } else {
        prev_year = currentYear;
        prev_month = currentMonth - 1;
    }

    var prev_lastDate = lastDay(prev_year, prev_month, 1);
    
    for (var i=startIndex-1; i>=0; i--) {
        $(".date_display_area ul li").eq(i).find(".date").addClass("prevnext");
        $(".date_display_area ul li").eq(i).find(".date").text(prev_lastDate+"");
        prev_lastDate--;
    }



    // 댜음달 일 뿌리기
    var ll = 1;
    for (var i=startIndex+jj-1; i<42; i++) {
        $(".date_display_area ul li").eq(i).find(".date").addClass("prevnext");
        $(".date_display_area ul li").eq(i).find(".date").text(ll+"");
        ll++;
    }   
}

//현재달의 마지막날(일자) 구하기
function lastDay(year, month, date) {
    var date = new Date(year, month, date);
    date.setMonth(date.getMonth() + 1);
    date.setDate(0);
    return date.getDate();
}

function converDateString(dt) {
    return dt.getFullYear() + "-" + addZero(eval(dt.getMonth()+1)) + "-" + addZero(dt.getDate());
}
function addZero(i){
    var rtn = i + 100;
    return rtn.toString().substring(1,3);
}



// 이전달/다음달 버튼 이벤트 설정하기
function setPrevNextMonthButtonEvent() {
    var p = ".month_and_arrow_button_area .prev_month_button";
    var n = ".month_and_arrow_button_area .next_month_button";

    // 이전달 버튼 이벤트 설정
    $(p).on("click", function(){
        var prev_year = null;
        var prev_month = null;
        if (currentMonth - 1 < 0) {
            prev_year = currentYear - 1;
            prev_month = 11;
        } else {
            prev_year = currentYear;
            prev_month = currentMonth - 1;
        }

        currentYear = prev_year;
        currentMonth = prev_month;
        fillCalendar(currentYear, currentMonth);
    });

    // 다음달 버튼 이벤트 설정
    $(n).on("click", function(){
        var next_year = null;
        var next_month = null;
        if (currentMonth + 1 > 11) {
            next_year = currentYear + 1;
            next_month = 0;
        } else {
            next_year = currentYear;
            next_month = currentMonth + 1;
        }

        currentYear = next_year;
        currentMonth = next_month;
        fillCalendar(currentYear, currentMonth);
    });
}



// 날짜를 선택하면 활성화 표시 하기
function setClickDateEvent() {
    $(".date_display_area ul li").on("click", function(){
        if ($(this).hasClass("normal1") === true || $(this).hasClass("normal2") === true) {
        	selected_hour = null;
        	selected_minute = null;

            currentSelectedYear = Number($(this).attr("data-year"));
            currentSelectedMonth = Number($(this).attr("data-month"));
            currentSelectedDate = Number($(this).attr("data-date"));

            var monthss = null;
            if (currentSelectedMonth - 1 < 0) {
            	monthss = 11;
            } else {
            	monthss = currentSelectedMonth - 1;
            }


            var tempDate = new Date();
            var today = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate()); // 오늘 날짜
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
            var select_day = new Date(currentSelectedYear, monthss, currentSelectedDate); // 선택된 날짜
            select_day.setHours(0);
            select_day.setMinutes(0);
            select_day.setSeconds(0);


            var differTime = select_day - today;
            var differDay = differTime / (1000*60*60*24);
            if (today.getTime() <= select_day.getTime()) {
            	selected_year = currentSelectedYear;
	            selected_month = currentSelectedMonth;
	            selected_date = currentSelectedDate;
	            selected_day = select_day.getDay();

	            web2011(); // 날짜 선택시 의료진, 날짜에 맞는 시간 뿌리기

	            var fullDate = currentSelectedYear+"-"+currentSelectedMonth+"-"+currentSelectedDate+" "+dayArray[selected_day]+"요일";
	            console.log("현재 선택된 날짜 = "+fullDate);

	            $(".date_display_area ul li.active").removeClass("active");
	            $(this).addClass("active");




	            var days = select_day.getDay();
	            $(".current_status .step03_selected_1").empty();
	            $(".current_status .step03_selected_2").empty();
	            $(".current_status .step03_selected_1").text("▣ 날짜 : "+selected_year+"-"+attachZeroA(currentSelectedMonth)+"-"+attachZeroA(currentSelectedDate)+"-"+dayArray[selected_day]);
            	
            } else {
            	alert("진료예약은 오늘날짜부터 가능합니다.");
            }



            
        }
    });
}

// 의료진, 날짜에 맞는 시간 뿌리기
function web2011() {
	var datetimeHypon = selected_year+"-"+selected_month+"-"+selected_date;

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2011",
			department:selected_department_pk,
			doctor:selected_doctor_pk,
			datetimeHypon:datetimeHypon
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				console.log("json 맞음!!");
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$(".step03_content .time_box ul").empty();
				console.log("empty 까지는 적용됨!");

				if (result == "ok") {
					$.each(jsonObj.data, function(key, value){
						var year = value.year;
						var month = value.month;
						var date = value.date;
						var hour = value.hour;
						var minute = value.minute;
						var already_suhCRH_pk = value.already_suhCRH_pk;
						
						var classApply = '';
						if (already_suhCRH_pk > 0) {
							classApply = 'class="lock" ';
						}

						console.log("li 추가됨!");

						var text = '<li '+classApply+'data-year="'+year+'" data-month="'+month+'" data-date="'+date+'" data-hour="'+hour+'" data-minute="'+minute+'">'+attachZeroA(hour)+":"+attachZeroA(minute)+'</li>';
						$(".step03_content .time_box ul").append(text);
					});


					web2012(); // 시간 선택시 이벤트 설정하기
				} else {

				}
			} else {

			}
		},
		fail: function() {
			
		}
	});
}

// 시간 선택시 이벤트 설정하기
function web2012() {
	$(".step03_content .time_box ul li").on("click", function(){
		if ($(this).hasClass("lock") === false) {
			var hour = Number($(this).attr("data-hour"));
			var minute = Number($(this).attr("data-minute"));

			selected_hour = hour;
			selected_minute = minute;

			$(".step03_content .time_box ul li.active").removeClass("active");
			$(this).addClass("active");


	        $(".current_status .step03_selected_2").empty();
	        $(".current_status .step03_selected_2").text("▣ 시간 : "+attachZeroA(selected_hour)+"시 "+attachZeroA(selected_minute)+"분");
		




	        var full_date = selected_year+"-"+attachZeroA(selected_month)+"-"+attachZeroA(selected_date)+"-"+dayArray[selected_day];
	        $(".step04_content ul.check_info li div.value").eq(0).text(selected_department_string);
	        $(".step04_content ul.check_info li div.value").eq(1).text(selected_doctor_string);
	        $(".step04_content ul.check_info li div.value").eq(2).text(full_date);
	        $(".step04_content ul.check_info li div.value").eq(3).text(attachZeroA(selected_hour)+"시 "+attachZeroA(selected_minute)+"분");




	        $(".step05_content .result .datetime").empty();
	        $(".step05_content .result .datetime").text(selected_year+"년 "+attachZeroA(selected_month)+"월 "+attachZeroA(selected_date)+"일 "+attachZeroA(selected_hour)+"시 "+attachZeroA(selected_minute)+"분");
		
	        $(".step05_content .result .doctor_department").empty();
	        $(".step05_content .result .doctor_department").text(selected_doctor_string+" 의료진, "+selected_department_string+" 진료");
		} else {
			alert("이미 예약된 시간입니다. 다른 시간을 선택해주세요.");
		}
	});
}

// 서버에 입력받은 예약정보를 DB에 저장 요청
function web2020() {
	isLoading = true;

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2020",
			step01_department:selected_department_pk,
			step02_doctor_primarykey:selected_doctor_pk,
			step03_select_year:selected_year,
			step03_select_month:selected_month,
			step03_select_date:selected_date,
			step03_select_hour:selected_hour,
			step03_select_minute:selected_minute
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					currentSTEP = 5;
					activeStep(currentSTEP);
					$(".clinic_reservation_content_box_1200 .step_content.show").removeClass("show");
					$(".clinic_reservation_content_box_1200 .step_content.step05_content").addClass("show");
					$(".controll_button_box_1200 .button_box .nagative_button").remove();

				} else if (result == "notlogin") {

					alert("잘못된 접근입니다.");
					location.href = "./index.php";

				} else if (result == "already") {

					alert("이미 예약된 날짜입니다. 다른 날짜를 선택해주세요.");

				} else {

					alert("진료예약 신청중 오류가 발생하였습니다. 다시 시도해주세요.");

				}
			}
		},
		fail: function() {
			
		}
	});
}





// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}




function attachZeroA(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}