$(document).ready(function(){
	web6008(); // 병원뉴스 최근 3개 가져오기

	web6010(); // 병원뉴스 리스트 가져오기
	web6011(); // 병원뉴스 게시판 검색버튼 클릭시 이벤트 설정하기

	setNextPageButtonClickEvent(); // 다음 페이지 클릭시 이벤트 설정
	setBestNextPageButtonClickEvent(); // 맨 마지막 패이지로 가기 버튼 클릭시 이벤트 설정

	setPrevPageButtonClickEvent(); // 이전 페이지 클릭시 이벤트 설정
	setBestPrevPageButtonClickEvent(); // 맨 처음 패이지로 가기 버튼 클릭시 이벤트 설정
});

var suhnews_index = 0;
var suhnews_view_num = 10;

var page_reckoning = 1; // 0은 false, 1은 true;

// 병원뉴스 최근 3개 가져오기
function web6008() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web6008"
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				$(".web6000.recent_news_box ul.list").empty();
				if (result == "ok") {
					var i = 0;
					$.each(jsonObj.data, function(key, value){
						var title = value.title;
						var content = value.content;
						var pk = value.suhnews_pk;

						$(".web6000.recent_news_box ul.list li .text .top").eq(i).text(title);
						$(".web6000.recent_news_box ul.list li .text .bottom").eq(i).text(content);


						var style = '';
						$.each(value.attach_file_array, function(key, value){
							var filename = value.filename;
							var imageurl = 'background-image:url(./suhnews/suhnews'+pk+'/'+filename+')';

							if (filename.indexOf("jpg") != -1 || filename.indexOf("png") != -1 || filename.indexOf("gif") != -1) {
								style = imageurl;
								return;
							}
						});

						var text = '';
						text += '<li class="clearFix">';
						text += '	<a href="./web6100.php?pk='+pk+'">';
						text += '		<div class="image" style="'+style+'">';
						text += '			';
						text += '		</div>';
						text += '		<div class="text setTopVirtualBox">';
						text += '			<div class="top">';
						text += '				'+cutString2(title, 15);
						text += '			</div>';
						text += '			<div class="bottom">';
						text += '				'+cutString2(content, 65);
						text += '			</div>';
						text += '		</div>';
						text += '	</a>';
						text += '</li>';


						$(".web6000.recent_news_box ul.list").append(text);



						i++;
					})
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}



function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}




var search_word = '';

// 병원뉴스 리스트 가져오기
function web6010() {
	console.log("병원뉴스 리스트 가져오기");
	console.log("search_word = "+search_word);

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web6010",
			index:suhnews_index,
			view_num:suhnews_view_num,
			page_reckoning:page_reckoning,
			search_word:search_word
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$("ul.board500301 li").not(".board_title").remove();

					$.each(jsonObj.data, function(key, value){
						var suhnews_pk = value.suhnews_pk;
						var admin = value.admin;
						var admin_name = value.admin_name;
						var title = htmlspecialchar_decode(value.title);
						var datetime = value.datetime;
						var attach_file_flag = value.attach_file_flag;
						var view_index = value.view_index;

						var attachClass = '';
						if (attach_file_flag == 1) {
							attachClass = 'attach_file';
						}

						var text = '';
						text += '<li class="clearFix">';
						text += '	<div class="center">';
						text += '		'+suhnews_pk;
						text += '	</div>';
						text += '	<div class="padding_left_30">';
						text += '		<a href="./web6100.php?pk='+suhnews_pk+'">'+title+'</a>';
						text += '	</div>';
						text += '	<div class="center">';
						text += '		'+datetime.split(" ")[0];
						text += '	</div>';
						text += '	<div class="'+attachClass+'">';
						text += '		';
						text += '	</div>';
						text += '	<div class="center">';
						text += '		'+view_index;
						text += '	</div>';
						text += '</li>';

						$("ul.board500301").append(text);
					});


					if (page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (suhnews_index == 0) {
							$(".allboard_paging_controller_box .best_prev").css({"display":"none"});
							$(".allboard_paging_controller_box .prev").css({"display":"none"});
							$(".allboard_paging_controller_box .line.prev").css({"display":"none"});
						} else {
							$(".allboard_paging_controller_box .best_prev").css({"display":"block"});
							$(".allboard_paging_controller_box .prev").css({"display":"block"});
							$(".allboard_paging_controller_box .line.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".allboard_paging_controller_box .best_next").css({"display":"block"});
							$(".allboard_paging_controller_box .next").css({"display":"block"});
							$(".allboard_paging_controller_box .line.next").css({"display":"block"});	
						} else {
							$(".allboard_paging_controller_box .best_next").css({"display":"none"});
							$(".allboard_paging_controller_box .next").css({"display":"none"});
							$(".allboard_paging_controller_box .line.next").css({"display":"none"});
						}


						$(".allboard_paging_controller_box ul.page_num").empty();



						var first_page_index = suhnews_index;
						for (var ii=0; ii<10; ii++) {
							if (first_page_index % 10 == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("suhnews_index = "+suhnews_index);
							console.log("page_number-1 = "+(page_number-1));
							if (suhnews_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".allboard_paging_controller_box ul.page_num").append(text);
						}


						web6015(); // 페이지 번호 클릭시 이벤트 설정



						page_reckoning = 0;
					}
				} else {
					$("ul.board500301 li").not(".board_title").remove();
					$("ul.board500301").append('<li class="not_board">게시글이 없습니다.</li>');
				

					$(".allboard_paging_controller_box .best_prev").css({"display":"none"});
					$(".allboard_paging_controller_box .prev").css({"display":"none"});
					$(".allboard_paging_controller_box .line.prev").css({"display":"none"});
					$(".allboard_paging_controller_box .best_next").css({"display":"none"});
					$(".allboard_paging_controller_box .next").css({"display":"none"});
					$(".allboard_paging_controller_box .line.next").css({"display":"none"});

					$(".allboard_paging_controller_box ul.page_num").empty();
					$(".allboard_paging_controller_box ul.page_num").append('<li class="active">1</li>');
				}
			}
		},
		fail: function() {
			
		}
	});
}

// 병원뉴스 게시판 검색버튼 클릭시 이벤트 설정하기
function web6011() {
	$(".allboard_search_box .allboard_search_button").on("click", function(){
		var inputed_search_word = $.trim($("input[name=web6000_search]").val());


		console.log("검색 함수 실행");
		suhnews_index = 0;
		search_word = inputed_search_word;
		page_reckoning = 1;

		web6010(); // 병원뉴스 리스트 가져오기
		
	});

	$("input[name=web6000_search]").keypress(function(event){
		if (event.keyCode == 13) {
			var inputed_search_word = $.trim($("input[name=web6000_search]").val());

			console.log("검색 함수 실행");
			suhnews_index = 0;
			search_word = inputed_search_word;
			page_reckoning = 1;

			web6010(); // 병원뉴스 리스트 가져오기
		}
	});

}

// 페이지 번호 클릭시 이벤트 설정
function web6015() {
	$(".allboard_paging_controller_box ul.page_num li").on("click", function(){
		$(".allboard_paging_controller_box ul.page_num li.active").removeClass("active");
		$(this).addClass("active");



		var selected_index = Number($(this).attr("data-index"));

		suhnews_index = selected_index;
		web6010();
	});
}

// 다음 페이지 클릭시 이벤트 설정
function setNextPageButtonClickEvent() {
	$(".allboard_paging_controller_box .next").on("click", function(){
		page_reckoning = 1;

		var minimum_index = suhnews_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + 10;
		suhnews_index = next_first_index;
		web6010();
	});
}

// 최고다음 페이지 클릭시 이벤트 설정
function setBestNextPageButtonClickEvent() {
	$(".allboard_paging_controller_box .best_next").on("click", function(){
		web6020(); // 페이지 맨 마지막 번호 알아내기
	});
}

// 이전 페이지 클릭시 이벤트 설정
function setPrevPageButtonClickEvent() {
	$(".allboard_paging_controller_box .prev").on("click", function(){
		page_reckoning = 1;

		var minimum_index = suhnews_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - 10;
		suhnews_index = prev_first_index;
		web6010();
	});
}

// 최고이전 페이지 클릭시 이벤트 설정
function setBestPrevPageButtonClickEvent() {
	$(".allboard_paging_controller_box .best_prev").on("click", function(){
		page_reckoning = 1;

		suhnews_index = 0;
		web6010();
	});
}

// 페이지 맨 마지막 번호 알아내기
function web6020() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web6020"
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;

					page_reckoning = 1;
					suhnews_index = last_index;

					console.log("last_index = "+last_index);

					web6010();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}












// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}


function cutString2(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}