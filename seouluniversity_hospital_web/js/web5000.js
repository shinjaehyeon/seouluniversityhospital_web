$(document).ready(function(){
	web5001(); // 고객의소리 - 감사합니다 가져오기
	web5002(); // 고객의소리 - 감사합니다 페이징 - 다음 페이지 클릭시 이벤트 설정
	web5003(); // 고객의소리 - 감사합니다 페이징 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
	web5004(); // 고객의소리 - 감사합니다 페이징 - 이전 페이지 클릭시 이벤트 설정
	web5005(); // 고객의소리 - 감사합니다 페이징 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
	web5008(); // 고객의소리 - 감사합니다 검색버튼 클릭시 이벤트 설정하기


	web5010(); // 게시판 탭 버튼 이벤트 설정하기

	web5011(); // 고객의소리 - 건의합니다 가져오기
	web5012(); // 고객의소리 - 건의합니다 페이징 - 다음 페이지 클릭시 이벤트 설정
	web5013(); // 고객의소리 - 건의합니다 페이징 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
	web5014(); // 고객의소리 - 건의합니다 페이징 - 이전 페이지 클릭시 이벤트 설정
	web5015(); // 고객의소리 - 건의합니다 페이징 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
	web5018(); // 고객의소리 - 건의합니다 검색버튼 클릭시 이벤트 설정하기

	web5020(); // 글쓰기 버튼 클릭했을 때 이벤트 설정하기
});

var customer800701_index = 0;
var customer800701_view_num = 10;
var customer800701_page_reckoning = 1;
var customer800701_search_word = '';

// 고객의소리 - 감사합니다 가져오기
function web5001() {
	console.log("감사합니다 가져오기");

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web5001",
			index:customer800701_index,
			view_num:customer800701_view_num,
			page_reckoning:customer800701_page_reckoning,
			board_type:800701,
			content_type:0,
			search_word:customer800701_search_word
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$(".web5000.board_box .tab_1_board > ul li").not(".title").remove();

					$.each(jsonObj.data, function(key, value){
						var suhc = value.suhc;
						var board_type = value.board_type;
						var content_type = value.content_type;
						var title = value.title;
						var member = value.member;
						var member_name = value.member_name;
						var datetime = value.datetime;
						var onlydate = datetime.split(" ")[0];
						var attach_file_flag = value.attach_file_flag;
						var view_index = value.view_index;
						var status = value.status;

						var attach_class = "";
						if (attach_file_flag == 1) {
							attach_class = "attach";
						}

						var text = '';
						text += '<li class="clearFix">';
						text += '	<div>';
						text += '		'+suhc;
						text += '	</div> ';
						text += '	<div class="left">';
						text += '		<a href="./web5200.php?pk='+suhc+'">'+title+'</a>';
						text += '	</div>';
						text += '	<div>';
						text += '		'+member_name;
						text += '	</div>';
						text += '	<div>';
						text += '		'+onlydate;
						text += '	</div>';
						text += '	<div class="'+attach_class+'">';
						text += '		';
						text += '	</div>';
						text += '	<div>';
						text += '		'+view_index;
						text += '	</div>';
						text += '</li>';

						$(".web5000.board_box .tab_1_board > ul").append(text);
					});


					if (customer800701_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (customer800701_index == 0) {
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_prev").css({"display":"none"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .prev").css({"display":"none"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .line").eq(0).css({"display":"none"});
						} else {
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_prev").css({"display":"block"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .prev").css({"display":"block"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .line").eq(0).css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_next").css({"display":"block"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .next").css({"display":"block"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .line").eq(1).css({"display":"block"});	
						} else {
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_next").css({"display":"none"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .next").css({"display":"none"});
							$(".web5000.board_box .tab_1_board.board_paging_controller_box .line").eq(1).css({"display":"none"});
						}


						$(".web5000.board_box .tab_1_board.board_paging_controller_box ul.page_num").empty();



						var first_page_index = customer800701_index;
						for (var ii=0; ii<10; ii++) {
							if (first_page_index % 10 == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("customer800701_index = "+customer800701_index);
							console.log("page_number-1 = "+(page_number-1));
							if (customer800701_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".web5000.board_box .tab_1_board.board_paging_controller_box ul.page_num").append(text);
						}


						web5006(); // 진료예약 내역 페이징 - 페이지 번호 클릭시 이벤트 설정

						customer800701_page_reckoning = 0;
					}
				} else {
					console.log("글 없을경우");
					$(".web5000.board_box .tab_1_board > ul li").not(".title").remove();
					$(".web5000.board_box .tab_1_board > ul").append('<li class="not_board">게시글이 없습니다.</li>');
				

					$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_prev").css({"display":"none"});
					$(".web5000.board_box .tab_1_board.board_paging_controller_box .prev").css({"display":"none"});
					$(".web5000.board_box .tab_1_board.board_paging_controller_box .line").eq(0).css({"display":"none"});
					$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_next").css({"display":"none"});
					$(".web5000.board_box .tab_1_board.board_paging_controller_box .next").css({"display":"none"});
					$(".web5000.board_box .tab_1_board.board_paging_controller_box .line").eq(1).css({"display":"none"});

					$(".web5000.board_box .tab_1_board.board_paging_controller_box ul.page_num").empty();
					$(".web5000.board_box .tab_1_board.board_paging_controller_box ul.page_num").append('<li class="active">1</li>');
				}
			}
		},
		fail: function() {
			
		}
	});
}

// 고객의소리 - 감사합니다 - 다음 페이지 클릭시 이벤트 설정
function web5002() {
	$(".web5000.board_box .tab_1_board.board_paging_controller_box .next").on("click", function(){
		customer800701_page_reckoning = 1;

		var minimum_index = customer800701_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + 10;
		customer800701_index = next_first_index;
		web5001();
	});
}

// 고객의소리 - 감사합니다 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function web5003() {
	$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_next").on("click", function(){
		if (isLoading == false) {
			web5007(); // 고객의소리 - 감사합니다 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 고객의소리 - 감사합니다 - 이전 페이지 클릭시 이벤트 설정
function web5004() {
	$(".web5000.board_box .tab_1_board.board_paging_controller_box .prev").on("click", function(){
		customer800701_page_reckoning = 1;

		var minimum_index = customer800701_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - 10;
		customer800701_index = prev_first_index;
		web5001();
	});
}

// 고객의소리 - 감사합니다 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function web5005() {
	$(".web5000.board_box .tab_1_board.board_paging_controller_box .best_prev").on("click", function(){
		customer800701_page_reckoning = 1;
		customer800701_index = 0;
		web5001();
	});
}


// 고객의소리 - 감사합니다 - 페이지 번호 클릭시 이벤트 설정
function web5006() {
	$(".web5000.board_box .tab_1_board.board_paging_controller_box ul.page_num li").on("click", function(){
		if (isLoading == false) {	
			$(".web5000.board_box .tab_1_board.board_paging_controller_box ul.page_num li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			customer800701_index = selected_index;
			web5001();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 고객의소리 - 감사합니다 - 페이지 맨 마지막 번호 알아내기
function web5007() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web5007",
			board_type:800701
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					customer800701_index = last_index;

					console.log("last_index = "+last_index);

					customer800701_page_reckoning = 1;
					web5001();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 고객의소리 - 감사합니다 검색버튼 클릭시 이벤트 설정하기
function web5008() {
	$(".web5000.board_box .tab_1_board .search_write_box .search_box .tab1_search_button").on("click", function(){
		var inputed_search_word = $.trim($("input[name=web5000_tab1_search]").val());


		console.log("검색 함수 실행");
		customer800701_index = 0;
		customer800701_search_word = inputed_search_word;
		customer800701_page_reckoning = 1;

		web5001(); // 고객의소리 - 감사합니다 가져오기
		
	});



	$("input[name=web5000_tab1_search]").keypress(function(event){
		if (event.keyCode == 13) {
			var inputed_search_word = $.trim($("input[name=web5000_tab1_search]").val());

			console.log("검색 함수 실행");
			customer800701_index = 0;
			customer800701_search_word = inputed_search_word;
			customer800701_page_reckoning = 1;

			web5001(); // 고객의소리 - 감사합니다 가져오기
		}
	});
}



var currentTabIndex = 0;

// 게시판 탭 버튼 이벤트 설정하기
function web5010() {
	$(".web5000.tab_button_box .common").on("click", function() {
		var tabIndex = Number($(this).attr("tab-index"));
		currentTabIndex = tabIndex;

		$(".web5000.tab_button_box .common.active").removeClass("active");
		$(this).addClass("active");

		$(".web5000.board_box .tab_board.show").removeClass("show");
		$(".web5000.tab_line_box .line.tab_btn_1").removeClass("tab_btn_1");
		$(".web5000.tab_line_box .line.tab_btn_2").removeClass("tab_btn_2");


		if (tabIndex == 0) {
			$(".web5000.board_box .tab_1_board").addClass("show");
			$(".web5000.tab_line_box .line").addClass("tab_btn_1");
		} else {
			$(".web5000.board_box .tab_2_board").addClass("show");
			$(".web5000.tab_line_box .line").addClass("tab_btn_2");
		}
	});
}





var customer800702_index = 0;
var customer800702_view_num = 10;
var customer800702_page_reckoning = 1;
var customer800702_search_word = '';

// 고객의소리 - 건의합니다 가져오기
function web5011() {
	console.log("건의합니다 가져오기");

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web5011",
			index:customer800702_index,
			view_num:customer800702_view_num,
			page_reckoning:customer800702_page_reckoning,
			board_type:800702,
			content_type:0,
			search_word:customer800702_search_word
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("건의합니다 data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					$(".web5000.board_box .tab_2_board > ul li").not(".title").remove();

					$.each(jsonObj.data, function(key, value){
						var suhc = value.suhc;
						var board_type = value.board_type;
						var content_type = value.content_type;
						var title = value.title;
						var member = value.member;
						var member_name = value.member_name;
						var datetime = value.datetime;
						var onlydate = datetime.split(" ")[0];
						var attach_file_flag = value.attach_file_flag;
						var view_index = value.view_index;
						var status = value.status;

						var attach_class = "";
						if (attach_file_flag == 1) {
							attach_class = "attach";
						}

						var text = '';
						text += '<li class="clearFix">';
						text += '	<div>';
						text += '		'+suhc;
						text += '	</div> ';
						text += '	<div class="left">';
						text += '		<a href="./web5200.php?pk='+suhc+'">'+title+'</a>';
						text += '	</div>';
						text += '	<div>';
						text += '		'+member_name;
						text += '	</div>';
						text += '	<div>';
						text += '		'+onlydate;
						text += '	</div>';
						text += '	<div class="'+attach_class+'">';
						text += '		';
						text += '	</div>';
						text += '	<div>';
						text += '		'+view_index;
						text += '	</div>';
						text += '</li>';

						$(".web5000.board_box .tab_2_board > ul").append(text);
					});


					if (customer800702_page_reckoning == 1) {
						var pagenum = jsonObj.pagenum;
						var nextpageflag = jsonObj.nextpageflag;

						// 이전 페이지 버튼 여부
						if (customer800702_index == 0) {
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_prev").css({"display":"none"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .prev").css({"display":"none"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .line.prev").css({"display":"none"});
						} else {
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_prev").css({"display":"block"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .prev").css({"display":"block"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .line.prev").css({"display":"block"});
						}

						// 다음 페이지 버튼 여부
						if (nextpageflag == 1) {
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_next").css({"display":"block"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .next").css({"display":"block"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .line.next").css({"display":"block"});	
						} else {
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_next").css({"display":"none"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .next").css({"display":"none"});
							$(".web5000.board_box .tab_2_board.board_paging_controller_box .line.next").css({"display":"none"});
						}


						$(".web5000.board_box .tab_2_board.board_paging_controller_box ul.page_num").empty();



						var first_page_index = customer800702_index;
						for (var ii=0; ii<10; ii++) {
							if (first_page_index % 10 == 0) {
								break;
							}
							first_page_index--;
						}

						for (var i=0; i<pagenum; i++) {
							var page_number = first_page_index + 1 + i;
							var text = '';
							console.log("customer800702_index = "+customer800702_index);
							console.log("page_number-1 = "+(page_number-1));
							if (customer800702_index == (page_number-1)) {
								text += '<li class="active" data-index="'+(page_number-1)+'">'+page_number+'</li>';
							} else {
								text += '<li data-index="'+(page_number-1)+'">'+page_number+'</li>';
							}
							$(".web5000.board_box .tab_2_board.board_paging_controller_box ul.page_num").append(text);
						}


						web5016(); // 고객의소리 - 건의합니다 페이징 - 페이지 번호 클릭시 이벤트 설정

						customer800702_page_reckoning = 0;
					}
				} else {
					console.log("글 없을경우");
					$(".web5000.board_box .tab_2_board > ul li").not(".title").remove();
					$(".web5000.board_box .tab_2_board > ul").append('<li class="not_board">게시글이 없습니다.</li>');
				

					$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_prev").css({"display":"none"});
					$(".web5000.board_box .tab_2_board.board_paging_controller_box .prev").css({"display":"none"});
					$(".web5000.board_box .tab_2_board.board_paging_controller_box .line").eq(0).css({"display":"none"});
					$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_next").css({"display":"none"});
					$(".web5000.board_box .tab_2_board.board_paging_controller_box .next").css({"display":"none"});
					$(".web5000.board_box .tab_2_board.board_paging_controller_box .line").eq(1).css({"display":"none"});

					$(".web5000.board_box .tab_2_board.board_paging_controller_box ul.page_num").empty();
					$(".web5000.board_box .tab_2_board.board_paging_controller_box ul.page_num").append('<li class="active">1</li>');
				}
			}
		},
		fail: function() {
			
		}
	});
}

// 고객의소리 - 건의합니다 - 다음 페이지 클릭시 이벤트 설정
function web5012() {
	$(".web5000.board_box .tab_2_board.board_paging_controller_box .next").on("click", function(){
		customer800702_page_reckoning = 1;

		var minimum_index = customer800702_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var next_first_index = minimum_index + 10;
		customer800702_index = next_first_index;
		web5011();
	});
}

// 고객의소리 - 건의합니다 - 맨 마지막 페이지로 가기 버튼 클릭시 이벤트 설정
function web5013() {
	$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_next").on("click", function(){
		if (isLoading == false) {
			web5017(); // 고객의소리 - 건의합니다 페이징 - 페이지 맨 마지막 번호 알아내기
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 고객의소리 - 건의합니다 - 이전 페이지 클릭시 이벤트 설정
function web5014() {
	$(".web5000.board_box .tab_2_board.board_paging_controller_box .prev").on("click", function(){
		customer800702_page_reckoning = 1;

		var minimum_index = customer800702_index;
		for (var i=0; i<10; i++) {
			if (minimum_index % 10 == 0) {
				break;
			}
			minimum_index--;
		}

		var prev_first_index = minimum_index - 10;
		customer800702_index = prev_first_index;
		web5011();
	});
}

// 고객의소리 - 건의합니다 - 맨 처음 페이지로 가기 버튼 클릭시 이벤트 설정
function web5015() {
	$(".web5000.board_box .tab_2_board.board_paging_controller_box .best_prev").on("click", function(){
		customer800702_page_reckoning = 1;
		customer800702_index = 0;
		web5011();
	});
}


// 고객의소리 - 건의합니다 - 페이지 번호 클릭시 이벤트 설정
function web5016() {
	$(".web5000.board_box .tab_2_board.board_paging_controller_box ul.page_num li").on("click", function(){
		if (isLoading == false) {
			$(".web5000.board_box .tab_2_board.board_paging_controller_box ul.page_num li.active").removeClass("active");
			$(this).addClass("active");


			var selected_index = Number($(this).attr("data-index"));

			customer800702_index = selected_index;
			web5011();
		} else {
			alert("작업이 진행중입니다.");
		}
	});
}

// 고객의소리 - 건의합니다 - 페이지 맨 마지막 번호 알아내기
function web5017() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web5017",
			board_type:800702
		},
		timeout: 2000,
		beforeSend: function() {
			isLoading = true;
		},
		complete: function() {
			isLoading = false;
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var last_index = jsonObj.last_index;
					customer800702_index = last_index;

					console.log("last_index = "+last_index);

					customer800702_page_reckoning = 1;
					web5011();
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 고객의소리 - 건의합니다 검색버튼 클릭시 이벤트 설정하기
function web5018() {
	$(".web5000.board_box .tab_2_board .search_write_box .search_box .tab2_search_button").on("click", function(){
		var inputed_search_word = $.trim($("input[name=web5000_tab2_search]").val());


		console.log("검색 함수 실행");
		customer800702_index = 0;
		customer800702_search_word = inputed_search_word;
		customer800702_page_reckoning = 1;

		web5011(); // 고객의소리 - 건의합니다 가져오기
		
	});



	$("input[name=web5000_tab2_search]").keypress(function(event){
		if (event.keyCode == 13) {
			var inputed_search_word = $.trim($("input[name=web5000_tab2_search]").val());

			console.log("검색 함수 실행");
			customer800702_index = 0;
			customer800702_search_word = inputed_search_word;
			customer800702_page_reckoning = 1;

			web5011(); // 고객의소리 - 건의합니다 가져오기
		}
	});
}



// 글쓰기 버튼 클릭했을 때 이벤트 설정하기
function web5020() {
	$(".web5000.board_box .tab_1_board .search_write_box .write_button").on("click", function(){
		location.href = "./web5100.php?tab_index="+currentTabIndex;
	});
	$(".web5000.board_box .tab_2_board .search_write_box .write_button").on("click", function(){
		location.href = "./web5100.php?tab_index="+currentTabIndex;
	});
}












// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}