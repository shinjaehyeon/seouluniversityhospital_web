$(document).ready(function(){
	setLoginButtonEvent(); // 로그인 버튼 클릭시 서버에 계정 정보 인증 요청
	setFacebookLoginButtonEvent(); // 페이스북으로 로그인 버튼 클릭시 이벤트 설정
});


// 로그인 버튼 클릭시 서버에 계정 정보 인증 요청
function setLoginButtonEvent() {
	$(".web1100.basic_login_form_box form .login").on("click", function() {
		var id = $(".web1100.basic_login_form_box form input[name=id]").val();
		var pw = $(".web1100.basic_login_form_box form input[name=pw]").val();

		$.ajax({
			type: "POST",
			url: "./outlet.php",
			data:{
				act:"web1110",
				id:id,
				pw:pw
			},
			timeout: 2000,
			beforeSend: function() {

			},
			complete: function() {
				
			},
			success: function(data) {
				console.log("data = "+data);

				if (isJSON2(data)) {
					var jsonObj = $.parseJSON(data);
					var result = jsonObj.result;

					if (result == "ok") {
						alert("로그인 되었습니다.");
						location.href = "./index.php";
					} else {
						alert("일치하는 정보가 없습니다.");
					}
				}
			},
			fail: function() {
				
			}
		});
	});


	$("input[name=id]").keypress(function(event){
		if (event.keyCode == 13) {
			var id = $(".web1100.basic_login_form_box form input[name=id]").val();
			var pw = $(".web1100.basic_login_form_box form input[name=pw]").val();

			$.ajax({
				type: "POST",
				url: "./outlet.php",
				data:{
					act:"web1110",
					id:id,
					pw:pw
				},
				timeout: 2000,
				beforeSend: function() {

				},
				complete: function() {
					
				},
				success: function(data) {
					console.log("data = "+data);

					if (isJSON2(data)) {
						var jsonObj = $.parseJSON(data);
						var result = jsonObj.result;

						if (result == "ok") {
							alert("로그인 되었습니다.");
							location.href = "./index.php";
						} else {
							alert("일치하는 정보가 없습니다.");
						}
					}
				},
				fail: function() {
					
				}
			});
		}
	});

	$("input[name=pw]").keypress(function(event){
		if (event.keyCode == 13) {
			var id = $(".web1100.basic_login_form_box form input[name=id]").val();
			var pw = $(".web1100.basic_login_form_box form input[name=pw]").val();

			$.ajax({
				type: "POST",
				url: "./outlet.php",
				data:{
					act:"web1110",
					id:id,
					pw:pw
				},
				timeout: 2000,
				beforeSend: function() {

				},
				complete: function() {
					
				},
				success: function(data) {
					console.log("data = "+data);

					if (isJSON2(data)) {
						var jsonObj = $.parseJSON(data);
						var result = jsonObj.result;

						if (result == "ok") {
							alert("로그인 되었습니다.");
							location.href = "./index.php";
						} else {
							alert("일치하는 정보가 없습니다.");
						}
					}
				},
				fail: function() {
					
				}
			});
		}
	});
}

// 페이스북으로 로그인 버튼 클릭시 이벤트 설정
function setFacebookLoginButtonEvent() {
	$(".web1100.basic_login_form_box .sns_login_button_box .facebook").on("click", function(){
		

	});
}




// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}