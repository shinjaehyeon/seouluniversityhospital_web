










$(document).ready(function(){
	web2301(); // 진료과 리스트 가져오기

	web2304(); // 의료진 이름 검색 버튼 클릭시 이벤트 설정하기
});

// 진료과 리스트 가져오기
function web2301() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2301"
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			// console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					$.each(jsonObj.data, function(key, value){
						var suhd = value.suhd;
						var department = value.department;

						var text = '';
						text += '<li department-pk="'+suhd+'">';
						text += '	<div class="title">';
						text += '		'+department;
						text += '	</div>';
						text += '	<ul class="doctor_list clearFix" department-pk="'+suhd+'">';
						text += '		';
						text += '	</ul>';
						text += '</li>';

						$("ul.doctor_list_area").append(text);
					});

					web2302(); // 의료진 리스트 가져오기

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}



var doctorPkArrayLength = 0;
var doctorPkArray = new Array();
var doctorII = 0;
var doctorThread = [0, 0];

// 의료진 리스트 가져오기
function web2302() {
	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2302"
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			// console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					
					$.each(jsonObj.data, function(key, value){
						var doctor = value.doctor;
						var name = value.name;
						var affiliation_department = value.affiliation_department;

						doctorPkArray.push(doctor);
						doctorPkArrayLength++;

						var text = '';
						text += '<li>';
						text += '	<a href="./web2400.php?pk='+doctor+'">';
						text += '		<div class="overlay_display">';
						text += '			상세정보 바로가기	';
						text += '		</div>';
						text += '		<div class="doctor_photo" doctor-pks="'+doctor+'">';
						text += '			';
						text += '		</div>';
						text += '		<div class="doctor_name setTopVirtualBox">';
						text += '			<div>';
						text += '				'+name;
						text += '			</div>';
						text += '		</div>';
						text += '	</a>';
						text += '</li>';

						$("ul[department-pk="+affiliation_department+"]").append(text);
					});

					web2303(); // 의료진 이미지 가져오기 

				} else {
					

				}
			}
		},
		fail: function() {
			
		}
	});
}

// 의료진 이미지 가져오기 
function web2303() {
	console.log("web2303 실행");
	var pks = doctorPkArray[doctorII];

	$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web2303",
			doctor_pk:pks
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			if (data == 'no') {

			} else {
				$("div[doctor-pks="+pks+"]").css({"background-image":"url("+data+")", "background-size":"cover"});
			}

			doctorII++;
			if (doctorII < doctorPkArrayLength) {
				web2303();
			}
		},
		fail: function() {
			
		}
	});
}

// 의료진 이름 검색 버튼 클릭시 이벤트 설정하기
function web2304() {
	$(".search_area_type_a .search_button").on("click", function(){
		$(".list_area_type_a .doctor_list_area > li.hide").removeClass("hide");
		$(".list_area_type_a .doctor_list_area li .doctor_list li.hide").removeClass("hide");

		var value = $.trim($("input[name=search_value]").val());

		$("ul.doctor_list_area li").each(function(){

			var flag = 0;
			$(this).find(".doctor_name").each(function(){
				var doctor_name = $.trim($(this).children("div").text());

				if (doctor_name.indexOf(value) >= 0) {
					flag++;
				} else {
					$(this).parent().parent().addClass("hide");
				}
			});

			if (flag == 0) {
				$(this).addClass("hide");
			}

		});
	});

	$("input[name=search_value]").keypress(function(event){
		if (event.keyCode == 13) {
			$(".list_area_type_a .doctor_list_area > li.hide").removeClass("hide");
			$(".list_area_type_a .doctor_list_area li .doctor_list li.hide").removeClass("hide");

			var value = $.trim($("input[name=search_value]").val());

			$("ul.doctor_list_area li").each(function(){

				var flag = 0;
				$(this).find(".doctor_name").each(function(){
					var doctor_name = $.trim($(this).children("div").text());

					if (doctor_name.indexOf(value) >= 0) {
						flag++;
					} else {
						$(this).parent().parent().addClass("hide");
					}
				});

				if (flag == 0) {
					$(this).addClass("hide");
				}

			});
		}
	});
}








// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}