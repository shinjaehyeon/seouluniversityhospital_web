$(document).ready(function(){
	web6110(); // 병원뉴스 상세정보 가져오기
});

// 병원뉴스 상세정보 가져오기
function web6110() {
	var pk = $("input[name=pk]").val();

	if (pk == '') {
		alert("잘못된 접근입니다.");
		location.href = './index.php';
	} else {

		$.ajax({
		type: "POST",
		url: "./outlet.php",
		data:{
			act:"web6110",
			pk:pk
		},
		timeout: 2000,
		beforeSend: function() {
			
		},
		complete: function() {
			
		},
		success: function(data) {
			console.log("data = "+data);

			if (isJSON2(data)) {
				var jsonObj = $.parseJSON(data);
				var result = jsonObj.result;

				if (result == "ok") {
					var admin_name = jsonObj.admin_name;
					var title = jsonObj.title;
					var content = jsonObj.content;
					var datetime = jsonObj.datetime;
					var attach_file_flag = jsonObj.attach_file_flag;
					var view_index = jsonObj.view_index;
					var attach_file_array = jsonObj.attach_file_array;

					var prev_pk = jsonObj.prev_pk;
					var prev_title = jsonObj.prev_title;
					var next_pk = jsonObj.next_pk;
					var next_title = jsonObj.next_title;

					$(".board_detail_page_type_A .title_box .title").html(title);
					$(".board_detail_page_type_A .title_box .infobox1 .writer").text(admin_name);
					$(".board_detail_page_type_A .title_box .infobox1 .view_index").text(view_index);
					$(".board_detail_page_type_A .title_box .infobox1 .datetime").text(datetime);

					$(".board_detail_page_type_A .attach_file_box ul.attach_file_list").empty();
					$.each(attach_file_array, function(key, value){
						var filename = value.filename;
						var fileurl = './suhnews/suhnews'+pk+'/'+filename;

						var text = '';
						text += '<li>';
						text += '	-';
						text += '	<a href="'+fileurl+'" download>';
						text += '		'+filename;
						text += '	</a>';
						text += '</li>';

						$(".board_detail_page_type_A .attach_file_box ul.attach_file_list").append(text);
					});

					if (attach_file_flag == 0) {
						$(".board_detail_page_type_A .attach_file_box .no_result").css({"display":"block"});
					}

					$(".board_detail_page_type_A .content_box").html(content);




					$(".board_detail_page_type_A .prevnextletter .next_board").empty();
					if (next_pk != 0) {
						$(".board_detail_page_type_A .prevnextletter .next_board").append('<a href="./web6100.php?pk='+next_pk+'">'+next_title+'</a>');
					} else {
						$(".board_detail_page_type_A .prevnextletter .next_board").append('<a href="#">다음글이 없습니다.</a>');
					}



					$(".board_detail_page_type_A .prevnextletter .prev_board").empty();
					if (prev_pk != 0) {
						$(".board_detail_page_type_A .prevnextletter .prev_board").append('<a href="./web6100.php?pk='+prev_pk+'">'+prev_title+'</a>');
					} else {
						$(".board_detail_page_type_A .prevnextletter .prev_board").append('<a href="#">이전글이 없습니다.</a>');
					}
				} else {

				}
			}
		},
		fail: function() {
			
		}
	});

	}
}






function htmlspecialchar_decode(str) {
	var aa = $("<span></span>");
	aa.html(str);
	return aa.text();
}








// json 인지 체크하는 함수
function isJSON2(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}