var isLoading = false;
var topButtonVisible = false;
$(document).ready(function(){
	// alert("여기 실행됨!4");
	setMainNaviEventListener(); // 메인 네비게이션 효과 설정
	setLogoutButtonEvent(); // 로그아웃 버튼 클릭시 이벤트 설정



	setBottomScrollTopButtonVisible(); // 일정 이상 아래로 스크롤 하면 top 버튼 보이게 하기
});

// 메인 네비게이션 효과 설정
function setMainNaviEventListener() {


	// 메인 메뉴에 마우스가 올라왔을 때
	$("ul.main_nav_box li a").on("mouseenter", function() {
		var menuIndex = Number($(this).attr("menu-index")); // 마우스 올린 메뉴의 인덱스 값 가져오기 
		var childNumber = menuIndex + 1; // 하위 메뉴 번호 설정

		// active 클래스가 있는 메뉴 박스를 찾아 클래스 제거하기
		$("ul.main_nav_box li a.active").removeClass("active");

		// 현재 마우스 올린 메뉴에 활성화 이벤트 주기
		$(this).addClass("active");

		// show 클래스가 있는 하위메뉴 박스를 찾아 클래스 제거하기
		$(".child_menu_box.show").removeClass("show");

		// 해당 하위메뉴 박스에 show 클래스 추가하기
		$(".menu_"+childNumber+"_child_menu_box").addClass("show");
	});

	// 메인 메뉴에 마우스가 벗어났을 때
	$("ul.main_nav_box li a").on("mouseleave", function() {
		var menuIndex = Number($(this).attr("menu-index")); // 마우스 벗어난 메뉴의 인덱스 값 가져오기 
		var childNumber = menuIndex + 1; // 하위 메뉴 번호 설정

		// active 클래스가 있는 메뉴 박스를 찾아 클래스 제거하기
		$("ul.main_nav_box li a.active").removeClass("active");

		// show 클래스가 있는 하위메뉴 박스를 찾아 클래스 제거하기
		$(".child_menu_box.show").removeClass("show");
	});

	// 하위 메뉴 1200px 박스에 마우스가 올라왔을 때
	$(".child_menu_1200_box").on("mouseenter", function() {
		var childMenuIndex = Number($(this).parent().attr("child-menu-index"));

		// 하위 메뉴박스에맞는 메뉴를 찾아 활성화 이벤트 주기
		$("ul.main_nav_box li a[menu-index="+childMenuIndex+"]").addClass("active");

		// show 클래스가 있는 하위메뉴 박스찾아서 클래스 제거하기
		$(".child_menu_box.show").removeClass("show");

		// 현재 마우스 올린 하위메뉴의 부모 박스를 보이게 하기
		$(this).parent().addClass("show");
	});

	// 하위 메뉴 1200px 박스에 마우스가 벗어났을 때
	$(".child_menu_1200_box").on("mouseleave", function() {

		// active 클래스가 있는 메뉴 박스를 찾아 클래스 제거하기
		$("ul.main_nav_box li a.active").removeClass("active");

		// show 클래스가 있는 하위메뉴 박스를 찾아 클래스 제거하기
		$(".child_menu_box.show").removeClass("show");
	});
}

// 로그아웃 버튼 클릭시 이벤트 설정
function setLogoutButtonEvent() {
	$("header.header ul.header_small_menu_box .logout_button").on("click", function(){
		$.ajax({
			type: "POST",
			url: "./outlet.php",
			data:{
				act:"web0010"
			},
			timeout: 2000,
			beforeSend: function() {
				
			},
			complete: function() {
				
			},
			success: function(data) {
				console.log("data = "+data);

				if (isJSON(data)) {
					var jsonObj = $.parseJSON(data);
					var result = jsonObj.result;

					if (result == "ok") {
						alert("로그아웃 되었습니다.");
						location.href = "./index.php";
					} else {
						alert("로그아웃 중 오류가 발생하였습니다.");
					}
				}
			},
			fail: function() {
				
			}
		});
	});
}


// 일정 이상 아래로 스크롤 하면 top 버튼 보이게 하기

function setBottomScrollTopButtonVisible() {
	$(".goTopButton").on("click", function(){
		$('html,body').animate({scrollTop: 0}, 300);
	});

	$(window).scroll(function() {
		var scrollTop = $(this).scrollTop();

		console.log("scrollTop = "+scrollTop);

		if (scrollTop >= 300) {
			if (topButtonVisible == false) {
				$(".goTopButton").css({"display":"block", "opacity":"0"});
				$(".goTopButton").stop().animate({"opacity":"1"}, 400, function(){

				});


				topButtonVisible = true;
			}
		} else {
			if (topButtonVisible == true) {

				$(".goTopButton").stop().animate({"opacity":"0"}, 400, function(){
					$(".goTopButton").css({"display":"none"});
				});


				topButtonVisible = false;
			}
		}
	});
}









// 이메일 형식인지 체크하는 함수
function inValidEmail(str) {
	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if (str.match(regExp) != null) {
		return true;
	} else {
		return false;
	}
}

// json 인지 체크하는 함수
function isJSON(text) {
	var IS_JSON = true;
	try {
		var json = $.parseJSON(text);
	} catch(err) {
		IS_JSON = false;
	} 
	return IS_JSON;
}

function attachZero(number) {

	var return_text = number;
	if (Number(number) < 10) {
		return_text = "0"+number;
	}
	return return_text;
}

function cutString(str, cutting) {
	if (str.length >= cutting) {
		return str.substring(0, cutting-1)+"...";
	} else {
		return str;
	}
}