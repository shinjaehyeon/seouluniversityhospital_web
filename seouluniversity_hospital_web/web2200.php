<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 진료과 상세페이지" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 진료과 상세페이지</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web2200.js"></script>
	</head>
	<body id="body" page-code="web2200">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>


		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>


		<input type="hidden" name="pk" value="<?php echo $_REQUEST['pk']; ?>" />




		<div class="sub_big_title_box web2100">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					진료과 상세정보
				</div>
				<div class="comment">
					서울대학교병원의 다양한 진료과를 소개합니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 진료과/의료진 > <a href="./web2100.php">진료과 소개</a> > 진료과 상세정보
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>








		<div class="department_title web2200">
			내분비내과
		</div>

		<section class="department_image_box" style="height:400px;">
			
		</section>




		<section class="description_area web2200 clearFix">
			<div class="left">
				<div class="title">
					소개/설명
				</div>
				<section class="department_description">
						
				</section>
			</div>
			<div class="right">
				<div class="title">
					소속 의료진
				</div>
				<section class="chain_doctor_list">
					<ul class="clearFix">
						
					</ul>
				</section>
			</div>
		</section>

		

		
		


		<?php include "footer.php"; ?>
	</body>
</html>