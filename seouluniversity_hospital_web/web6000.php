<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 병원뉴스" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 병원뉴스</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web6000.js"></script>
	</head>
	<body id="body" page-code="web6000">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>



		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>



		<div class="sub_big_title_box web6000">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					병원뉴스
				</div>
				<div class="comment">
					서울대학교병원의 다양한 소식들을 전해드립니다. 
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 병원소개 > 병원소식 > <a href="./web6000.php">병원뉴스</a>
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>






		<section class="web6000 recent_news_box">
			<div class="title">
				최근 뉴스
			</div>
			<ul class="list clearFix">
				<!-- <li class="clearFix">
					<div class="image">
						
					</div>
					<div class="text setTopVirtualBox">
						<div class="top">
							대한소아신경외과학회,
							소아신경외과..
						</div>
						<div class="bottom">
							대한소아신경외과학회는 5월 
							18일, 학회 창립 30주년에 맞춰 
							소아신경외과학 한글교과..
						</div>
					</div>
				</li> -->
			</ul>
		</section>




		<section class="web6000 board_box">
			<ul class="board500301">
				<li class="board_title clearFix">
					<div>
						번호
					</div>
					<div>
						제목
					</div>
					<div>
						작성일
					</div>
					<div>
						첨부파일
					</div>
					<div>
						조회수
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
				<li class="clearFix">
					<div class="center">
						2018
					</div>
					<div class="padding_left_30">
						<a href="#">대한소아신경외과학회, 소아신경외과학 교과서 개정판 발행</a>
					</div>
					<div class="center">
						2018-05-18
					</div>
					<div class="attach_file">
						
					</div>
					<div class="center">
						34
					</div>
				</li>
			</ul>
		</section>




		<section class="web6000 search_box">
			<div class="allboard_search_box clearFix">
				<input type="text" name="web6000_search" />
				<div class="tab1_search_button allboard_search_button">
					
				</div>
			</div>
		</section>



		<section class="allboard_paging_controller_box">
			<div class="clearFix">
				<div class="best_prev">
					<<
				</div>
				<div class="line prev">

				</div>
				<div class="prev">
					<
				</div>
				<ul class="page_num clearFix">
					<li class="active">
						1
					</li>
					<li>
						2
					</li>
				</ul>
				<div class="next">
					>
				</div>
				<div class="line next">

				</div>
				<div class="best_next">
					>>
				</div>
			</div>
		</section>




		<?php include "footer.php"; ?>
	</body>
</html>