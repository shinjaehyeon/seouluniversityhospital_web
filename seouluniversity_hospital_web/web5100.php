<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 고객의소리 등록" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 고객의소리 등록</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web5100.js"></script>
	</head>
	<body id="body" page-code="web5100">
		<?php 
			session_start();

			if ($_SESSION['is_login'] != 'ok') {
				echo '
				<script>
					window.alert("로그인 후 이용 가능합니다.");
					history.back(1);
				</script>
				';
				exit;
			}
		?>

		
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>


		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>



		<input type="hidden" name="first_tab_index" value="<?php echo $_REQUEST['tab_index']; ?>" />

		



		<div class="sub_big_title_box web5100">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					고객의소리 등록
				</div>
				<div class="comment">
					고객의소리 등록화면 입니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 고객참여 > 고객의소리 > <a href="./web5000.php">고객의소리 게시판</a> > 등록
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>






		<section class="web5100 form_box">
			<form name="web5100form" action="./outlet.php" method="post" enctype="multipart/form-data">
				<input type="hidden" name="act" value="web5121" />
				<input type="hidden" name="member" value="<?php echo $_SESSION['user_primarykey']; ?>" />
				<ul>
					<li class="clearFix">
						<div class="title common">
							분류
						</div>
						<div class="content common">
							<select name="board_type" class="select_style">
								<option value="800701"<?php if ($_REQUEST['tab_index'] == 0) { echo ' selected="selected"'; } ?>>감사합니다</option>
								<option value="800702"<?php if ($_REQUEST['tab_index'] == 1) { echo ' selected="selected"'; } ?>>건의합니다</option>
							</select>
						</div>
						<div class="common" style="width:57px;height:10px;">

						</div>
						<div class="title common">
							종류
						</div>
						<div class="content common">
							<select name="content_type" class="select_style">
								<option value="800801">의료진 칭찬 및 감사내용</option>
								<option value="800802">간호사 칭찬 및 감사내용</option>
								<option value="800803">환자 칭찬 및 감사내용</option>
								<option value="800804">기타 내용</option>

								<!-- 
								<option value="800901">시설 관련</option>
								<option value="800902">진료 관련</option>
								<option value="800903">예약 관련</option>
								<option value="800904">기타 내용</option> 
								-->
							</select>
						</div>
						<div class="common" style="width:57px;height:10px;">

						</div>
						<div class="title common">
							첨부파일
						</div>
						<div class="content common">
							<input class="file_style" type="file" name="attach_file[]" multiple="multiple" />
						</div>
						
					</li>
					<li class="clearFix">
						<div class="title common">
							제목
						</div>
						<div class="content common">
							<input type="text" class="input_text_style" name="title" />
						</div>
					</li>
					<li class="clearFix contents">
						<div class="title common">
							내용
						</div>
						<div class="content common">
							<textarea name="content" class="textarea_style"></textarea>
						</div>
					</li>
				</ul>
				<div class="submit_button">
					등록하기
				</div>
			</form>
		</section>








		<?php include "footer.php"; ?>
	</body>
</html>