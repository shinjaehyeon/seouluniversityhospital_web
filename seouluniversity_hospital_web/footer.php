<footer class="footer">
	<div class="footer_top_box">
		<div class="footer_top_menu_box setTopVirtualBox">
			<ul class="footer_top_menu clearFix">
				<li>
					<a href="#">
						어린이병원
					</a>
				</li>
				<li class="decoration">

				</li>
				<li>
					<a href="#">
						암병원
					</a>
				</li>
				<li>
					<a href="#">
						분당서울대병원
					</a>
				</li>
				<li class="decoration">

				</li>
				<li>
					<a href="#">
						보라매병원
					</a>
				</li>
				<li class="decoration">

				</li>
				<li>
					<a href="#">
						강남센터
					</a>
				</li>
				<li class="decoration">

				</li>
				<li>
					<a href="#">
						서울대학교의과대학
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="footer_bottom_box setTopVirtualBox">
		<div class="footer_bottom_box_1200 setTopVirtualBox">
			<ul class="footer_bottom_menu clearFix">
				<li>
					<a href="#">
						비급여진료비용
					</a>
				</li>
				<li>
					<a href="#">
						병원보퀴즈
					</a>
				</li>
				<li>
					<a href="#">
						환자권리장전
					</a>
				</li>
				<li>
					<a href="#">
						이용약관
					</a>
				</li>
				<li>
					<a href="#">
						개인정보처리방침
					</a>
				</li>
				<li>
					<a href="#">
						정보공개
					</a>
				</li>
				<li>
					<a href="#">
						정보무단수집거부공개
					</a>
				</li>
				<li>
					<a href="#">
						뷰어다운로드
					</a>
				</li>
				<li>
					<a href="#">
						장례식장
					</a>
				</li>
			</ul>
			<div class="footer_logo_box">
				<img src="./images/logo_black.png" alt="풋터 로고" title="풋터 로고" />
			</div>
			<div class="footer_addr">
				주소 : 03080 서울특별시 종로구 대학로 101(연건동 28)
			</div>
			<div class="footer_tel">
				대표전화 : 1588-5700
			</div>
			<div class="footer_copyright">
				COPYRIGHT 2018 SEOUL NATIONAL UNIVERSITY HOSPITAL. ALL RIGHTS RESERVED
			</div>
			<div class="footer_award_box">
				<ul class="footer_award clearFix">
					<li class="setTopVirtualBox">
						<div class="image">
							<img src="./images/award_icon_1.png" alt="상 관련 아이콘" title="상 관련 아이콘" />
						</div>
						<div class="content setTopVirtualBox">
							<div class="top">
								보건복지부 제 1호
							</div>
							<div class="bottom">
								인증의료기관
							</div>
						</div>
					</li>
					<li class="setTopVirtualBox">
						<div class="image">
							<img src="./images/award_icon_2.png" alt="상 관련 아이콘" title="상 관련 아이콘" />
						</div>
						<div class="content setTopVirtualBox">
							<div class="top">
								브랜드파워
							</div>
							<div class="bottom">
								18년 연속 1위 
							</div>
						</div>
					</li>
					<li class="setTopVirtualBox">
						<div class="image">
							<img src="./images/award_icon_3.png" alt="상 관련 아이콘" title="상 관련 아이콘" />
						</div>
						<div class="content setTopVirtualBox">
							<div class="top">
								국가브랜드
							</div>
							<div class="bottom">
								경쟁력 지수 1위
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>