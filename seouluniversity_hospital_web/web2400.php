<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 의료진 상세페이지" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 의료진 상세페이지</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web2400.js"></script>
	</head>
	<body id="body" page-code="web2400">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>


		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>


		<input type="hidden" name="pk" value="<?php echo $_REQUEST['pk']; ?>" />




		<div class="sub_big_title_box web2300">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					의료진 상세정보
				</div>
				<div class="comment">
					서울대학교병원의 다양한 의료진을 소개합니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 진료과/의료진 > <a href="./web2300.php">의료진 소개</a> > 의료진 상세정보
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>








		<div class="doctor_name_title web2400">
			김진원
		</div>

		<section class="doctor_image_box_egg">
			<section class="doctor_image_box web2400">
				
			</section>
		</section>

		<section class="small_info web2400">
			<ul class="clearFix">
				<li class="setTopVirtualBox">
					<div class="title">
						의료진 한마디
					</div>
					<div class="content doctor_one_word">
						의료진 한마디
					</div>
				</li>
				<li class="setTopVirtualBox">
					<div class="title">
						소속 진료과
					</div>
					<div class="content doctor_chain_department">
						의료진 한마디
					</div>
				</li>
			</ul>
		</section>





		<section class="school_list_area list_type_g">
			<div class="title">
				학력
			</div>
			<ul class="school_list list">
				<!-- <li class="clearFix">
					<div class="period">
						1994~1998
					</div>
					<div class="content">
						서울대학교 입학
					</div>
				</li> -->
				
			</ul>
		</section>






		<section class="career_list_area list_type_g">
			<div class="title">
				경력 및 연수
			</div>
			<ul class="career_list list">
				<!-- <li class="clearFix">
					<div class="period">
						1994~1998
					</div>
					<div class="content">
						서울대학교 입학
					</div>
				</li> -->
				
			</ul>
		</section>



		
		


		<?php include "footer.php"; ?>
	</body>
</html>