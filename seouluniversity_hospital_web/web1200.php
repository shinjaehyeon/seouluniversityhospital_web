<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 회원가입" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 회원가입</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web1200.js"></script>
		
	</head>
	<script language="javascript">
		// opener관련 오류가 발생하는 경우 아래 주석을 해지하고, 사용자의 도메인정보를 입력합니다. ("팝업API 호출 소스"도 동일하게 적용시켜야 합니다.)
		//document.domain = "abc.go.kr";

		function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr,jibunAddr,zipNo,admCd,rnMgtSn,bdMgtSn,detBdNmList,bdNm,bdKdcd,siNm,sggNm,emdNm,liNm,rn,udrtYn,buldMnnm,buldSlno,mtYn,lnbrMnnm,lnbrSlno,emdNo){
			document.getElementById('roadFullAddr').value = roadFullAddr;
			document.getElementById('roadAddrPart1').value = roadAddrPart1;
			document.getElementById('addrDetail').value = addrDetail;
			document.getElementById('roadAddrPart2').value = roadAddrPart2;
			document.getElementById('engAddr').value = engAddr;
			document.getElementById('jibunAddr').value = jibunAddr;
			document.getElementById('zipNo').value = zipNo;
			document.getElementById('admCd').value = admCd;
			document.getElementById('rnMgtSn').value = rnMgtSn;
			document.getElementById('bdMgtSn').value = bdMgtSn;
			document.getElementById('detBdNmList').value = detBdNmList;
			//** 2017년 2월 제공항목 추가 **/
			document.getElementById('bdNm').value = bdNm;
			document.getElementById('bdKdcd').value = bdKdcd;
			document.getElementById('siNm').value = siNm;
			document.getElementById('sggNm').value = sggNm;
			document.getElementById('emdNm').value = emdNm;
			document.getElementById('liNm').value = liNm;
			document.getElementById('rn').value = rn;
			document.getElementById('udrtYn').value = udrtYn;
			document.getElementById('buldMnnm').value = buldMnnm;
			document.getElementById('buldSlno').value = buldSlno;
			document.getElementById('mtYn').value = mtYn;
			document.getElementById('lnbrMnnm').value = lnbrMnnm;
			document.getElementById('lnbrSlno').value = lnbrSlno;
			//** 2017년 3월 제공항목 추가 **/
			document.getElementById('emdNo').value = emdNo;
		}

		function goPopup(){
			// 주소검색을 수행할 팝업 페이지를 호출합니다.
			// 호출된 페이지(jusoPopup_utf8.php)에서 실제 주소검색URL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)를 호출하게 됩니다.
			var pop = window.open("./jusoPopup_utf8.php","pop","width=570,height=420, scrollbars=yes, resizable=yes"); 
			
			// 모바일 웹인 경우, 호출된 페이지(jusoPopup_utf8.php)에서 실제 주소검색URL(http://www.juso.go.kr/addrlink/addrMobileLinkUrl.do)를 호출하게 됩니다.
		    //var pop = window.open("/jusoPopup_utf8.php","pop","scrollbars=yes, resizable=yes"); 
		}
	</script>
	<body id="body" page-code="web1200">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>


		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>


		<div class="sub_big_title_box web1200">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					회원가입
				</div>
				<div class="comment">
					서울대학교병원 회원이 되시는 것을 환영합니다. 
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					홈 > 회원가입
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>








		<section class="web1200 terms_box">
			<ul class="clearFix">
				<li>
					<div class="title">
						서울대학교병원 서비스 약관 
					</div>
					<div class="terms_content">
						<?php
							$fp = fopen("./terms/service_terms.txt","r"); 
							$fr = fread($fp, filesize("./terms/service_terms.txt")); 
							fclose($fp);

							$fr = preg_replace('/\r\n|\r|\n/','<br />', $fr);

							echo $fr;
						?>
					</div>
					<div class="check_box clearFix">
						<input id="service_t" class="checkbox" type="checkbox" name="service_t" />
						<label for="service_t">위 서비스 약관에 동의합니다.</label> 
					</div>
				</li>
				<li>
					<div class="title">
						서울대학교병원 개인정보취급방침 
					</div>
					<div class="terms_content">
						<?php
							$fp = fopen("./terms/personal_terms.txt","r"); 
							$fr = fread($fp, filesize("./terms/personal_terms.txt")); 
							fclose($fp);

							$fr = preg_replace('/\r\n|\r|\n/','<br />', $fr);

							echo $fr;
						?>
					</div>
					<div class="check_box clearFix">
						<input id="personal_t" class="checkbox" type="checkbox" name="personal_t" />
						<label for="personal_t">위 개인정보취급방침에 동의합니다.</label> 
					</div>
				</li>
			</ul>
		</section>








		<section class="typeA join_form" style="margin-bottom:70px;">
			<div class="title">
				회원가입 정보 입력
			</div>
			<form class="form_box" name="rdnAddr">
				<ul class="form_ul" style="margin-bottom:28px;">
					<li>
						<div class="title">
							이름
						</div>
						<div class="content">
							<input class="inputbox_style" type="text" name="name" style="width:303px;height:43px;margin-top:18px;" />
							<div class="error_text">
								※ 이름을 입력해주세요.
							</div>
						</div>
					</li>
					<li>
						<div class="title">
							성별
						</div>
						<div class="content">
							<select class="inputbox_style" name="gender" style="width:303px;height:43px;margin-top:18px;">
								<option value="800501">남자</option>
								<option value="800502">여자</option>
							</select>
						</div>
					</li>
					<li>
						<div class="title">
							통신사
						</div>
						<div class="content">
							<select class="inputbox_style" name="theMobileCompany" style="width:303px;height:43px;margin-top:18px;">
								<option value="700101">LG U+</option>
								<option value="700102">SKT</option>
								<option value="700103">KT</option>
								<option value="700104">LG U+ 알뜰폰</option>
								<option value="700105">SKT 알뜰폰</option>
								<option value="700106">KT 알뜰폰</option>
							</select>
						</div>
					</li>
					<li>
						<div class="title">
							휴대폰							
						</div>
						<div class="content">
							<select class="inputbox_style" type="number" name="phoneFirst" style="width:131px;height:43px;margin-top:18px;">
								<option value="010">010</option>
								<option value="011">011</option>
								<option value="017">017</option>
							</select>
							<div style="float:left;margin:30px 10px 0px 10px;font-size:16px;color:#333333;font-weight:bold;">
								-
							</div>
							<input class="inputbox_style" type="text" name="phoneSecond" style="width:131px;height:43px;margin-top:18px;" />
							<div style="float:left;margin:30px 10px 0px 10px;font-size:16px;color:#333333;font-weight:bold;">
								-
							</div>
							<input class="inputbox_style" type="text" name="phoneThird" style="width:131px;height:43px;margin-top:18px;" />
							<div class="error_text">
								※ 휴대폰 번호를 입력해주세요.
							</div>
						</div>
					</li>
					<li>
						<div class="title">
							이메일(아이디)							
						</div>
						<div class="content">
							<input class="inputbox_style" type="text" name="emailBefore" style="width:180px;height:43px;margin-top:18px;" />
							<div style="float:left;margin:30px 10px 0px 10px;font-size:16px;color:#333333;font-weight:bold;">
								@
							</div>
							<select id="emailAfter1" class="inputbox_style" type="text" name="emailAfter1" style="width:180px;height:43px;margin-top:18px;">
								<option value="@naver.com" selected="true">naver.com</option>
								<option value="@daum.net">daum.net</option>
								<option value="@hanmail.net">hanmail.net</option>
								<option value="@nate.com">nate.com</option>
								<option value="@google.com">google.com</option>
								<option value="0">기타 (직접입력)</option>
							</select>
							<input class="inputbox_style" type="text" name="emailAfter2" style="width:180px;height:43px;margin-top:18px;margin-left:10px;display:none;" />
							<div id="overlap_check_button" class="buttontype1" style="margin-left:20px;">
								중복확인
							</div>
							<div class="error_text">
								※ 이메일을 입력해주세요.
							</div>
							<div class="error_text">
								※ 이메일 중복확인을 해주세요.
							</div>
							<div class="error_text" style="color:#008ace;">
								※ 이용가능한 이메일 입니다.
							</div>
							<div class="error_text">
								※ 이메일 양식에 맞지 않습니다.
							</div>
						</div>
					</li>
					<li>
						<div class="title">
							비밀번호
						</div>
						<div class="content">
							<input class="inputbox_style" type="password" name="pw" style="width:303px;height:43px;margin-top:18px;" />
							<div class="error_text">
								※ 비밀번호를 입력해주세요.
							</div>
							<div class="error_text">
								※ 비밀번호와 비밀번호 확인이 일치하지 않습니다.
							</div>
						</div>
					</li>
					<li>
						<div class="title">
							비밀번호 확인
						</div>
						<div class="content">
							<input class="inputbox_style" type="password" name="pwcheck" style="width:303px;height:43px;margin-top:18px;" />
							<div class="error_text">
								※ 비밀번호 확인을 입력해주세요.
							</div>
							<div class="error_text">
								※ 비밀번호와 비밀번호 확인이 일치하지 않습니다.
							</div>
						</div>
					</li>
					<li>
						<div class="title">
							생년월일							
						</div>
						<div class="content">
							<input class="inputbox_style" type="number" name="birthdayYear" style="width:131px;height:43px;margin-top:18px;" min="1" />
							<div style="float:left;margin:30px 10px 0px 10px;font-size:16px;color:#333333;font-weight:bold;">
								년
							</div>
							<input class="inputbox_style" type="number" name="birthdayMonth" style="width:80px;height:43px;margin-top:18px;" min="1" />
							<div style="float:left;margin:30px 10px 0px 10px;font-size:16px;color:#333333;font-weight:bold;">
								월
							</div>
							<input class="inputbox_style" type="number" name="birthdayDate" style="width:80px;height:43px;margin-top:18px;" min="1" />
							<div style="float:left;margin:30px 10px 0px 10px;font-size:16px;color:#333333;font-weight:bold;">
								일
							</div>
							<div class="error_text">
								※ 생년월일을 전부 입력해주세요.
							</div>
							<div class="error_text">
								※ 생년월일이 유효한 날짜가 아닙니다.
							</div>
						</div>
					</li>
					<li>
						<div class="title">
							혈액형							
						</div>
						<div class="content">
							<select class="inputbox_style" type="number" name="bloodType" style="width:131px;height:43px;margin-top:18px;">
								<option value="600101">O형</option>
								<option value="600102">A형</option>
								<option value="600103">B형</option>
								<option value="600104">AB형</option>
							</select>
						</div>
					</li>
					<li style="height:205px;">
						<div class="title">
							주소 (선택)
						</div>
						<div class="content" style="height:205px;">
							<div class="clearFix">
								<input id="zipNo" class="inputbox_style" type="number" name="postNumber" style="width:160px;height:43px;margin-top:18px;" readonly="readonly" />
								<div id="post_find_button" class="buttontype1" style="margin-left:20px;" onclick="goPopup();">
									우편번호검색
								</div>
							</div>
							<div class="clearFix">
								<input id="roadAddrPart1" class="inputbox_style" type="text" name="basicAddr" style="width:517px;height:43px;margin-top:18px;" readonly="readonly" />
							</div>
							<div class="clearFix">
								<input id="addrDetail" class="inputbox_style" type="text" name="detailAddr" style="width:517px;height:43px;margin-top:18px;" />
							</div>
						</div>
					</li>
				</ul>
				<div class="submit_button">
					회원가입
				</div>


				<div class="hide">
					<input id ="roadFullAddr">
					<!-- <input id ="roadAddrPart1"> -->
					<!-- <input id ="addrDetail"> -->
					<input id ="roadAddrPart2">
					<input id ="engAddr">
					<input id ="jibunAddr">
					<!-- <input id ="zipNo"> -->
					<input id ="admCd">
					<input id ="rnMgtSn">
					<input id ="bdMgtSn">
					<input id ="detBdNmList">
					
					<input id ="bdNm">
					<input id ="bdKdcd">
					<input id ="siNm">
					<input id ="sggNm">
					<input id ="emdNm">
					<input id ="liNm">
					<input id ="rn">
					<input id ="udrtYn">
					<input id ="buldMnnm">
					<input id ="buldSlno">
					<input id ="mtYn">
					<input id ="lnbrMnnm">
					<input id ="lnbrSlno">
					<input id ="emdNo">
				</div>
			</form>
		</section>	




	

		<?php include "footer.php"; ?>
	</body>
</html>