<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 N의학정보" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - N의학정보</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web4000.js"></script>
	</head>
	<body id="body" page-code="web4000">
		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>


		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>




		<div class="sub_big_title_box web4000">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					N의학정보
				</div>
				<div class="comment">
					여러가지 질병 및 의학에 대한 정보를 얻을 수 있습니다.
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					<a href="./index.php">홈</a> > 건강정보 > 질병/의학정보 > <a href="./web4000.php">N의학정보</a>
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>





		<section class="today_medical_information_box">
			<div class="title">
				오늘의 의학정보
			</div>
			<ul class="list clearFix">
				<!-- <li data-pk="">
					<a href="#" class="setTopVirtualBox">
						<div class="ko common">
							쯔쯔가무시
						</div>
						<div class="en common">
							[scrub typhus]
						</div>
						<div class="description">
							오리엔티아 쯔쯔가무시균에 의해 
							발생하는 급성 열성 질환
						</div>
					</a>
				</li> -->
			</ul>
		</section>


		<section class="web4000 search_box">
			<input type="text" name="web4000search" placeholder="검색어를 입력하세요." />
			<div class="search_button">

			</div>
		</section>

		<section class="web4000 search_filter_box">
			<ul class="clearFix top">
				<li class="all active" data-value="0">
					전체
				</li>	
				<li data-value="1">
					ㄱ
				</li>
				<li data-value="2">
					ㄴ
				</li>
				<li data-value="3">
					ㄷ
				</li>
				<li data-value="4">
					ㄹ
				</li>
				<li data-value="5">
					ㅁ
				</li>
				<li data-value="6">
					ㅂ
				</li>
				<li data-value="7">
					ㅅ
				</li>
				<li data-value="8">
					ㅇ
				</li>
				<li data-value="9">
					ㅈ
				</li>
				<li data-value="10">
					ㅊ
				</li>
				<li data-value="11">
					ㅋ
				</li>
				<li data-value="12">
					ㅊ
				</li>
				<li data-value="13">
					ㅍ
				</li>
				<li data-value="14">
					ㅎ
				</li>
			</ul>
			<ul class="clearFix bottom">
				<li data-value="15">
					A
				</li>
				<li data-value="16">
					B
				</li>
				<li data-value="17">
					C
				</li>
				<li data-value="18">
					D
				</li>
				<li data-value="19">
					E
				</li>
				<li data-value="20">
					F
				</li>
				<li data-value="21">
					G
				</li>
				<li data-value="22">
					H
				</li>
				<li data-value="23">
					I
				</li>
				<li data-value="24">
					J
				</li>
				<li data-value="25">
					K
				</li>
				<li data-value="26">
					L
				</li>
				<li data-value="27">
					M
				</li>
				<li data-value="28">
					N
				</li>
				<li data-value="29">
					O
				</li>
				<li data-value="30">
					P
				</li>
				<li data-value="31">
					Q
				</li>
				<li data-value="32">
					R
				</li>
				<li data-value="33">
					S
				</li>
				<li data-value="34">
					T
				</li>
				<li data-value="35">
					U
				</li>
				<li data-value="36">
					V
				</li>
				<li data-value="37">
					W
				</li>
				<li data-value="38">
					X
				</li>
				<li data-value="39">
					Y
				</li>
				<li data-value="40">
					Z
				</li>
			</ul>
		</section>



		<section class="web4000 all_board_num clearFix">
			<span class="left">
				총 게시물
			</span>
			<span class="right">
				1,235
			</span>
		</section>

		<section class="web4000 board_list">
			<ul>
				<?php for ($i=0; $i<2; $i++) { ?>
				<li class="setTopVirtualBox">
					<a href="#" class="setTopVirtualBox">
						<div class="title">
							자율신경 실조증 [autonomic dysfunction]
						</div>
						<div class="description">
							자율신경계는 내분비계와 더불어 심혈관, 호흡, 소화, 비뇨기 및 생식기관, 체온조절계, 동공 조절 등의 기능을 조절해 신체의 항상성을 유지하게 하는 역할을 한다. <br /><br />

							대뇌반구 수준과 뇌줄기 수준, 척수 수준과 말초신경 수준으로 그 구조를 나눌 수 있으며, 특히 말초신경 수준에서 <br />
							말초의 자율신경계는 서로 대항 작용을 하는 교감 신경계와 부교감 신경계로 구성된다.
						</div>
					</a>
				</li>
				<?php } ?>
			</ul>
		</section>


		<section class="web4000 board_paging_controller_box">
			<div class="clearFix">
				<div class="best_prev">
					<<
				</div>
				<div class="line">

				</div>
				<div class="prev">
					<
				</div>
				<ul class="page_num clearFix">
					<li class="active">
						1
					</li>
					<li>
						2
					</li>
				</ul>
				<div class="next">
					>
				</div>
				<div class="line">

				</div>
				<div class="best_next">
					>>
				</div>
			</div>
		</section>




		<?php include "footer.php"; ?>

	</body>
</html>