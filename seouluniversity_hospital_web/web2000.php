<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="subject" content="서울대학교병원" />
		<meta name="title" content="서울대학교병원 진료예약신청" />
		<meta name="author" content="shin jae hyeon" />
		<meta name="keywords" content="서울대학교병원, 서울대학교 병원, 서울대, 서울대병원, 대학병원, 대학교병원, 병원, seoul, university hospital, hospital, suh, seoul university hospital" />

		<title>서울대학교 병원 - 진료예약신청</title>

		<link href="./css/style.css" rel="stylesheet" type="text/css" />

		<script src="./js/jquery-3.3.1.min.js"></script>
		<script src="./js/jquery-ui.min.js"></script>
		<script src="./js/common.js"></script>
		<script src="./js/web2000.js"></script>
	</head>
	<body id="body" page-code="web2000">
		<?php 
			session_start();

			if ($_SESSION['is_login'] != 'ok') {
				echo '
				<script>
					window.alert("로그인 후 이용 가능합니다.");
					history.back(1);
				</script>
				';
				exit;
			}
		?>

		<?php include "header.php"; ?>
		<?php include "main_nav.php"; ?>




		<div class="goTopButton setTopVirtualBox">
			<div>↑</div>
			<div>TOP</div>
		</div>




		<div class="sub_big_title_box web2000">
			<div class="black_shading_effect">

			</div>
			<div class="sub_big_title_box_1200 setTopVirtualBox">
				<div class="title">
					진료예약신청
				</div>
				<div class="comment">
					인터넷을 통해 온라인으로 진료예약을 하실수 있습니다.   
				</div>
			</div>
		</div>


		<div class="current_position_and_others">
			<div class="current_position_and_others_1200">
				<div class="current_position">
					홈 > 진료안내 > 진료예약신청
				</div>
				<div class="others clearFix">
					<div class="print" onclick="window.print();">
						<div class="icon">
							<img src="./images/web0000_print_icon.png" alt="인쇄 아이콘" title="인쇄 아이콘" />
						</div>
						<div class="text">
							인쇄
						</div>
					</div>
					<div class="interest">
						<div class="icon">
							<img src="./images/web0000_interest_icon.png" alt="관심 아이콘" title="관심 아이콘" />
						</div>
						<div class="text">
							관심컨텐츠
						</div>
					</div>
				</div>
			</div>
		</div>




		<div class="clinic_reservation_step_display_box">
			<div class="clinic_reservation_step_display_box_1200">
				<ul class="clinic_reservation_step_display clearFix">
					<li class="active">
						<div class="top">
							STEP 01
						</div>
						<div class="bottom">
							진료과 선택
						</div>
					</li>
					<li class="icon">
						<img src="./images/web0000_right_icon.png" alt="오른쪽 방향 아이콘" title="오른쪽 방향 아이콘" />
					</li>
					<li>
						<div class="top">
							STEP 02
						</div>
						<div class="bottom">
							의료진 선택
						</div>
					</li>
					<li class="icon">
						<img src="./images/web0000_right_icon.png" alt="오른쪽 방향 아이콘" title="오른쪽 방향 아이콘" />
					</li>
					<li>
						<div class="top">
							STEP 03
						</div>
						<div class="bottom">
							진료날짜 선택
						</div>
					</li>
					<li class="icon">
						<img src="./images/web0000_right_icon.png" alt="오른쪽 방향 아이콘" title="오른쪽 방향 아이콘" />
					</li>
					<li>
						<div class="top">
							STEP 04
						</div>
						<div class="bottom">
							정보 확인
						</div>
					</li>
					<li class="icon">
						<img src="./images/web0000_right_icon.png" alt="오른쪽 방향 아이콘" title="오른쪽 방향 아이콘" />
					</li>
					<li>
						<div class="top">
							STEP 05
						</div>
						<div class="bottom">
							접수 완료
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="step_position_line_box">
			<div class="step_position_line_box_1200">
				<div class="step_position_line step01">
					<!-- 현재 STEP 단계 표시 막대기 -->
				</div>
			</div>
		</div>	


		<section class="clinic_reservation_content_box">
			<div class="clinic_reservation_content_box_1200">






				<!-- step 01 -->
				<div class="step_content step01_content show">

					<div class="left_window setTopVirtualBox">
						<div class="current_step clearFix">
							<div class="step_number">
								STEP 01
							</div>
							<div class="step_title">
								진료과선택
							</div>
						</div>
						<div class="current_step_comment">
							원하시는 진료과를<br />
							선택해주세요. 
						</div>	
						<div class="radio_button_box clearFix" style="visibility:hidden;">
							<input id="hospital_type_1" type="radio" name="hospital_type" value="700201" checked="true" />
							<label for="hospital_type_1">
								본원
							</label>
							<input id="hospital_type_2" type="radio" name="hospital_type" value="700202" />
							<label for="hospital_type_2">
								어린이병원
							</label>
							<input id="hospital_type_3" type="radio" name="hospital_type" value="700203" />
							<label for="hospital_type_3">
								암병원
							</label>
						</div>
						<div class="step_search_box">
							<input class="step_search" type="text" name="search" placeholder="진료과를 검색하세요." />
							<div class="step_search_button">
								<!-- 검색 아이콘 이미지 bg -->
							</div>
						</div>
						<div class="current_status">
							<div class="step01_selected">
								<!-- ▣ 진료과 : 가정의학과 -->
							</div>
							<div class="step02_selected">
								<!-- ▣ 의료진 : 정현채 -->
							</div>
							<div class="step03_selected_1">
								<!-- ▣ 날짜 : 2018-05-25-금요일 -->
							</div>
							<div class="step03_selected_2">
								<!-- ▣ 시간 : 13시 40분 -->
							</div>
						</div>
					</div>

					<div class="right_window setTopVirtualBox">
						<ul class="department_list_box clearFix">
							<!-- ajax로 리스트가 뿌려질 공간 -->
						</ul>
					</div>
				</div>









				<!-- step 02 -->
				<div class="step_content step02_content">

					<div class="left_window setTopVirtualBox">
						<div class="current_step clearFix">
							<div class="step_number">
								STEP 02
							</div>
							<div class="step_title">
								의료진선택
							</div>
						</div>
						<div class="current_step_comment">
							원하시는 의료진을<br />
							선택해주세요.  
						</div>	
						<div class="radio_button_box clearFix" style="visibility:hidden;">
							<input id="align_type_1" type="radio" name="align_type" value="1" checked="true" />
							<label for="align_type_1">
								기본순
							</label>
							<input id="align_type_2" type="radio" name="align_type" value="2" />
							<label for="align_type_2">
								가나다순
							</label>
						</div>
						<div class="step_search_box">
							<input class="step_search" type="text" name="search" placeholder="의료진 검색하세요." />
							<div class="step_search_button">
								<!-- 검색 아이콘 이미지 bg -->
							</div>
						</div>
						<div class="current_status">
							<div class="step01_selected">
								<!-- ▣ 진료과 : 가정의학과 -->
							</div>
							<div class="step02_selected">
								<!-- ▣ 의료진 : 정현채 -->
							</div>
							<div class="step03_selected_1">
								<!-- ▣ 날짜 : 2018-05-25-금요일 -->
							</div>
							<div class="step03_selected_2">
								<!-- ▣ 시간 : 13시 40분 -->
							</div>
						</div>
					</div>

					<div class="right_window setTopVirtualBox">
						<ul class="doctor_list_box clearFix">
							<?php  for ($i=0; $i<30; $i++) { ?>
							<li>
								<div class="doctor_detail_page_button">

								</div>
								<div class="doctor_image">

								</div>
								<div class="doctor_info setTopVirtualBox">
									<div class="name_and_department clearFix">
										<div class="name">
											정현채
										</div>
										<div class="department">
											가정의학과
										</div>
									</div>
									<div class="detail_info">
										세부전공 : 췌장, 췌장염, 췌장암, 췌장질환, 췌장낭종, 담도계질환, 담석, 담낭암, 담낭염, 담관염, 담도암
									</div>
								</div>
							</li>
							<?php  } ?>
						</ul>
					</div>
				</div>









				<!-- step 03 -->
				<div class="step_content step03_content">

					<div class="left_window setTopVirtualBox">
						<div class="current_step clearFix">
							<div class="step_number">
								STEP 03
							</div>
							<div class="step_title">
								진료날짜 선택
							</div>
						</div>
						<div class="current_step_comment">
							원하시는 진료날짜 및 시간을<br />
							선택해주세요.  
						</div>	
						<div class="radio_button_box clearFix" style="visibility:hidden;">
							<input id="xxx" type="radio" name="align_type" value="123" />
							<label for="xxx">
								xxx
							</label>
						</div>
						<div class="step_search_box" style="visibility:hidden;">
							<input class="xxx" type="text" name="search" placeholder="" />
							<div class="xxx">
								<!-- 검색 아이콘 이미지 bg -->
							</div>
						</div>
						<div class="current_status">
							<div class="step01_selected">
								<!-- ▣ 진료과 : 가정의학과 -->
							</div>
							<div class="step02_selected">
								<!-- ▣ 의료진 : 정현채 -->
							</div>
							<div class="step03_selected_1">
								<!-- ▣ 날짜 : 2018-05-25-금요일 -->
							</div>
							<div class="step03_selected_2">
								<!-- ▣ 시간 : 13시 40분 -->
							</div>
						</div>
					</div>

					<div class="right_window setTopVirtualBox">
						<div class="calendar_box">
							<div class="clinic_calendar_area">
                                <div class="month_and_arrow_button_area setTopVirtualBox">
                                    <div class="month_and_arrow_button_area_inner clearFix">
                                        <div class="prev_month_button">

                                        </div>
                                        <div class="year_month_display">
                                            <span class="year">
                                                2018년
                                            </span>
                                            <span class="month">
                                                5월
                                            </span>
                                        </div>
                                        <div class="next_month_button">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="real_calendar">
                                    <div class="real_calendar_inner">
                                        <div class="day_display_area">
                                            <ul class="clearFix">
                                                <li>
                                                    일
                                                </li>
                                                <li>
                                                    월
                                                </li>
                                                <li>
                                                    화
                                                </li>
                                                <li>
                                                    수
                                                </li>
                                                <li>
                                                    목
                                                </li>
                                                <li>
                                                    금
                                                </li>
                                                <li>
                                                    토
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="date_display_area">
                                            <ul class="clearFix">
                                                <!-- 1주 -->
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <!-- 2주 -->
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <!-- 3주 -->
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <!-- 4주 -->
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <!-- 5주 -->
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <!-- 6주 -->
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                                <li data-year="" data-month="" data-date="">
                                                    <span class="date">
                                                        1
                                                    </span>
                                                    <div class="today">
                                                    	오늘
                                                    </div>
                                                </li>
                                            </ul>
                                        </div> 
                                    </div>
                                </div>
                                <div style="height:17px;">

                                </div>
                            </div>
						</div>
						<div class="time_box">
							<ul class="clearFix">
								<!-- ajax로 시간 리스트 뿌려질 부분 -->
							</ul>
						</div>
					</div>
				</div>







				<!-- step 04 -->
				<div class="step_content step04_content">

					<div class="left_window setTopVirtualBox">
						<div class="current_step clearFix">
							<div class="step_number">
								STEP 04
							</div>
							<div class="step_title">
								정보확인
							</div>
						</div>
						<div class="current_step_comment">
							선택하신 사항이 맞는지<br />
							다시 한번 확인해주세요.
						</div>	
						<div class="radio_button_box clearFix" style="visibility:hidden;">
							<input id="xxx" type="radio" name="xxx" value="123" />
							<label for="xxx">
								xxx
							</label>
							
						</div>
						<div class="step_search_box" style="visibility:hidden;">
							<input class="xxx" type="text" name="search" placeholder="" />
							<div class="xxx">
							</div>
						</div>
						<div class="current_status">
							<div class="step01_selected">
								<!-- ▣ 진료과 : 가정의학과 -->
							</div>
							<div class="step02_selected">
								<!-- ▣ 의료진 : 정현채 -->
							</div>
							<div class="step03_selected_1">
								<!-- ▣ 날짜 : 2018-05-25-금요일 -->
							</div>
							<div class="step03_selected_2">
								<!-- ▣ 시간 : 13시 40분 -->
							</div>
						</div>
					</div>

					<div class="right_window setTopVirtualBox">
						<div class="check_text">
							아래의 정보가 맞으면 계속 진행해주세요.
						</div>
						<ul class="check_info">
							<li class="clearFix">
								<div class="title">
									진료과
								</div>
								<div class="value">
									가정의학과
								</div>
							</li>
							<li class="clearFix">
								<div class="title">
									의료진
								</div>
								<div class="value">
									정현채
								</div>
							</li>
							<li class="clearFix">
								<div class="title">
									날짜
								</div>
								<div class="value">
									2018-05-25-금요일
								</div>
							</li>
							<li class="clearFix">
								<div class="title">
									시간
								</div>
								<div class="value">
									13시 40분
								</div>
							</li>
						</ul>
					</div>
				</div>








				<!-- step 05 -->
				<div class="step_content step05_content">

					<div class="left_window setTopVirtualBox">
						<div class="current_step clearFix">
							<div class="step_number">
								STEP 05
							</div>
							<div class="step_title">
								접수완료
							</div>
						</div>
						<div class="current_step_comment">
							진료예약 접수가<br />
							완료되었습니다.
						</div>	
						<div class="radio_button_box clearFix" style="visibility:hidden;">
							<input id="xxx" type="radio" name="xxx" value="123" />
							<label for="xxx">
								xxx
							</label>
							
						</div>
						<div class="step_search_box" style="visibility:hidden;">
							<input class="xxx" type="text" name="search" placeholder="" />
							<div class="xxx">
							</div>
						</div>
					</div>

					<div class="right_window setTopVirtualBox">
						<div class="logo_box">
							<img src="./images/logo_color.png" alt="로고 이미지" title="로고 이미지" />
						</div>
						<div class="result">
							<div>
								<span class="name">
									<?php echo $_SESSION['user_name']; ?>
								</span>
								<span>
									님
								</span>
							</div>
							<div>
								<span class="datetime">
									2018년 6월 12일 오전 11시 15분
								</span>
								<span>
									으로
								</span>
							</div>
							<div>
								<span class="doctor_department">
									김주승 의료진, 내과진료 
								</span>
								<span>
									예약이 완료되었습니다. 
								</span>
							</div>
						</div>
					</div>
				</div>





			</div>


			<div class="controll_button_box_1200 clearFix">
				<div class="button_box clearFix">
					<div class="nagative_button">
						이전
					</div>
					<div class="positive_button">
						다음
					</div>
				</div>
			</div>

		</section>



		<?php include "footer.php"; ?>
	</body>
</html>